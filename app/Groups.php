<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Groups extends Model
{
    protected $table = 'groups';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'g_type',
        'created_by',
        'updated_by',
    ];

    public function vendor()
    {
        return $this->belongsTo('App\Vendors');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }

    public function updateUser()
    {
        return $this->hasOne('App\User','id','updated_by');
    }
}
