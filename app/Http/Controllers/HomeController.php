<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendors;
use App\Bank;
use App\Brands;
use App\CounterAssigned;
use App\Category;
use App\Products;
use App\PurchaseOrder;
use App\Sales;
use App\SaleCounter;
use App\PurchaseOrderDetails;
use App\SaleDetails;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\Warehouse;
use DB;
use Session;
use Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\incomeStatementExport;
use App\Exports\incomeStatementYearExport;
use App\Exports\incomeStatementMonthExport;
use App\Exports\incomeStatementDateExport;
use App\Exports\SaleCostingExport;
use App\Exports\SaleCostingYearExport;
use App\Exports\SaleCostingMonthExport;
use App\Exports\SaleCostingDateExport;
use App\Exports\SaleCostingDatesExport;
use App\Exports\TrialBalanceExport;
use App\Exports\TrialBalanceSearchExport;
use PDF;
use Carbon\Carbon;
use App\ExpenseVoucher;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dailyPLreport()
    {
        $date=Carbon::now()->format('d/m/Y');
        $today=Carbon::now()->format('Y-m-d');
        $warehouse = Warehouse::all();

        $Count = Sales::withCount(['sdetails as total_quantity' => function($query) use ($today) {
            $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(discounted_amount)'));
        }])
        ->withCount(['sdetails as salecount' => function($query) use ($today) {
            $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(price*quantity)'));
        }])
        ->withCount(['sdetails as purchase' => function($query) use ($today) {
            $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(cost * quantity)'));
        }])
        ->where('s_status','!=','Pending')->get();


        $exp = GeneralLedger::where('account_code','Like','EXP%')->where('accounting_date',$today)->sum('debit');
        return [$date,$warehouse,$Count,$exp];
    }

    public function dailyPLreportWarehouse($id)
    {
        $today=Carbon::now()->format('Y-m-d');
        $exp = GeneralLedger::where('account_code','Like','EXP%')->where('accounting_date',$today)->sum('debit');
        if($id == "All")
        {
            $Count = Sales::withCount(['sdetails as total_quantity' => function($query) use ($today) {
                $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(discounted_amount)'));
            }])
            ->withCount(['sdetails as salecount' => function($query) use ($today) {
                $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(price*quantity)'));
            }])
            ->withCount(['sdetails as purchase' => function($query) use ($today) {
                $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(cost * quantity)'));
            }])
            ->where('s_status','!=','Pending')->get();
        }
        else
        {
            $Count = Sales::withCount(['sdetails as total_quantity' => function($query) use ($today) {
                $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(discounted_amount)'));
            }])
            ->withCount(['sdetails as salecount' => function($query) use ($today) {
                $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(price*quantity)'));
            }])
            ->withCount(['sdetails as purchase' => function($query) use ($today) {
                $avg = $query->whereDate('created_at',$today)->select(DB::raw('sum(cost * quantity)'));
            }])
            ->where('s_status','!=','Pending')->where('w_id',$id)->get();
        }

        return [$Count,$exp];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    //pos opening and closing
    public function POS_OC()
    {
        $today = Carbon::now()->format('Y-m-d');
        $counter = Session::get('assign');
        if ($counter == null)
        {
            $sales = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sales s INNER JOIN sale_counter c ON s.id=c.s_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' and c.ret_id is null GROUP BY c.date,c.co_id,c.ret_id");
            $return = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sale_return s INNER JOIN sale_counter c ON s.id=c.ret_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' GROUP BY c.date,c.co_id,c.ret_id");
        }
        else
        {
            $sales = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sales s INNER JOIN sale_counter c ON s.id=c.s_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' and c.co_id = '".$counter."' and c.ret_id is null GROUP BY c.date,c.co_id,c.ret_id");
            $return = DB::Select("SELECT sum(s.total) as total,c.date,c.co_id,c.ret_id from sale_return s INNER JOIN sale_counter c ON s.id=c.ret_id
            INNER JOIN counter co on co.id = c.co_id WHERE c.date='".$today."' and c.co_id = '".$counter."' GROUP BY c.date,c.co_id,c.ret_id");
        }



        return view('pos_oc',compact('sales','return'));
    }

    //pos view
    public function pos()
    {
        $counter = Session::get('assign');

        // $curl_handle_cat = curl_init();

        // $caturl = "http://165.227.69.207/poschcare_erp/public/api/category";

        // // // Set the curl URL option
        // curl_setopt($curl_handle_cat, CURLOPT_URL, $caturl);

        // // // This option will return data as a string instead of direct output
        // curl_setopt($curl_handle_cat, CURLOPT_RETURNTRANSFER, true);

        // // // Execute curl & store data in a variable
        // $cat_data = curl_exec($curl_handle_cat);

        // curl_close($curl_handle_cat);

        // // // Decode JSON into PHP array
        // $cat = json_decode($cat_data);

        // $curl_handle_brand = curl_init();

        // $brandurl = "http://165.227.69.207/poschcare_erp/public/api/brands";

        // // // Set the curl URL option
        // curl_setopt($curl_handle_brand, CURLOPT_URL, $brandurl);

        // // // This option will return data as a string instead of direct output
        // curl_setopt($curl_handle_brand, CURLOPT_RETURNTRANSFER, true);

        // // // Execute curl & store data in a variable
        // $brand_data = curl_exec($curl_handle_brand);

        // curl_close($curl_handle_brand);

        // // // Decode JSON into PHP array
        // $brand = json_decode($brand_data);
        // dd($brand);
        $cat = Category::where('status',1)->get();
        $bank = Bank::where('status',1)->get();
        $brand = Brands::where('status',1)->get();
        $biller = Vendors::where('v_type','Biller')->where('status',1)->get();
        $customer = Vendors::where('v_type','Customer')->where('status',1)->get();
        $product = Products::where('vstatus',0)->where('status',1)->get();
        return view('pos',compact('cat','brand','biller','customer','product','bank'));
    }

    //pos find product using id
    public function posProduct($id)
    {
        $prod = Products::with(['unit'])->where('id',$id)->first();
        $w_id = Auth::user()->w_id;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {
            // dd('y');
            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->where('w_id',$w_id)->where('type',$prod->vstatus)->select(DB::raw('sum(unit_quantity)'));
            }])
            ->where('id',$id)
            ->first();
        }
        else
        {
            // dd('n');

            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$prod->vstatus);
            }])
            ->where('id',$id)
            ->first();
            // dd($product);

        }
        return $product;
    }

    //pos find product using code
    public function posProductCode($code)
    {
        $prod = Products::with(['unit'])->where('pro_code',$code)->first();
        $w_id = Auth::user()->w_id;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {
            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->where('w_id',$w_id)->where('type',$prod->vstatus)->select(DB::raw('sum(unit_quantity)'));
            }])
            ->where('id',$prod->id)
            ->first();
        }
        else
        {

            $product = Products::with(['brands','unit','category','subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$prod) {
                $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$prod->vstatus);
            }])
            ->where('id',$prod->id)
            ->first();
        }
        return $product;
    }

    //price checker
    public function priceChecker()
    {
        $product = Products::where('vstatus',0)->where('status',1)->get();
        return view('priceChecker',compact('product'));
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permission         =   getRolePermission($menu_id);



        $assign = CounterAssigned::where('u_id',Auth::user()->id)->first();
        if($assign != null)
        {
            Session::put('assign',$assign->c_id);
            $counter = Session::get('assign');
        }
        $biller=Vendors::where('v_type','Biller')->count('id');
        $customer=Vendors::where('v_type','Customer')->count('id');
        $supplier=Vendors::where('v_type','Supplier')->count('id');
        $sp=Vendors::where('v_type','Saleperson')->count('id');
        $bank=Bank::count('id');
        $brand=Brands::count('id');
        $cat=Category::count('id');
        $po=PurchaseOrder::count('id');
        $sale=Sales::count('id');
        $raw=DB::select("SELECT sum(current_stock.quantity) as quantity from current_stock , products
        where products.p_type = 'Raw Material'
        and products.id = current_stock.p_id
        and current_stock.type=0
        GROUP BY (products.p_type)");
        $pakg=DB::select("SELECT sum(current_stock.quantity) as quantity from current_stock , products
        where products.p_type = 'Packaging'
        and products.id = current_stock.p_id
        and current_stock.type=0
        GROUP BY (products.p_type)");
        $finish=DB::select("SELECT sum(current_stock.quantity) as quantity from current_stock , products
        where products.p_type = 'Finished'
        and products.id = current_stock.p_id
        and current_stock.type=0
        GROUP BY (products.p_type)");
        return view('Dashboard',compact('biller','customer','supplier','sp','bank','brand','cat'
        ,'raw','pakg','finish','po','sale','permission'));
    }

    public function salesCosting(Request $request)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $index=0;
        return view('reports.saleCosting',compact('stock','permissions','menu_id','index'));
    }

    public function saleCostingPDF()
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $pdf = PDF::loadView('reports.saleCostingPDF', compact('stock'));

        return $pdf->download('saleCosting.pdf');
    }

    public function saleCostingExcel()
    {
        return Excel::download(new SaleCostingExport, 'saleCostingExport.xlsx');
    }

    public function saleCostingSearch(Request $request)
    {
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        if($request->optradio == 'Year')
        {
            $stock = SaleDetails::with('purchase','products','sale.customer','variant')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
            ->get()->groupBy(function($item){
                return $item->created_at->format('Y-m-d');
            });
            $index = 1;
            $year = $request->year;
            return view('reports.saleCosting',compact('stock','index','year','permissions','menu_id'));
        }
        else if($request->optradio == 'Month')
        {
            $stock = SaleDetails::with('purchase','products','sale.customer','variant')
            ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
            ->get()->groupBy(function($item){
                return $item->created_at->format('Y-m-d');
            });
            $index = 2;
            // $monthName = Carbon::parse($request->month)->format('M-Y');
            $month = $request->month;
            // dd($stock);
            return view('reports.saleCosting',compact('stock','index','month','permissions','menu_id'));
        }

        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                $index = 3;
                $stock = SaleDetails::with('purchase','products','sale.customer','variant')
                ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                ->get()->groupBy(function($item){
                    return $item->created_at->format('Y-m-d');
                });
                // dd($stock);
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                $stock = SaleDetails::with('purchase','products','sale.customer','variant')
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                ->get()->groupBy(function($item){
                    return $item->created_at->format('Y-m-d');
                });
                // dd($stock);
            }
            if($index == 3)
            {
                return view('reports.saleCosting',compact('stock','index','from','to','permissions','menu_id'));
            }
            if($index == 4)
            {
                return view('reports.saleCosting',compact('stock','index','date','permissions','menu_id'));
            }
        }
    }

    public function saleCostingSearchYExcel($year)
    {
        return Excel::download(new SaleCostingYearExport($year), 'SaleCostingExportYearly.xlsx');
    }

    public function saleCostingSearchYPDF($year)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$year)
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $pdf = PDF::loadView('reports.saleCostingYearlyPDF', compact('stock','year'));

        return $pdf->download('saleCostingYearly.pdf');
    }

    public function saleCostingSearchMExcel($month)
    {
        return Excel::download(new SaleCostingMonthExport($month), 'SaleCostingExportMonthly.xlsx');
    }

    public function saleCostingSearchMPDF($month)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$month)
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $monthName = Carbon::parse($month)->format('M Y');
        $pdf = PDF::loadView('reports.saleCostingMonthlyPDF', compact('stock','monthName'));

        return $pdf->download('saleCostingMonthly.pdf');
    }

    public function saleCostingSearchDExcel($date)
    {
        return Excel::download(new SaleCostingDateExport($date), 'SaleCostingExportDate.xlsx');
    }

    public function saleCostingSearchDPDF($date)
    {
        $stock = SaleDetails::with('purchase','products','sale.customer','variant')
        ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
        ->get()->groupBy(function($item){
            return $item->created_at->format('Y-m-d');
        });
        $pdf = PDF::loadView('reports.saleCostingDatePDF', compact('stock','date'));

        return $pdf->download('saleCostingDate.pdf');
    }

    public function saleCostingSearchDatesExcel($from,$to)
    {
        return Excel::download(new SaleCostingDatesExport($from,$to), 'SaleCostingExportDateDifference.xlsx');
    }


    public function incomeStatement(Request $request)
    {
        $index=0;

        $sales = GeneralLedger::where('account_name','Sales')->sum('credit');
        $return = GeneralLedger::where('account_name','Damages Warehouse')->sum('debit');
        $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->sum('debit');
        $purchase = GeneralLedger::where('account_name','Purchase')->sum('debit');
        $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->sum('credit');
        $closing_stock = $opening_stock - $closing_stock_credit;

        $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')->sum('debit');
        $transportHead = HeadCategory::where('name','Transportation')->first();
        $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

        $expHead = HeadCategory::where('name','!=','Transportation')
        ->where('name','!=','Sales Incentive')
        ->where('code','Like','EXP%')
        ->get();

        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $year = '';
        $month = '';

        return view('reports.incomeStatement',compact('sales','return','opening_stock','purchase'
        ,'closing_stock','saleinc','index','permissions','menu_id','year','month','transportAccount','expHead'));

    }

    public function incomeStatementExcel()
    {
        return Excel::download(new incomeStatementExport, 'incomeStatementExport.xlsx');
    }

    public function incomeStatementPDF()
    {
        $sales = GeneralLedger::where('account_name','Sales')->sum('credit');
        $return = GeneralLedger::where('account_name','Damages Warehouse')->sum('debit');
        $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->sum('debit');
        $purchase = GeneralLedger::where('account_name','Purchase')->sum('debit');
        $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->sum('credit');
        $closing_stock = $opening_stock - $closing_stock_credit;

        $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')->sum('debit');
        $transportHead = HeadCategory::where('name','Transportation')->first();
        $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

        $expHead = HeadCategory::where('name','!=','Transportation')
        ->where('name','!=','Sales Incentive')
        ->where('code','Like','EXP%')
        ->get();

        $pdf = PDF::loadView('reports.incomeStatementPDF', compact('sales','opening_stock','purchase'
        ,'closing_stock','return','saleinc','transportHead','transportAccount','expHead'));

        return $pdf->download('incomeStatement.pdf');
    }

    public function incomeStatementSearch(Request $request)
    {
        if($request->optradio == 'Year')
        {
            $index = 1;
            $month = '';
            $year = $request->year;
            $a = Carbon::parse('Mar-'.$year);
            $year_p = $a->copy()->subYears(1)->format('Y');


            $sales = GeneralLedger::where('account_name','Sales')
            ->where('posted_date','LIKE',$year.'%')
            ->sum('credit');
            $return = GeneralLedger::where('account_name','Damages Warehouse')
            ->where('posted_date','LIKE',$year.'%')
            ->sum('debit');
            $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
            ->where('account_code','NOT Like','NCA-02%')
            ->where('posted_date','LIKE',$year_p.'%')
            ->sum('debit');
            $purchase = GeneralLedger::where('account_name','Purchase')
            ->where('posted_date','LIKE',$year.'%')
            ->sum('debit');
            $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
            ->where('account_code','NOT Like','NCA-02%')
            ->where('posted_date','LIKE',$year.'%')
            ->sum('credit');
            $closing_stock = $opening_stock - $closing_stock_credit;

            $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')
            ->where('posted_date','LIKE',$year.'%')
            ->sum('debit');
            $transportHead = HeadCategory::where('name','Transportation')->first();
            $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

            $expHead = HeadCategory::where('name','!=','Transportation')
            ->where('name','!=','Sales Incentive')
            ->where('code','Like','EXP%')
            ->get();

            $menu_id            =   $request->menuid;
            $permissions        =   getRolePermission($menu_id);

            return view('reports.incomeStatement',compact('sales','return','opening_stock','purchase',
            'closing_stock','saleinc','index','permissions','menu_id','year','month','transportAccount','expHead'));

        }
        else if($request->optradio == 'Month')
        {
            $year = '';
            $index = 2;
            $month = Carbon::parse($request->month)->format('M-y');
            $monthNumber =  Carbon::parse($request->month)->firstOfMonth();
            $lastMonth =  $monthNumber->subMonth()->format('M-y');

            $sales = GeneralLedger::where('account_name','Sales')
            ->where('period',$month)
            ->sum('credit');
            $return = GeneralLedger::where('account_name','Damages Warehouse')
            ->where('period',$month)
            ->sum('debit');
            $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
            ->where('account_code','NOT Like','NCA-02%')
            ->where('period',$lastMonth)
            ->sum('debit');
            $purchase = GeneralLedger::where('account_name','Purchase')
            ->where('period',$month)
            ->sum('debit');
            $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
            ->where('account_code','NOT Like','NCA-02%')
            ->where('period',$month)
            ->sum('credit');
            $closing_stock = $opening_stock - $closing_stock_credit;

            $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')
            ->where('period',$month)
            ->sum('debit');
            $transportHead = HeadCategory::where('name','Transportation')->first();
            $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

            $expHead = HeadCategory::where('name','!=','Transportation')
            ->where('name','!=','Sales Incentive')
            ->where('code','Like','EXP%')
            ->get();

            $menu_id            =   $request->menuid;
            $permissions        =   getRolePermission($menu_id);
            return view('reports.incomeStatement',compact('sales','return','opening_stock','purchase',
            'closing_stock','saleinc','index','permissions','menu_id','year','month','transportAccount','expHead'));
        }
    }

    public function incomeStatementSearchYExcel($year)
    {
        return Excel::download(new incomeStatementYearExport($year), 'incomeStatementExportYearly.xlsx');
    }

    public function incomeStatementSearchYPDF($year)
    {
        $a = Carbon::parse('Mar-'.$year);
        $year_p = $a->copy()->subYears(1)->format('Y');

        $sales = GeneralLedger::where('account_name','Sales')
        ->where('posted_date','LIKE',$year.'%')
        ->sum('credit');
        $return = GeneralLedger::where('account_name','Damages Warehouse')
        ->where('posted_date','LIKE',$year.'%')
        ->sum('debit');
        $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->where('posted_date','LIKE',$year_p.'%')
        ->sum('debit');
        $purchase = GeneralLedger::where('account_name','Purchase')
        ->where('posted_date','LIKE',$year.'%')
        ->sum('debit');
        $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->where('posted_date','LIKE',$year.'%')
        ->sum('credit');
        $closing_stock = $opening_stock - $closing_stock_credit;

        $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')
        ->where('posted_date','LIKE',$year.'%')
        ->sum('debit');
        $transportHead = HeadCategory::where('name','Transportation')->first();
        $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

        $expHead = HeadCategory::where('name','!=','Transportation')
        ->where('name','!=','Sales Incentive')
        ->where('code','Like','EXP%')
        ->get();


        $pdf = PDF::loadView('reports.incomeStatementYearlyPDF', compact('sales','opening_stock','purchase'
        ,'closing_stock','return','saleinc','transportHead','transportAccount','expHead','year'));

        return $pdf->download('incomeStatementYearly.pdf');
    }

    public function incomeStatementSearchMExcel($month)
    {
        return Excel::download(new incomeStatementMonthExport($month), 'incomeStatementExportMonthly.xlsx');
    }

    public function incomeStatementSearchMPDF($month)
    {
        $monthName = Carbon::parse($month)->format('M Y');
        $monthNumber =  Carbon::parse($month)->firstOfMonth();
        $lastMonth =  $monthNumber->subMonth()->format('M-y');

        $sales = GeneralLedger::where('account_name','Sales')
        ->where('period',$month)
        ->sum('credit');
        $return = GeneralLedger::where('account_name','Damages Warehouse')
        ->where('period',$month)
        ->sum('debit');
        $opening_stock = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->where('period',$lastMonth)
        ->sum('debit');
        $purchase = GeneralLedger::where('account_name','Purchase')
        ->where('period',$month)
        ->sum('debit');
        $closing_stock_credit = GeneralLedger::where('account_code','Like','CA-02%')
        ->where('account_code','NOT Like','NCA-02%')
        ->where('period',$month)
        ->sum('credit');
        $closing_stock = $opening_stock - $closing_stock_credit;

        $saleinc = GeneralLedger::where('account_code','Like','EXP-06%')
        ->where('period',$month)
        ->sum('debit');
        $transportHead = HeadCategory::where('name','Transportation')->first();
        $transportAccount = AccountDetails::where('Code','Like',$transportHead->code.'%')->get();

        $expHead = HeadCategory::where('name','!=','Transportation')
        ->where('name','!=','Sales Incentive')
        ->where('code','Like','EXP%')
        ->get();
        $pdf = PDF::loadView('reports.incomeStatementMonthlyPDF', compact('sales','opening_stock','purchase'
        ,'closing_stock','return','saleinc','transportHead','transportAccount','expHead','monthName','month'));

        return $pdf->download('incomeStatementMonthly.pdf');
    }


    public function trialBalance(Request $request)
    {
        $hcat=HeadCategory::all();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $trial = 1;
        $index = 0;
        return view('trial.trialBalance',compact('hcat','permissions','trial','menu_id','index'));
    }

    public function trialBalanceExcel()
    {
        return Excel::download(new TrialBalanceExport, 'TrialBalanceExport.xlsx');
    }

    public function trialBalanceSearchExcel($lastMonth,$trial,$period)
    {
        return Excel::download(new TrialBalanceSearchExport($lastMonth,$trial,$period), 'TrialBalanceSearchExport.xlsx');
    }

    public function trialBalanceSearch(Request $request)
    {
        // $lastMonth =  Carbon::parse($request->period)->format('M-y');
        // dd($lastMonth);
        $request->validate([
            'p_type' => 'required',
            'b_type' => 'required',
            'period' => 'required',
        ]);

        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        $p_type = $request->p_type;
        $b_type = $request->b_type;
        $index = 1;
        $hcat=HeadCategory::all();
        if($request->p_type == 'PTD' && $request->b_type == 'Opening')
        {
            $monthNumber =  Carbon::parse($request->period)->firstOfMonth();
            $lastMonth =  $monthNumber->subMonth()->format('M-y');
            $trial = 2;
            $period = '';
            return view('trial.trialBalance',compact('p_type','b_type','period','index','trial','hcat','lastMonth','menu_id','permissions'));
        }

        if($request->p_type == 'PTD' && $request->b_type == 'Closing')
        {
            $lastMonth =  Carbon::parse($request->period)->format('M-y');
            $trial = 2;
            $period = '';
            return view('trial.trialBalance',compact('p_type','b_type','period','index','trial','hcat','lastMonth','menu_id','permissions'));
        }

        if($request->p_type == 'YTD' && $request->b_type == 'Opening')
        {
            $period = GeneralLedger::first()->period;
            $monthNumber =  Carbon::parse($request->period)->firstOfMonth();
            $lastMonth =  $monthNumber->subMonth()->format('M-y');
            $trial = 3;
            return view('trial.trialBalance',compact('p_type','b_type','index','trial','hcat','lastMonth','period','menu_id','permissions'));
        }

        if($request->p_type == 'YTD' && $request->b_type == 'Closing')
        {
            $period = GeneralLedger::first()->period;
            $lastMonth =  Carbon::parse($request->period)->format('M-y');
            $trial = 3;
            return view('trial.trialBalance',compact('p_type','b_type','index','trial','hcat','lastMonth','period','menu_id','permissions'));
        }


    }
}
