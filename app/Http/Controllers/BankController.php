<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bank;
use App\City;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\User;
use Auth;
use DataTables;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class BankController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $city=City::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('bank.index',compact('city','permissions'));
    }

    public function datatable()//return datatable in index
    {
        $bank=Bank::with('city')->get();
        return DataTables::of($bank)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $city=City::where('status',1)->get();
        $menu_id =   getMenuId($request);
        $data= [
            'isEdit' => false,
            'city' => $city,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('bank.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data2 = $request->validate([
            'name'      => 'required',
            'address'     => 'required',
            'branch'      => 'required',
            'c_id'      => 'nullable',
            'c_no'      => 'nullable',
        ]);
        $bank = Bank::where('branch',$request->branch)->where('name',$request->name)->first();
        if($bank == null)
        {
            $role_id = Auth::user()->r_id;
            $env_a_id = config('app.adminId');
            $env_m_id = config('app.managerId');
            if($role_id == $env_a_id || $role_id == $env_m_id)
            {
                $data2['status'] = 1;
            }
            else
            {
                $data2['status'] = 0;
            }
            $hcat = HeadCategory::where('name','Bank')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;

            $u_id = Auth::user()->id;
            $data = [
                'Code' => $code,
                'name_of_account' => $request->name.' - '.$request->branch,
                'c_id' => $hcat->id,
                'created_by' => $u_id,
            ];
            AccountDetails::create($data);
            Bank::create($data2);
            $u_name = Auth::user()->name;
            $user = User::where('r_id',config('app.adminId'))->get();
            $data1 = [
                'notification' => 'New bank has been added by '.$u_name,
                'link' => url('')."/bank",
                'name' => 'View Banks',
            ];
            Notification::send($user, new AddNotification($data1));
            toastr()->success('Bank added successfully!');
            return redirect()->back();
        }
        else
        {
            toastr()->error('Bank account already exist!');
            return redirect()->back();
        }




    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Bank $bank)
    {
        $data= [
            'isEdit' => true,
            'city' => City::all(),
            'bank' => $bank
        ];
        return view('bank.create',$data);
    }


    public function status(Request $request)
    {
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Bank::find($id);
        if ($item->update(['status' => $status])) {
            $item->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bank $bank)
    {
        $data2 = $request->validate([
            'name'      => 'required',
            'address'     => 'required',
            'branch'      => 'required',
            'c_id'      => 'nullable',
            'c_no'      => 'nullable',
        ]);
        $account = $bank->name. ' - '. $bank->branch;
        $ledger = GeneralLedger::where('account_name',$account)
        ->first();
        if($ledger == null)
        {
            AccountDetails::where('name_of_account',$account)
            ->update([
                'name_of_account' => $request->name. ' - '. $request->branch
            ]);
            $bank->update($data2);
            toastr()->success('Bank Data updated successfully!');
            return redirect(url('')."/bank");
        }
        else
        {
            toastr()->danger('Bank Data is not allowed to update because its ledger is created!');
            return redirect(url('')."/bank");
        }
    }
}
