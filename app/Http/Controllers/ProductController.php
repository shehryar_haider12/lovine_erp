<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Products;
use App\ProductsUpdateRequest;
use App\Unit;
use App\Brands;
use App\Category;
use App\Vendors;
use App\Variants;
use App\ProductVariants;
use App\ProductVariantUpdate;
use App\Stocks;
use App\StockReceiving;
use App\Subcategory;
use App\Warehouse;
use App\CurrentStock;
use App\FinishProducts;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\User;
use App\ProductSupplier;
use App\ProductSupplierUpdate;
use DataTables;
use Storage;
use DB;
use App\Exports\ProductsExport;
use App\Exports\FinishProductExport;
use Maatwebsite\Excel\Facades\Excel;
use Auth;
use Carbon\Carbon;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //pricechecker
    public function productsearch($id)
    {
        $product = Products::find($id);
        return $product;
    }

    public function index(Request $request)
    {
        // $woocommerce        =   wooCommerce();
        // return json_decode(json_encode($woocommerce->get('products/7710')),true);

        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.index',compact('cat','brands','permissions'));
    }

    public function datatable()
    {
        $product=Products::with(['brands','unit','category','subcategory','variants'])
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'))->where('type',0);
        }])
        ->withCount(['currentstocks as unit_total' => function($query) {
            $avg = $query->select(DB::raw('sum(unit_quantity)'))->where('type',0);
        }])
        ->where('status',1)
        ->get();
        return DataTables::of($product)->make();
    }

    public function finishProducts(Request $request)
    {
        $cat=Category::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('product.Finishindex',compact('cat','brands','permissions'));
    }

    public function finishProductsDatatable()
    {
        $product=Products::with(['brands','unit','category','subcategory'])
        ->has('finish.rawproducts')
        ->withCount(['currentstocks as total' => function($query) {
            $avg = $query->select(DB::raw('sum(quantity)'))->where('type',0);
        }])
        ->where('p_type','Finished')
        ->get();
        // return $product;
        return DataTables::of($product)->make();
    }

    public function modal($id)//sales create using warehouse
    {
        $product=CurrentStock::with(['products','products.brands','products.unit','products.category','products.subcategory','variant.product.brands','variant.product.category','variant.product.unit'])
        ->where('w_id',$id)
        ->get();
        return DataTables::of($product)->make();
    }

    public function detail($id)//each finish product detail
    {
        $fp = FinishProducts::with(['rawproducts','rawproducts2'])
        ->where('id',$id)
        ->get();
        return $fp;
    }

    public function finishProductEdit($id)
    {
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $code ='P-'.rand();
        $fp = FinishProducts::with(['rawproducts.brands','rawproducts.unit'])
        ->where('id',$id)
        ->get();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $product = Products::find($id);
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $ps = ProductSupplier::where('p_id',$id)->get();
        $data= [
            'isEdit' => true,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'product' => $product,
            'pro' => $pro,
            'fp' => $fp,
            'code' => $code,
            'supplier' => $supplier,
            'ps' => $ps,
        ];
        return view('product.finish',$data);
    }

    public function finishProductUpdate($id,Request $request)
    {
        $product = Products::find($id);
        $u_id = Auth::user()->id;
        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        DB::table('finish_products')->where('id', $id)->delete();
        DB::table('product_supplier')->where('p_id', $id)->delete();
        $final = 0;
        for ($i=0; $i <count($request->p_id) ; $i++)
        {
            if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i] * 1000;
            }
            else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i] * 1000;
            }
            else
            {
                $final =$request->quantity[$i] * 1;
            }
            FinishProducts::create([
                'id' => $id,
                'p_id' => $request->p_id[$i],
                'quantity' => $final
            ]);
        }
        if(isset($request->image))
        {
            $data=([
                'image' => $request->image,
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'updated_by' => $u_id,
                'mrp' => $request->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$request->image);
            Products::where('id',$id)
            ->update($data);
            toastr()->success('Product updated successfully!');
            return redirect(url('').'/product/finishProducts');
        }
        else
        {
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                // 'quantity' => $request->quantity,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'updated_by' => $u_id,
                'mrp' => $request->mrp
            ]);
            Products::where('id',$id)
            ->update($data);
        }
        for ($i=0; $i < count($request->s_id) ; $i++) {
            ProductSupplier::create([
                'p_id' => $p->id,
                'type' => 0,
                's_id' => $request->s_id[$i],
            ]);
        }
        $hcat = HeadCategory::where('name','Inventory')->first();

        $account = AccountDetails::where('c_id',$hcat->id)
        ->latest('created_at')->first();

        if($account == null)
        {
            $id = 001;
        }
        else
        {
            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
        }

        $str_length = strlen((string)$id)+2;
        $id = substr("0000{$id}", -$str_length);
        $code = $hcat->code.'-'.$id;
        $data1 = [
            'Code' => $code,
            'name_of_account' => $request->pro_name.' - '.$request->pro_code,
            'c_id' => $hcat->id,
            'created_by' => $u_id
        ];
        AccountDetails::create($data1);

        toastr()->success('Product updated successfully!');
        return redirect(url('').'/product/finishProducts');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $menu_id =   getMenuId($request);
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $variant=Variants::where('p_id','!=','0')->get();
        $code =rand();
        $data= [
            'isEdit' => false,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'code' => $code,
            'variant' => $variant,
            'supplier' => $supplier,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('product.create',$data);
    }


    public function addDT()
    {
        $product=DB::select("SELECT * FROM brands,units, products where (products.p_type='Raw Material' or products.p_type='Packaging')
        and brands.id = products.brand_id and units.id = products.unit_id");
        return DataTables::of($product)->make();
    }

    public function add(Request $request)
    {
        $menu_id =   getMenuId($request);
        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $ware=Warehouse::all();
        $code = rand();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $pro=Products::with(['brands','unit','category','subcategory'])
        ->doesntHave('finish.rawproducts')
        ->get();
        $data= [
            'isEdit' => false,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'ware' => $ware,
            'pro' => $pro,
            'code' => $code,
            'supplier' => $supplier,
            'permissions' => getRolePermission($menu_id)
        ];
        return view('product.finish',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function addFinishProduct(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pro_code'      =>  'required|string|max:255|unique:products'
        ]);

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }

        $vstatusp=0;
        // dd($status);

        $u_id = Auth::user()->id;
        $unit = Unit::find($request->unit_id);
        $quantity = 0;
        $date=Carbon::now()->format('Y-m-d');
        if(isset($request->image))
        {
            $data=([
                'image' => $request->image,
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $request->unit_id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => 'Finished',
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp
            ]);
            $data['image']= Storage::disk('uploads')->putFile('',$request->image);
        }
        else
        {
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $request->unit_id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                // 'w_id' => $request->w_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'p_type' => 'Finished',
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp
            ]);
        }

        $p = Products::create($data);
        if(isset($request->s_id))
        {
            for ($i=0; $i < count($request->s_id) ; $i++) {
                ProductSupplier::create([
                    'p_id' => $p->id,
                    'type' => 0,
                    's_id' => $request->s_id[$i],
                ]);
            }
        }
        $final = 0;
        for ($i=1; $i <= count($request->p_id) ; $i++) {
            if($unit->u_name == 'Mililiter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Liter' && $request->unit1[$i] == 'Liter')
            {
                $final = 1 * $request->quantity[$i] ;
            }
            else if($unit->u_name == 'Grams' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i];
            }
            else if($unit->u_name == 'Kilograms' && $request->unit1[$i] == 'Kilograms')
            {
                $final = 1 * $request->quantity[$i] ;
            }
            else
            {
                $final =$request->quantity[$i] * 1;
            }
            FinishProducts::create([
                'id' => $p->id,
                'p_id' => $request->p_id[$i],
                'quantity' => $final,
                'type' => $request->type[$i]
            ]);
        }
           $hcat = HeadCategory::where('name','Inventory')->first();

            $account = AccountDetails::where('c_id',$hcat->id)
            ->latest('created_at')->first();

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }

            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;
            $data1 = [
                'Code' => $code,
                'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                'c_id' => $hcat->id,
                'created_by' => $u_id,
                'type' => 0
            ];
            AccountDetails::create($data1);

        $u_name = Auth::user()->name;
        $user = User::where('r_id',env('ADMIN_ID'))->get();
        $data2 = [
            'notification' => 'New Finished product has been added by '.$u_name,
            'link' => url('').'/product/finishProducts',
            'name' => 'View Finish Products',
        ];
        Notification::send($user, new AddNotification($data2));
        toastr()->success('Final Product added successfully!');
        return redirect()->back();
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'pro_code'      =>  'required|string|max:255|unique:products'
        ]);

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            $status = 1;
            $vstatus = 1;
        }
        else
        {
            $status = 0;
            $vstatus = 0;
        }
        if(isset($request->v_id))
        {
            $vstatusp=1;
        }
        else
        {
            $vstatusp=0;
        }
        $u_id = Auth::user()->id;

        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        $pro=Products::where('pro_name',$request->pro_name)
        ->where('brand_id',$request->brand_id)
        ->where('s_cat_id',$request->s_cat_id)
        ->where('cat_id',$request->cat_id)
        ->first();
        if($pro == null)
        {
            if(isset($request->image))
            {
                $data['image'] = Storage::disk('uploads')->putFile('',$request->image);
            }
            $data=([
                's_cat_id' => $request->s_cat_id,
                'pro_name' => $request->pro_name,
                'pro_code' => $request->pro_code,
                'weight' => $request->weight,
                'unit_id' => $unit->id,
                'cost' => $request->cost,
                'price' => $request->price,
                'alert_quantity' => $request->alert_quantity,
                'quantity' => $request->quantity,
                'brand_id' => $request->brand_id,
                'cat_id' => $request->cat_id,
                'visibility' => $request->visibility,
                'description' => $request->description,
                'created_by' => $u_id,
                'status' => $status,
                'vstatus' => $vstatusp,
                'mrp' => $request->mrp,
                'company_name' => $request->company_name,
                'case' => $request->case,
                'percentage' => $request->percentage,
            ]);
            $p=Products::create($data);
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Product added successfully!';
                $response['p'] = $p;
                return response()->json($response, 200);
            }

            $u_name = Auth::user()->name;
            $user   = User::where('r_id',config('app.adminId'))->get();
            $data2  = [
                'notification' => 'New product has been added by '.$u_name,
                'link' => url('').'/product',
                'name' => 'View Products',
            ];


            if(isset($request->v_id))
            {
                for ($i=0; $i < count($request->v_id) ; $i++) {
                    $name ='';
                    $name = $request->pro_code.' - '.$request->pro_name.'-';
                    $name .=$request->v_id[$i];
                    $pv = ProductVariants::create([
                        'p_id' => $p->id,
                        'name' => $name,
                        'status' => $vstatus,
                        'cost' => $request->cost,
                        'price' => $request->price,
                        'mrp' => $request->mrp,
                    ]);
                    if(isset($request->s_id))
                    {
                        for ($k=0; $k < count($request->s_id) ; $k++) {
                            ProductSupplier::create([
                                'p_id' => $pv->id,
                                's_id' => $request->s_id[$k],
                                'type' => 1,
                            ]);
                        }
                    }
                    $hcat = HeadCategory::where('name','Inventory')->first();

                    $account = AccountDetails::where('c_id',$hcat->id)
                    ->latest('created_at')->orderBy('id','desc')->first();

                    if($account == null)
                    {
                        $id = 001;
                    }
                    else
                    {
                        $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                    }
                    // dd($id);
                    $str_length = strlen((string)$id)+2;
                    $id = substr("0000{$id}", -$str_length);
                    $code = $hcat->code.'-'.$id;

                    $data1 = [
                        'Code' => $code,
                        'name_of_account' => $name,
                        'c_id' => $hcat->id,
                        'created_by' => $u_id,
                        'type' => 1
                    ];
                    $accountProduct = AccountDetails::create($data1);
                    if($request->quantity != null)
                    {
                        $date=Carbon::now()->format('Y-m-d');
                        $period = Carbon::now()->format('M-y');
                        Stocks::create([
                            'p_id' => $pv->id,
                            'w_id' => 1,
                            'quantity' => $request->quantity,
                            'stock_date' => $date,
                            's_type' => 'Manual',
                            'cost' => $request->cost,
                            'created_by' => $u_id,
                            'type' => 1
                        ]);
                        CurrentStock::create([
                            'p_id' => $pv->id,
                            'w_id' => 1,
                            'quantity' => $request->quantity,
                            'unit_quantity' => null,
                            'type' => 1
                        ]);
                        $gl = GeneralLedger::max('id');
                        if($gl == null)
                        {
                            $link_id = 1;
                        }
                        else
                        {
                            $ledger1 = GeneralLedger::where('id',$gl)->first();
                            $link_id = $ledger1->link_id + 1;
                        }
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Added product manually',
                            'account_name' => $accountProduct->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $date,
                            'posted_date' => $date,
                            'period' => $period,
                            'account_code' => $accountProduct->Code,
                            'currency_code' => 'PKR',
                            'stock_in' => $request->quantity,
                            'stock_out' => '0',
                            'net_value' => $request->quantity,
                            'balance' => $request->quantity,
                            'debit' => ($request->cost * $request->quantity),
                            'credit' => 0,
                            'amount' => ($request->cost),
                            'type' => 1,
                            'w_id' => 1
                        ]);
                    }
                }
            }
            else
            {
                if(isset($request->s_id))
                {
                    for ($i=0; $i < count($request->s_id) ; $i++) {
                        ProductSupplier::create([
                            'p_id' => $p->id,
                            'type' => 0,
                            's_id' => $request->s_id[$i],
                        ]);
                    }
                }
                $hcat = HeadCategory::where('name','Inventory')->first();

                $account = AccountDetails::where('c_id',$hcat->id)
                ->latest('created_at')->orderBy('id','desc')->first();

                if($account == null)
                {
                    $id = 001;
                }
                else
                {
                    $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                }

                $str_length = strlen((string)$id)+2;
                $id = substr("0000{$id}", -$str_length);
                $code = $hcat->code.'-'.$id;
                // dd($accoun);
                $data1 = [
                    'Code' => $code,
                    'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                    'c_id' => $hcat->id,
                    'created_by' => $u_id,
                    'type' => 0
                ];
                $accountProduct = AccountDetails::create($data1);
                if($request->quantity != null)
                {
                    $date=Carbon::now()->format('Y-m-d');
                    $period = Carbon::now()->format('M-y');
                    Stocks::create([
                        'p_id' => $p->id,
                        'w_id' => 1,
                        'quantity' => $request->quantity,
                        'stock_date' => $date,
                        's_type' => 'Manual',
                        'cost' => $request->cost,
                        'created_by' => $u_id,
                        'type' => 0
                    ]);
                    CurrentStock::create([
                        'p_id' => $p->id,
                        'w_id' => 1,
                        'quantity' => $request->quantity,
                        'unit_quantity' => null,
                        'type' => 0
                    ]);
                    $gl = GeneralLedger::max('id');
                    if($gl == null)
                    {
                        $link_id = 1;
                    }
                    else
                    {
                        $ledger1 = GeneralLedger::where('id',$gl)->first();
                        $link_id = $ledger1->link_id + 1;
                    }
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Added product manually',
                        'account_name' => $accountProduct->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $date,
                        'posted_date' => $date,
                        'period' => $period,
                        'account_code' => $accountProduct->Code,
                        'currency_code' => 'PKR',
                        'stock_in' => $request->quantity,
                        'stock_out' => '0',
                        'net_value' => $request->quantity,
                        'balance' => $request->quantity,
                        'debit' => ($request->cost * $request->quantity),
                        'credit' => 0,
                        'amount' => ($request->cost),
                        'type' => 0,
                        'w_id' => 1
                    ]);
                }

            }
            // dd($id);
            Notification::send($user, new AddNotification($data2));
            toastr()->success('Product added successfully!');
            return redirect()->back();
        }
        else
        {
            if(isset($request->request_type) && $request->request_type == "ajax")
            {
                $response['message'] = 'Product already exist!';
                $p = null;
                return response()->json($response, 200);
            }
            toastr()->error('Product already exist!');
            return redirect()->back();
        }
    }

    public function status(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $u_id = Auth::user()->id;
        $item = Products::find($id);
        if ($item->update(['status' => $status])) {
            Products::where('id',$id)
            ->update([
                'status' => $status,
                'updated_by' =>$u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }


    public function Vstatus(Request $request)
    {
        // dd($request->all());
        $response['status'] = false;
        $response['message'] = 'Oops! Something went wrong.';

        $id     = $request->input('id');
        $status = $request->input('status');
        $item = ProductVariants::find($id);
        if ($item->update(['status' => $status])) {
            ProductVariants::where('id',$id)
            ->update([
                'status' => $status
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Products::with(['brands','unit','category','subcategory','finish'])
        ->where('id',$id)
        ->first();
        return $product;
    }
    public function stockShow($id)
    {
        if(request()->ajax())
        {
            $p_id = substr($id, 0, strpos($id, '-'));
            $type = substr($id, strpos($id, '-')+1);
            $idntype = [];
            $qty = [];
            if($type == 0)
            {
                $product = Products::with(['brands','unit','category','subcategory','finish'])
                ->where('id',$id)
                ->first();
                if($product->finish->isNotEmpty())
                {
                    foreach ($product->finish as $key => $f) {
                        if($f->type == 1)
                        {
                            $punit = ProductVariants::with('product.unit')
                            ->where('id',$f->p_id)
                            ->first();
                            if($punit->product->unit->u_name == 'Liter' || $punit->product->unit->u_name == 'Mililiter' || $punit->product->unit->u_name == 'Grams' || $punit->product->unit->u_name == 'Kilograms')
                            {
                                array_push($idntype, array($f->p_id, $f->type));
                            }
                        }
                        else
                        {
                            $punit = Products::with('unit')->where('id',$f->p_id)->first();
                            if($punit->unit->u_name == 'Liter' || $punit->unit->u_name == 'Mililiter' || $punit->unit->u_name == 'Grams' || $punit->unit->u_name == 'Kilograms')
                            {
                                array_push($idntype, array($f->p_id, $f->type));
                            }
                        }
                    }
                    if(count($idntype) > 0)
                    {

                        foreach ($idntype as $key => $value) {
                            if($value[1] == 1)
                            {
                                $qty = StockReceiving::with(['warehouse','variant','unit'])->where('p_id',$value[0])
                                ->where('type',$value[1])->get();
                            }
                            else {
                                $qty = StockReceiving::with(['warehouse','products','unit'])->where('p_id',$value[0])
                                ->where('type',$value[1])->get();
                            }
                        }
                    }
                    else
                    {
                        $qty = [];
                    }

                }
                else {
                    $qty = [];
                }
                return [$product,$type,$qty];
            }
            else
            {
                $product = ProductVariants::with(['product.brands','product.unit','product.category','product.subcategory'])
                ->where('id',$id)
                ->first();
                return [$product,$type];
            }

        }

    }

    public function sales($id)
    {
        $p_id = substr($id, 0, strpos($id, '.'));
        $w_id = substr($id, strpos($id, '.')+1);
        $prod = Products::with(['unit'])->where('id',$p_id)->first();
        $type = $prod->vstatus;
        if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
        {

            if($prod->vstatus == 0)
            {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(unit_quantity)'));
                }])
                ->where('id',$p_id)
                ->first();
            }
            else {
                $product = Products::with(['brands','unit','category','subcategory','variants'])
                // ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id) {
                //     $avg = $query->where('w_id',$w_id)->select(DB::raw('sum(unit_quantity)'));
                // }])
                ->where('id',$p_id)
                ->first();
            }
        }
        else
        {
            if ($prod->vstatus == 0) {
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$type);
                }])
                ->where('id',$p_id)
                ->first();
            }
            else {
                $product = Products::with(['brands','unit','category','subcategory','variants'])
                // ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id) {
                //     $avg = $query->select(DB::raw('quantity'))->where('w_id',$w_id);
                // }])
                ->where('id',$p_id)
                ->first();
            }
        }
        // dd($product);
        return $product;
    }

    public function salesProduct($id) //use to fetch product data in sales create
    {
        // dd($id);
        $p_id = substr($id, 0, strpos($id, '.'));
        $type = substr($id, strpos($id, '.')+1);
        $type = substr($type,0, strpos($type, '.'));


        $w_id = substr($id,  strpos($id, '.', strpos($id, '.')+1)+1);
        // dd($type,$w_id,$id,$p_id);
        if($type == 1)
        {
            // dd('1');
            $prod = ProductVariants::find($p_id);
            $product = ProductVariants::with(['product.brands','product.unit','product.category','product.subcategory'])
            ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(quantity)'));
            }])
            ->where('id',$p_id)
            ->first();
        }
        else
        {
            // dd('0');
            $prod = Products::with(['unit'])->where('id',$p_id)->first();
            if($prod->unit->u_name == 'Liter' || $prod->unit->u_name == 'Mililiter' || $prod->unit->u_name =='Kilograms' || $prod->unit->u_name == 'Grams')
            {
                // dd('y');
                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->where('w_id',$w_id)->where('type',$type)->select(DB::raw('sum(quantity)'));
                }])
                ->where('id',$p_id)
                ->first();
            }
            else
            {
                // dd('n');

                $product = Products::with(['brands','unit','category','subcategory'])
                ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
                    $avg = $query->select(DB::raw('sum(quantity)'))->where('w_id',$w_id)->where('type',$type);
                }])
                ->where('id',$p_id)
                ->first();
                // dd($product);

            }
        }
        // dd($product);

        return [$product,$type];
    }

    public function addSingleProduct($id)//creating finish product it shows data of single product
    {
        if(request()->ajax())
        {
            $product = Products::with(['brands','unit','variants'])
            ->where('id',$id)->first();
            return $product;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $unit=Unit::where('status',1)->get();
        $cat=Category::where('status',1)->get();
        $sub=Subcategory::where('status',1)->get();
        $brands=Brands::where('status',1)->get();
        $variant=Variants::where('p_id','!=','0')->get();
        $supplier = Vendors::where('v_type','Supplier')
        ->where('status',1)->get();
        $p_variant=ProductVariants::where('p_id',$id)->get();
        $product = Products::find($id);
        $ps = ProductSupplier::where('p_id',$id)->get();
        $stock = 0;
        if($product->vstatus == 0)
        {
            $stock = CurrentStock::where('p_id',$id)->where('type',0)
            ->sum('quantity');
        }
        else
        {
            $stock = 0;
        }

        $data=[
            'isEdit' => true,
            'product' => $product,
            'unit' => $unit,
            'cat' => $cat,
            'sub' => $sub,
            'brands' => $brands,
            'variant' => $variant,
            'p_variant' => $p_variant,
            'supplier' => $supplier,
            'ps' => $ps,
            'stock' => $stock,
        ];
        return view('product.create',$data);
    }

    public function variants($id)
    {
        $variants = ProductVariants::where('p_id',$id)->get();
        return $variants;
    }

    public function Pvariants($id)
    {
        $v_id = substr($id, 0, strpos($id, '.'));
        $w_id = substr($id, strpos($id, '.')+1);
        $type = 1;
        $variants = ProductVariants::where('id',$v_id)
        ->withCount(['currentstocks as total_quantity' => function($query) use ($w_id,$type) {
            $avg = $query->select(DB::raw('quantity'))->where('w_id',$w_id)->where('type',$type);
        }])
        ->with('product')
        ->where('status',1)
        ->first();
        return $variants;
    }

    public function Pvariantsforfinishproduct($id)
    {
        $variants = ProductVariants::where('id',$id)
        ->with('product.unit')
        ->first();
        return $variants;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $productid = $id;
        $product = Products::find($id);
        $name = $product->pro_code.' - '.$product->pro_name;
        $var = ProductVariants::where('p_id',$id)->get();
        $u_id = Auth::user()->id;

        $role_id = Auth::user()->r_id;
        $env_a_id = config('app.adminId');
        $env_m_id = config('app.managerId');
        $unit = Unit::where('u_name' , $request->unit_id)
        ->first();
        if(isset($request->v_id))
        {
            $vstatusp=1;
        }
        else
        {
            $vstatusp=0;
        }

        if($role_id == $env_a_id || $role_id == $env_m_id)
        {
            if(isset($request->image))
            {
                $data=([
                    'image' => $request->image,
                    's_cat_id' => $request->s_cat_id,
                    'pro_name' => $request->pro_name,
                    'pro_code' => $request->pro_code,
                    'weight' => $request->weight,
                    'unit_id' => $unit->id,
                    'cost' => $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    'visibility' => $request->visibility,
                    'description' => $request->description,
                    'updated_by' => $u_id,
                    'mrp' => $request->mrp,
                    'price' => $request->price,
                    'vstatus' => $vstatusp,
                    'percentage' => $request->percentage,

                ]);
                $data['image']= Storage::disk('uploads')->putFile('',$request->image);
                Products::where('id',$id)
                ->update($data);
            }
            else
            {
                $data=([
                    'pro_code' => $request->pro_code,
                    'pro_name' => $request->pro_name,
                    // 'p_type' => $request->p_type,
                    'weight' => $request->weight,
                    'description' => $request->description,
                    'cost' => $request->cost,
                    'alert_quantity' => $request->alert_quantity,
                    'unit_id' => $unit->id,
                    'brand_id' => $request->brand_id,
                    'cat_id' => $request->cat_id,
                    's_cat_id' => $request->s_cat_id,
                    'visibility' => $request->visibility,
                    'updated_by' => $u_id,
                    'mrp' => $request->mrp,
                    'price' => $request->price,
                    'vstatus' => $vstatusp,
                    'percentage' => $request->percentage,
                ]);
                Products::where('id',$id)
                ->update($data);
            }
            if(isset($request->v_id))
            {
                if($var->isNotEmpty())
                {
                    for ($i=0; $i < count($request->v_id) ; $i++) {
                        if($request->pro_name == $product->pro_name)
                        {
                            $namevar ='';
                            $namevar = $request->pro_code.' - '.$request->pro_name.'-';
                            $namevar .=$request->v_id[$i];
                            $ProductVariants = ProductVariants::where('p_id',$id)
                            ->where('name',$namevar)->first();
                            if($ProductVariants == null)
                            {
                                ProductVariants::create([
                                    'p_id' => $id,
                                    'name' => $namevar,
                                    'status' => 1,
                                    'cost' => $request->cost,
                                    'price' => $request->price,
                                    'mrp' => $request->mrp,
                                ]);
                                $hcat = HeadCategory::where('name','Inventory')->first();

                                $account = AccountDetails::where('c_id',$hcat->id)
                                ->latest('created_at')->orderBy('id','desc')->first();

                                if($account == null)
                                {
                                    $id1 = 001;
                                }
                                else
                                {
                                    $id1 = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                                }
                                $str_length = strlen((string)$id1)+2;
                                $id1 = substr("0000{$id1}", -$str_length);
                                $code = $hcat->code.'-'.$id1;

                                $data1 = [
                                    'Code' => $code,
                                    'name_of_account' => $namevar,
                                    'c_id' => $hcat->id,
                                    'created_by' => $u_id,
                                    'type' => 1
                                ];
                                AccountDetails::create($data1);
                            }
                            else
                            {
                                $varname = explode('-',$ProductVariants->name);
                                ProductVariants::where('id',$ProductVariants->id)
                                ->update([
                                    'cost' => $request->cost,
                                    'price' => $request->price,
                                    'mrp' => $request->mrp,
                                ]);
                            }
                        }
                        else
                        {
                            $namevar ='';
                            $namevar = $request->pro_code.' - '.$product->pro_name.'-';
                            $namevar .=$request->v_id[$i];
                            $ProductVariants = ProductVariants::where('p_id',$id)
                            ->where('name',$namevar)->first();
                            if($ProductVariants == null)
                            {
                                $namevar ='';
                                $namevar = $request->pro_code.' - '.$request->pro_name.'-';
                                $namevar .=$request->v_id[$i];
                                ProductVariants::create([
                                    'p_id' => $id,
                                    'name' => $namevar,
                                    'status' => 1,
                                    'cost' => $request->cost,
                                    'price' => $request->price,
                                    'mrp' => $request->mrp,
                                ]);
                                $hcat = HeadCategory::where('name','Inventory')->first();

                                $account = AccountDetails::where('c_id',$hcat->id)
                                ->latest('created_at')->orderBy('id','desc')->first();

                                if($account == null)
                                {
                                    $id1 = 001;
                                }
                                else
                                {
                                    $id1 = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                                }
                                $str_length = strlen((string)$id1)+2;
                                $id1 = substr("0000{$id1}", -$str_length);
                                $code = $hcat->code.'-'.$id1;

                                $data1 = [
                                    'Code' => $code,
                                    'name_of_account' => $namevar,
                                    'c_id' => $hcat->id,
                                    'created_by' => $u_id,
                                    'type' => 1
                                ];
                                AccountDetails::create($data1);
                            }
                            else
                            {
                                $namevar2 ='';
                                $namevar2 = $request->pro_code.' - '.$request->pro_name.'-';
                                $namevar2 .=$request->v_id[$i];
                                ProductVariants::where('p_id',$id)
                                ->where('name',$namevar)->update([
                                    'cost' => $request->cost,
                                    'price' => $request->price,
                                    'mrp' => $request->mrp,
                                    'name' => $namevar2
                                ]);
                                AccountDetails::where('name_of_account',$namevar)
                                ->update([
                                    'name_of_account' => $namevar2
                                ]);
                                GeneralLedger::where('account_name',$namevar)->update([
                                    'account_name' => $namevar2
                                ]);
                            }
                        }
                    }
                    toastr()->success('Product updated successfully!');
                    return redirect(url('').'/product');
                }
                else
                {
                    $ledger = CurrentStock::where('p_id',$id)->where('type',0)
                    ->sum('quantity');
                    if($ledger == 0)
                    {
                        for ($i=0; $i < count($request->v_id) ; $i++) {
                            $namevar ='';
                            $namevar = $request->pro_code.' - '.$request->pro_name.'-';
                            $namevar .=$request->v_id[$i];

                            ProductVariants::create([
                                'p_id' => $id,
                                'name' => $namevar,
                                'status' => 1,
                                'cost' => $request->cost,
                                'price' => $request->price,
                                'mrp' => $request->mrp,
                            ]);
                            $hcat = HeadCategory::where('name','Inventory')->first();

                            $account = AccountDetails::where('c_id',$hcat->id)
                            ->latest('created_at')->orderBy('id','desc')->first();

                            if($account == null)
                            {
                                $id1 = 001;
                            }
                            else
                            {
                                $id1 = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                            }
                            $str_length = strlen((string)$id1)+2;
                            $id1 = substr("0000{$id1}", -$str_length);
                            $code = $hcat->code.'-'.$id1;

                            $data1 = [
                                'Code' => $code,
                                'name_of_account' => $namevar,
                                'c_id' => $hcat->id,
                                'created_by' => $u_id,
                                'type' => 1
                            ];
                            AccountDetails::create($data1);
                        }
                        AccountDetails::where('name_of_account',$name)
                        ->update([
                            'name_of_account' => $request->pro_code.' - '.$request->pro_name
                        ]);
                        GeneralLedger::where('account_name',$name)->update([
                            'account_name' => $request->pro_code.' - '.$request->pro_name
                        ]);
                        toastr()->success('Product updated successfully!');
                        return redirect(url('').'/product');
                    }
                    else
                    {
                        Products::where('id',$id)
                        ->update([
                            'vstatus' => 0
                        ]);
                        AccountDetails::where('name_of_account',$name)
                        ->update([
                            'name_of_account' => $request->pro_code.' - '.$request->pro_name
                        ]);
                        GeneralLedger::where('account_name',$name)->update([
                            'account_name' => $request->pro_code.' - '.$request->pro_name
                        ]);
                        toastr()->error('Variants can not be added as there is stock maintained in warehouse');
                        return redirect(url('').'/product');
                    }
                }

            }
            else
            {
                if($var->isNotEmpty())
                {
                    foreach ($var as $key => $v) {
                        $pv = ProductVariants::where('id',$v->id)->first();
                        $ledger = GeneralLedger::where('account_name',$pv->name)
                        ->first();
                        if($ledger == null)
                        {
                            ProductVariants::where('id',$v->id)->delete();
                            AccountDetails::where('name_of_account',$v->name)->delete();
                        }
                        else
                        {
                            $varname = explode('-',$v->name);
                            AccountDetails::where('name_of_account',$v->name)
                            ->update([
                                'name_of_account' => $request->pro_code.' - '.$request->pro_name.'-'.$varname[2]
                            ]);
                            GeneralLedger::where('account_name',$v->name)->update([
                                'account_name' => $request->pro_code.' - '.$request->pro_name.'-'.$varname[2]
                            ]);
                        }
                    }
                    $var2 = ProductVariants::where('p_id',$id)->get();
                    if($var2->isNotEmpty())
                    {
                        Products::where('id',$id)
                        ->update([
                            'vstatus' => 1
                        ]);
                    }
                    else
                    {
                        Products::where('id',$id)
                        ->update([
                            'vstatus' => 0
                        ]);
                        $hcat = HeadCategory::where('name','Inventory')->first();

                        $account = AccountDetails::where('c_id',$hcat->id)
                        ->latest('created_at')->orderBy('id','desc')->first();

                        if($account == null)
                        {
                            $id1 = 001;
                        }
                        else
                        {
                            $id1 = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
                        }
                        $str_length = strlen((string)$id1)+2;
                        $id1 = substr("0000{$id1}", -$str_length);
                        $code = $hcat->code.'-'.$id1;

                        $data1 = [
                            'Code' => $code,
                            'name_of_account' => $request->pro_code.' - '.$request->pro_name,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id,
                            'type' => 1
                        ];
                        AccountDetails::create($data1);
                    }
                }
                else
                {
                    AccountDetails::where('name_of_account',$name)
                    ->update([
                        'name_of_account' => $request->pro_code.' - '.$request->pro_name
                    ]);
                    GeneralLedger::where('account_name',$name)->update([
                        'account_name' => $request->pro_code.' - '.$request->pro_name
                    ]);
                }
                toastr()->success('Product updated successfully!');
                return redirect(url('').'/product');
            }


        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function excel()
    {
        return Excel::download(new ProductsExport, 'Products.xlsx');
    }

    public function Finishexcel()
    {
        return Excel::download(new FinishProductExport, 'FinishProducts.xlsx');
    }
}
