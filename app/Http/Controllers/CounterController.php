<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Counter;
use App\User;
use DataTables;
use Auth;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;

class CounterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('counter.index',compact('permissions'));
    }

    public function datatable()
    {
        $counter = Counter::all();
        return DataTables::of($counter)->make();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $isEdit = false;
        return view('counter.create',compact('isEdit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'counterNo' => 'required',
            'open' => 'required',
            'close' => 'required',
        ]);

        $hcat = HeadCategory::where('name','Cash')->first();

        $account = AccountDetails::where('c_id',$hcat->id)
        ->latest('created_at')->first();

        if($account == null)
        {
            $id = 001;
        }
        else
        {
            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
        }

        $str_length = strlen((string)$id)+2;
        $id = substr("0000{$id}", -$str_length);
        $code = $hcat->code.'-'.$id;


        Counter::create($data);

        $data1 = [
            'Code' => $code,
            'name_of_account' => 'Cash Counter '.$request->counterNo,
            'c_id' => $hcat->id,
            'created_by' => Auth::user()->id,
        ];
        AccountDetails::create($data1);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'New Counter has been added by '.$u_name,
            'link' => url('').'/countner',
            'name' => 'View Counters',
        ];
        Notification::send($user, new AddNotification($data1));
        toastr()->success('Counter added successfully!');
        return redirect()->back();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Counter $counter)
    {
        $isEdit = true;
        return view('counter.create',compact('isEdit','counter'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Counter $counter)
    {
        $data = $request->validate([
            'counterNo' => 'required',
            'open' => 'required',
            'close' => 'required',
        ]);
        $counter->update($data);
        toastr()->success('Counter updated successfully!');
        return redirect(url('').'/counter');
    }
}
