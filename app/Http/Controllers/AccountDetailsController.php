<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HeadCategory;
use App\AccountDetails;
use App\Bank;
use App\Vendors;
use App\Products;
use App\ProductVariants;
use App\GeneralLedger;
use Auth;
// use Schema;

class AccountDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $hcat=HeadCategory::all();
        $account=AccountDetails::with(['headCategory','generalLedger'])
        ->get();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        // dd($account);

        return view('accounts.index',compact('account','hcat','permissions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $hcat=HeadCategory::all();
        $data=[
            'hcat' => $hcat
        ];
        return view('accounts.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'Code'      =>  'required|string|max:255|unique:account_details'
        ]);
        if(str_contains($request->name_of_account,'_'))
        {
            $type =  substr($request->name_of_account,  strpos($request->name_of_account, '_')+1);
        }
        else {
            $type = null;
        }
        if($type == null)
        {
            $account=AccountDetails::where('name_of_account',$request->name_of_account)
            ->where('c_id',$request->c_id)
            ->first();
            $u_id = Auth::user()->id;
            $data = [
                'Code' => $request->Code,
                'name_of_account' => $request->name_of_account,
                'c_id' => $request->c_id,
                'created_by' => $u_id,
                'type' => $type
            ];
            if($account==null)
            {
                AccountDetails::create($data);
                toastr()->success('Account created successfully!');
                return redirect()->back();
            }
            else
            {
                toastr()->error('Account already exist of this Category!');
                return redirect()->back();
            }
        }
        else
        {
            $name = substr($request->name_of_account,0,strpos($request->name_of_account,'_'));
            $account=AccountDetails::where('name_of_account',$name)
            ->where('c_id',$request->c_id)
            ->first();
            $u_id = Auth::user()->id;
            $data = [
                'Code' => $request->Code,
                'name_of_account' => $name,
                'c_id' => $request->c_id,
                'created_by' => $u_id,
                'type' => $type
            ];
            if($account==null)
            {
                AccountDetails::create($data);
                toastr()->success('Account created successfully!');
                return redirect()->back();
            }
            else
            {
                toastr()->error('Account already exist of this Category!');
                return redirect()->back();
            }
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $account = AccountDetails::where('Code',$id)
        ->first();
        return $account;
    }

    public function prodName($id)
    {
        // dd($id);
        $account = AccountDetails::where('name_of_account',$id)
        ->first();
        if($account->type == 0)
        {
            $id =  substr($id,  strpos($id, '-')+2);
            $product = Products::where('pro_name',$id)->first();
            $code = $product->pro_code;
            return [$account,$product,$code];
        }
        else {

            $product = ProductVariants::where('name',$id)->first();
            $code = substr($product->name,0,strpos($product->name,"-"));
            return [$account,$product,$code];
        }

    }

    public function view($id)
    {
        $account = AccountDetails::where('c_id',$id)
        ->latest('created_at')->first();

        $hcat = HeadCategory::find($id);

        if($account == null)
        {
            $id = 001;
        }
        else
        {
            $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1;
        }
        $str_length = strlen((string)$id)+2;
        $id = substr("0000{$id}", -$str_length);
        // if($hcat->name == 'Furniture' || $hcat->name == 'IT Equipment' || $hcat->name == 'Machinery' || $hcat->name == 'Lab Equipment' || $hcat->name == 'Cash' || $hcat->name == 'Marketing' || $hcat->name == 'Admin' || $hcat->name == 'Utility' || $hcat->name == 'Equity' || $hcat->name == 'Revenue' || $hcat->name == 'Purchases' || $hcat->name == 'Discount' || $hcat->name == 'Tax' || $hcat->name == 'Damages')
        // {

        // }

        if($hcat->name == 'Receivables')
        {
            $customer=Vendors::where('v_type','Customer')->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$customer];
        }

        if($hcat->name == 'Loan')
        {
            $customer=Vendors::where('v_type','Customer')->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$customer];
        }

        if($hcat->name == 'Short Loan')
        {
            $customer=Vendors::where('v_type','Customer')->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$customer];
        }

        if($hcat->name == 'Inventory')
        {
            $product=Products::with('variants')->where('status',1)->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$product];
        }

        if($hcat->name == 'Bank')
        {
            $bank=Bank::with('city')->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$bank];
        }
        if($hcat->name == 'Payables')
        {
            $supplier=Vendors::where('v_type','Supplier')->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$supplier];
        }
        if($hcat->name == 'Sales Incentive')
        {
            $Saleperson=Vendors::where('v_type','Saleperson')->get();
            $code = $hcat->code.'-'.$id;
            return [$code,$Saleperson];
        }
        else
        {
            $code = $hcat->code.'-'.$id;
            return [$code];
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    public function chkHistory($id)
    {
        $ledger = GeneralLedger::where('account_code',$id)
        ->get();
        if($ledger->isEmpty())
        {
            return $ledger=0;
        }
        else
        {
            return $ledger;
        }

    }

    public function history($id)
    {
        $ledger = GeneralLedger::with('createUser')
        ->where('account_code',$id)
        ->get();
        $id = substr($id, 0, strpos($id, '-', strpos($id, '-')+1));
        // dd($ledger);
        $hcat=AccountDetails::all();
        return view('accounts.history',compact('ledger','hcat','id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
