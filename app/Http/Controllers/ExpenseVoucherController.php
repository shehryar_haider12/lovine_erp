<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ExpenseVoucher;
use App\HeadCategory;
use App\AccountDetails;
use App\GeneralLedger;
use App\TransactionHistory;
use App\Bank;
use Carbon\Carbon;
use Auth;
use DataTables;
use PDF;
use App\Notifications\AddNotification;
use Illuminate\Support\Facades\Notification;
use App\User;

class ExpenseVoucherController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cat = HeadCategory::where('code','like','EXP%')->get();
        $scat = AccountDetails::where('Code','like','EXP%')->get();
        $bank = Bank::all();
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return view('expensevoucher.index',compact('cat','scat','permissions','bank'));
    }

    public function datatable()
    {
        $exp = ExpenseVoucher::with(['preparedUser','category','subcategory'])->get();
        return DataTables::of($exp)->make();
    }

    public function status(Request $request)
    {
        $id     = $request->id;
        $status = $request->status;
        $u_id = Auth::user()->id;
        $item = ExpenseVoucher::find($id);
        if ($item->update(['status' => $status])) {
            ExpenseVoucher::where('id',$id)
            ->update([
                'status' => $status,
                'approved_by' => $u_id
            ]);
            $response['status'] = $status;
            $response['message'] = 'status updated successfully.';
            return response()->json($response, 200);
        }
        return response()->json($response, 409);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $exp=ExpenseVoucher::max('id');
        if($exp == null)
        {
            $id=1;
        }
        else
        {
            $id=$exp+1;
        }
        $pby = Auth::user()->name;
        $isEdit = false;
        $date = Carbon::now()->format("Y-m-d");
        $cat = HeadCategory::where('code','like','EXP%')->get();
        return view('expensevoucher.create',compact('id','isEdit','date','pby','cat'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pby = Auth::user()->id;
        ExpenseVoucher::create([
            'v_date' => $request->v_date,
            'c_id' => $request->c_id,
            'sc_id' => $request->sc_id,
            'description' => $request->description,
            'amount' => $request->amount,
            'prepared_by' => $pby,
        ]);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Expense Voucher request is added by '.$u_name,
            'link' => url('')."/expenseVoucher",
            'name' => 'View Expense Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));

        toastr()->success('Expense Voucher created successfully!');
        return redirect(url('')."/expenseVoucher");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exp = ExpenseVoucher::find($id);
        return $exp;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $isEdit = true;
        $exp = ExpenseVoucher::with(['preparedUser','category','subcategory'])->where('id',$id)->first();
        $account = AccountDetails::where('c_id',$exp->c_id)->get();
        $cat = HeadCategory::where('code','like','EXP%')->get();
        return view('expensevoucher.create',compact('isEdit','cat','exp','account'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        ExpenseVoucher::where('id',$id)
        ->update([
            'v_date' => $request->v_date,
            'c_id' => $request->c_id,
            'sc_id' => $request->sc_id,
            'description' => $request->description,
            'amount' => $request->amount,
        ]);
        $u_name = Auth::user()->name;
        $user = User::where('r_id',config('app.adminId'))->get();
        $data1 = [
            'notification' => 'Expense Voucher is updated by '.$u_name,
            'link' => url('')."/expenseVoucher",
            'name' => 'View Expense Vouchers',
        ];
        Notification::send($user, new AddNotification($data1));

        toastr()->success('Expense Voucher updated successfully!');
        return redirect(url('')."/expenseVoucher");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /// to get account names or sub-categories of a specific category

    public function accounts($id)
    {
        $account = AccountDetails::where('c_id',$id)->get();
        return $account;
    }

    public function pdf($id)
    {
        $exp = ExpenseVoucher::with(['preparedUser','category','subcategory','approvedUser'])->where('id',$id)->first();
        $pdf = PDF::loadView('expensevoucher.voucher', compact('exp'));
        // return view('expensevoucher.voucher', compact('exp'));

        return $pdf->download('ExpenseVoucher.pdf');
    }

    public function rcv(Request $request)
    {
        // dd($request->all());
        $u_id = Auth::user()->id;
        ExpenseVoucher::where('id',$request->id)->update(['received_by' => $request->received_by]);
        $exp = ExpenseVoucher::with(['subcategory'])->where('id',$request->id)->first();
        $id = GeneralLedger::max('id');
        $ledger = GeneralLedger::where('id',$id)->first();
        if($ledger == null)
        {
            $link_id=1;
        }
        else
        {
            $link_id = $ledger->link_id + 1;
        }
        $posted_date = Carbon::now()->format('Y-m-d');
        $period = Carbon::now()->format('M-y');
        $cash_c = 0;
        $cash_d = 0;
        $cash_n = 0;
        $bank_c = 0;
        $bank_d = 0;
        $bank_n = 0;


        if($request->paid_by == 'Cash')
        {
            TransactionHistory::create([
                'p_s_id' => 0,
                'p_type' => 'Expense',
                't_type' => 'Sent',
                'paid_by' => $request->paid_by,
                'total' => $request->amount2,
                'created_by' => $u_id
            ]);
            $ref_no = TransactionHistory::max('id');
            $account_c = AccountDetails::where('name_of_account','Cash In Hand')->first();
            $cash = GeneralLedger::where('account_code',$account_c->Code)
            ->get();
            if($cash->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Amount Paid to: '.$request->received_by,
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' =>'PKR',
                    'debit' => '0',
                    'credit' => $request->amount2,
                    'net_value' => 0 - $request->amount2,
                    'balance' => 0 - $request->amount2
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($cash as $key => $c) {
                    $cash_c+=$c->credit;
                    $cash_d+=$c->debit;
                    $balance+=$c->net_value;
                }
                $cash_c += $request->amount2;
                $cash_n = $balance + (0 - $request->amount2);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Amount Paid to: '.$request->received_by,
                    'account_name' => $account_c->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_c->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' =>'PKR',
                    'debit' => '0',
                    'credit' => $request->amount2,
                    'net_value' => 0 - $request->amount2,
                    'balance' =>$cash_n
                ]);
            }
        }
        else
        {
            TransactionHistory::create([
                'p_s_id' => 0,
                'p_type' => 'Expense',
                't_type' => 'Sent',
                'paid_by' => $request->paid_by,
                'total' => $request->amount2,
                'created_by' => $u_id,
                'b_id' => $request->bid
            ]);
            $ref_no = TransactionHistory::max('id');
            $bank=Bank::find($request->b_id);
            $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
            $bank_l = GeneralLedger::where('account_code',$account_b->Code)
            ->get();
            if($bank_l->isEmpty())
            {
                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Amount Paid to: '.$request->received_by,
                    'account_name' => $account_b->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_b->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' =>'PKR',
                    'debit' => '0',
                    'credit' => $request->amount2,
                    'net_value' => 0 - $request->amount2,
                    'balance' => 0 - $request->amount2
                ]);
            }
            else
            {
                $balance = 0;
                foreach ($bank_l as $key => $c) {
                    $bank_c+=$c->credit;
                    $bank_d+=$c->debit;
                    $balance+=$c->net_value;
                }
                $bank_c += $request->amount2;
                $bank_n = $balance + (0 - $request->amount2);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Amount Paid to: '.$request->received_by,
                    'account_name' => $account_b->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_b->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' =>'PKR',
                    'debit' => '0',
                    'credit' => $request->amount2,
                    'net_value' => 0 - $request->amount2,
                    'balance' => $bank_n
                ]);
            }
        }

        $exp_c = 0;
        $exp_d = 0;
        $exp_n = 0;
        $account_s = AccountDetails::where('name_of_account',$exp->subcategory->name_of_account)
        ->first();
        $expense = GeneralLedger::where('account_code',$account_s->Code)
        ->get();
        ExpenseVoucher::where('id',$request->id)
        ->update([
            'status' => 'Paid',
            'approved_by' => $u_id
        ]);

        if($expense->isEmpty())
        {
            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Amount Paid to: '.$request->received_by,
                'account_name' => $account_s->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_s->Code,
                'transaction_no' => $ref_no,
                'currency_code' =>'PKR',
                'credit' => '0',
                'debit' => $request->amount2,
                'net_value' => $request->amount2,
                'balance' => $request->amount2
            ]);
        }
        else
        {
            $balance = 0;
                foreach ($expense as $key => $c) {
                    $exp_c+=$c->credit;
                    $exp_d+=$c->debit;
                    $balance+=$c->net_value;
                }
                $exp_c += $request->amount2;
                $exp_n = $balance + ($request->amount2 - 0);

                GeneralLedger::create([
                    'source' => 'Automated',
                    'description' => 'Amount Paid to: '.$request->received_by,
                    'account_name' => $account_s->name_of_account,
                    'link_id' => $link_id,
                    'created_by' => $u_id,
                    'accounting_date' => $posted_date,
                    'posted_date' => $posted_date,
                    'period' => $period,
                    'account_code' => $account_s->Code,
                    'transaction_no' => $ref_no,
                    'currency_code' =>'PKR',
                    'credit' => '0',
                    'debit' => $request->amount2,
                    'net_value' => $request->amount2,
                    'balance' =>$exp_n
                ]);
        }
        return redirect()->back();
    }

}
