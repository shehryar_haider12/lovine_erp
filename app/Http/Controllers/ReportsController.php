<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Vendors;
use App\Sales;
use App\SaleDetails;
use App\Brands;
use App\Products;
use App\Category;
use App\PurchaseOrder;
use App\CurrentStock;

class ReportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function customerReport(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        $customer = Vendors::with(['sales' => function($query1) {
            $query1->withCount(['transaction as paid' => function($query2){
                $avg = $query2->select(DB::raw('sum(total)'))->where('p_type','Sales');
            }]);
        }])
        ->with('area')
        ->where('v_type','Customer')
        ->withCount(['sales as total' => function($query) {
            $avg = $query->select(DB::raw('count(id)'));
        }])
        ->withCount(['sales as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'));
        }])

        ->get();
        return view('reports.customer',compact('customer','permissions'));
    }


    public function customerReportDetail($id)
    {
        $sales = Sales::where('c_id',$id)
        ->with(['warehouse','customer','biller','saleperson','sdetails.products','sdetails.variant'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Sales');
        }])
        ->get();
        return view('reports.customerDetail',compact('sales'));
    }


    public function supplierReport(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);

        $supplier = Vendors::with(['purchases' => function($query1) {
            $query1->withCount(['transaction as paid' => function($query2){
                $avg = $query2->select(DB::raw('sum(total)'))->where('p_type','Purchase');
            }]);
        }])
        ->where('v_type','Supplier')
        ->withCount(['purchases as total' => function($query) {
            $avg = $query->select(DB::raw('count(id)'));
        }])
        ->withCount(['purchases as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'));
        }])

        ->get();
        return view('reports.supplier',compact('supplier','permissions'));
    }


    public function supplierReportDetail($id)
    {
        $purchase = PurchaseOrder::where('s_id',$id)
        ->with(['warehouse','supplier','biller','orderdetails.products','orderdetails.variant'])
        ->withCount(['transaction as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'))->where('p_type','Purchase');
        }])
        ->get();
        return view('reports.supplierDetail',compact('purchase'));
    }

    public function brandsReport()
    {
        $brand = Brands::with(['products' => function($query){
            $query->withCount(['stockIn as Vcost' => function($query2){
                $avg = $query2->select(DB::raw('sum(cost)'))->where('type',1)->where('s_type','Purchase');
            }])
            ->withCount(['stockIn as VquantityIn' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',1)->where('s_type','Purchase');
            }])
            ->withCount(['stocksOut as Vprice' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(price)'))->where('type',1)->where('s_type','Sales');
            }])
            ->withCount(['stocksOut as VquantityOut' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',1)->where('s_type','Sales');
            }])
            ->withCount(['stockIn' => function($query2){
                $avg = $query2->select(DB::raw('sum(cost)'))->where('type',0)->where('s_type','Purchase');
            }])
            ->withCount(['stockIn as quantityIn' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',0)->where('s_type','Purchase');
            }])
            ->withCount(['stocksOut' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(price)'))->where('type',0)->where('s_type','Sales');
            }])
            ->withCount(['stocksOut as quantityOut' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',0)->where('s_type','Sales');
            }]);
        }])
        ->get();
        return view('reports.brands',compact('brand'));
    }

    public function categoryReport()
    {
        $category = Category::with(['products' => function($query){
            $query->withCount(['stockIn as Vcost' => function($query2){
                $avg = $query2->select(DB::raw('sum(cost)'))->where('type',1)->where('s_type','Purchase');
            }])
            ->withCount(['stockIn as VquantityIn' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',1)->where('s_type','Purchase');
            }])
            ->withCount(['stocksOut as Vprice' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(price)'))->where('type',1)->where('s_type','Sales');
            }])
            ->withCount(['stocksOut as VquantityOut' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',1)->where('s_type','Sales');
            }])
            ->withCount(['stockIn' => function($query2){
                $avg = $query2->select(DB::raw('sum(cost)'))->where('type',0)->where('s_type','Purchase');
            }])
            ->withCount(['stockIn as quantityIn' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',0)->where('s_type','Purchase');
            }])
            ->withCount(['stocksOut' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(price)'))->where('type',0)->where('s_type','Sales');
            }])
            ->withCount(['stocksOut as quantityOut' => function($query3){
                $avg1 = $query3->select(DB::raw('sum(quantity)'))->where('type',0)->where('s_type','Sales');
            }]);
        }])
        ->get();
        // return $category;
        return view('reports.category',compact('category'));
    }

    public function bestSellerReport()
    {
        $details = SaleDetails::with(['products.variants'])
        ->select('p_id','type',DB::raw('sum(quantity) as quantity'))
        ->groupBy('p_id','type')
        ->orderBy('quantity','DESC')
        ->get();
        return view('reports.bestseller',compact('details'));

    }

    public function warehouseReport()
    {
        $total = CurrentStock::count('p_id');
        $qty = CurrentStock::sum('quantity');
        return view('reports.warehouse',compact('total','qty'));
    }

    public function productProfitReport()
    {
        $product = Products::withCount(['odetails' => function($query){
            $query->select(DB::raw('sum(received_quantity)'))->where('type',0);
        }])
        ->withCount(['sdetails' => function($query){
            $query->select(DB::raw('sum(delivered_quantity)'))->where('type',0);
        }])
        ->withCount(['odetails as Tcost' => function($query){
            $a = $query->select(DB::raw('sum(cost)'))->where('type',0);
        }])
        ->withCount(['sdetails as Tprice' => function($query4){
            $query4->select(DB::raw('sum(price)'))->where('type',0);
        }])
        ->with(['variants' => function($query3){
            $query3->withCount(['odetails as Vrqty' => function($query2){
                $query2->select(DB::raw('sum(received_quantity)'))->where('type',1);
            }]);
            $query3->withCount(['odetails as Vcost' => function($query2){
                $query2->select(DB::raw('sum(cost)'))->where('type',1);
            }]);

            $query3->withCount(['sdetails as Vdqty' => function($query2){
                $query2->select(DB::raw('sum(delivered_quantity)'))->where('type',1);
            }]);
            $query3->withCount(['sdetails as Vprice' => function($query2){
                $query2->select(DB::raw('sum(price)'))->where('type',1);
            }]);
        }])
        ->get();
        return view('reports.product',compact('product'));
    }

    public function salepersonReport()
    {
        $saleperson = Vendors::where('v_type','Saleperson')
        ->withCount(['salesp as total' => function($query) {
            $avg = $query->select(DB::raw('count(id)'));
        }])
        ->withCount(['salesp as total_amount' => function($query) {
            $avg = $query->select(DB::raw('sum(total)'));
        }])
        ->with(['salesp' => function($query2){
            $query2->has('return')->with('return');
        }])
        ->get();
        // return $saleperson[0]->salesp->pluck('return.total')->toArray() ;
        return view('reports.saleperson',compact('saleperson'));
    }

}
