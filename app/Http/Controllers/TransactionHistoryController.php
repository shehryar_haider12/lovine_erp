<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sales;
use App\SaleDetails;
use App\Bank;
use App\TransactionHistory;
use App\PurchaseOrder;
use App\AccountDetails;
use App\GeneralLedger;
use App\HeadCategory;
use App\StockOut;
use DataTables;
use Auth;
use App\Exports\TransactionHistoryExport;
use App\Exports\TransactionHistoryYearExport;
use App\Exports\TransactionHistoryMonthExport;
use App\Exports\TransactionHistoryDateExport;
use App\Exports\TransactionHistoryDatesExport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use Carbon\Carbon;

class TransactionHistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
        ->whereIn('p_type',['Sales','Purchase'])
        ->orderBy('created_at','desc')
        ->get();
        $index=0;
        return view('transaction.index',compact('trans','index','menu_id','permissions'));
    }

    // public function datatable()
    // {
    //     $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
    //     ->whereIn('p_type',['Sales','Purchase'])
    //     ->orderBy('created_at','desc')
    //     ->get();
    //     return DataTables::of($trans)->make();
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function view(Request $request)
    {
        // dd($request->all());
        $sales=TransactionHistory::where('p_s_id',$request->p_s_id)
        ->where('p_type',$request->p_type)
        ->get();
        // dd($sales);
        return $sales;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());

        $u_id = Auth::user()->id;
        $sum = TransactionHistory::where('p_s_id',$request->p_s_id)
        ->where('p_type','Sales')
        ->sum('total');

        // dd($sum);
        TransactionHistory::create([
            'p_s_id' => $request->p_s_id,
            'p_type' => $request->p_type,
            't_type' => $request->t_type,
            'paid_by' => $request->paid_by,
            // 'ref_no' => $request->p_ref_no,
            'total' => $request->p_total,
            'cheque_no' => $request->cheque_no,
            'cc_no' => $request->cc_no,
            'gift_no' => $request->gift_no,
            'cc_holder' => $request->cc_holder,
            'note' => $request->editor2,
            'b_id' => $request->b_id,
            'created_by' => $u_id
        ]);
        $ref_no = TransactionHistory::max('id');
        // dd($request->p_s_id);
        // dd($p_status);
        if($request->p_type == 'Sales')
        {
            $sales = Sales::with('customer')
            ->where('id',$request->p_s_id)->first();
            if($request->p_total == $sales->total)
            {
                $p_status='Paid';
            }
            if($request->p_total < $sales->total)
            {
                $p_status='Partial';
            }
            Sales::where('id',$request->p_s_id)
            ->update([
                'p_status' => $p_status
            ]);
            $quantity_r = 0 ;
            $quantity_d= 0 ;
            $sub= 0 ;
            $sales_total = 0;
            $final_amount = 0;
            $paid_amount = 0;

            $saleDetails = SaleDetails::where('s_id',$request->p_s_id)
            ->get();
            foreach ($saleDetails as $key => $s) {
                $quantity_r += $s->quantity;
                $quantity_d += $s->delivered_quantity;
                $sub += $s->price * $s->delivered_quantity;
            }
            // dd($sub);


            $sales_total = $sales->total;

            $gl = GeneralLedger::max('id');
            if($gl == null)
            {
                $link_id = 1;
            }
            else
            {
                $ledger = GeneralLedger::where('id',$gl)->first();
                $link_id = $ledger->link_id + 1;
            }
            $posted_date = Carbon::now()->format('Y-m-d');
            $period = Carbon::now()->format('M-y');
            $cash_c = 0;
            $cash_d = 0;
            $cash_n = 0;
            $bank_c = 0;
            $bank_d = 0;
            $bank_n = 0;
            $customer_c = 0;
            $customer_d = 0;
            $customer_n = 0;
            $account_c = AccountDetails::where('name_of_account',$sales->customer->company.' - '.$sales->customer->name)
            ->first();
            $receivable = GeneralLedger::where('account_code',$account_c->Code)
            ->get();
            $account = AccountDetails::where('Code','like','CL-02%')->where('Code','not like','NCL-02%')->latest('created_at')->first();
            $accountName = $sales->customer->company.' - '.$sales->customer->name;
            $hcat = HeadCategory::where('code','like','CL-02%')->where('Code','not like','NCL-02%')->first();
            // dd($account_c);

            if($account == null)
            {
                $id = 001;
            }
            else
            {
                $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            }
            // dd($id);
            $str_length = strlen((string)$id)+2;
            $id = substr("0000{$id}", -$str_length);
            $code = $hcat->code.'-'.$id;

            $account_l=AccountDetails::where('name_of_account',$accountName)
            ->where('c_id',$hcat->id)
            ->first();


            if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
            {
                $account_cash = AccountDetails::where('name_of_account','Cash In Hand')->first();
                $cash = GeneralLedger::where('account_code',$account_cash->Code)
                ->get();
                if($cash->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid by: '.$sales->customer->name,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_cash->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $request->p_total,
                        'credit' => '0',
                        'net_value' => $request->p_total - 0,
                        'balance' => $request->p_total - 0,
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($cash as $key => $c) {
                        $cash_c+=$c->credit;
                        $cash_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $cash_d += $request->p_total;
                    $cash_n = $balance + ($request->p_total - 0);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid by: '.$sales->customer->name,
                        'account_name' => $account_cash->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_cash->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $request->p_total,
                        'credit' => '0',
                        'net_value' => $request->p_total - 0,
                        'balance' => $cash_n
                    ]);
                }
            }
            else
            {
                $bank=Bank::find($request->b_id);
                // dd($bank);
                $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
                $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                ->get();
                if($bank_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid By: '.$sales->customer->name,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_b->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $request->p_total,
                        'credit' => '0',
                        'net_value' => $request->p_total - 0,
                        'balance' => $request->p_total - 0
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($bank_l as $key => $c) {
                        $bank_c+=$c->credit;
                        $bank_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $bank_d += $request->p_total;
                    $bank_n = $balance + ($request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid By: '.$sales->customer->name,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_b->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => $request->p_total,
                        'credit' => '0',
                        'net_value' => $request->p_total - 0,
                        'balance' => $bank_n
                    ]);
                }
            }

            if($quantity_r == $quantity_d && $request->actual <= $request->p_total)//done
            {
                // dd('a1');
                if($receivable->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to'.$sales->customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'credit' => $request->p_total,
                        'debit' => '0',
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($receivable as $key => $c) {
                        $customer_c+=$c->credit;
                        $customer_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $customer_c += $request->p_total;
                    $customer_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to'.$sales->customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'credit' => $request->p_total,
                        'debit' => '0',
                        'net_value' => 0 - $request->p_total,
                        'balance' => $customer_n
                    ]);
                }
            }

            $paid_amount = $request->p_total + $sum;

            // dd($paid_amount);
            if($paid_amount == $sales_total)
            {
                $final_amount = $sales_total;
            }
            if($paid_amount < $sales_total)
            {
                $final_amount = $sub;
            }

            if($quantity_d < $quantity_r && $paid_amount == $sales_total)//done
            {
                // dd('2');
                $stock_outs = StockOut::with(['sdetails'=>function($query) use ($sales){
                    $query->where('s_id',$sales->id)->get();
                }])->orderBy('created_at','desc')->first();
                $debit_rcv = GeneralLedger::where('account_code',$account_c->Code)
                ->latest('created_at')->first();
                if($debit_rcv->debit == 0)
                {
                    // dd('d0');
                    if($account_l == null)
                    {
                        // dd('dded');
                        $data = [
                            'Code' => $code,
                            'name_of_account' => $accountName,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id
                        ];
                        $liability = AccountDetails::create($data);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$sales->customer->name,
                            'account_name' => $liability->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>$posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $liability->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $request->p_total,
                            'net_value' => 0 - ($request->p_total),
                            'balance' => 0 - ($request->p_total)
                        ]);
                        // if($receivable->isEmpty())
                        // {
                        //     GeneralLedger::create([
                        //         'source' => 'Automated',
                        //         'description' => 'Sales to'.$sales->customer->name,
                        //         'account_name' => $account_c->name_of_account,
                        //         'link_id' => $link_id,
                        //         'created_by' => $u_id,
                        //         'accounting_date' => $posted_date,
                        //         'posted_date' => $posted_date,
                        //         'period' => $period,
                        //         'account_code' => $account_c->Code,
                        //         'transaction_no' => $ref_no,
                        //         'currency_code' => 'PKR',
                        //         'credit' => $request->p_total,
                        //         'debit' => '0',
                        //         'net_value' => 0 - $request->p_total,
                        //         'balance' => 0 - $request->p_total
                        //     ]);
                        // }
                        // else
                        // {
                        //     $balance = 0;
                        //     foreach ($receivable as $key => $c) {
                        //         $customer_c+=$c->credit;
                        //         $customer_d+=$c->debit;
                        //         $balance+=$c->net_value;
                        //     }
                        //     $customer_c += $request->p_total;
                        //     $customer_n = $balance + (0 - $request->p_total);

                        //     GeneralLedger::create([
                        //         'source' => 'Automated',
                        //         'description' => 'Sales to'.$sales->customer->name,
                        //         'account_name' => $account_c->name_of_account,
                        //         'link_id' => $link_id,
                        //         'created_by' => $u_id,
                        //         'accounting_date' => $posted_date,
                        //         'posted_date' => $posted_date,
                        //         'period' => $period,
                        //         'account_code' => $account_c->Code,
                        //         'transaction_no' => $ref_no,
                        //         'currency_code' => 'PKR',
                        //         'credit' => $request->p_total,
                        //         'debit' => '0',
                        //         'net_value' => 0 - $request->p_total,
                        //         'balance' => $customer_n
                        //     ]);
                        // }
                    }

                    else
                    {
                        // dd($account_l->Code);
                        $liability = GeneralLedger::where('account_code',$account_l->Code)
                        ->get();
                        $balance = 0;
                        $liability_n = 0;
                        $total = $request->p_total ;
                        foreach ($liability as $key => $d) {
                            $balance+=$d->net_value;
                        }
                        $liability_n = $balance + (0 - $total);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$sales->customer->name,
                            'account_name' => $account_l->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_l->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $total,
                            'net_value' => 0 - $total,
                            'balance' => $liability_n
                        ]);


                    }
                }
                else
                {
                    // dd('dn');
                    if($account_l == null)
                    {
                        $data = [
                            'Code' => $code,
                            'name_of_account' => $accountName,
                            'c_id' => $hcat->id,
                            'created_by' => $u_id
                        ];
                        $liability = AccountDetails::create($data);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$sales->customer->name,
                            'account_name' => $liability->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' =>$posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $liability->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $request->p_total - $debit_rcv->debit,
                            'net_value' => 0 - ($request->p_total - $debit_rcv->debit),
                            'balance' => 0 - ($request->p_total - $debit_rcv->debit)
                        ]);

                        $balance = 0;
                        foreach ($receivable as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_c += $debit_rcv->debit;
                        $customer_n = $balance + (0 - $debit_rcv->debit);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$sales->customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'credit' => $debit_rcv->debit,
                            'debit' => '0',
                            'net_value' => 0 - $debit_rcv->debit,
                            'balance' => $customer_n
                        ]);

                    }

                    else
                    {
                        $liability = GeneralLedger::where('account_code',$account_l->Code)
                        ->get();
                        $balance = 0;
                        $liability_n = 0;
                        $total = $request->p_total - $debit_rcv->debit;
                        foreach ($liability as $key => $d) {
                            $balance+=$d->net_value;
                        }
                        $liability_n = $balance + (0 - $total);
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to '.$sales->customer->name,
                            'account_name' => $account_l->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_l->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'debit' => '0',
                            'credit' => $total,
                            'net_value' => 0 - $total,
                            'balance' => $liability_n
                        ]);

                        $balance = 0;
                        foreach ($receivable as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_c +=$debit_rcv->debit;
                        $customer_n = $balance + (0 - $debit_rcv->debit);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$sales->customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'credit' => $debit_rcv->debit,
                            'debit' => '0',
                            'net_value' => 0 - $debit_rcv->debit,
                            'balance' => $customer_n
                        ]);

                    }
                }


            }
            // dd($paid_amount,$final_amount,$sub);

            if($quantity_d < $quantity_r && $paid_amount < $sales_total && $paid_amount > $sub)//done
            {
                // dd('24');
                if($account_l == null)
                {
                    $data = [
                        'Code' => $code,
                        'name_of_account' => $accountName,
                        'c_id' => $hcat->id,
                        'created_by' => $u_id
                    ];
                    $liability = AccountDetails::create($data);
                    $total = $request->p_total + $sum - $sub;
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$sales->customer->name,
                        'account_name' => $liability->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' =>$posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $liability->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'debit' => '0',
                        'credit' => $total,
                        'net_value' => 0 - ($total),
                        'balance' => 0 - ($total)
                    ]);
                    if($receivable->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$sales->customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'credit' => $sub - $sum,
                            'debit' => '0',
                            'net_value' => 0 - $sub - $sum,
                            'balance' => 0 - $sub - $sum
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($receivable as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_c += $request->p_total;
                        $customer_n = $balance + (0 - $sub - $sum);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$sales->customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'credit' => $sub - $sum,
                            'debit' => '0',
                            'net_value' => 0 - $sub - $sum,
                            'balance' => $customer_n
                        ]);
                    }
                }

                else
                {
                    $liability = GeneralLedger::where('account_code',$account_l->Code)
                    ->get();
                    $balance = 0;
                    $liability_n = 0;
                    $total = $request->p_total + $sum - $sub;
                    // dd($total,$request->p_total,$sub);
                    foreach ($liability as $key => $d) {
                        $balance+=$d->net_value;
                    }
                    $liability_n = $balance + (0 - $total);
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$sales->customer->name,
                        'account_name' => $account_l->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_l->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'debit' => '0',
                        'credit' => $total,
                        'net_value' => 0 - $total,
                        'balance' => $liability_n
                    ]);

                    if($receivable->isEmpty())
                    {
                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$sales->customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'credit' => $sub,
                            'debit' => '0',
                            'net_value' => 0 - $sub,
                            'balance' => 0 - $sub
                        ]);
                    }
                    else
                    {
                        $balance = 0;
                        foreach ($receivable as $key => $c) {
                            $customer_c+=$c->credit;
                            $customer_d+=$c->debit;
                            $balance+=$c->net_value;
                        }
                        $customer_c += $request->p_total;
                        $customer_n = $balance + (0 - $sub);

                        GeneralLedger::create([
                            'source' => 'Automated',
                            'description' => 'Sales to'.$sales->customer->name,
                            'account_name' => $account_c->name_of_account,
                            'link_id' => $link_id,
                            'created_by' => $u_id,
                            'accounting_date' => $posted_date,
                            'posted_date' => $posted_date,
                            'period' => $period,
                            'account_code' => $account_c->Code,
                            'transaction_no' => $ref_no,
                            'currency_code' => 'PKR',
                            'credit' => $sub,
                            'debit' => '0',
                            'net_value' => 0 - $sub,
                            'balance' => $customer_n
                        ]);
                    }
                }


            }

            if($quantity_d < $quantity_r && $paid_amount == $sub && $paid_amount < $sales_total )//done
            {
                // dd('34');
                if($receivable->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$sales->customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'credit' => $request->p_total,
                        'debit' => '0',
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($receivable as $key => $c) {
                        $customer_c+=$c->credit;
                        $customer_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $customer_c += $request->p_total;
                    $customer_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$sales->customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'credit' => $request->p_total,
                        'debit' => '0',
                        'net_value' => 0 - $request->p_total,
                        'balance' => $customer_n
                    ]);
                }

            }

            if($quantity_d < $quantity_r && $paid_amount < $sub && $paid_amount < $sales_total )//done
            {
                // dd('4');
                if($receivable->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$sales->customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'credit' => $request->p_total,
                        'debit' => '0',
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($receivable as $key => $c) {
                        $customer_c+=$c->credit;
                        $customer_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $customer_c += $request->p_total;
                    $customer_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Sales to '.$sales->customer->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' => 'PKR',
                        'credit' => $request->p_total,
                        'debit' => '0',
                        'net_value' => 0 - $request->p_total,
                        'balance' => $customer_n
                    ]);
                }

            }


        }

        if($request->p_type == 'Purchase')
        {
            $purchase = PurchaseOrder::where('id',$request->p_s_id)->first();
            if($request->p_total == $purchase->total)
            {
                $p_status='Paid';
            }
            if($request->p_total < $purchase->total)
            {
                $p_status='Partial';
            }
            PurchaseOrder::where('id',$request->p_s_id)
            ->update([
                'p_status' => $p_status,
                'updated_by' => $u_id
            ]);

            $po = PurchaseOrder::with('supplier')
            ->where('id',$request->p_s_id)->first();
            $id = GeneralLedger::max('id');
            $ledger = GeneralLedger::where('id',$id)->first();
            if($ledger == null)
            {
                $link_id=1;
            }
            else
            {
                $link_id = $ledger->link_id + 1;
            }
            $posted_date = Carbon::now()->format('Y-m-d');
            $period = Carbon::now()->format('M-y');
            $cash_c = 0;
            $cash_d = 0;
            $cash_n = 0;
            $bank_c = 0;
            $bank_d = 0;
            $bank_n = 0;
            $supplier_c = 0;
            $supplier_d = 0;
            $supplier_n = 0;
            $account_s = AccountDetails::where('name_of_account',$po->supplier->name.' - '.$po->supplier->company)
            ->first();
            $payable = GeneralLedger::where('account_code',$account_s->Code)
            ->get();
            // dd($account_s);
            $balance = 0;
            foreach ($payable as $key => $c) {
                $supplier_c+=$c->credit;
                $supplier_d+=$c->debit;
                $balance+=$c->net_value;
            }
            $supplier_d += $request->p_total;
            $supplier_n = $balance + ($request->p_total);

            GeneralLedger::create([
                'source' => 'Automated',
                'description' => 'Purchase from'.$po->supplier->name,
                'account_name' => $account_s->name_of_account,
                'link_id' => $link_id,
                'created_by' => $u_id,
                'accounting_date' => $posted_date,
                'posted_date' => $posted_date,
                'period' => $period,
                'account_code' => $account_s->Code,
                'transaction_no' => $ref_no,
                'currency_code' => 'PKR',
                'debit' => $request->p_total,
                'credit' => '0',
                'net_value' => $request->p_total,
                'balance' => $supplier_n
            ]);

            if($request->paid_by == 'Cash' || $request->paid_by == 'Gift')
            {
                $account_c = AccountDetails::where('name_of_account','Cash In Hand')->first();
                $cash = GeneralLedger::where('account_code',$account_c->Code)
                ->get();
                if($cash->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$po->supplier->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($cash as $key => $c) {
                        $cash_c+=$c->credit;
                        $cash_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $cash_c += $request->p_total;
                    $cash_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$po->supplier->name,
                        'account_name' => $account_c->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_c->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' =>$cash_n
                    ]);
                }
            }
            else
            {
                $bank=Bank::find($request->b_id);
                $account_b = AccountDetails::where('name_of_account',$bank->name.' - '.$bank->branch)->first();
                $bank_l = GeneralLedger::where('account_code',$account_b->Code)
                ->get();
                if($bank_l->isEmpty())
                {
                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$po->supplier->name,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_b->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' => 0 - $request->p_total
                    ]);
                }
                else
                {
                    $balance = 0;
                    foreach ($bank_l as $key => $c) {
                        $bank_c+=$c->credit;
                        $bank_d+=$c->debit;
                        $balance+=$c->net_value;
                    }
                    $bank_c += $request->p_total;
                    $bank_n = $balance + (0 - $request->p_total);

                    GeneralLedger::create([
                        'source' => 'Automated',
                        'description' => 'Amount Paid to: '.$po->supplier->name,
                        'account_name' => $account_b->name_of_account,
                        'link_id' => $link_id,
                        'created_by' => $u_id,
                        'accounting_date' => $posted_date,
                        'posted_date' => $posted_date,
                        'period' => $period,
                        'account_code' => $account_b->Code,
                        'transaction_no' => $ref_no,
                        'currency_code' =>'PKR',
                        'debit' => '0',
                        'credit' => $request->p_total,
                        'net_value' => 0 - $request->p_total,
                        'balance' => $bank_n
                    ]);
                }
            }
        }
        toastr()->success('Transaction Added successfully!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(request()->ajax())
        {
            $sales=TransactionHistory::with(['bank'])
            ->where('id',$id)->first();
            return $sales;
        }
    }

    public function excel()
    {
        return Excel::download(new TransactionHistoryExport, 'TransactionHistory.xlsx');
    }

    public function SearchYExcel($year,$tf,$pb)
    {
        return Excel::download(new TransactionHistoryYearExport($year,$tf,$pb), 'TransactionHistoryYearly.xlsx');
    }

    public function SearchMExcel($month,$tf,$pb)
    {
        return Excel::download(new TransactionHistoryMonthExport($month,$tf,$pb), 'TransactionHistoryMonthly.xlsx');
    }

    public function SearchDExcel($date,$tf,$pb)
    {
        return Excel::download(new TransactionHistoryDateExport($date,$tf,$pb), 'TransactionHistoryDate.xlsx');
    }

    public function SearchDatesExcel($from,$to,$tf,$pb)
    {
        return Excel::download(new TransactionHistoryDatesExport($from,$to,$tf,$pb), 'TransactionHistoryDateDifference.xlsx');
    }

    public function search(Request $request)
    {
        // dd($request->all());
        $menu_id            =   $request->menuid;
        $permissions        =   getRolePermission($menu_id);
        if($request->optradio == 'Year')
        {
            if($request->tf == null && $request->pb == null)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->orderBy('created_at','desc')
                ->get();
                $tf = '0';
                $pb = '0';
            }
            if($request->tf == null && $request->pb != null)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('paid_by',$request->pb)
                ->orderBy('created_at','desc')
                ->get();
                $tf = '0';
                $pb = $request->pb;
            }
            if($request->tf != null && $request->pb == null)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$request->tf)
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->orderBy('created_at','desc')
                ->get();
                $tf = $request->tf;
                $pb = '0';
            }
            if($request->tf != null && $request->pb != null)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$request->tf)
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"),$request->year)
                ->where('paid_by',$request->pb)
                ->orderBy('created_at','desc')
                ->get();
                $tf = $request->tf;
                $pb = $request->pb;
            }

            $index = 1;
            $year = $request->year;
            return view('transaction.index',compact('trans','index','year','tf','pb','menu_id','permissions'));
        }
        else if($request->optradio == 'Month')
        {
            if($request->tf == null && $request->pb == null)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->orderBy('created_at','desc')
                ->get();
                $tf = '0';
                $pb = '0';
            }
            if($request->tf == null && $request->pb != null)
            {
                $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                ->whereIn('p_type',['Sales','Purchase'])
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('paid_by',$request->pb)
                ->orderBy('created_at','desc')
                ->get();
                $tf = '0';
                $pb = $request->pb;
            }
            if($request->tf != null && $request->pb == null)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$request->tf)
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->orderBy('created_at','desc')
                ->get();
                $tf = $request->tf;
                $pb = '0';
            }
            if($request->tf != null && $request->pb != null)
            {
                $trans=TransactionHistory::with('sales.customer','sales')
                ->where('p_type',$request->tf)
                ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m'))"),$request->month)
                ->where('paid_by',$request->pb)
                ->orderBy('created_at','desc')
                ->get();
                $tf = $request->tf;
                $pb = $request->pb;
            }
            $index = 2;
            $month = $request->month;
            return view('transaction.index',compact('trans','index','month','tf','pb','menu_id','permissions'));
        }

        else if($request->optradio == 'Date')
        {
            if($request->from != null && $request->to != null)
            {
                $from = $request->from;
                $to = $request->to;
                $index = 3;
                if($request->tf == null && $request->pb == null)
                {
                    $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                    ->whereIn('p_type',['Sales','Purchase'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = '0';
                    $pb = '0';
                }
                if($request->tf == null && $request->pb != null)
                {
                    $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                    ->whereIn('p_type',['Sales','Purchase'])
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('paid_by',$request->pb)
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = '0';
                    $pb = $request->pb;
                }
                if($request->tf != null && $request->pb == null)
                {
                    $trans=TransactionHistory::with('sales.customer','sales')
                    ->where('p_type',$request->tf)
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = $request->tf;
                    $pb = '0';
                }
                if($request->tf != null && $request->pb != null)
                {
                    $trans=TransactionHistory::with('sales.customer','sales')
                    ->where('p_type',$request->tf)
                    ->whereBetween(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),[$request->from,$request->to])
                    ->where('paid_by',$request->pb)
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = $request->tf;
                    $pb = $request->pb;
                }
            }
            else
            {
                $index = 4;
                if($request->to == null)
                {
                    $date = $request->from;
                }
                if($request->from == null)
                {
                    $date = $request->to;
                }
                if($request->tf == null && $request->pb == null)
                {
                    $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                    ->whereIn('p_type',['Sales','Purchase'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = '0';
                    $pb = '0';
                }
                if($request->tf == null && $request->pb != null)
                {
                    $trans=TransactionHistory::with('sales.customer','purchase.supplier','sales','purchase')
                    ->whereIn('p_type',['Sales','Purchase'])
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('paid_by',$request->pb)
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = '0';
                    $pb = $request->pb;
                }
                if($request->tf != null && $request->pb == null)
                {
                    $trans=TransactionHistory::with('sales.customer','sales')
                    ->where('p_type',$request->tf)
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = $request->tf;
                    $pb = '0';
                }
                if($request->tf != null && $request->pb != null)
                {
                    $trans=TransactionHistory::with('sales.customer','sales')
                    ->where('p_type',$request->tf)
                    ->where(DB::raw("(DATE_FORMAT(created_at,'%Y-%m-%d'))"),$date)
                    ->where('paid_by',$request->pb)
                    ->orderBy('created_at','desc')
                    ->get();
                    $tf = $request->tf;
                    $pb = $request->pb;
                }
            }
            if($index == 3)
            {
                return view('transaction.index',compact('trans','index','from','to','tf','pb','menu_id','permissions'));
            }
            if($index == 4)
            {
                return view('transaction.index',compact('trans','index','date','tf','pb','menu_id','permissions'));
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
