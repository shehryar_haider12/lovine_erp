<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products;
use App\Category;
use App\Subcategory;
use App\Brands;
use App\Vendors;
use App\Sales;
use App\SaleReturn;
use App\SaleCounter;
use App\Counter;
use App\SaleDetails;
use App\HeadCategory;
use App\GeneralLedger;
use App\Unit;
use App\Bank;
use App\CurrentStock;
use App\AccountDetails;
use App\TransactionHistory;
use DB;
use Auth;
use Session;
use Carbon\Carbon;
use App\StockOut;

class WoocommerceController extends Controller
{
    // create sales order in erp using woocommerce order number
    public function saleOrder(Request $request)
    {
        // $orders = findOrder($request->order);
        // $biller = Vendors::where('v_type','Biller')->where('name','lovine')->first();
        // $customer = Vendors::where('v_type','Customer')->where('name','Customer')->first();
        // $w_id = 1;

        // if(count($orders['line_items']) > 0)
        // {
        //     if($orders['prices_include_tax'] == false)
        //     {
        //         $tax_status = 'No';
        //         $tax = null;
        //     }
        //     else {
        //         $tax_status = 'Yes';
        //         $tax = $orders['total_tax'];
        //     }
        //     if($orders['status'] == 'processing')
        //     {
        //         $s_status = 'Approved';
        //         $p_status = 'Pending';
        //     }
        //     $data=([
        //         'sale_date' => Carbon::parse($orders['date_created'])->format('Y-m-d'),
        //         'b_id' => $biller->id,
        //         'c_id' => $customer->id,
        //         'w_id' => $w_id,
        //         's_address' => $orders['billing']['address_1'],
        //         's_status' => $s_status,
        //         'p_status' => $p_status,
        //         'tax' => $tax,
        //         'total' => $orders['total'],
        //         'advance' => 'No',
        //         'tax_status' => $tax_status,
        //         'pay_type' => 'Cash on Delivery',
        //         'return_status' => 'No Return',
        //         's_type' => 'Woo Commerce',
        //         'order_no_w' => $orders['id']
        //     ]);
        //     $p=Sales::create($data);
        //     for ($j=0; $j <  count($orders['line_items'])  ; $j++)
        //     {
        //         $pid = Products::where('wooId', $orders['line_items'][$j]['product_id'])->first();
        //         SaleDetails::create([
        //             's_id' => $p->id,
        //             'p_id' => $pid->id,
        //             'wooId' => $orders['line_items'][$j]['product_id'],
        //             'quantity' => $orders['line_items'][$j]['quantity'],
        //             'sub_total' => $orders['line_items'][$j]['subtotal'],
        //             'price' => $orders['line_items'][$j]['subtotal'] /  $orders['line_items'][$j]['quantity'] ,
        //             'type' => 0,
        //             'delivered_quantity' => null
        //         ]);
        //     }
        // }
        return response()->json(true, 200);
    }

    // create product in erp using woocommerce product id
    public function createProduct(Request $request)
    {
        // dd($request->sku);
        // if(!$request->dashboard){
        //     return false;
        // }
        // $productID = $request->p_id;
        // $product = findProduct($productID);
        // if($request->sku == null || empty($request->sku) || is_null($request->sku)){
        //     dd("asdf");
        // }
        // // return $product;
        // if($product['name'] != 'AUTO-DRAFT')
        // {
        //     if(count($product['categories']) > 0)
        //     {
        //         $cat = Category::where('cat_name',$product['categories'][0]['name'])->first();
        //         if($cat == null)
        //         {
        //             $cat = Category::create([
        //                 'cat_name' => $product['categories'][0]['name']
        //             ]);
        //         }
        //     }

        //     else
        //     {
        //         $cat = Category::where('cat_name' , 'No Category')->first();
        //     }
        //     $brand = Brands::where('b_name','No Brand')->first();
        //     $unit = Unit::where('u_name','No Unit')->first();
        //     $scat = Subcategory::where('s_cat_name','No Subcategory')->first();

        //     $manage_stock = [
        //         'manage_stock' => true
        //     ];
        //     $woocommerce        =   wooCommerce();
        //     $woocommerce->put('products/'.$productID, $manage_stock);
        //     $data=([
        //         'pro_code' => $request->sku,
        //         'pro_name' => $product['name'],
        //         'unit_id' => $unit->id,
        //         'p_type' => 'Finished',
        //         'brand_id' => $brand->id,
        //         'cat_id' => $cat->id,
        //         's_cat_id' => $scat->id,
        //         'image' => count($product['images']) > 0 ? $product['images'][0]['src'] : null,
        //         'vstatus' => 0,
        //         'wooId' => $product['id'],
        //         'price' => $product['regular_price'] == "" ? 0 : $product['regular_price'],
        //     ]);
        //     $p = Products::create($data);
            // $product = findProduct($productID);
            // $p->update([
            //     'pro_code' => $product['sku']
            // ]);
            // $hcat = HeadCategory::where('name','Inventory')->first();

            // $account = AccountDetails::where('c_id',$hcat->id)
            // ->latest('created_at')->orderBy('id','desc')->first();

            // if($account == null)
            // {
            //     $id = 001;
            // }
            // else
            // {
            //     $id = substr($account->Code, strpos($account->Code, '-',strpos($account->Code, '-')+1)+1) +1 ;
            // }

            // $str_length = strlen((string)$id)+2;
            // $id = substr("0000{$id}", -$str_length);
            // $code = $hcat->code.'-'.$id;
            // $data1 = [
            //     'Code' => $code,
            //     'name_of_account' => $p->pro_code.' - '.$p->pro_name,
            //     'c_id' => $hcat->id,
            //     'type' => 0
            // ];
            // AccountDetails::create($data1);
            return response()->json(true, 200);
        // }
    }

    // update sales order status, if status is completed it impacts on sale products ledger and inventory
    public function saleOrderStatus(Request $request)
    {
        // $orders = findOrder($request->order);
        // $sales = Sales::where('order_no_w',$orders['id'])->first();
        // $sales->update([
        //     's_status' => $request->status
        // ]);
        // $quan = 0;
        // if($request->status == 'completed')
        // {
        //     $sales->update([
        //         's_status' => 'Complete'
        //     ]);
        //     TransactionHistory::create([
        //         'p_s_id' => $sales->id,
        //         'p_type' => 'Sales',
        //         't_type' => 'Received',
        //         'paid_by' => 'Cash',
        //         'total' => $sales->total
        //     ]);
        //     $saleDetails = SaleDetails::where('s_id',$sales->id)->get();

        //     foreach ($saleDetails as $key => $s) {
        //         $s->update([
        //             'delivered_quantity' => $s->quantity
        //         ]);
        //         $prod=CurrentStock::where('p_id',$s->p_id)
        //         ->where('w_id',$sales->w_id)
        //         ->where('type',0)
        //         ->first();
        //         $quan=$prod->quantity -  $s->quantity;
        //         CurrentStock::where('p_id',$s->p_id)
        //         ->where('w_id',$sales->w_id)
        //         ->where('type',0)->update(['quantity'=>$quan]);
        //     }
        // }
        return response()->json(true, 200);
    }


}
