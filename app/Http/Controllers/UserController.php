<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Warehouse;
use App\Roles;
use App\Notifications;
use DataTables;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $menu_id            =   getMenuId($request);
        $permissions        =   getRolePermission($menu_id);
        return  view('user.index',compact('permissions'));
    }

    public function datatable()
    {
        $user=User::with('role')->get();
        return DataTables::of($user)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data=[
            'isEdit' => false,
            'roles' =>  Roles::where('status',1)->get(),
            'ware' =>  Warehouse::all(),
        ];
        return view('user.create',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name'      => 'required',
            'email'     => 'required|unique:users',
            'r_id'      => 'required',
            'w_id'      => 'required',
        ]);
        $data['password'] = Hash::make('12345678');
        User::create($data);

        toastr()->success('User added successfully!');
        return redirect(url('').'/users');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $data=[
            'isEdit' => true,
            'roles' =>Roles::where('status',1)->get(),
            'user' =>$user,
            'ware' =>  Warehouse::all(),
        ];
        return view('user.create',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $data = $request->validate([
            'name'      => 'required',
            'email'     => 'required|unique:users,email,'.$user->id,
            'r_id'      => 'nullable',
            'w_id'      => 'nullable',
        ]);
        if($request->password)
        {
            $data['password'] = Hash::make($request->password);
        }
        $user->update($data);
        toastr()->success('User updated successfully!');
        return redirect(url('').'/users');
    }

    public function editProfile($id)
    {
        $user = User::find($id);
        return view('user.profile',compact('user'));
    }
}
