<?php
use App\RoleMenu;
use App\RolePermission;
use App\Permissions;
use App\UserMenu;
use Automattic\WooCommerce\Client;

function getMenuId($request){
    return UserMenu::where('route',$request->route()->getName())->first()->id;
}
function getRolePermission($menu_id){
    $permission_id      =   Permissions::where('m_id',$menu_id)->get()->pluck('id')->toArray();
    $role_permissions   =   RolePermission::with('permission')->where('r_id',Auth::user()->r_id)->whereIn('p_id',$permission_id)->get()->pluck('p_id')->toArray();
    return Permissions::whereIn('id',$role_permissions)->get()->pluck('name')->toArray();
}

function wooCommerce()
{

    $woocommerce = new Client(
        'https://lovinegallery.com/', // Your store URL
        'ck_f3e07a220fd542cded692bd3923c00b8f2c7831b', // Your consumer key
        'cs_7a534b6305361c93bfc0358b86e56d60c4319666', // Your consumer secret
        [
            'wp_api' => true, // Enable the WP REST API integration
            'version' => 'wc/v3' // WooCommerce WP REST API version
        ]
    );
    return $woocommerce;
}


?>
