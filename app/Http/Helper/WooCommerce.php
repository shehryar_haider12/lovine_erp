<?php

use App\Products;
use App\Variants;
use App\Sales;

//Add product in woocommerce
function storeProduct(array $request)
{
    // dd($request);
    $woocommerce        =   wooCommerce();
    if(isset($request['image']))
    {
        $wp_Data=([
            'name' => $request['pro_name'],
            'sku' => $request['pro_code'],
            'regular_price' => $request['price'],
            'description' => $request['description'],
            'short_description' => null,
            'manage_stock' => true,
            'post' => true,
            'button_text' => 'POS',
            'categories' => [
                ['id' => $request['cat_id']]
            ],
            'images' => [
                [
                   'src' => "http://165.227.69.207/lovine_erp/public/uploads/".$request['image']
                ]
            ]
        ]);
    }
    else {
        $wp_Data=([
            'name' => $request['pro_name'],
            'sku' => $request['pro_code'],
            'regular_price' => $request['price'],
            'description' => $request['description'],
            'short_description' => null,
            'manage_stock' => true,
            'post' => true,
            'button_text' => 'POS',
            'categories' => [
                ['id' => $request['cat_id']]
            ],
            'images' => [

            ]
        ]);
    }
    // dd($wp_Data);
    $woo = $woocommerce->post('products', $wp_Data);
    return $woo;
    // if(count($variants['variants']) > 0 )
    // {
    //     for ($i=1; $i <= count($variants['variants']) ; $i++)
    //     {
    //         for ($j=0; $j < count($variants['variants'][$i]) ; $j++) {
    //             $pv = Variants::where('name',$variants['variants'][$i][$j])->first();
    //             $data = [
    //                 'regular_price' => $variants['pricev'][$i] == null ? $request['price'] :  $variants['pricev'][$i],
    //                 'image' => [],
    //                 'attributes' => [
    //                     [
    //                         'id' => 5,
    //                         'variation' => true,
    //                         'visible' => true,
    //                         'option' => $variants['variants'][$i][$j]
    //                     ]
    //                 ]
    //             ];
    //             // dd($data);
    //             $woocommerce->post('products/'.$woo->id.'/variations', $data);
    //         }
    //     }
    // }
}

//show all woocommerce products
function listProduct()
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('products')),true);
}

//add stock in woocommerce single product
function addStock(array $request,$id)
{
    $woocommerce        =   wooCommerce();
    $product = Products::where('id',$id)
    ->first();
    $products_w = json_decode(json_encode($woocommerce->get('products/'.$product->wooId)),true);
    if($products_w['stock_quantity'] == null)
    {
        $stock_quantity = [
            'stock_quantity' => $request['quantity']
        ];
    }
    else {
        $stock_quantity = [
            'stock_quantity' => $request['quantity'] + $products_w['stock_quantity']
        ];
    }
    $woocommerce->put('products/'.$product->wooId, $stock_quantity);

}


//list all orders of woocommerce
function listOrders()
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('orders')),true);
}

//add quantity of multiple products in woocommerce using purchase order
function addPurchaseQty(array $request,$id)
{
    $woocommerce        =   wooCommerce();
    $product = Products::where('id',$id)
    ->first();

    if($product->wooId != null)
    {
        if((json_decode($woocommerce->get('products/'.$product->wooId),false)))
        {
            $products_w = json_decode(json_encode($woocommerce->get('products/'.$product->wooId)),true);
            // dd($products_w);
            if(count($products_w) > 0)
            {
                for ($i=0; $i <count($request['p_id']) ; $i++)
                {
                    if($products_w['stock_quantity'] == null)
                    {
                        $stock_quantity = [
                            'manage_stock' => true,
                            'stock_quantity' => $request['received_quantity'][$i]
                        ];
                    }
                    else {
                        $stock_quantity = [
                            'manage_stock' => true,
                            'stock_quantity' => $request['received_quantity'][$i] + $products_w['stock_quantity']
                        ];
                    }
                    $woocommerce->put('products/'.$product->wooId, $stock_quantity);

                }
            }
        }
    }
}

//subtracts quantity of multiple products in woocommerce using sales
function subSaleQty(array $request,$id,$sid,$qty)
{
    $woocommerce        =   wooCommerce();
    $product = Products::with('unit')
    ->where('id',$id)
    ->first();
    $products_w = json_decode(json_encode($woocommerce->get('products/'.$product->wooId)),true);
    $stock_quantity = [
        'stock_quantity' => $products_w['stock_quantity'] - $qty
    ];
    $woocommerce->put('products/'.$product->wooId, $stock_quantity);
    $sale = Sales::find($sid);
    $salestatus = [
        'status' => 'completed'
    ];
    if($sale->order_no_w != null)
    {
        // dd('dsds');
        $woocommerce->put('orders/'.$sale->order_no_w, $salestatus);
    }
    // dd($sale);

}


//find single ordere of woocommerce
function findOrder($id)
{
    $woocommerce        =   wooCommerce();
    return json_decode(json_encode($woocommerce->get('orders/'.$id)),true);
}

//find single product of woocommerce
function findProduct($id)
{
    $woocommerce = wooCommerce();
    return json_decode( json_encode( $woocommerce->get('products/'.$id) ), true );
}



?>
