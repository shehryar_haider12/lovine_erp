<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariants extends Model
{
    protected $table = 'product_variants';
    protected $primaryKey = 'id';
    protected $fillable = [
        'p_id',
        'name',
        'status',
        'cost',
        'price',
        'mrp',
    ];
    public function product()
    {
        return $this->hasOne('App\Products','id','p_id');
    }
    public function currentstocks()
    {
        return $this->hasMany('App\CurrentStock','p_id','id');
    }
    public function odetails()
    {
        return $this->hasMany('App\PurchaseOrderDetails','p_id','id');
    }
    public function sdetails()
    {
        return $this->hasMany('App\SaleDetails','p_id','id');
    }
    public function stockIn()
    {
        return $this->hasMany('App\Stocks','p_id','id');
    }

    public function stocksOut()
    {
        return $this->hasMany('App\StockOut','p_id','id');
    }
}
