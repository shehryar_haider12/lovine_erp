<?php

namespace App\Exports;

use App\PurchaseOrder;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;

class PurchaseExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;

    public function collection()
    {
        $purchase =  PurchaseOrder::with(['warehouse' => function($query) {
            $avg = $query->select('id','w_name')->get();
        },'supplier' => function($query) {
            $avg = $query->select('id','name')->get();
        }
        ])
        ->select('id','order_date','ref_no','w_id','s_id','total')
        ->get();
        $this->count = count($purchase);
        return $purchase;
    }

    public function map($purchase): array
    {
        return [
            $purchase->id,
            $purchase->order_date,
            $purchase->ref_no,
            $purchase->warehouse->w_name,
            $purchase->supplier->name,
            $purchase->total
        ];
    }

    public function headings(): array
    {
        return
        [
            ['PURCHASE ORDERS'],
            [],
            ['S.NO',
            'DATE',
            'REF.NO',
            'WAREHOUSE',
            'SUPPLIER',
            'TOTAL']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:F1'; // All headers
                $cellRange1 = 'A3:F3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':F'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
