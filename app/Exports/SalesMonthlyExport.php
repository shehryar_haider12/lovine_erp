<?php

namespace App\Exports;

use App\Sales;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

class SalesMonthlyExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithMapping
{
    /**
    * @return \Illuminate\Support\Collection
    */
    protected $count = 0;
    function __construct($month,$c_id,$sp_id,$ss,$ps,$check) {
        $this->sp_id = $sp_id;
        $this->c_id = $c_id;
        $this->ss = $ss;
        $this->ps = $ps;
        $this->check = $check;
        $this->month = $month;
    }

    public function collection()
    {
        if($this->check == 0)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 1)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 2)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('p_status',$this->ps)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 3)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('c_id',$this->c_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 4)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 5)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('c_id',$this->c_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 6)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('sp_id',$this->sp_id)
            ->where('c_id',$this->c_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 7)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('p_status',$this->ps)
            ->where('c_id',$this->c_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 8)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('p_status',$this->ps)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 9)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 10)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('p_status',$this->ps)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 11)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('c_id',$this->c_id)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 12)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('p_status',$this->ps)
            ->where('c_id',$this->c_id)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 13)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('p_status',$this->ps)
            ->where('c_id',$this->c_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 14)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('p_status',$this->ps)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }
        if($this->check == 15)
        {
            $sales = Sales::with(['warehouse' => function($query) {
                $avg = $query->select('id','w_name')->get();
            },'biller' => function($query) {
                $avg = $query->select('id','name')->get();
            },'customer' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ,'saleperson' => function($query) {
                $avg = $query->select('id','name')->get();
            }
            ])
            ->where(DB::raw("(DATE_FORMAT(sale_date,'%Y-%m'))"),$this->month)
            ->where('s_status',$this->ss)
            ->where('p_status',$this->ps)
            ->where('c_id',$this->c_id)
            ->where('sp_id',$this->sp_id)
            ->select('id','w_id','c_id','b_id','sp_id','sale_date','total','p_status','s_status')
            ->get();
        }



        $this->count = count($sales);
        return $sales;
    }

    public function map($sales): array
    {
        return [
            $sales->id,
            $sales->sale_date,
            $sales->warehouse->w_name,
            $sales->biller->name,
            $sales->customer->name,
            $sales->saleperson->name,
            $sales->p_status,
            $sales->s_status,
            $sales->total
        ];
    }

    public function headings(): array
    {
        return
        [
            ['SALE ORDERS'],
            ['MONTH'.Carbon::parse($this->month)->format('M-y')],
            [],
            ['S.NO',
            'DATE',
            'WAREHOUSE',
            'BILLER',
            'CUSTOMER',
            'SALE PERSON',
            'PAYMENT STATUS',
            'SALE STATUS',
            'TOTAL']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:I1'; // All headers
                $cellRange2 = 'A2:I2'; // All headers
                $cellRange1 = 'A4:I4'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->mergeCells($cellRange2);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange2)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=4; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':I'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
