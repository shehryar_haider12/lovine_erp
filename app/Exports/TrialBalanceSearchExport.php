<?php

namespace App\Exports;

use App\HeadCategory;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class TrialBalanceSearchExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents, WithStrictNullComparison
{
    /**
    * @return \Illuminate\Support\Collection
    */

    protected $count = 0;

    function __construct($lastMonth,$trial,$period) {
        $this->period = $period;
        $this->lastMonth = $lastMonth;
        $this->trial = $trial;
    }

    public function collection()
    {
        $hcat=HeadCategory::all();
        $index = 0;
        $count = 1;
        $data=[];
        foreach ($hcat as $key => $h) {
            $data[$index]['s_no'] = $count;
            $data[$index]['code'] = $h->code;
            $data[$index]['name'] = $h->name;
            if($this->trial == 2)
            {
                $account = DB::select("SELECT sum(debit) as debit,sum(credit) as credit
                FROM `general_ledger` where account_code LIKE '%".$h->code."%'
                and period LIKE '%".$this->lastMonth."%'");
            }
            else
            {
                $account = DB::select("SELECT sum(debit) as debit,sum(credit) as credit
                FROM `general_ledger` where account_code LIKE '%".$h->code."%'
                and period between '".$this->period."' and '".$this->lastMonth."' ");
            }
            $balance =  $account[0]->debit - $account[0]->credit;
            if ($balance > 0 )
            {
                $total_d += $balance;
                $data[$index]['debit'] = $total_d;
                $data[$index]['credit'] = '-';
            }
            if($balance < 0)
            {
                $total_c += $balance;
                $data[$index]['credit'] = $total_c;
                $data[$index]['debit'] = '-';
            }
            if($balance == 0)
            {
                $data[$index]['credit'] = '-';
                $data[$index]['debit'] = '-';
            }
            $count++;
            $index++;
        }
        $this->count = count($hcat);
        return collect($data);
    }

    public function headings(): array
    {
        return
        [
            ['TRIAL BALANCE SHEET'],
            [],
            ['S.NO',
            'CODE',
            'HEAD CATEGORY',
            'DEBIT',
            'CREDIT']
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:E1'; // All headers
                $cellRange1 = 'A3:E3'; // All headers
                $last_row = $this->count + 4;
                $event->sheet->getDelegate()->getStyle($cellRange)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->getDelegate()->getStyle($cellRange1)->applyFromArray([
                    'font' => [
                        'bold' => true
                    ]
                ]);
                $event->sheet->mergeCells($cellRange);
                $event->sheet->getDelegate()->getStyle($cellRange)->getAlignment()->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(14);
                $event->sheet->getDelegate()->getStyle($cellRange1)->getFont()->setSize(11);
                for ($i=3; $i < $last_row ; $i++) {
                    $event->sheet->getStyle('A'.$i.':E'.$i)->applyFromArray([
                        'borders' => [
                            'allBorders' => [
                                'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                                'color' => ['argb' => '000000'],
                            ],
                        ],
                    ]);
                }
            },
        ];
    }
}
