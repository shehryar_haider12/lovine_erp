<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeadofAccounts extends Model
{
    protected $table = 'head_of_accounts';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'created_by',
        'updated_by',
    ];

    public function headCategory()
    {
        return $this->belongsTo('App\HeadCategory');
    }
}
