<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleReturn extends Model
{
    protected $table = 'sale_return';
    protected $primaryKey = 'id';
    protected $fillable = [
        'return_date',
        'c_id',
        'w_id',
        'status',
        'total',
        'created_by',
        'sale_id',
        'r_type',
    ];

    public function warehouse()
    {
        return $this->hasOne('App\Warehouse','id','w_id');
    }

    public function sales()
    {
        return $this->hasOne('App\Sales','id','sale_id');
    }

    public function customer()
    {
        return $this->hasOne('App\Vendors','id','c_id');
    }

    public function rdetails()
    {
        return $this->hasMany('App\SaleReturnDetails','ret_id','id');
    }

    public function createUser()
    {
        return $this->hasOne('App\User','id','created_by');
    }
}
