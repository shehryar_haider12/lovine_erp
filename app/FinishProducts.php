<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FinishProducts extends Model
{
    protected $table = 'finish_products';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'p_id',
        'quantity',
        'w_id',
        'type',
    ];

    public function finishproducts()
    {
        return $this->hasOne('App\Products','id','id');
    }

    public function rawproducts()
    {
        return $this->hasOne('App\Products','id','p_id');
    }

    public function rawproducts2()
    {
        return $this->hasOne('App\ProductVariants','id','p_id');
    }
}
