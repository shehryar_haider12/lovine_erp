<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_history', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('p_s_id');
            $table->string('p_type', 100);
            $table->string('t_type')->nullable();
            $table->string('paid_by', 100);
            $table->string('ref_no', 100)->nullable();
            $table->double('total');
            $table->string('cheque_no')->nullable();
            $table->string('cc_no')->nullable();
            $table->string('gift_no')->nullable();
            $table->string('cc_holder')->nullable();
            $table->string('note')->nullable();
            $table->string('doc')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_history');
    }
}
