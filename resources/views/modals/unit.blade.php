

<div id="unitModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Unit</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Unit Name*</label>
                                <input class="form-control u_name" type="text" placeholder="Enter Unit Name" name="u_name" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitUnit" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>

<script>
    $('#submitUnit').click(function(e){
        e.preventDefault();
        var u_name = $('.u_name').val();
        var request_type = $('.request_type').val();
         axios
        .post('{{route("unit.store")}}', {
                _token: '{{csrf_token()}}',
                _method: 'post',
                u_name:u_name,
                request_type:request_type,
            })
            .then(function (responsive) {
                $('#unitModal').modal('hide');
                $('#unit_id').selectpicker('destroy');
                $('#unit_id').append(`<option selected value="`+responsive.data.unit.u_name+`">`+responsive.data.unit.u_name+`</option>`);
                $('#unit_id').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
            })
            .catch(function (error) {
                alert(error);
        });
    });
</script>
