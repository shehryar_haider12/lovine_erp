<div id="delivery" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg"  style="width: 70%;">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Sale Order Delivery</h4>
            </div>
            <form action="{{url('')}}/sales/delivery" class="form-horizontal" method="POST" >
                @csrf
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label >Sale Numer</label>
                            <input readonly class="form-control" type="text"  name="s_id" id="s_id1">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-outline">
                            <label >Delivery Status*</label>
                            <select class="form-control selectpicker" name="s_status" id="s_status" required>
                                <option value="" disabled selected>Select...</option>
                                <option value="Partial">Partial</option>
                                <option value="Complete">Complete</option>
                            </select>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <table id="example3" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width='5%'>S.No</th>
                                    <th width='30%'>Name - Code</th>
                                    <th width='5%'>Price</th>
                                    <th width='5%'>Discount Percent</th>
                                    <th width='5%'>Sale Quantity</th>
                                    <th width='5%'>Delivered Quantity</th>
                                    <th width='5%'>Delivery Quantity</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>


                        </table>
                        <input type="hidden" name="w_id" id="w_id1">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-outline">
                            <label >Amount*</label>
                            <input  class="form-control" readonly type="number" min="0" id="p_total1" name="p_total1">
                        </div>
                    </div>
                </div>
                <br>
            <button type="submit" id="delivered" class="btn green">Submit</button>
        </div>

            {{-- <div class="row">
                <div class="col-md-offset-0 col-md-12">
                </div>
            </div> --}}
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
