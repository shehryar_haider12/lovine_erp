

<div id="Addvariant" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Shades</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Shade Name*</label>
                                <input class="form-control shadename" type="text" placeholder="Enter Shade Name" name="name" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_typeShade" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitVariant" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>
    $('#submitVariant').click(function(e){
        e.preventDefault();
        var name = $('.shadename').val();
        var request_type = $('.request_typeShade').val();
        if(name == '')
        {
            alert('Enter Shade Name');
        }
        else
        {

            axios
            .post('{{route("variants.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    name:name,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#Addvariant').modal('hide');
                    $('#v_id').selectpicker('destroy');
                    $('#v_id').append(`<option selected value="`+responsive.data.var.name+`">`+responsive.data.var.name+`</option>`);
                    $('#v_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
