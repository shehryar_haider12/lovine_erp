

<div id="customerModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Customer</h4>
            </div>
            <form action="{{url('')}}/customer" class="form-horizontal" method="POST" >
                @csrf
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name*</label>
                                <input class="form-control" type="text" placeholder="Enter Company Name" name="company" required>
                                <span class="text-danger">{{$errors->first('company') ? 'Company already exist' : null}}</span>
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >City <small>(optional)</small></label>
                                <select class="form-control selectpicker" data-live-search="true" name="c_id" id="c_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($city as $s)
                                        <option value="{{$s->id}}">{{$s->c_name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <input type="hidden" name="v_type" value="Customer" id="">
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Group*</label>
                                <select required class="form-control selectpicker" name="c_group" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($cgroup as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Price Group*</label>
                                <select required class="form-control selectpicker" name="p_group" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($pgroup as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Address*</label>
                                <input class="form-control" type="text" placeholder="Enter Address" name="address" required>
                                <span class="text-danger">{{$errors->first('address') ? 'Address already exist' : null}}</span>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Country <small>(optional)</small></label>
                                <input class="form-control" type="text" placeholder="Enter Country" name="country" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Name*</label>
                                <input class="form-control" type="text" placeholder="Enter Customer Name" name="name" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No*</label>
                                <input min="0" class="form-control" type="number" placeholder="Enter Contact Number" name="c_no" required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Customer Name 2*</label>
                                <input class="form-control" type="text" placeholder="Enter Customer Name" name="name2" required>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact No 2*</label>
                                <input min="0" class="form-control" type="number" placeholder="Enter Contact Number" name="c_no2" required>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >NTN <small>(optional)</small></label>
                                <input class="form-control" type="text" placeholder="Enter NTN Number" name="VAT" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >GST <small>(optional)</small></label>
                                <input class="form-control" type="text" placeholder="Enter GST Number" name="GST" >
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >State  <small>(optional)</small></label>
                                <input class="form-control" type="text" placeholder="Enter State" name="state" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Email <small>(optional)</small></label>
                                <input class="form-control" type="email" placeholder="Enter Email" name="email" >
                                <span class="text-danger">{{$errors->first('email') ? 'email already exist' : null}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Postal Code <small>(optional)</small></label>
                                <input class="form-control" type="number" placeholder="Enter Postal Code" name="postalCode" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Balance <small>(optional)</small></label>
                                <input  class="form-control" type="number" placeholder="Enter Balance" name="balance" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Area <small>(optional)</small></label>
                                <select id="a_id" class="form-control selectpicker" data-live-search="true" name="a_id" >
                                    <option value="" disabled selected>Select...</option>
                                        @foreach ($area as $s)
                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                        @endforeach

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
