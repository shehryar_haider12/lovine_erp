

<div id="catModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title">Category</h4>
            </div>
            <form class="form-horizontal">
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Category Name*</label>
                                <input class="form-control cat_name" type="text" placeholder="Enter Category Name" name="cat_name" >
                            </div>
                        </div>
                    </div>
                    <input type="hidden" class="request_type" name="request_type" value="ajax">
                    <div class="row">
                        <div class="col-md-offset-0 col-md-9">
                            <button id="submitCategory" type="submit" class="btn green">Submit</button>
                        </div>
                    </div>

                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>
<script>

    $('#submitCategory').click(function(e){
        e.preventDefault();
        var cat_name = $('.cat_name').val();
        var request_type = $('.request_type').val();
        if(cat_name == '')
        {
            alert('Enter Category');
        }
        else
        {
            axios
            .post('{{route("category.store")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    cat_name:cat_name,
                    request_type:request_type,
                })
                .then(function (responsive) {
                    $('#catModal').modal('hide');
                    $('#cat_id').selectpicker('destroy');
                    $('#cat_id').append(`<option selected value="`+responsive.data.cat.id+`">`+responsive.data.cat.cat_name+`</option>`);
                    $('#cat_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                })
                .catch(function (error) {
                    alert(error);
            });
        }
    });
</script>
