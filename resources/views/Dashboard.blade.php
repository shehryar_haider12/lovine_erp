@extends('layouts.master')
@section('top-styles')
<style>
    stat,.stat .display{margin-bottom:20px}
    .stat{-webkit-border-radius:4px;-moz-border-radius:4px;-ms-border-radius:4px;-o-border-radius:4px;border-radius:2px;background:#FFf;padding:10px 10px 10px}
    .stat.bordered{border:1px solid #e7ecf1}
    .stat .display:after,.stat .display:before{content:" ";display:table}
    .stat .display:after{clear:both}
    .stat .display .number{float:left;display:inline-block}
    .stat .display .number h3{margin:0 0 2px;padding:0;font-size:30px;font-weight:400}
    .stat .display .number h3>small{font-size:23px}
    .stat .display .number small{font-size:14px;color:#AAB5BC;font-weight:600;text-transform:uppercase}
    .stat .display .icon{display:inline-block;float:right;padding:7px 0 0}
    .stat .display .icon>i{color:#cbd4e0;font-size:26px}
    .stat .progress-info{clear:both}
    .stat .progress-info .progress{margin:0;height:4px;clear:both;display:block}
    .stat .progress-info .status{margin-top:5px;font-size:11px;color:#AAB5BC;font-weight:600;text-transform:uppercase}
    .stat .progress-info .status .status-title{float:left;display:inline-block}
    .stat .progress-info .status .status-number{float:right;display:inline-block}
</style>
<style>
   .small-box-footer {
    background: rgb(54, 65, 80);
    color: rgba(255,255,255,.8);
    display: block;
    position: relative;
    text-align: center;
    }
    .hr {
    display: block;
    border-style: inset;
    border-width: 1px;
    margin-top: 10px;
    margin-bottom: 10px;
    }
    .links {
        background: rgb(54, 65, 80);
        color: rgba(255,255,255,.8);
        display: block;
        text-align: center;
        padding-top: 10px;
        padding-bottom: 10px;
    }
    #icon
    {
        margin-right: 20px;
    }
</style>
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/dashboard">Dashboard</a>
</li>
@endsection
@section('content')
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Admin Dashboard
        {{-- <small>statistics, charts, recent events and reports</small> --}}
    </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row" style="margin-bottom: 20px">
        @if (in_array('count biller',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px" >
            <div class="stat">
                <div class="display ">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$biller}}">0</span>
                        </h3>
                        <small>TOTAL BILLERS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                <a href="{{in_array('View Billers',$permission) ? route('biller.index') : ''}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        @endif
        @if (in_array('count supplier',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$supplier}}">0</span>
                        </h3>
                        <small>TOTAL SUPPLIERS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                @if (in_array('View Suppliers',$permission))
                    <a href="{{route('supplier.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
        @if (in_array('count customer',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$customer}}"></span>
                        </h3>
                        <small>TOTAL CUSTOMERS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                @if (in_array('View Customers',$permission))
                    <a href="{{route('customer.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
        @if (in_array('count sale person',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$sp}}"></span>
                        </h3>
                        <small>TOTAL SALE PERSONS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>
                </div>
                @if (in_array('View Sale Persons',$permission))
                    <a href="{{route('saleperson.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
        @if (in_array('count brands',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$brand}}"></span>
                        </h3>
                        <small>TOTAL BRANDS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-bold"></i>
                    </div>
                </div>
                @if (in_array('View Brands',$permission))
                    <a href="{{route('brands.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
        @if (in_array('count category',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$cat}}"></span>
                        </h3>
                        <small>TOTAL CATEGORIES</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-contao"></i>
                    </div>
                </div>
                @if (in_array('View Categories',$permission))
                    <a href="{{route('category.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
        @if (in_array('count purchase orders',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$po}}"></span>
                        </h3>
                        <small>PURCHASE ORDERS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-shopping-cart"></i>
                    </div>
                </div>
                @if (in_array('View Purchase Orders',$permission))
                    <a href="{{route('purchase.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
        @if (in_array('count sales',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12" style="margin-bottom: 20px">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$sale}}"></span>
                        </h3>
                        <small>TOTAL SALES</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-credit-card"></i>
                    </div>
                </div>
                @if (in_array('View Sales',$permission))
                    <a href="{{route('sales.index')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                @endif
            </div>
        </div>
        @endif
    </div>
    <hr color="black" class="hr">
    <div class="row">
        @if (in_array('count raw materials',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$raw == null ? 0 : $raw[0]->quantity}}"></span>
                        </h3>
                        <small>TOTAL RAW MATERIALS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (in_array('count packaging',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$pakg == null ? 0 : $pakg[0]->quantity}}"></span>
                        </h3>
                        <small>PACKAGING PRODUCTS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif
        @if (in_array('count finish',$permission))
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="stat">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">
                            <span data-counter="counterup" data-value="{{$finish== null ? 0 : $finish[0]->quantity}}"></span>
                        </h3>
                        <small>FINISHED PRODUCTS</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-list-alt"></i>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
    @if (in_array('Add Bank',$permission) || in_array('Add Brand',$permission) || in_array('Add Product',$permission) || in_array('Add Finish Product',$permission) || in_array('Add Stock',$permission) || in_array('Add Purchase Order',$permission) || in_array('Add Sale',$permission))

    <hr color="black" class="hr">
    <h3 style="text-decoration: underline"><b>QUICK LINKS</b></h3>
    @endif
    <div class="row">
        @if (in_array('Add Bank',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/bank/create" class="links"><i id="icon" class="fa fa-bank"></i>Add Bank</a>
        </div>
        @endif
        @if (in_array('Add Brand',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/brands/create" class="links"><i id="icon" class="fa fa-bold"></i>Add Brand</a>
        </div>
        @endif
        @if (in_array('Add Product',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/product/create" class="links"><i id="icon" class="fa fa-list-alt"></i>Add Product</a>
        </div>
        @endif
        @if (in_array('Add Finish Product',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/product/add" class="links"><i id="icon" class="fa fa-list-alt"></i>Add Finish Product</a>
        </div>
        @endif
    </div>
    <br>
    <div class="row">
        @if (in_array('Add Stock',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/stocks/create" class="links"><i id="icon" class="fa fa-tasks"></i>Add Stock</a>
        </div>
        @endif
        @if (in_array('Add Purchase Order',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/purchase/create" class="links"><i id="icon" class="fa fa-shopping-cart"></i>New Purchase Order</a>
        </div>
        @endif
        @if (in_array('Add Sale',$permission))
        <div class="col-sm-3">
            <a href="{{url('')}}/sales/create" class="links"><i id="icon" class="fa fa-credit-card"></i>New Sale Order</a>
        </div>
        @endif

    </div>
    <br>
    {{-- <div class="row">
        <div class="col-sm-6 col-xs-12 col-md-6 col-lg-6">
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-info-circle font-green"></i>
                        <span class="caption-subject font-green sbold uppercase"> Sale Details</span>
                    </div>

                </div>
                <div class="portlet-body">
                    {{-- <canvas class="chart" id="line-chart"
                            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    <canvas id="myChart" style="display: block; width: 858px; height: 429px;" width="1716" height="858" class="chartjs-render-monitor"></canvas>
                </div>
            </div>
        </div>
    </div> --}}

@endsection
@section('page-scripts')
<!-- ChartJS -->

{{-- <script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script> --}}
@endsection
@section('custom-scripts')

<script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [12, 19, 3, 5, 2, 3],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    });
    </script>
@endsection
