@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/variants">Variants</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-user font-white"></i>View Variants
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('variants.create')}}" class="col-md-2" style="float: right">
                                                    <button  style="background: #00CCFF" type="button"  class="btn btn-block btn-info btn-md ">Add Variants</button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Parent Variant</th>
                                                            <th>Variant Name</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($variant as $v)
                                                            <tr>
                                                                <td>
                                                                    {{$v->id}}
                                                                </td>
                                                                <td>
                                                                    {{$v->parent == null ? 'No Parent' : $v->parent->name}}
                                                                </td>
                                                                <td>
                                                                    {{$v->name}}
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('custom-script')
    @toastr_js
    @toastr_render
        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable({
                    order: [[ 0, "desc" ]],
                    scrollX: true
                });
            });

        </script>
    @endsection
@endsection
