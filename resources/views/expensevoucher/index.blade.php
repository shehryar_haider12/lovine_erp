@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/expenseVoucher">Expense Voucher</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-sticky-note font-white"></i>View Expense Voucher
                                            </div>
                                            <div class="col-md-5 col-sm-5 "></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('add',$permissions))
                                                    <a id="GFG" href="{{route('expenseVoucher.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; " type="button"  class="btn btn-block btn-primary btn-md ">Add Expense Voucher</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="tableview">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Expense Category</label>
                                                        <select name="category" id="category" class="form-control selectpicker" data-live-search="true">
                                                            <option selected value="">Select</option>
                                                            @foreach ($cat as $c)
                                                                <option value="{{$c->name}}">{{$c->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label>Expense Sub-Category</label>
                                                            <select name="scat" class="form-control selectpicker" data-live-search="true">
                                                                <option selected value="">Select</option>
                                                                @foreach ($scat as $s)
                                                                    <option value="{{$s->name_of_account}}">{{$s->name_of_account}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Voucher Date</label>
                                                            <input type="date" name="v_date"  class="form-control" placeholder="Head Category Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Status</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="status" id="status" >
                                                                <option value=""  selected>Select...</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                                <option>Paid</option>
                                                            </select>
                                                        </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">Voucher#</th>
                                                            <th>Date</th>
                                                            <th>Prepared By</th>
                                                            <th>Category</th>
                                                            <th>SubCategory</th>
                                                            <th>Description</th>
                                                            <th>Amount</th>
                                                            <th>Status</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
            <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="pModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Receive Voucher</h4>
                    </div>
                    <form action="{{url('')}}/expenseVoucher/rcv" class="form-horizontal" method="POST" >
                        @csrf
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Receiver Name*</label>
                                        <input class="form-control" type="text" placeholder="Enter Receiver Name" name="received_by" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Amount</label>
                                        <input type="text" readonly class="form-control" name="amount2" id="amount">
                                        <input type="hidden" readonly class="form-control" name="id" id="id">
                                    </div>
                                </div>

                            </div>
                            <div class="row" >
                                <div class="col-md-6">
                                    <div class="form-outline">
                                        <label >Paying By*</label>
                                        <select required class="form-control selectpicker" name="paid_by" id="paid_by" >
                                            <option value="" disabled selected>Select...</option>
                                            <option value="Cash">Cash</option>
                                            <option value="Bank">Bank</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" hidden id="bank">
                                    <div class="form-outline">
                                        <label>Bank Name*</label>
                                        <select name="bid" id="bid" class="form-control selectpicker" data-live-search="true">
                                            <option selected disabled>Select</option>
                                            @foreach ($bank as $b)
                                                <option value="{{$b->id}}">{{$b->name}} - {{$b->branch}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-offset-0 col-md-9">
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>

                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    @endsection
@section('custom-script')
@toastr_js
@toastr_render
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                order: [[ 0, "desc" ]],
                processing: true,
                serverSide: true,
                ajax: '{{route("expenseVoucher.datatable")}}',
                "columns": [{
                        "data": "id",
                        "defaultContent": ""
                    },
                    {
                        "data": "v_date",
                        "defaultContent": ""
                    },
                    {
                        "data": "prepared_user.name",
                        "defaultContent": ""
                    },
                    {
                        "data": "category.name",
                        "defaultContent": ""
                    },
                    {
                        "data": "subcategory.name_of_account",
                        "defaultContent": ""
                    },
                    {
                        "data": "description",
                        "defaultContent": ""
                    },
                    {
                        "data": "amount",
                        "defaultContent": ""
                    },
                    {
                        "data": "status",
                        "defaultContent": ""
                    },
                    {
                        "data": "id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            if(row.status == "Pending")
                            {
                                return `
                                <select class="form-control action" id="`+row.id+`">
                                    <option>Actions</option>
                                    @if(in_array('status',$permissions))
                                        <option >Approved</option>
                                    @endif
                                    @if(in_array('edit',$permissions))
                                        <option>Edit Voucher</option>
                                    @endif
                                </select>`;
                            }
                            else  if(row.status == "Approved")
                            {
                                return `
                                <select class="form-control action" id="`+row.id+`">
                                    <option>Actions</option>
                                    @if(in_array('Generate voucher',$permissions))
                                        <option>Generate Voucher</option>
                                    @endif
                                    @if(in_array('status',$permissions))
                                        <option>Paid</option>
                                    @endif
                                </select>`;
                            }
                            else  if(row.status == "Paid")
                            {
                                return `
                                <select class="form-control action" id="`+row.id+`">
                                    <option>Actions</option>
                                    @if(in_array('Generate voucher',$permissions))
                                        <option>Generate Voucher</option>
                                    @endif
                                </select>`;
                            }
                        },
                    },
                    {
                        "targets": -2,
                        "render": function (data, type, row, meta) {
                            if(row.status == "Pending")
                            {
                                return `<button type="button" class="btn btn-xs  red" id="`+row.id+`">
                                    `+row.status+`
                                </button>`;
                            }
                            if(row.status == "Approved")
                            {
                                return `<button type="button" class="btn btn-xs  blue" id="`+row.id+`">
                                    `+row.status+`
                                </button>`;
                            }
                            if(row.status == "Paid")
                            {
                                return `<button type="button" class="btn btn-xs  btn-success" id="`+row.id+`">
                                    `+row.status+`
                                </button>`;
                            }
                        }
                    }
                ],
                "footerCallback": function( tfoot, data, start, end, display ) {
                    var amount = 0;
                    for (var i = 0; i < data.length; i++) {
                        amount = amount + parseInt(data[i]['amount']);
                    }
                    $( table.column( 6 ).footer() ).html(Number.isNaN(amount) ? '0' : amount);
                },

            });
            $('#advanceSearch').submit(function(e){
                    e.preventDefault();
                    console.log($('select[name="category"]').val());
                    table.columns(3).search($('select[name="category"]').val());
                    table.columns(4).search($('select[name="scat"]').val());
                    table.columns(7).search($('select[name="status"]').val());
                    table.columns(1).search($('input[name="v_date"]').val());
                    table.draw();
            });
            $(document).on('change','.action',function(){
                var val=$(this).val();
                if(val == "Approved")
                {
                    var id = $(this).attr('id');
                    var status = val;
                    axios
                    .post('{{route("expenseVoucher.status")}}', {
                        _token: '{{csrf_token()}}',
                        _method: 'post',
                        id: id,
                        status: status,
                        })
                        .then(function (responsive) {
                        console.log('responsive');
                        location.reload();
                        })
                        .catch(function (error) {
                        console.log(error);
                    });
                }
                if(val == 'Edit Voucher')
                {
                    var id=$(this).attr("id");
                    window.location.href='{{url('')}}/expenseVoucher/'+id+'/edit';
                }
                if(val == 'Generate Voucher')
                {
                    var id=$(this).attr("id");
                    $.ajax({
                        url:"{{url('')}}/expenseVoucher/pdf/"+id,
                        method:"GET",
                        data:
                        {
                            id:id,
                        },
                        error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                        success:function(data){
                            console.log(data);
                            window.location.href= '{{url('')}}/expenseVoucher/pdf/'+id;
                            // location.reload();
                            $('.action').val('Actions');
                        }
                    });
                }
                if(val == 'Paid')
                {
                    var id=$(this).attr("id");
                    $.ajax({
                        url:"{{url('')}}/expenseVoucher/"+id,
                        method:"GET",
                        error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                        success:function(data){
                            console.log(data);
                            $('#pModal').modal("show");
                            $('#amount').val(data.amount);
                            $('#id').val(data.id);
                        }
                    });
                }
            });


            $(document).on('change','#paid_by',function(){
                var p=$(this).val();
                if(p == 'Cash')
                {
                    $('#bank').hide();
                }
                if(p == 'Bank')
                {
                    $('#bank').show();
                }
            });
        });

    </script>

@endsection
@endsection
