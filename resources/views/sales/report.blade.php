@extends('layouts.master')
@section('top-styles')
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/sales">Sales</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-credit-card font-white"></i>Sales Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if ($index == 0)
                                                    <a style="margin-left:300px; "  href="{{route('sales.report.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 1)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$year}}/{{$c}}/{{$sp}}/{{$p}}/{{$types}}/{{$w}}/{{$check}}/excel/year">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 2)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$month}}/{{$c}}/{{$sp}}/{{$p}}/{{$types}}/{{$w}}/{{$check}}/excel/month">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 3)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$date}}/{{$c}}/{{$sp}}/{{$p}}/{{$types}}/{{$w}}/{{$check}}/excel/date">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 4)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$from}}/{{$to}}/{{$c}}/{{$sp}}/{{$p}}/{{$types}}/{{$w}}/{{$check}}/excel/date">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 5)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$w}}/excel/w">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 6)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$p}}/{{$types}}/excel/p">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 7)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$c}}/excel/c">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if ($index == 8)
                                                    <a style="margin-left:300px; "  href="/sales/report/search/{{$sp}}/excel/sp">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="{{url('')}}/sales/report/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Product">By Product
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Warehouse">By Warehouse
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                        </label>
                                                    </div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Customer Name</label>
                                                            <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                <option value="" disabled selected>Select...</option>
                                                                @foreach ($customer as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sale Persons Name</label>
                                                            <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                <option value=""  disabled selected>Select...</option>
                                                                @foreach ($person as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Warehouse</label>
                                                            <select name="w_id" id="w_id" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                @foreach ($ware as $u)
                                                                <option  value="{{$u->id}}">{{$u->w_name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Products</label>
                                                            <select name="p_id" id="pro_id" class="form-control" disabled>
                                                                <option disabled  selected>Select</option>
                                                                @foreach ($product as $s)
                                                                    @if ($s->vstatus == 0)
                                                                        <option value="{{$s->id.'-'.'0'}}">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                    @else
                                                                        @foreach ($s->variants as $v)
                                                                            <option value="{{$v->id.'-'.'1'}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                        {{-- <div class="col-md-8"></div> --}}
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            {{-- <label for="">Unit Name</label> --}}
                                                            <label for="" style="visibility: hidden">.</label>
                                                            <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Invoice#</th>
                                                            <th>Sales Date</th>
                                                            <th>Product</th>
                                                            <th>Price</th>
                                                            <th>Warehouse</th>
                                                            <th>Accountant</th>
                                                            <th>Customer</th>
                                                            <th>Sale Person</th>
                                                            <th>Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a=1;
                                                            $tp=0;
                                                            $tq=0;
                                                        @endphp
                                                        @foreach ($saledetail as $s)
                                                            @if ($s->sale == null)
                                                            @else
                                                                @if ($counter == null)
                                                                    @if ($s->type == 1)
                                                                        <tr>
                                                                            <td>{{$a}}</td>
                                                                            <td>{{$s->s_id}}</td>
                                                                            <td>{{$s->sale->sale_date}}</td>
                                                                            <td>{{$s->variant->name}}</td>
                                                                            <td>{{$s->price}}</td>
                                                                            <td>{{$s->sale->warehouse->w_name}}</td>
                                                                            <td>{{$s->sale->biller->name}}</td>
                                                                            <td>{{$s->sale->customer->name}}</td>
                                                                            <td>{{$s->sale->saleperson->name}}</td>
                                                                            <td>{{$s->quantity}}</td>
                                                                        </tr>

                                                                    @else

                                                                        <tr>
                                                                            <td>{{$a}}</td>
                                                                            <td>{{$s->s_id}}</td>
                                                                            <td>{{$s->sale->sale_date}}</td>
                                                                            <td>{{$s->products->pro_name}}</td>
                                                                            <td>{{$s->price}}</td>
                                                                            <td>{{$s->sale->warehouse->w_name}}</td>
                                                                            <td>{{$s->sale->biller->name}}</td>
                                                                            <td>{{$s->sale->customer->name}}</td>
                                                                            <td>{{$s->sale->saleperson->name}}</td>
                                                                            <td>{{$s->quantity}}</td>
                                                                        </tr>
                                                                    @endif
                                                                @else
                                                                    @php
                                                                        $uid = Auth::user()->id;
                                                                        $sale =  \App\SaleCounter::where('u_id',$uid)->where('co_id',$counter)->get();
                                                                    @endphp
                                                                        @foreach ($sale as $ss)
                                                                            @if ($ss->s_id == $s->s_id)
                                                                                @if ($s->type == 1)
                                                                                    <tr>
                                                                                        <td>{{$a}}</td>
                                                                                        <td>{{$s->s_id}}</td>
                                                                                        <td>{{$s->sale->sale_date}}</td>
                                                                                        <td>{{$s->variant->name}}</td>
                                                                                        <td>{{$s->price}}</td>
                                                                                        <td>{{$s->sale->warehouse->w_name}}</td>
                                                                                        <td>{{$s->sale->biller->name}}</td>
                                                                                        <td>{{$s->sale->customer->name}}</td>
                                                                                        <td>-</td>
                                                                                        <td>{{$s->quantity}}</td>
                                                                                    </tr>
                                                                                @else

                                                                                    <tr>
                                                                                        <td>{{$a}}</td>
                                                                                        <td>{{$s->s_id}}</td>
                                                                                        <td>{{$s->sale->sale_date}}</td>
                                                                                        <td>{{$s->products->pro_name}}</td>
                                                                                        <td>{{$s->price}}</td>
                                                                                        <td>{{$s->sale->warehouse->w_name}}</td>
                                                                                        <td>{{$s->sale->biller->name}}</td>
                                                                                        <td>{{$s->sale->customer->name}}</td>
                                                                                        <td>-</td>
                                                                                        <td>{{$s->quantity}}</td>
                                                                                    </tr>
                                                                                @endif
                                                                            @endif
                                                                        @endforeach
                                                                @endif
                                                                @php
                                                                    $a++;
                                                                    $tp+=$s->price;
                                                                    $tq+=$s->quantity;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th>{{$tp}}</th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>{{$tq}}</th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render
<script type="text/javascript">

    $(document).ready(function () {
        var table = $('#example').DataTable({
            scrollX: true,
            order: [[ 0, "desc" ]]
        });
        $('input:radio[name="optradio"]').change(function(){
            if ($(this).is(':checked')) {
                $('#search').prop('disabled',false);
                var val = $(this).val();
                if(val == 'Year')
                {
                    $('#year').prop('disabled',false);
                    $('#year').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#pro_id').prop('disabled',false);
                    $('#pro_id').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#w_id').attr('required',false);
                    $('#w_id').attr('disabled',false);
                    $('#pro_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');

                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'Month')
                {
                    $('#month').prop('disabled',false);
                    $('#month').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#pro_id').attr('required',false);
                    $('#pro_id').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#w_id').prop('disabled',false);
                    $('#w_id').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#pro_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'Date')
                {
                    $('#from').prop('disabled',false);
                    $('#from').attr('required',true);
                    $('#to').prop('disabled',false);
                    $('#to').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#pro_id').attr('required',false);
                    $('#pro_id').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#w_id').prop('disabled',false);
                    $('#w_id').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                    $('#pro_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'Product')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#pro_id').attr('required',true);
                    $('#pro_id').attr('disabled',false);
                    $('#c_id').prop('disabled',true);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',true);
                    $('#sp_id').prop('required',false);
                    $('#w_id').prop('disabled',true);
                    $('#w_id').prop('required',false);
                    $('#pro_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }

                if(val == 'Warehouse')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#pro_id').attr('disabled',true);
                    $('#pro_id').attr('required',false);
                    $('#w_id').prop('disabled',false);
                    $('#w_id').attr('required',true);
                    $('#c_id').prop('disabled',true);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',true);
                    $('#sp_id').prop('required',false);
                }
                if(val == 'customer')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#pro_id').attr('disabled',true);
                    $('#pro_id').attr('required',false);
                    $('#w_id').prop('disabled',true);
                    $('#w_id').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',true);
                    $('#sp_id').prop('disabled',true);
                    $('#sp_id').prop('required',false);
                    $('#c_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
                if(val == 'saleperson')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#pro_id').attr('disabled',true);
                    $('#pro_id').attr('required',false);
                    $('#w_id').prop('disabled',true);
                    $('#w_id').attr('required',false);
                    $('#c_id').prop('disabled',true);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',true);
                    $('#sp_id').addClass('selectpicker');
                    $('.selectpicker').selectpicker('render');
                }
            }
        });

    });

</script>

    @endsection
@endsection
