<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{config('app.Logoblack')}}" />
    <title>Mungalo POS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <style type="text/css">
        * {
            font-size: 14px;
            line-height: 24px;
            font-family: 'Ubuntu', sans-serif;
            text-transform: capitalize;
        }
        .btn {
            padding: 7px 10px;
            text-decoration: none;
            border: none;
            display: block;
            text-align: center;
            margin: 7px;
            cursor:pointer;
        }

        .btn-info {
            background-color: #999;
            color: #FFF;
        }

        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }
        td,
        th,
        tr,
        table {
            border-collapse: collapse;
        }
        tr {border-bottom: 1px dotted #ddd;}
        td,th {padding: 7px 0;width: 50%;}

        table {width: 100%;}
        tfoot tr th:first-child {text-align: left;}

        .centered {
            text-align: center;
            align-content: center;
        }
        small{font-size:11px;}

        @media print {
            * {
                font-size:12px;
                line-height: 20px;
            }
            td,th {padding: 5px 0;}
            .hidden-print {
                display: none !important;
            }
            @page { margin: 0; } body { margin: 0.5cm; margin-bottom:1.6cm; }
        }
    </style>
  </head>
<body>

<div style="max-width:850px;margin:0 auto">
    @if(preg_match('~[0-9]~', url()->previous()))
        @php $url = '../pos'; @endphp
    @else
        @php $url = url()->previous(); @endphp
    @endif
    <div class="hidden-print">
        <table>
            <tr>
                <td><a href="{{$url}}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a> </td>
                <td><button onclick="window.print();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button></td>
            </tr>
        </table>
        <br>
    </div>

    <div id="receipt-data" style="width:400px; margin-right:50px; float:left;">
        <div class="centered">
            <img src="{{config('app.Logoblack')}}" height="50" style="margin:10px 0;">


            <p>Address: {{$sales->warehouse->w_address}}
                <br>Phone Number: {{$sales->warehouse->w_contactNo}}
            </p>
        </div>
        <p>Date: {{$sales->sale_date}}<br>
            Customer: {{$sales->customer->name}}
        </p>
        <table>
            <tbody>
                @php
                    $total=0;
                @endphp
                @foreach($saledetail as $p)
                @php
                    $total+=$p->sub_total;
                @endphp
                <tr><td colspan="2">{{$p->products->pro_name}}<br>{{$p->quantity}} x {{number_format((float)($p->price / $p->quantity), 2, '.', '')}}</td>
                    <td style="text-align:right;vertical-align:bottom">{{number_format((float)$p->price, 2, '.', '')}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">Total</th>
                    <th style="text-align:right">{{number_format((float)$total, 2, '.', '')}}</th>
                </tr>
                @if($sales->tax)
                <tr>
                    <th colspan="2">Tax</th>
                    <th style="text-align:right">{{number_format((float)$sales->tax, 2, '.', '')}}</th>
                </tr>
                @endif
                @if($sales->discount)
                <tr>
                    <th colspan="2">Discount</th>
                    <th style="text-align:right">{{number_format((float)$sales->discount, 2, '.', '')}}</th>
                </tr>
                @endif
                <tr>
                    <th colspan="2">Grand Total</th>
                    <th style="text-align:right">{{number_format((float)$sales->total, 2, '.', '')}}</th>
                </tr>
                {{-- <tr>
                    <th class="centered" colspan="3">In Words: <span>{{str_replace("-"," ",$numberInWords)}}</span></th>
                </tr> --}}
            </tfoot>
        </table>
        <table>
            <tbody>
                @foreach($th as $t)
                <tr style="background-color:#ddd;">
                    <td style="padding: 5px;width:30%">Paid By: {{$t->paid_by}}</td>
                    <td style="padding: 5px;width:40%">Amount: {{number_format((float)$t->total, 2, '.', '')}}</td>
                </tr>
                <tr><td class="centered" colspan="3">Thank you for shopping with us. Please come again</td></tr>
                @endforeach
            </tbody>
        </table>
        <div class="centered" style="margin:30px 0 50px">
            <small>Invoice Generated By Posch Care POS.
                Developed By Hashlob</strong></small>
        </div>
    </div>
     <div id="receipt-data" style="width:400px; float:left;">
        <div class="centered">
            <img src="{{config('app.Logoblack')}}" height="50" style="margin:10px 0;">


            <p>Address: {{$sales->warehouse->w_address}}
                <br>Phone Number: {{$sales->warehouse->w_contactNo}}
            </p>
        </div>
        <p>Date: {{$sales->sale_date}}<br>
            Customer: {{$sales->customer->name}}
        </p>
        <table>
            <tbody>
                @php
                    $total=0;
                @endphp
                @foreach($saledetail as $p)
                @php
                    $total+=$p->sub_total;
                @endphp
                <tr><td colspan="2">{{$p->products->pro_name}}<br>{{$p->quantity}} x {{number_format((float)($p->price / $p->quantity), 2, '.', '')}}</td>
                    <td style="text-align:right;vertical-align:bottom">{{number_format((float)$p->price, 2, '.', '')}}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="2">Total</th>
                    <th style="text-align:right">{{number_format((float)$total, 2, '.', '')}}</th>
                </tr>
                @if($sales->tax)
                <tr>
                    <th colspan="2">Tax</th>
                    <th style="text-align:right">{{number_format((float)$sales->tax, 2, '.', '')}}</th>
                </tr>
                @endif
                @if($sales->discount)
                <tr>
                    <th colspan="2">Discount</th>
                    <th style="text-align:right">{{number_format((float)$sales->discount, 2, '.', '')}}</th>
                </tr>
                @endif
                <tr>
                    <th colspan="2">Grand Total</th>
                    <th style="text-align:right">{{number_format((float)$sales->total, 2, '.', '')}}</th>
                </tr>
                {{-- <tr>
                    <th class="centered" colspan="3">In Words: <span>{{str_replace("-"," ",$numberInWords)}}</span></th>
                </tr> --}}
            </tfoot>
        </table>
        <table>
            <tbody>
                @foreach($th as $t)
                <tr style="background-color:#ddd;">
                    <td style="padding: 5px;width:30%">Paid By: {{$t->paid_by}}</td>
                    <td style="padding: 5px;width:40%">Amount: {{number_format((float)$t->total, 2, '.', '')}}</td>
                </tr>
                <tr><td class="centered" colspan="3">{{trans('file.Thank you for shopping with us. Please come again')}}</td></tr>
                @endforeach
            </tbody>
        </table>
        <div class="centered" style="margin:30px 0 50px">
            <small>Invoice Generated By Posch Care POS.
                Developed By Hashlob</strong></small>
        </div>
    </div>
</div>

<script type="text/javascript">
    function auto_print() {
        window.print()
    }
    setTimeout(auto_print, 1000);
</script>

</body>
</html>
