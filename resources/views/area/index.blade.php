@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/area">Areas</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-area-chart font-white"></i>View Areas
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('area.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:0px" type="button"  class="btn btn-block btn-primary btn-md ">Add Area</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="tableview">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="">Area Name</label>
                                                        <input type="text" name="name" id="autocomplete-ajax1" class="form-control" placeholder="Area Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>

                                                    <div class="col-md-4"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Total Customers</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
@toastr_js
@toastr_render

    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                processing: true,
                order: [[ 0, "desc" ]],
                serverSide: true,
                ajax: '{{route("area.datatable")}}',
                "columns": [{
                        "data": "id",
                        "defaultContent": ""
                    },
                    {
                        "data": "name",
                        "defaultContent": ""
                    },
                    {
                        "data": "total",
                        "defaultContent": ""
                    },
                    {
                        "data": "id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            var edit = '{{route("area.edit",[":id"])}}';
                            edit = edit.replace(':id', row.id );
                            return `
                            @if(in_array('edit',$permissions))
                                <a id="GFG" href="` + edit + `" class="text-info p-1">
                                    <button type="button" class="btn blue edit" >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            @endif
                            `;
                        },
                    },
                ],
            });
            $('#advanceSearch').submit(function(e){
                e.preventDefault();
                // var date = moment($('input[name="created_at"]').val(),'YYYY/MM/DD').format('YYYY-MM-DD');
                // if(date === 'Invalid date')
                // {
                // date = '';
                // }
                // console.log($('.ver-status').val());
                table.columns(1).search($('input[name="name"]').val());
                table.draw();
            });
        });

    </script>
@endsection
@endsection
