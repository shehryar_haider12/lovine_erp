@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/accountdetails">Account</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title"  style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-money font-white"></i>View Accounts
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('accountdetails.create')}}" class="col-md-2" style="float: right">
                                                    <button style="background: #00CCFF" type="button"  class="btn btn-block btn-primary btn-md ">Add Account</button>
                                                </a>
                                            @endif

                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Account Name</label>
                                                        <select class="form-control selectpicker" data-live-search="true" name="name" id="name" >
                                                            <option value=""  selected>Select...</option>
                                                            @foreach ($account as $u)
                                                            <option  value="{{$u->name_of_account}}">{{$u->name_of_account}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Head Category Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="a_id" id="a_id" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($hcat as $u)
                                                                <option  value="{{$u->name}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Code</label>
                                                            <input type="text" name="code" id="autocomplete-ajax1" class="form-control" placeholder="Account Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Account Code</th>
                                                            <th>Account Name</th>
                                                            <th>Head Category</th>
                                                            <th>Balance</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $s = 1;
                                                            $bln = 0;
                                                        @endphp
                                                        @foreach ($account as $a)
                                                        @php
                                                        // if($a->Code == 'CA-01-003')
                                                        // {
                                                            $credit = $a->generalLedger->pluck('credit')->toArray();
                                                            $sum_credit = array_sum($credit);
                                                            $debit = $a->generalLedger->pluck('debit')->toArray();
                                                            $sum_debit = array_sum($debit);

                                                            $balance =  $sum_debit - $sum_credit;
                                                        // }

                                                        @endphp
                                                            <tr>
                                                                <td>
                                                                    {{$s}}
                                                                </td>
                                                                <td>
                                                                    {{$a->Code}}
                                                                </td>
                                                                <td>
                                                                    {{$a->name_of_account}}
                                                                </td>
                                                                <td>
                                                                    {{$a->headCategory->name}}
                                                                </td>
                                                                <td>
                                                                    {{$balance}}
                                                                </td>
                                                                <td>
                                                                    @if(in_array('history',$permissions))
                                                                    <button type="button" class="btn blue view" id="{{$a->Code}}">
                                                                        <i class="fa fa-book"></i>
                                                                    </button>
                                                                    @else
                                                                        <button disabled type="button" class="btn blue view" id="{{$a->Code}}">
                                                                            <i class="fa fa-book"></i>
                                                                        </button>
                                                                    @endif
                                                                        {{-- <a id="GFG" href="/headcategory/{{$a->id}}/edit" class="text-info p-1">
                                                                            <button type="button" class="btn blue edit" >
                                                                                <i class="fa fa-edit"></i>
                                                                            </button>
                                                                        </a> --}}
                                                                </td>
                                                            </tr>
                                                            @php
                                                                $s++;
                                                                $bln+=$balance;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th>{{$bln}}</th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('custom-script')
    @toastr_js
    @toastr_render

        <script type="text/javascript">
            $(document).ready(function () {
                var table = $('#example').DataTable();

                $('#advanceSearch').submit(function(e){
                e.preventDefault();
                table.columns(1).search($('input[name="code"]').val());
                table.columns(2).search($('select[name="name"]').val());
                table.columns(3).search($('select[name="a_id"]').val());
                table.draw();
            });
        });
        $(document).on('click','.view',function(){
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/accountdetails/chkHistory/"+id,
                    method:"GET",
                    error: function (request, error) {
                                alert(" Can't do because: " + error +request);
                            },
                    success:function(data){
                        console.log(data);
                        if(data == 0)
                        {
                            alert('No Record Found');
                        }
                        else
                        {
                            window.location.href='{{url('')}}/accountdetails/history/'+id;
                        }
                    }
                });
            });


        </script>
    @endsection
@endsection
