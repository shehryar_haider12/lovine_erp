<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/png" href="{{url('')}}/uploads/posch.jpg" />
    <title>Posch Care POS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">

    <style type="text/css">
        * {
            font-size: 14px;
            line-height: 24px;
            font-family: 'Ubuntu', sans-serif;
            text-transform: capitalize;
        }
        .btn {
            padding: 7px 10px;
            text-decoration: none;
            border: none;
            display: block;
            text-align: center;
            margin: 7px;
            cursor:pointer;
        }

        .btn-info {
            background-color: #999;
            color: #FFF;
        }

        .btn-primary {
            background-color: #6449e7;
            color: #FFF;
            width: 100%;
        }
        td,
        th,
        tr,
        table {
            border-collapse: collapse;
        }
        tr {border-bottom: 1px dotted #ddd;}
        /* td,th {padding: 7px 0;width: 50%;} */

        table {width: 100%;}
        tfoot tr th:first-child {text-align: left;}

        .centered {
            text-align: center;
            align-content: center;
        }
        small{font-size:11px;}

        @media print {
            * {
                font-size:12px;
                line-height: 20px;
            }
            td,th {padding: 5px 0;}
            .hidden-print {
                display: none !important;
            }
            @page { margin: 0; } body { margin: 0.5cm; margin-bottom:1.6cm; }
        }
    </style>
  </head>
<body>

<div style="max-width:850px;margin:0 auto">
    @if(preg_match('~[0-9]~', url()->previous()))
        @php $url = '../pos'; @endphp
    @else
        @php $url = url()->previous(); @endphp
    @endif
    <div class="hidden-print">
        <table>
            <tr>
                <td><a href="{{$url}}" class="btn btn-info"><i class="fa fa-arrow-left"></i> Back</a> </td>
                <td><button onclick="window.print();" class="btn btn-primary"><i class="fa fa-print"></i> Print</button></td>
            </tr>
        </table>
        <br>
    </div>

    <div id="receipt-data" style="width:400px; margin-right:50px; float:left;">
        <div class="centered">
            <img src="{{url('')}}/uploads/posch.jpg" height="50" style="margin:10px 0;">



        <div class="centered" style="margin:30px 0 50px">
            <label style="border-style: solid; padding: 8px;display: block" ><b>POS Session Activity Report</b></label>
            <br>
            <label style="border-style: dashed; padding: 8px; display: block" ><b>Counter: POS#{{$counter}}</b></label>
            <br>
            <label style="border-style: solid; padding: 8px; display: block">
                <label><b> Opening Date &nbsp;&nbsp;&nbsp; {{$date}} &nbsp; {{$day}} &nbsp; {{\Carbon\Carbon::parse($counters->open)->format('g:i A')}} </b></label>
                <br>
                <label><b> Closing Date &nbsp;&nbsp;&nbsp; {{$date}} &nbsp; {{$day}} &nbsp; {{\Carbon\Carbon::parse($counters->close)->format('g:i A')}}</b></label>
                <br>
                <label><b> Session Open Operator: &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;{{$sales[0]->user->name}} </b></label>
                <br>
                <label><b> Session Closed Operator: &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;{{$sales[$saleCount-1]->user->name}}  </b></label>
            </label>
            <br>
            <label style="border-style: solid; padding: 8px;display: block" ><b>POS Document Activity</b></label>
            <br>
            <table >
                <thead>
                    <tr>
                        <th>Document Type</th>
                        <th># of Doc</th>
                        <th>Amount</th>
                        <th>Discount</th>
                    </tr>
                </thead>
                <tbody style="border: 1px dashed black;">
                    <tr>
                        <td>Sale Invoice</td>
                        <td>{{$totalSale}}</td>
                        <td>{{$SaleAmount}}</td>
                        <td>{{$discount}}</td>
                    </tr>
                    <tr>
                        <td>Sale Return</td>
                        <td>{{$totalReturn}}</td>
                        <td>{{$ReturnAmount}}</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>Net Amount</td>
                        <td>{{$totalSale + $totalReturn}}</td>
                        <td>{{$SaleAmount - $ReturnAmount}}</td>
                        <td>{{$discount}}</td>
                    </tr>
                </tbody>
            </table>
            <br>
            <label style="border-style: solid; padding: 8px;display: block" ><b>Cash Received Summary</b></label>
            <br>
            <label style="border-style: dashed; padding: 8px; display: block">
                <label><b> Opening Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{$totalCashOpening}}</b></label>
                <br>
                <label><b> Sale - Cash &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{array_sum($totalCashClosing)}} </b></label>
            </label>
            <br>
            <label><b> Total Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{$totalCashOpening + array_sum($totalCashClosing)}}</b></label>
            <br>
            <label style="border-style: solid; padding: 8px;display: block" ><b>Cash Paid Summary</b></label>
            <br>
            <label style="border-style: dashed; padding: 8px; display: block">
                <label><b> Cash Back / Sale Return &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{$ReturnAmount}}</b></label>
            </label>
            <label><b> Total Amount &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{($totalCashOpening + array_sum($totalCashClosing)) - $ReturnAmount}}</b></label>
            <br>
            <label style="border-style: solid; padding: 8px;display: block" ><b>Credit Card Merchant History</b></label>
            @if (count($th)>0)

                <table >
                    <thead>
                        <tr>
                            <th>Merchant Name</th>
                            <th># of Doc</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <tbody style="border: 1px dashed black;">
                    @foreach ($th as $t)
                        <tr>
                            <td>
                                {{$t->merchant_type}}
                            </td>
                            <td>
                                {{$t->id}}
                            </td>
                            <td>
                                {{$t->total}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
            <br>
            <small>Invoice Generated By Posch Care POS.
                Developed By Hashlob</strong></small>
        </div>
    </div>
</div>

<script type="text/javascript">
    function auto_print() {
        window.print()
    }
    setTimeout(auto_print, 1000);
</script>

</body>
</html>
