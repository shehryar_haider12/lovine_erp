@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/purchase">Purchase Order</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 row">
                                            <div class="col-md-4">
                                                <i class="fa fa-shopping-cart font-white"></i>Purchase Order Report
                                            </div>
                                            <div class="col-md-5"></div>
                                            <div class="col-md-3">
                                                {{-- <a style="margin-left:-20px"  href="{{route('purchase.excel')}}">
                                                    <i class="fa fa-file-excel-o  font-white"></i>
                                                </a> --}}
                                                {{-- <a id="GFG" href="{{route('purchase.create')}}" >
                                                    <button style="background: #00CCFF; margin-left:20px; " type="button"  class="btn btn-block btn-primary btn-md ">Add Purchase Order</button>
                                                </a> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Products</label>
                                                            <select  name="p_name" class="form-control selectpicker" data-live-search="true">
                                                                <option selected="" value="">No Filter</option>
                                                                @foreach ($product as $s)
                                                                    @if ($s->vstatus == 0)
                                                                        <option value="{{$s->pro_code}} - {{$s->pro_name}} ">{{$s->pro_code}} - {{$s->pro_name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                    @else
                                                                        @foreach ($s->variants as $v)
                                                                            <option value="{{$v->name}}">{{$v->name}} - {{$s->p_type}} - {{$s->brands->b_name}}</option>
                                                                        @endforeach
                                                                    @endif
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Reference Number</label>
                                                            <input type="text" name="ref"  class="form-control" placeholder="Reference Number" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Order Date</label>
                                                            <input type="date"  dateFormat= 'yyyy-mm-dd' name="date"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Warehouse Name</label>
                                                        <select class="form-control selectpicker" data-live-search="true" name="w_name">
                                                            <option value=""  selected>Select...</option>
                                                            @foreach ($ware as $u)
                                                            <option  value="{{$u->w_name}}">{{$u->w_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Supplier Name</label>
                                                            <select class="form-control selectpicker" data-live-search="true" name="s_name" >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($supplier as $u)
                                                                <option  value="{{$u->name}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    {{-- <div class="col-md-8"></div> --}}
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Invoice#</th>
                                                            <th>Reference #</th>
                                                            <th>Order Date</th>
                                                            <th>Warehouse</th>
                                                            <th>Supplier</th>
                                                            <th>Product</th>
                                                            <th>Cost</th>
                                                            <th>Quantity</th>
                                                            <th>Received Quantity</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a=1;
                                                        @endphp
                                                        @foreach ($purchasedetail as $p)
                                                            <tr>
                                                                <td>{{$a}}</td>
                                                                <td>{{$p->id}}</td>
                                                                <td>{{$p->purchase->ref_no}}</td>
                                                                <td>{{$p->purchase->order_date}}</td>
                                                                <td>{{$p->purchase->warehouse->w_name}}</td>
                                                                <td>{{$p->purchase->supplier->name}}</td>
                                                                @if ($p->type == 1)
                                                                    <td>{{$p->variant->name}}</td>
                                                                @else

                                                                    <td>{{$p->products->pro_name}}</td>
                                                                @endif
                                                                <td>{{$p->cost}}</td>
                                                                <td>{{$p->quantity}}</td>
                                                                <td>{{$p->received_quantity}}</td>
                                                            </tr>
                                                            @php
                                                                $a++;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <td>TOTAL</td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                            <td></td>
                                                        </tr>
                                                    </tfoot>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

    @section('custom-script')
        @toastr_js
        @toastr_render
<script type="text/javascript">
    $(document).ready(function () {
        var table = $('#example').DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                // Total over all pages
                totalqty = api
                    .column( 8 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                totalcost = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );
                totalrcv = api
                    .column( 9 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 8 ).footer() ).html(totalqty);
                $( api.column( 7 ).footer() ).html(totalcost);
                $( api.column( 9 ).footer() ).html(totalrcv);
            }
        });
        $('#advanceSearch').submit(function(e){
            e.preventDefault();
            table.columns(2).search($('input[name="ref"]').val());
            table.columns(3).search($('input[name="date"]').val());
            table.columns(4).search($('select[name="w_name"]').val());
            table.columns(5).search($('select[name="s_name"]').val());
            table.columns(6).search($('select[name="p_name"]').val());
            table.draw();
        });
    });

</script>

    @endsection
@endsection
