<html>
  <head>
    {{-- <link href="{{url('')}}/style-lik/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /> --}}
    <meta charset="utf-8">
    <title></title>
  </head>
  <style>
      .hr {
            display: block;
            border-style: inset;
            border-width: 1px;
            margin-top: -120px;
            width: 100%;
        }
      .attendance-table table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #000;
         font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }

        .blank-cell{

        min-width: 50px;


        }

        .attendance-cell{

        padding: 8px;


        }

        .attendance-table table th.attendance-cell, .attendance-table table td.attendance-cell {
            border: 1px solid #000;
        }
        /* h2,h4 {
            color: #00CCFF;
        } */
        .table-inv
        {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 50%;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
        }
        .table-inv thead
        {
            background-color: #ADD8E6;
        }
        .table-inv td,th
        {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }
        .label
        {
            background: #F0F8FF;
            font-size: 15px;
            text-align: left;
            color: #000;
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            height: 60px;
            padding-left: 30px;
            /* font-weight: bold; */
        }
        .row
        {
             font-family: 'Franklin Gothic Medium', 'Arial Narrow', Arial, sans-serif;
            padding-left: 30px;
        }
        .col
        {
            font-size: 18px;
            /* background: #F0FFFF; */
            /* height: 28px; */
        }
        @page
        {
            margin: 0;
            size: A3;
        }
        .logo
        {
            margin-left: 255px;
            margin-top: -200px;
        }
        #overlay
        {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            background-image: url('{{url('')}}/uploads/CN7R2pF2hxI5A9KsFYNhsFSJ2HYFi0bsNDjt016D.jpg');
            background-position: center top;
            background-repeat: no-repeat;
            z-index: -1;
        }
  </style>
  <body>
      <header>
            <div style="color: #4169E1; margin-top: 30px;margin-left: 20px" >
                <label>Company Name: AZK Traders</label>
                <br>
                <label>Website: www.mungalo.pk</label>
                <br>
                <label>NTN: 3968036-3</label>
                <br>
                <label>Company Phone: 0333-2918981</label>
                <br>
                <label>Email: ops@mungalo.pk</label>
                <br>
                <label>Headoffice: Plot No C-2-C 1st Floor
                    <br>
                    Sadi Lane 1 DHA Ph 7 Khi
                </label>
            </div>
            <img src="{{config('app.Logoblack')}}" style="margin-top: -70px" height="250px" width="550px"  alt="logo" class="logo"  />
            <div style="margin-top: -300px;margin-left: 800px">
                <h1>Purchase Order</h1>
                <label>Dated As: {{\Carbon\Carbon::parse($purchase->order_date)->format('d-m-y')}}</label>
                <br>
                <label>Purchase Order: {{$purchase->id}}</label>
                <br>
                <label>Payment Mode: {{$purchase->payment_mode}} {{$purchase->days == null ? '' : '('.$purchase->days.'Days)'}}</label>
            </div>
    </header>
    <hr class="hr" color="black" >

    <div class="form-body">

        <div class="row col">
            <div>
                <div class="form-outline">
                    <label for=""><b>About Vendor:</b></label>
                </div>
            </div>
            <div style="margin-top: -18px; margin-left: 850px">
                <div class="form-outline">
                    <label for=""><b>Ship To:</b></label>
                </div>
            </div>
        </div>
        <div class="row">
            <div>
                <div class="form-outline">
                    <p><b>Vendor Name: {{ucwords($purchase->supplier->name)}}</b></p>
                    <p><b>Company Name: {{ucwords($purchase->supplier->company)}}</b></p>
                    <p><b>Phone: {{$purchase->supplier->c_no}}</b></p>
                </div>
            </div>
            <div style="margin-top: -150px; margin-left: 850px">
                <div class="form-outline">
                    <p><b>Company Name: {{ $purchase->biller == null ? 'Posch Care' :  ucwords($purchase->biller->name)}}</b></p>
                    <p><b>Warehouse Name: {{ucwords($purchase->warehouse->w_name)}}</b></p>
                    <p><b>Address: {{ucwords($purchase->warehouse->w_address)}}</b></p>
                </div>
            </div>
        </div>
    </div>
         <div class="attendance-table" style="margin-top: 25px">
        <table class="table table-striped table-bordered">

            <thead>
                <tr>
                    <th class="attendance-cell">S.No</th>
                    <th class="attendance-cell">Code - Name</th>
                    <th class="attendance-cell" >Cost</th>
                    <th class="attendance-cell">Quantity</th>
                    <th class="attendance-cell">Received Quantity</th>
                    <th class="attendance-cell">Sub total</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $amount=0;
                    $balance=0;
                @endphp
                @foreach($pdetail as $d)
                    <tr>
                        <td class="attendance-cell" >{{$d->id}}</td>
                        @if ($d->type == 1)
                            <td class="attendance-cell" >{{$d->variant->name}}</td>
                        @else
                            <td class="attendance-cell" >{{$d->products->pro_code}} - {{$d->products->pro_name}}</td>
                        @endif
                        <td class="attendance-cell" >{{$d->cost}}</td>
                        <td class="attendance-cell" >{{$d->quantity}}</td>
                        <td class="attendance-cell" >{{$d->received_quantity}}</td>
                        <td class="attendance-cell" >{{$d->sub_total}}</td>
                    </tr>
                    @php
                    if($purchase->p_status == 'Pending')
                    {
                        $amount=0;
                        $balance+=$d->sub_total;
                    }
                    if($purchase->p_status == 'Paid' || $purchase->p_status == 'Partial')
                    {
                        $amount+=$d->received_quantity * $d->cost;
                        $balance=0;
                    }
                    @endphp
                @endforeach

                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>TOTAL</b></td>
                    <td class="attendance-cell" >{{$purchase->total}}</td>
                </tr>
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>PAID AMOUNT</b></td>
                    <td class="attendance-cell" >{{$th}}</td>
                </tr>
                @if ($purchase->total != $th)
                <tr>
                    <td class="attendance-cell"  colspan="4"></td>
                    <td class="attendance-cell" ><b>BALANCE</b></td>
                    <td class="attendance-cell" >{{$balance == 0 ? $purchase->total - $th : $balance}}</td>
                </tr>
                @endif
            </tbody>

        </table>

    </div>
  </body>
</html>
