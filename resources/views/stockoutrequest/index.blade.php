@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css

<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="{{url('')}}/stockoutRequest">Stockout Request</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption" style="width: -webkit-fill-available; ">
                                            <i class="fa fa-houzz font-white"></i>View Stockout Requests
                                            @if(in_array('Add',$permissions))
                                                <a id="GFG" href="{{route('stockoutRequest.create')}}" class="col-md-2" style="float: right">
                                                    <button  style="background: #00CCFF" type="button"  class="btn btn-block btn-info btn-md ">Add Stockout Request</button>
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="bg-black-transparent1 m-b-15 p15 pb0" style="background-color:#DCDCDC; padding: 20px;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Request Date</label>
                                                            <input type="date" name="date"  class="form-control" placeholder="Code" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Request Status</label>
                                                            <select name="status" class="form-control selectpicker">
                                                                <option selected="" value="">No Filter</option>
                                                                <option value="Pending">Pending</option>
                                                                <option value="Approved">Approved</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Warehouse Name</label>
                                                        <select class="form-control selectpicker" data-live-search="true" name="w_name">
                                                            <option value=""  selected>Select...</option>
                                                            @foreach ($ware as $u)
                                                            <option  value="{{$u->w_name}}">{{$u->w_name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    </div>
                                                    <div class="col-md-8"></div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        {{-- <label for="" style="visibility: hidden">.</label> --}}
                                                        <button style="background: #32c5d2" id="search" style="color: " class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th >Request No</th>
                                                            <th>Request Date</th>
                                                            <th>Warehouse</th>
                                                            <th >Order Status</th>
                                                            <th >Reason</th>
                                                            <th >Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($prequest as $p)
                                                            <tr>
                                                                <td>
                                                                    {{$p->id}}
                                                                </td>
                                                                <td>
                                                                    {{$p->req_date}}
                                                                </td>
                                                                <td>
                                                                    {{$p->warehouse->w_name}}
                                                                </td>
                                                                @if ($p->status == 'Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                    <td> {{$p->reason}} </td>
                                                                    <td>
                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('status',$permissions))
                                                                                <option >Approved</option>
                                                                            @endif
                                                                            @if(in_array('edit',$permissions))
                                                                                <option >Edit Request</option>
                                                                            @endif
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Request Details</option>
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                                @if ($p->status == 'Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                    <td> {{$p->reason}} </td>
                                                                    <td>
                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Request Details</option>
                                                                            @endif
                                                                            @if(in_array('stockout',$permissions))
                                                                                <option >Stock Out</option>
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                @endif
                                                                @if ($p->status == 'Delivered')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  yellow">
                                                                            {{$p->status}}
                                                                        </button>
                                                                    </td>
                                                                    <td> {{$p->reason}} </td>
                                                                    <td>
                                                                        <select class="form-control action" id="{{$p->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Request Details</option>
                                                                            @endif
                                                                        </select>
                                                                    </td>
                                                                @endif

                                                            </tr>

                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Stockout Request</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Request Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="order_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Request No</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Code" id="o_no" readonly>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Reason</label>
                                    <input class="form-control" type="text" placeholder="Enter Reason" id="reason" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th width="5%">S.No</th>
                                            <th width="35%">Product Code -  Name</th>
                                            <th width="5%">Quantity</th>
                                            <th width="5%">In Stock</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--End Modal-->
        <div id="sout" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Stockout Request</h4>
                    </div>
                    <form action="{{url('')}}/stockoutRequest/delivery" class="form-horizontal" method="POST" >
                        @csrf
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Request Date</label>
                                        <input class="form-control" type="text" placeholder="Enter Product Name" name="order_date1" id="order_date1" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Request No</label>
                                        <input class="form-control" type="text" placeholder="Enter Product Code" name="o_no1" id="o_no1" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Warehouse</label>
                                        <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name1" name="w_name1" readonly>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Reason</label>
                                        <input class="form-control" type="text" placeholder="Enter Reason" id="reason1" name="reason1" readonly>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <table id="example2" class="table table-striped table-bordered" style="width:100%">
                                        <thead>
                                            <tr>
                                                <th width="5%">S.No</th>
                                                <th width="35%">Product Code -  Name</th>
                                                <th width="5%">Requested Quantity</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>


                                    </table>
                                </div>
                            </div>
                            <button type="submit" id="delivered" class="btn green">Submit</button>
                        </div>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>

    @endsection
    @section('custom-script')
        @toastr_js
        @toastr_render

<script type="text/javascript">

    function view(id)
    {
        $("#example1 tbody").empty();
        $.ajax({
            url:"{{url('')}}/stockoutRequest/"+id,
            method:"GET",
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                $('#order_date').val(data[0].req_date);
                $('#o_no').val(data[0].id);
                $('#reason').val(data[0].reason);
                $('#w_name').val(data[0].warehouse.w_name);
                for (let i = 0; i < data[1].length; i++) {
                    if(data[1][i].type == 0)
                    {
                        var qty = 0;
                        for (let j = 0; j < data[1][i].products.currentstocks.length; j++) {
                            if(data[1][i].products.currentstocks[j].w_id == data[0].w_id && data[1][i].products.currentstocks[j].p_id == data[1][i].p_id && data[1][i].products.currentstocks[j].type == data[1][i].type )
                            {
                                qty = data[1][i].products.currentstocks[j].quantity;
                            }
                            else
                            {
                                qty = 0;
                            }
                        }
                        $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].products.pro_code+` - `+data[1][i].products.pro_name+`</td><td>`+data[1][i].quantity+`</td><td>`+qty+`</td></tr>`);
                    }
                    else
                    {
                        var qty = 0;
                        for (let j = 0; j < data[1][i].products.currentstocks.length; j++) {
                            if(data[1][i].products.currentstocks[j].w_id == data[0].w_id && data[1][i].products.currentstocks[j].p_id == data[1][i].p_id && data[1][i].products.currentstocks[j].type == data[1][i].type )
                            {
                                qty = data[1][i].products.currentstocks[j].quantity;
                            }
                            else
                            {
                                qty = 0;
                            }
                        }
                        $('#example1').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].variant.name+`</td><td>`+data[1][i].quantity+`</td><td>`+qty+`</td></tr>`);
                    }
                }

                $('#myModal').modal("show");
                $('#example1').DataTable();

            }
        });
    }
    $(document).ready(function () {

        var table = $('#example').DataTable({
            scrollX: true
        });
        $('#advanceSearch').submit(function(e){
            e.preventDefault();
            console.log($('select[name="status"]').val());
            table.columns(1).search($('input[name="date"]').val());
            table.columns(2).search($('select[name="w_name"]').val());
            table.columns(3).search($('select[name="status"]').val());
            table.draw();
        });

    });
    $(document).on('change','.action',function(){
            var val=$(this).val();
            if(val == 'Approved' )
            {
                var id = $(this).attr('id');
                var status = val;
                axios
                .post('{{route("stockoutRequest.status")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                });
            }

            if(val == 'Stock Out')
            {
                var id=$(this).attr("id");
                $("#example2 tbody").empty();
                $.ajax({
                    url:"{{url('')}}/stockoutRequest/"+id,
                    method:"GET",
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        $('#order_date1').val(data[0].req_date);
                        $('#o_no1').val(data[0].id);
                        $('#reason1').val(data[0].reason);
                        $('#w_name1').val(data[0].warehouse.w_name);
                        for (let i = 0; i < data[1].length; i++) {
                            if(data[1][i].type == 0)
                            {

                                $('#example2').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].products.pro_code+` - `+data[1][i].products.pro_name+`</td><td>`+data[1][i].quantity+`</td>
                                   </tr>`);
                            }
                            else
                            {

                                $('#example2').append(`<tr><td>`+data[1][i].id+`</td><td>`+data[1][i].variant.name+`</td><td>`+data[1][i].quantity+`</td></tr>`);
                            }
                        }

                        $('#sout').modal("show");
                        $('.action').val('Actions');
                        $('#example2').DataTable();

                    }
                });
            }


            if(val == 'Request Details')
            {
                var id=$(this).attr("id");
                view(id);
                $('.action').val('Actions');
            }
            if(val == 'Edit Request')
            {
                var id=$(this).attr("id");
                window.location.href='{{url('')}}/stockoutRequest/'+id+'/edit';
                // $('.action').val('Actions');
            }

        });
        $('#example').on('click','td', function () {
            var col_index = $(this).index();
            var row_index = $(this).parent().index();
            var count = document.getElementById('example').rows[row_index].cells.length - 1;
            if(col_index <count)
            {
                var $tr = $(this).closest('tr');
                var id = $tr.find("td:eq(0)").text();
                view(id);
            }
        });

</script>

    @endsection
@endsection
