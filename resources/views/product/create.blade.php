@extends('layouts.master')
@section('top-styles')
@toastr_css
<style>
    thead {
       background-color: #ADD8E6;
   }
</style>
@section('sidebar-name1')
<li>
    <a href="{{url('')}}/product">Product</a>
    <i class="fa fa-circle"></i>
</li>
<li>
    <span>{{$isEdit ? 'Edit' : 'Add'}} Product</span>
</li>
@endsection
@section('content')

    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light portlet-fit portlet-form bordered" id="form_wizard_1">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" fa fa-list-alt font-green"></i>
                        <span class="caption-subject font-green sbold uppercase">{{$isEdit ? 'Edit' : 'Add'}} Product</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <!-- BEGIN FORM-->
                    <form action="{{$isEdit ? route('product.update',$product->id) :  route('product.store')}} " class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @if ($isEdit)
                            @method('PUT')
                        @endif
                        <div class="form-body">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type='file' name="image" id="imageUpload1" placeholder="add image"
                                            accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload1"><span>Featured Image <small>(optional)</small> </span></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview1"
                                                style="background-image : url({{url('').'/uploads/'}}{{$isEdit ? $product->image : 'placeholder.jpg'}}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Name*</label>
                                        <input value="{{$product->pro_name ?? old('pro_name')}}"  class="form-control" type="text" placeholder="Enter Product Name" name="pro_name" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Barcode <small>(optional)</small> </label>
                                        <select id="barcode" {{$isEdit ? 'disabled' : ''}} class="form-control selectpicker" >
                                            <option disabled selected>Select</option>
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Code*</label>
                                        <input readonly value="{{$product->pro_code ??old('pro_code') ?? $code }}" autofocus class="form-control" type="text" placeholder="Enter Product Code" id="pro_code" name="pro_code" required>
                                        {{-- <span class="text-danger">{{$errors->first('pro_code') ? 'Product Code already exist' : null}}</span> --}}
                                    </div>
                                </div>
                                {{-- <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Type*</label>
                                        <select class="form-control selectpicker" name="p_type" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->p_type == 'Raw' ? 'selected' : null }}>Raw</option>
                                            <option {{$product->p_type == 'Material' ? 'selected' : null }}>Material</option>
                                            <option {{$product->p_type == 'Packaging' ? 'selected' : null }}>Packaging</option>
                                            <option {{$product->p_type == 'Finished' ? 'selected' : null }}>Finished</option>
                                            @else
                                            <option>Raw</option>
                                            <option>Material</option>
                                            <option>Packaging</option>
                                            <option>Finished</option>

                                            @endif
                                        </select>
                                    </div>
                                </div> --}}
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Unit',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Product Unit*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="unit_id" required id="unit_id">
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                                <option>other</option>
                                            @endif
                                            {{-- @php
                                                $unitName = \App\Unit::find($product->unit_id);
                                            @endphp --}}
                                            @if ($isEdit)
                                                @foreach ($unit as $u)
                                                    <option {{$product->unit_id == $u->id ? 'selected' : null}} value="{{$u->u_name}}">{{$u->u_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($unit as $u)
                                                    <option  value="{{$u->u_name}}">{{$u->u_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Unit',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#unitModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Product Weight <small>(optionall)</small></label>
                                        <input min="0" value="{{$product->weight ?? old('weight')}}" class="form-control" type="number" placeholder="Enter Product Weight" name="weight" id="weight">
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Cost*</label>
                                        <input  id="cost" value="{{$product->cost ?? old('cost')}}" class="form-control" type="text" placeholder="Enter Product Cost" name="cost" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Percentage*</label>
                                        <input  id="pr" value="{{$product->percentage ?? old('percentage')}}" class="form-control" type="text" placeholder="Enter Product Percentage" name="percentage" required>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Price*</label>
                                        <input id="price" value="{{$product->price ?? old('price')}}" class="form-control" type="text" placeholder="Enter Product Price" autocomplete="off" name="price" readonly>
                                    </div>
                                </div>

                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Variant',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Shades <small>(optional)</small> </label>
                                        <select class="form-control selectpicker" data-live-search="true" name="v_id[]" id="v_id" multiple >
                                            <option value="" disabled >Select...</option>
                                            @foreach ($variant as $u)
                                                @if ($isEdit)
                                                    @if (count($p_variant) > 0)
                                                        @php
                                                            $status = false;
                                                        @endphp
                                                        @foreach ($p_variant as $va)
                                                            @php
                                                                $name = explode('-',$va->name);
                                                            @endphp
                                                            @if ($name[2] == $u->name)
                                                                @php
                                                                    $status = true;
                                                                @endphp
                                                                <option selected value="{{$u->name}}">{{$u->name}}</option>
                                                            @break
                                                            @else
                                                                @php
                                                                    $status = false;
                                                                @endphp
                                                            @endif
                                                        @endforeach
                                                        @if ($status == true)
                                                        @else
                                                        <option value="{{$u->name}}">{{$u->name}}</option>
                                                        @endif
                                                    @else
                                                        <option  value="{{$u->name}}">{{$u->name}}</option>
                                                    @endif
                                                @else
                                                    <option  value="{{$u->name}}">{{$u->name}}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Variant',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#" id="Addvariants" data-toggle="modal" data-target="#Addvariant">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Description <small>(optional)</small></label>
                                        <input value="{{$product->description ?? old('description')}}" class="form-control" type="text" placeholder="Enter Product Description" name="description">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Alert Quantity*</label>
                                        <input min="1" value="{{$product->alert_quantity ?? old('alert_quantity')}}" class="form-control" type="number" placeholder="Enter Product Alert Quantity" name="alert_quantity" >
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>Quantity <small>(optional)</small></label>
                                        <input type="number" min="1" name="quantity" class="form-control" {{$isEdit ? 'readonly' : ''}} value="{{$product->quantity ?? old('quantity')}}" placeholder="Enter Product Quantity" >
                                    </div>
                                </div>
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Brand',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Brand*</label>
                                        <select style="overflow-y: scroll; height: 20px;"  class="form-control selectpicker" data-live-search="true" name="brand_id" id="brand_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($brands as $u)
                                                    <option {{$product->brand_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->b_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($brands as $u)
                                                    <option  value="{{$u->id}}">{{$u->b_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Brand',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top:23px;  width:50px;">
                                            <a href="#"  data-toggle="modal" data-target="#brandModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Category*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="cat_id" id="cat_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($cat as $u)
                                                    <option {{$product->cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($cat as $u)
                                                    <option  value="{{$u->id}}">{{$u->cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#"  data-toggle="modal" data-target="#catModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="{{$isEdit ? 'col-sm-6' : (in_array('Add Sub Category',$permissions) ? 'col-sm-5' : 'col-sm-6')}}">
                                    <div class="form-outline">
                                        <label >Sub Category*</label>
                                        <select class="form-control selectpicker" data-live-search="true" name="s_cat_id" id="s_cat_id" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                                <option>other</option>
                                            @endif
                                            @if ($isEdit)
                                                @foreach ($sub as $u)
                                                    <option {{$product->s_cat_id == $u->id ? 'selected' : null}} value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @else
                                                @foreach ($sub as $u)
                                                    <option  value="{{$u->id}}">{{$u->s_cat_name}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                @if ($isEdit ? '' : in_array('Add Sub Category',$permissions))
                                    <div class="col-sm-1">
                                        <div class="form-control" style="margin-top: 23px;  width:50px">
                                            <a href="#" id="subcat" data-toggle="modal" data-target="#subModal">
                                                <i class="fa fa-2x fa-plus-circle addIcon font-green" style="margin-top: 3px"></i>
                                            </a>
                                        </div>
                                    </div>
                                @endif
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Visible to POS*</label>
                                        <select class="form-control selectpicker" name="visibility" required>
                                            <option value="" disabled selected>Select...</option>
                                            @if ($isEdit)
                                            <option {{$product->visibility == '1' ? 'selected' : null }} value="1">Yes</option>
                                            <option {{$product->visibility == '0' ? 'selected' : null }} value="0">No</option>
                                            @else
                                            <option value="1">Yes</option>
                                            <option value="0">No</option>

                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label>MRP <small>(optional)</small> </label>
                                        <input value="{{$product->mrp ?? old('mrp')}}" type="text" placeholder="Enter MRP" class="form-control" name="mrp">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-outline">
                                        <label >Supplier Name<span >(optional)</span></label>
                                        @php
                                            $status1 = false;
                                        @endphp
                                        <select multiple class="form-control selectpicker" data-live-search="true" id="s_id" name="s_id[]" >
                                            <option disabled >Select...</option>
                                            @foreach ($supplier as $s)
                                                @if ($isEdit)

                                                    @if (count($ps) > 0)
                                                        @foreach ($ps as $p)
                                                            @if ($p->s_id == $s->id)
                                                                @php
                                                                    $status1 = true;
                                                                @endphp

                                                            @break
                                                            @else
                                                                @php
                                                                    $status1 = false;
                                                                @endphp

                                                            @endif
                                                        @endforeach
                                                        @if ($status1 == true)
                                                        <option selected value="{{$s->id}}">{{$s->name}}  </option>
                                                        @else
                                                        <option value="{{$s->id}}">{{$s->name}} </option>
                                                        @endif
                                                    @else
                                                        <option value="{{$s->id}}">{{$s->name}} </option>
                                                    @endif
                                                @else
                                                    <option value="{{$s->id}}">{{$s->name}} </option>
                                                @endif

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-outline">
                                        <button style="display: none" class="btn btn-info variant" type="button">Add Variant</button>
                                    </div>
                                </div>
                            </div>
                            <div class="variants" id="variants">
                                {{-- @if ($isEdit)
                                    @php
                                        $count = 1;
                                    @endphp
                                    @foreach ($p_variant as $va)
                                        @php
                                            $name = explode('-',$va->name);
                                            $status = false;
                                        @endphp
                                        <br>
                                        <div class="row addvariants" id="{{$count}}" >
                                            @foreach($variant->where('p_id',0) as $v)
                                                <div class="col-sm-3">
                                                    <div class="form-outline">
                                                        <label><b> {{$v->name}} </b>
                                                        </label>
                                                        <select name="variants[{{$count}}][]" class="form-control va selectpicker">
                                                            <option selected disabled> select</option>
                                                            @foreach($variant->where('p_id',$v->id) as $c)
                                                                @foreach ($name as $n)
                                                                    @if ($n == $c->name)
                                                                        @php
                                                                            $status = true;
                                                                        @endphp
                                                                    @break
                                                                    @else
                                                                        @php
                                                                            $status = false;
                                                                        @endphp
                                                                    @endif
                                                                @endforeach
                                                                @if ($status == true)
                                                                    <option selected>{{$c->name}}</option>
                                                                @else
                                                                    <option>{{$c->name}}</option>
                                                                @endif
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                            @endforeach
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> Cost </b>
                                                        </label>
                                                        <input type="number" class="form-control co" value="{{$va->cost}}" name="costv[{{$count}}]">
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-outline">
                                                        <label><b> Price </b>
                                                        </label>
                                                        <input type="number" class="form-control pr" value="{{$va->price}}" name="pricev[{{$count}}]">
                                                    </div>
                                                </div>
                                                <input type="hidden" name="v_id[{{$count}}]" class="vid" value="{{$va->id}}">
                                                <div class="col-sm-2"> <button type="button" style="margin-top:0px" id="{{$count}}" class="btn red deletev d{{$count}}" ><i class="fa fa-trash"></i></button></div>
                                        </div>
                                        @php
                                            $count++;
                                        @endphp
                                    @endforeach
                                @endif --}}
                            </div>


                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-0" >
                                    <button type="submit" class="btn green">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
        @include('modals.unit')
        @include('modals.category')
        @include('modals.subcategory')
        @include('modals.brand')
        @include('modals.addVariant')
        @include('modals.products')
@endsection


@section('custom-script')
@toastr_js
@toastr_render
    <script>
        $(document).ready(function(){ //Make script DOM ready
            $('#unit_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#unitModal').modal("show"); //Open Modal
            }
            if(opval == 'pieces')
            {
                $('#weight').attr('readonly',true);
                $('#weight').prop('required',false);
            }
            else
            {
                $('#weight').attr('readonly',false);
                $('#weight').prop('required',false);
            }
        });
        $('#cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#catname').addClass('selectpicker');
                $('.selectpicker').selectpicker('render');
                $('#catModal').modal("show"); //Open Modal
            }
        });
        $('#s_cat_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#subModal').modal("show"); //Open Modal
            }
        });
        $('#brand_id').change(function() { //jQuery Change Function
            var opval = $(this).val(); //Get value from select element
            console.log(opval);
            if(opval=="other"){ //Compare it and if true
                $('#brandModal').modal("show"); //Open Modal
            }
        });


        function readURL(input, number) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#imagePreview' + number).css('background-image', 'url(' + e.target.result + ')');
                    $('#imagePreview' + number).hide();
                    $('#imagePreview' + number).fadeIn(650);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
            $("#imageUpload1").change(function () {
                readURL(this, 1);
        });
    });

    $(document).on('click','#subcat',function(){
        $('#catname').addClass('selectpicker');
        $('#catname').selectpicker('render');
    })

    $(document).on('click','.variant',function(){
        var a = 0;
        var parentDiv = document.getElementById("variants");
        if (parentDiv.hasChildNodes())
        {
            var last = 0;
            $(".addvariants").each(function(){
                last = ($(this).attr("id"));
            });
            a = +last + +1;
        }
        else
        {
            a = 1;
        }
        $('.variants').append(`<br><div class="row addvariants" id="`+a+`" >
            @foreach($variant->where('p_id',0) as $v)
            <div class="col-sm-3">
                <div class="form-outline">
                    <label><b> {{$v->name}} </b>
                    </label>
                    <select name="variants[`+a+`][]" class="form-control va v`+a+`">
                        <option selected disabled> select</option>
                        @foreach($variant->where('p_id',$v->id) as $c)
                        <option>{{$c->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            @endforeach
            <div class="col-sm-2"><div class="form-outline">
                <label><b> Cost </b>
                </label>
                <input type="number" class="form-control co c`+a+`" name="costv[`+a+`]">
            </div></div>
            <div class="col-sm-2"><div class="form-outline">
                <label><b> Price </b>
                </label>
                <input type="number" class="form-control pr p`+a+`" name="pricev[`+a+`]">
            </div></div>
            <div class="col-sm-2"> <button type="button" style="margin-top:0px" id="`+a+`" class="btn red deletev d`+a+`" ><i class="fa fa-trash"></i></button></div>
            </div>

        `);
        $('.va').addClass('selectpicker');
        $('.va').selectpicker('render');
    });

    $(document).on('click','.deletev',function(){
        var id=$(this).attr('id');
        $(this).closest('.addvariants').remove();
        $('.addvariants').each(function(i){
            i=+i + +1;
            $(this).attr('id',i);
        });
        $(".co").each(function (i){
            i=+i + +1;
            $(this).attr('name','costv['+i+']');
        });
        $(".pr").each(function (i){
            i=+i + +1;
            $(this).attr('name','pricev['+i+']');
        });
        $(".vid").each(function (i){
            i=+i + +1;
            $(this).attr('name','v_id['+i+']');
        });
        $(".deletev").each(function (i){
            i=+i + +1;
            $(this).attr('id',i);
        });
        $('.addvariants').each(function(i){
            i=+i + +1;
            $('#'+i+' .va').attr('name','variants['+i+'][]');
        });
    });

    $(document).on('change','#barcode',function(){
        var val = $(this).val();
        var code =  Math.floor((Math.random() * 10000000) + 1);
        if(val == "Yes")
        {
            $('#pro_code').val('');
            $('#pro_code').focus();
            $('#pro_code').attr('readonly',false);
        }
        else{
            $('#pro_code').val(code);
            $('#pro_code').attr('readonly',true);
        }
    });

    $(document).on('keyup','#pr',function(){
        var percentage = $(this).val();
        var cost = $('#cost').val();
        console.log(percentage);
        var price = 0;
        price = +(cost * (percentage/100)) + +cost;
        $('#price').val(price);
    });








    </script>

@endsection

