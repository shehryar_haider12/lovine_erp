@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a>Reports</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-bar-chart font-white"></i>Customer Detail Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            {{-- <form action="{{url('')}}/reports/incomeStatementsearch" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" name="menuid" value="{{$menu_id}}">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                      <div class="form-check-inline">
                                                          <label class="form-check-label" for="radio2">
                                                              <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                            </label>
                                                        </div>

                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="">Year</label>
                                                                <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                            </div>
                                                        </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button disabled id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form> --}}

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Date</th>
                                                            <th>Ref. No</th>
                                                            <th>Biller</th>
                                                            <th>Customer</th>
                                                            <th>Product(Qty)</th>
                                                            <th>Total</th>
                                                            <th>Paid</th>
                                                            <th>Balance</th>
                                                            <th>Sale Status</th>
                                                            <th>Payment Status</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                            $total = 0;
                                                            $t=0;
                                                            $p=0;
                                                            $b=0;
                                                        @endphp
                                                        @foreach ($sales as $c)
                                                            <tr>
                                                                <td>
                                                                    {{$a}}
                                                                </td>
                                                                <td>
                                                                    {{$c->sale_date}}
                                                                </td>
                                                                <td>
                                                                    {{$c->ref_no}}
                                                                </td>
                                                                <td>
                                                                    {{$c->biller->name}}
                                                                </td>
                                                                <td>
                                                                    {{$c->customer->name}}
                                                                </td>
                                                                <td>
                                                                    @foreach ($c->sdetails as $d)
                                                                        {{$d->type == 0 ? $d->products->pro_name.'('.$d->quantity.')' : $d->variant->name.'('.$d->quantity.')'}}
                                                                    @endforeach
                                                                </td>
                                                                <td>
                                                                    {{$c->total}}
                                                                </td>
                                                                <td>
                                                                    {{$c->total_amount == null ? 0 : $c->total_amount }}
                                                                </td>
                                                                <td>
                                                                    {{$c->total - $c->total_amount}}
                                                                </td>
                                                                @if($c->s_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Complete')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Return')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->s_status=='Delivered')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Partial')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  blue" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Paid')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  green" id="{{$c->id}}">
                                                                            {{$c->p_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($c->p_status=='Return')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-info" id="{{$c->id}}">
                                                                            {{$c->s_status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                            </tr>
                                                            @php
                                                                $a++;
                                                                $total = 0;
                                                                $t += $c->total;
                                                                $p += $c->total_amount == null ? 0 : $c->total_amount;
                                                                $b += ($c->total - $c->total_amount);
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                    <tfoot>
                                                        <tr>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th>TOTAL</th>
                                                            <th></th>
                                                            <th>{{$t}}</th>
                                                            <th>{{$p}}</th>
                                                            <th>{{$b}}</th>
                                                            <th></th>
                                                            <th></th>
                                                        </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endsection
@endsection
