@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
    display: inline-flex;
    align-items: center;
    padding-left: 0;
    margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
    position: static;
    margin-top: 0;
    margin-right: .3125rem;
    margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a>Reports</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-bar-chart font-white"></i>Products Profitability Report
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            <div class="table-responsive">
                                                <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Product Code - Name</th>
                                                            <th>Purchased</th>
                                                            <th>Sold</th>
                                                            <th>Profit/Loss</th>
                                                            <th>Stock (Qty) Amount</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $a = 1;
                                                        @endphp
                                                        @foreach ($product as $p)
                                                            @if ($p->vstatus == 0)
                                                                <tr>
                                                                    <td>
                                                                        {{$a}}
                                                                    </td>
                                                                    <td>
                                                                        {{$p->pro_code}} - {{$p->pro_name}}
                                                                    </td>
                                                                    <td>
                                                                        ({{$p->odetails_count == null ? 0 : $p->odetails_count}}) {{$p->Tcost == null ? 0 : $p->Tcost}}
                                                                    </td>
                                                                    <td>
                                                                        ({{$p->sdetails_count == null ? 0 : $p->sdetails_count}}) {{$p->Tprice == null ? 0 : $p->Tprice}}
                                                                    </td>
                                                                    <td>
                                                                        @php
                                                                            $cost = $p->Tcost == null ? 0 : $p->Tcost;
                                                                            $price = $p->Tprice == null ? 0 : $p->Tprice;
                                                                        @endphp
                                                                        {{$price - $cost}}
                                                                    </td>
                                                                    <td>
                                                                        @php
                                                                            $remainder = 0;
                                                                            $odetails_count = $p->odetails_count == null ? 0 : $p->odetails_count;
                                                                            $sdetails_count = $p->sdetails_count == null ? 0 : $p->sdetails_count;
                                                                            if($odetails_count > 0 )
                                                                            {
                                                                                $remainder = $p->Tcost / $p->odetails_count;
                                                                                $remainder = $remainder * ($odetails_count - $sdetails_count);
                                                                            }
                                                                            else {
                                                                                $remainder = 0;
                                                                            }
                                                                        @endphp
                                                                        ({{$odetails_count - $sdetails_count}}) {{$remainder}}
                                                                    </td>
                                                                </tr>
                                                            @else
                                                               @foreach ($p->variants as $v)
                                                                    <tr>
                                                                        <td>
                                                                            {{$a}}
                                                                        </td>
                                                                        <td>
                                                                            {{$v->name}}
                                                                        </td>
                                                                        <td>
                                                                            ({{$v->Vrqty == null ? 0 : $v->Vrqty}}) {{$v->Vcost == null ? 0 : $v->Vcost}}
                                                                        </td>
                                                                        <td>
                                                                            ({{$v->Vdqty == null ? 0 : $v->Vdqty}}) {{$v->Vprice == null ? 0 : $v->Vprice}}
                                                                        </td>
                                                                        <td>
                                                                            @php
                                                                                $Vcost = $v->Vcost == null ? 0 : $v->Vcost;
                                                                                $Vprice = $v->Vprice == null ? 0 : $v->Vprice;
                                                                            @endphp
                                                                            {{$Vprice - $Vcost}}
                                                                        </td>
                                                                        <td>
                                                                            @php
                                                                                $remainder = 0;
                                                                                $Vrqty = $v->Vrqty == null ? 0 : $v->Vrqty;
                                                                                $Vdqty = $v->Vdqty == null ? 0 : $v->Vdqty;
                                                                                if($Vrqty > 0 )
                                                                                {
                                                                                    $remainder = $v->Vcost / $v->Vrqty;
                                                                                    $remainder = $remainder * ($Vrqty - $Vdqty);
                                                                                }
                                                                                else {
                                                                                    $remainder = 0;
                                                                                }

                                                                            @endphp
                                                                            ({{$Vrqty - $Vdqty}}) {{$remainder}}
                                                                        </td>
                                                                    </tr>
                                                                    @php
                                                                        $a++;
                                                                    @endphp
                                                               @endforeach
                                                            @endif
                                                            @php
                                                                $a++;
                                                            @endphp
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#example').DataTable();
        });
    </script>
@endsection
@endsection
