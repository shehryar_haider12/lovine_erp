@extends('layouts.master')
@section('top-styles')
@toastr_css
<script src="{{url('')}}/style-lik/ckeditor/ckeditor.js"></script>
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
    .form-check-inline {
        display: inline-flex;
        align-items: center;
        padding-left: 0;
        margin-right: .75rem;
    }
    .form-check-inline .form-check-input {
        position: static;
        margin-top: 0;
        margin-right: .3125rem;
        margin-left: 0;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
@if(Session::has('download'))
    <meta http-equiv="refresh" content="5;url={{ Session::get('download') }}">
@endif
<li>
    <a href="{{url('')}}/sales/return/view">Sale Returns</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4  col-sm-4 col-xs-5">
                                                <i class="fa fa-credit-card font-white"></i>View Sale Returns
                                            </div>
                                            <div class="col-md-5 col-sm-3"></div>
                                            <div class="col-md-3 col-sm-5 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    @if ($index == 0)
                                                        <a style="margin-left:-20px; "  href="{{route('sales.excel')}}">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 1)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$year}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/year">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 2)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$month}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/month">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 3)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$date}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 4)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$from}}/{{$to}}/{{$c}}/{{$sp}}/{{$ss}}/{{$ps}}/{{$check}}/excel/date">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 5)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$c}}/{{$sp}}/{{$ss}}/{{$check}}/excel/ss">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 6)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$c}}/{{$sp}}/{{$ps}}/{{$check}}/excel/ps">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 7)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$c}}/excel/c">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                    @if ($index == 8)
                                                        <a style="margin-left:-20px; "  href="/sales/search/{{$sp}}/excel/sp">
                                                            <i class="fa fa-file-excel-o  font-white"></i>
                                                        </a>
                                                    @endif
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('sales.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Sales</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">

                                            {{-- <form action="{{url('')}}/sales/search" method="POST" id="advanceSearch">
                                                @csrf
                                                <input type="hidden" value="{{$menu_id}}" id="menuid" name="menuid">
                                                <div class="tableview">
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio1">
                                                          <input type="radio" class="form-check-input" id="radio1" name="optradio"  value="Year">By Year
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label" for="radio2">
                                                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="Month">By Month
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="Date">By Sale Date
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="Status">By Sale Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="PStatus">By Payment Status
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="customer">By Customer
                                                        </label>
                                                    </div>
                                                    <div class="form-check-inline">
                                                        <label class="form-check-label">
                                                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="saleperson">By Sale Person
                                                        </label>
                                                    </div>
                                                <div class="row">

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Year</label>
                                                            <input type="text" id="year" disabled name="year" placeholder="Enter Year" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="">Month</label>
                                                        <input type="month" disabled name="month" id="month" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">From</label>
                                                            <input type="date" disabled name="from" id="from" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">To</label>
                                                            <input type="date" disabled name="to" id="to" class="form-control" placeholder="Month Name" style=" z-index: 2;" autocomplete="off">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Customer Name</label>
                                                            <select class="form-control" data-live-search="true" name="c_id" id="c_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($customer as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sale Persons Name</label>
                                                            <select class="form-control" data-live-search="true" name="sp_id" id="sp_id" disabled >
                                                                <option value=""  selected>Select...</option>
                                                                @foreach ($saleperson as $u)
                                                                <option  value="{{$u->id}}">{{$u->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Sales Status</label>
                                                            <select name="status" id="status" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Approved</option>
                                                                <option>Partial</option>
                                                                <option>Complete</option>
                                                                <option>Delivered</option>
                                                                <option>Return</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Payment Status</label>
                                                            <select name="p_status" id="p_status" class="form-control " disabled>
                                                                <option disabled selected>No Filter</option>
                                                                <option>Pending</option>
                                                                <option>Partial</option>
                                                                <option>Paid</option>
                                                                <option>Return</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                        <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="" style="visibility: hidden">.</label>
                                                            <button id="search" disabled style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                            <i class="fa fa-search pr-1"></i> Search</button>
                                                        </div>
                                                        </div>
                                                </div>
                                                </div>
                                            </form> --}}
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th>S.No</th>
                                                            <th>Return Date</th>
                                                            <th>Warehouse</th>
                                                            <th>Customer</th>
                                                            <th>Total</th>
                                                            <th>Status</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($return as $s)
                                                        {{-- @if ($counter == null) --}}
                                                            <tr>
                                                                <td>
                                                                    {{$s->id}}
                                                                </td>
                                                                <td>
                                                                    {{$s->return_date}}
                                                                </td>
                                                                <td>
                                                                    {{$s->warehouse->w_name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->customer->name}}
                                                                </td>
                                                                <td>
                                                                    {{$s->total}}
                                                                </td>
                                                                @if($s->status=='Pending')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                            {{$s->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                @if($s->status=='Approved')
                                                                    <td>
                                                                        <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                            {{$s->status}}
                                                                        </button>
                                                                    </td>
                                                                @endif
                                                                <td>
                                                                    @if ($s->status == 'Approved')
                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Sale Return Details</option>
                                                                            @endif
                                                                            @if(in_array('voucher',$permissions))
                                                                                <option >Generate Voucher</option>
                                                                            @endif
                                                                        </select>
                                                                    @else
                                                                        <select class="form-control action" id="{{$s->id}}" >
                                                                            <option >Actions</option>
                                                                            @if(in_array('status',$permissions))
                                                                                <option >Approved</option>
                                                                            @endif
                                                                            @if(in_array('show',$permissions))
                                                                                <option >Sale Return Details</option>
                                                                            @endif
                                                                        </select>
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                        {{-- @else
                                                            @php
                                                                $uid = Auth::user()->id;
                                                                $sale =  \App\SaleCounter::where('u_id',$uid)->where('co_id',$counter)->get();
                                                            @endphp
                                                            @foreach ($sale as $ss)
                                                                @if ($ss->ret_id == $s->id)
                                                                    <tr>
                                                                        <td>
                                                                            {{$s->id}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->return_date}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->warehouse->w_name}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->customer->name}}
                                                                        </td>
                                                                        <td>
                                                                            {{$s->total}}
                                                                        </td>
                                                                        @if($s->status=='Pending')
                                                                            <td>
                                                                                <button type="button" class="btn btn-xs  red" id="{{$s->id}}">
                                                                                    {{$s->status}}
                                                                                </button>
                                                                            </td>
                                                                        @endif
                                                                        @if($s->status=='Approved')
                                                                            <td>
                                                                                <button type="button" class="btn btn-xs  btn-primary" id="{{$s->id}}">
                                                                                    {{$s->status}}
                                                                                </button>
                                                                            </td>
                                                                        @endif
                                                                        <td>
                                                                            @if ($s->status == 'Approved')
                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Sale Return Details</option>
                                                                                    @endif
                                                                                    @if(in_array('voucher',$permissions))
                                                                                        <option >Generate Voucher</option>
                                                                                    @endif
                                                                                </select>
                                                                            @else
                                                                                <select class="form-control action" id="{{$s->id}}" >
                                                                                    <option >Actions</option>
                                                                                    @if(in_array('status',$permissions))
                                                                                        <option >Approved</option>
                                                                                    @endif
                                                                                    @if(in_array('show',$permissions))
                                                                                        <option >Sale Return Details</option>
                                                                                    @endif
                                                                                </select>
                                                                            @endif
                                                                        </td>
                                                                    </tr>
                                                                @endif
                                                            @endforeach
                                                        @endif --}}
                                                        @endforeach
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
        <!-- End: life time stats -->
    </div>
    @section('modal')


        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">×</button>
                        <h4 class="modal-title">Sale Return Details</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Return Date</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Name" id="return_date" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Warehouse</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Weight" id="w_name" readonly>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-outline">
                                    <label >Customer</label>
                                    <input class="form-control" type="text" placeholder="Enter Product Unit" id="c_name" readonly>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <table id="example1" class="table table-striped table-bordered" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>S.No</th>
                                            <th>Code - Name</th>
                                            <th>Brand</th>
                                            <th>Quantity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>


                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    @endsection
    @section('custom-script')
    <script>
        CKEDITOR.replace( 'editor2' );
   </script>
    @toastr_js
    @toastr_render
<script type="text/javascript">

    function view(id)
    {
        $("#example1 tbody").empty();
        $.ajax({
            url:"{{url('')}}/sales/return/show/"+id,
            method:"GET",
            error: function (request, error) {
                alert(" Can't do because: " + error +request);
            },
            success:function(data){
                console.log(data);
                $('#return_date').val(data[0].return_date);
                $('#w_name').val(data[0].warehouse.w_name);
                $('#c_name').val(data[0].customer.name);
                for (let i = 0; i < data[1].length; i++) {
                    if(data[1][i].type == 1)
                    {
                        $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].saledetail.variant.name+"</td><td>"+data[1][i].saledetail.variant.product.brands.b_name+"</td> <td>"+data[1][i].quantity+"</td></tr>");
                    }
                    else
                    {
                        $("#example1").append("<tr><td>"+data[1][i].id+"</td><td>"+data[1][i].saledetail.products.pro_code+" - "+data[1][i].saledetail.products.pro_name+"</td><td>"+data[1][i].saledetail.products.brands.b_name+"</td>  <td>"+data[1][i].quantity+"</td></tr>");
                    }
                }
                $('#myModal').modal("show");
                $('#example1').DataTable();

            }
        });
    }
    $(document).ready(function () {
        var table = $('#example').DataTable({
            scrollX: true,
            order: [[ 0, "desc" ]]
        });
        $('input:radio[name="optradio"]').change(function(){
            if ($(this).is(':checked')) {
                $('#search').prop('disabled',false);
                var val = $(this).val();
                if(val == 'Year')
                {
                    $('#year').prop('disabled',false);
                    $('#year').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                }
                if(val == 'Month')
                {
                    $('#month').prop('disabled',false);
                    $('#month').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                }
                if(val == 'Date')
                {
                    $('#from').prop('disabled',false);
                    $('#from').attr('required',true);
                    $('#to').prop('disabled',false);
                    $('#to').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#month').attr('required',false);
                    $('#month').prop('disabled',true);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#status').attr('required',false);
                    $('#status').attr('disabled',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').prop('required',false);
                }
                if(val == 'Status')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('required',true);
                    $('#status').attr('disabled',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').prop('required',false);
                }

                if(val == 'PStatus')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',false);
                    $('#p_status').attr('required',true);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',false);
                }
                if(val == 'customer')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').attr('required',false);
                    $('#c_id').prop('disabled',false);
                    $('#c_id').prop('required',true);
                    $('#sp_id').prop('disabled',true);
                    $('#sp_id').prop('required',false);
                }
                if(val == 'saleperson')
                {
                    $('#month').prop('disabled',true);
                    $('#month').attr('required',false);
                    $('#year').prop('disabled',true);
                    $('#year').attr('required',false);
                    $('#from').prop('disabled',true);
                    $('#to').prop('disabled',true);
                    $('#from').attr('required',false);
                    $('#to').attr('required',false);
                    $('#status').attr('disabled',true);
                    $('#status').attr('required',false);
                    $('#p_status').prop('disabled',true);
                    $('#p_status').attr('required',false);
                    $('#c_id').prop('disabled',true);
                    $('#c_id').prop('required',false);
                    $('#sp_id').prop('disabled',false);
                    $('#sp_id').prop('required',true);
                }
            }
        });
        $(document).on('change','.action',function(){
            var val=$(this).val();
            if( val == 'Approved')
            {
                var id = $(this).attr('id');
                var status = val;
                axios
                .post('{{route("sales.returnstatus")}}', {
                    _token: '{{csrf_token()}}',
                    _method: 'post',
                    id: id,
                    status: status,
                    })
                    .then(function (responsive) {
                    console.log('responsive');
                    location.reload();
                    })
                    .catch(function (error) {
                    console.log(error);
                });
            }
            if(val == 'Sale Return Details')//
            {

                var id=$(this).attr("id");
                view(id);
                $('.action').val('Actions');
            }
            if( val == 'Generate Voucher')
            {
                var id=$(this).attr("id");
                $.ajax({
                    url:"{{url('')}}/sales/return/voucher/"+id,
                    method:"GET",
                    data:
                    {
                        id:id,
                    },
                    error: function (request, error) {
                        alert(" Can't do because: " + error +request);
                    },
                    success:function(data){
                        console.log(data);
                        window.location.href= '{{url('')}}/sales/return/voucher/'+id;
                        // location.reload();
                        $('.action').val('Actions');
                    }
                });
            }
        });
    });
    $('#example').on('click','td', function () {
        var col_index = $(this).index();
        var row_index = $(this).parent().index();
        var count = document.getElementById('example').rows[row_index].cells.length - 1;
        if(col_index <count)
        {
            var $tr = $(this).closest('tr');
            var id = $tr.find("td:eq(0)").text();
            view(id);
        }
    });

</script>

    @endsection
@endsection
