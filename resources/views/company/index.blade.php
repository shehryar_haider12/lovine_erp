@extends('layouts.master')
@section('top-styles')
<style>
    .tableview
    {
        background-color:#F0F0F0;
        padding: 20px;
        border-radius: 5px;
        border:1px solid  #e7ecf1;
    }
</style>
<style>
    #GFG {
        text-decoration: none;
    }
    thead {
	    background-color: #ADD8E6;
    }
    .modal-header{
        background: #32c5d2 !important;
    }
</style>
@toastr_css
<!-- DataTables -->
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="{{url('')}}/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

@section('sidebar-name1')
<li>
    <a href="{{url('')}}/company">Company</a>
</li>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- Begin: life time stats -->
            <div class="portlet light portlet-fit portlet-datatable bordered">
                <div class="portlet-title">

                </div>
                <div class="portlet-body">
                    <div class="tabbable-line">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet grey-cascade box">
                                    <div class="portlet-title" style="background: #32c5d2;">
                                        <div class="caption col-md-12 col-sm-12 col-xs-12 row">
                                            <div class="col-md-4 col-sm-4 col-xs-5">
                                                <i class="fa fa-industry font-white"></i>View Company
                                            </div>
                                            <div class="col-md-5 col-sm-5"></div>
                                            <div class="col-md-3 col-sm-3 col-xs-7">
                                                @if(in_array('excel',$permissions))
                                                    <a style="margin-left:-20px"  href="{{route('company.excel')}}">
                                                        <i class="fa fa-file-excel-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('pdf',$permissions))
                                                    <a style="margin-left:-50px;" href="{{route('company.pdf')}}">
                                                        <i class="fa fa-file-pdf-o  font-white"></i>
                                                    </a>
                                                @endif
                                                @if(in_array('Add',$permissions))
                                                    <a id="GFG" href="{{route('company.create')}}" >
                                                        <button style="background: #00CCFF; margin-left:20px; margin-top:-20px" type="button"  class="btn btn-block btn-primary btn-md ">Add Company</button>
                                                    </a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="custom_datatable">
                                            <form action="#" id="advanceSearch">
                                                <div class="tableview">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                    <div class="form-group">
                                                        <label for="">Company Name</label>
                                                        <input type="text" name="name" id="autocomplete-ajax1" class="form-control" placeholder="Company Name" style=" z-index: 2;" autocomplete="off">
                                                    </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                    <div class="form-group">
                                                        {{-- <label for="">Unit Name</label> --}}
                                                        <label for="" style="visibility: hidden">.</label>
                                                        <button id="search" style="background: #32c5d2" class="btn btn-light-theme btn-block waves-effect waves-light">
                                                        <i class="fa fa-search pr-1"></i> Search</button>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                            </form>
                                            <div class="table-responsive">
                                            <table id="example" class="table table-striped table-bordered" style="width:100%">
                                                    <thead>
                                                        <tr>
                                                            <th width="7%">S.No</th>
                                                            <th>Name</th>
                                                            <th>Address</th>
                                                            <th width="15%">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>

                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>
            <!-- End: life time stats -->
    </div>
@section('modal')


    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h4 class="modal-title">Company</h4>
                </div>
                <div class="modal-body">

                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Name</label>
                                <input class="form-control" type="text" placeholder="Enter Company Name" id="name" readonly>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Company Address</label>
                                <input readonly class="form-control" type="text" placeholder="Enter Company Address" id="address" >
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Name</label>
                                <input readonly class="form-control" type="text" placeholder="Enter Contact Person Name" id="c_p_name" >
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-outline">
                                <label >Contact Person Number</label>
                                <input readonly class="form-control" type="number" placeholder="Enter Contact Person Number" id="c_p_contactNo" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
        <!--End Modal-->
@endsection
@section('custom-script')
@toastr_js
@toastr_render
    {{-- view one product --}}
    <script>
        $(document).on('click','.view',function(){
            var id=$(this).attr("id");
            $.ajax({
                url:"{{url('')}}/company/"+id,
                method:"GET",
                error: function (request, error) {
                            alert(" Can't do because: " + error +request);
                        },
                success:function(data){
                    console.log(data);
                    $('#name').val(data.name);
                    $('#address').val(data.address);
                    $('#c_p_name').val(data.c_p_name);
                    $('#c_p_contactNo').val(data.c_p_contactNo);
                }
            });
        });
    </script>
    <script>

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var table = $('#example').DataTable({
                processing: true,
                order: [[ 0, "desc" ]],
                serverSide: true,
                ajax: '{{route("company.datatable")}}',
                "columns": [{
                        "data": "id",
                        "defaultContent": ""
                    },
                    {
                        "data": "name",
                        "defaultContent": ""
                    },
                    {
                        "data": "address",
                        "defaultContent": ""
                    },
                    {
                        "data": "id",
                        "defaultContent": ""
                    },
                ],
                "columnDefs": [{
                        "targets": 'no-sort',
                        "orderable": false,
                    },
                    {
                        "targets": 0,
                        "render": function (data, type, row, meta) {
                            return meta.row + 1;
                        },
                    },
                    {
                        "targets": -1,
                        "render": function (data, type, row, meta) {
                            var edit = '{{route("company.edit",[":id"])}}';
                            edit = edit.replace(':id', row.id);
                            var checked = row.status == 1 ? 'checked' : null;
                            return `
                            @if(in_array('edit',$permissions))
                                <a href="` + edit + `" class="text-info p-1" id="GFG">
                                    <button type="button" class="btn blue edit" >
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </a>
                            @endif
                            @if(in_array('show',$permissions))
                                <button type="button" data-target="#myModal" data-toggle="modal"  class="btn green view" id="`+row.id +`">
                                    <i class="icon-eye"></i>
                                </button>
                            @endif
                            `;
                        },
                    },
                ],
            });
            $('#advanceSearch').submit(function(e){
                e.preventDefault();
                table.columns(1).search($('input[name="name"]').val());
                table.draw();
            });
        });

    </script>

@endsection
@endsection
