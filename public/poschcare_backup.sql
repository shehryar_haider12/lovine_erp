-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2021 at 08:04 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `poschcare_backup`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_details`
--

CREATE TABLE `account_details` (
  `Code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_of_account` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account_details`
--

INSERT INTO `account_details` (`Code`, `name_of_account`, `c_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `type`) VALUES
('CA-01-001', 'Dolmen Mall Tariq Road - Naiyar', 5, '2021-06-14 07:31:13', '2021-06-14 07:31:13', 2, NULL, NULL),
('CA-01-002', 'domen real estate management pvt ltd - nayyar', 5, '2021-06-18 01:32:08', '2021-06-18 01:32:08', 2, NULL, NULL),
('CA-01-003', 'Medical store - touqeer', 5, '2021-06-18 01:32:18', '2021-06-18 01:32:18', 2, NULL, NULL),
('CA-01-004', 'Priya Assrani - Priya Assrani', 5, '2021-06-28 07:52:42', '2021-06-28 07:52:42', 2, NULL, NULL),
('CA-02-001', '1357433 - 3 Ply Surgical Mask With Nose Pin', 6, '2021-07-07 08:07:24', '2021-07-07 08:07:24', 2, NULL, 0),
('CA-02-0010', '1357441 - Latex Gloves-Medium', 6, '2021-07-07 08:10:01', '2021-07-07 08:10:01', 2, NULL, 1),
('CA-02-00100', '1357491 - Hand Wash Label-500ml-Lavander', 6, '2021-07-07 08:42:14', '2021-07-07 08:42:14', 2, NULL, 1),
('CA-02-00101', '1357491 - Hand Wash Label-500ml-Lemon', 6, '2021-07-07 08:42:27', '2021-07-07 08:42:27', 2, NULL, 1),
('CA-02-00102', '1357491 - Hand Wash Label-500ml-Red Rose', 6, '2021-07-07 08:42:39', '2021-07-07 08:42:39', 2, NULL, 1),
('CA-02-00103', '1357492 - Pet Bottles-500ml', 6, '2021-07-07 08:45:21', '2021-07-07 08:45:21', 2, NULL, 1),
('CA-02-00104', '1357493 - Mask Box A qlty', 6, '2021-07-07 08:45:32', '2021-07-07 08:45:32', 2, NULL, 0),
('CA-02-00105', '1357494 - Mask Box B qlty', 6, '2021-07-07 08:45:41', '2021-07-07 08:45:41', 2, NULL, 0),
('CA-02-00106', '1357495 - Ultimate Butter box', 6, '2021-07-07 08:45:52', '2021-07-07 08:45:52', 2, NULL, 0),
('CA-02-00107', '1357496 - Shea Butter Box', 6, '2021-07-07 08:46:31', '2021-07-07 08:46:31', 2, NULL, 0),
('CA-02-00108', '1357497 - Bottles Pumps ', 6, '2021-07-07 08:46:48', '2021-07-07 08:46:48', 2, NULL, 0),
('CA-02-00109', '1357498 - Gift Wooden Box-8x11x4.5', 6, '2021-07-07 08:47:20', '2021-07-07 08:47:20', 2, NULL, 1),
('CA-02-0011', '1357442 - P.P.E suite ', 6, '2021-07-07 08:10:09', '2021-07-07 08:10:09', 2, NULL, 0),
('CA-02-00110', '1357498 - Gift Wooden Box-8x8x4.5', 6, '2021-07-07 08:47:34', '2021-07-07 08:47:34', 2, NULL, 1),
('CA-02-00111', '1357499 - Petroleum Jelly Label-100gm', 6, '2021-07-07 08:47:46', '2021-07-07 08:47:46', 2, NULL, 1),
('CA-02-00112', '1357499 - Petroleum Jelly Label-50gm', 6, '2021-07-07 08:47:58', '2021-07-07 08:47:58', 2, NULL, 1),
('CA-02-00113', '1357500 - Empty jar-Large', 6, '2021-07-07 08:48:09', '2021-07-07 08:48:09', 2, NULL, 1),
('CA-02-00114', '1357500 - Empty jar-Medium', 6, '2021-07-07 08:48:20', '2021-07-07 08:48:20', 2, NULL, 1),
('CA-02-00115', '1357500 - Empty jar-Small', 6, '2021-07-07 08:48:40', '2021-07-07 08:48:40', 2, NULL, 1),
('CA-02-0012', '1357443 - Gown Suite Blue ', 6, '2021-07-07 08:10:18', '2021-07-07 08:10:18', 2, NULL, 0),
('CA-02-0013', '1357444 - Plastic Pump-20mm-Transparent', 6, '2021-07-07 08:10:26', '2021-07-07 08:10:26', 2, NULL, 1),
('CA-02-0014', '1357444 - Plastic Pump-28mm-Transparent', 6, '2021-07-07 08:10:33', '2021-07-07 08:10:33', 2, NULL, 1),
('CA-02-0015', '1357444 - Plastic Pump-28mm-White', 6, '2021-07-07 08:10:40', '2021-07-07 08:10:40', 2, NULL, 1),
('CA-02-0016', '1357445 - Infrared Thermometer Machine', 6, '2021-07-07 08:10:48', '2021-07-07 08:10:48', 2, NULL, 0),
('CA-02-0017', '1357446 - Plastic Sanitizer Bottle-1000ml', 6, '2021-07-07 08:11:03', '2021-07-07 08:11:03', 2, NULL, 1),
('CA-02-0018', '1357446 - Plastic Sanitizer Bottle-100ml', 6, '2021-07-07 08:11:12', '2021-07-07 08:11:12', 2, NULL, 1),
('CA-02-0019', '1357446 - Plastic Sanitizer Bottle-150ml', 6, '2021-07-07 08:11:21', '2021-07-07 08:11:21', 2, NULL, 1),
('CA-02-002', '1357434 - Face Shield-Large', 6, '2021-07-07 08:07:33', '2021-07-07 08:07:33', 2, NULL, 1),
('CA-02-0020', '1357446 - Plastic Sanitizer Bottle-250ml', 6, '2021-07-07 08:11:32', '2021-07-07 08:11:32', 2, NULL, 1),
('CA-02-0021', '1357446 - Plastic Sanitizer Bottle-500ml', 6, '2021-07-07 08:12:41', '2021-07-07 08:12:41', 2, NULL, 1),
('CA-02-0022', '1357446 - Plastic Sanitizer Bottle-60ml', 6, '2021-07-07 08:13:04', '2021-07-07 08:13:04', 2, NULL, 1),
('CA-02-0023', '1357446 - Plastic Sanitizer Bottle-70ml', 6, '2021-07-07 08:13:15', '2021-07-07 08:13:15', 2, NULL, 1),
('CA-02-0024', '1357447 - Hand wash Bottle-250ml', 6, '2021-07-07 08:13:29', '2021-07-07 08:13:29', 2, NULL, 1),
('CA-02-0025', '1357448 - sanitizer Label-100ml', 6, '2021-07-07 08:13:50', '2021-07-07 08:13:50', 2, NULL, 1),
('CA-02-0026', '1357448 - sanitizer Label-150ml', 6, '2021-07-07 08:14:16', '2021-07-07 08:14:16', 2, NULL, 1),
('CA-02-0027', '1357448 - sanitizer Label-500ml', 6, '2021-07-07 08:14:27', '2021-07-07 08:14:27', 2, NULL, 1),
('CA-02-0028', '1357448 - sanitizer Label-60ml', 6, '2021-07-07 08:14:36', '2021-07-07 08:14:36', 2, NULL, 1),
('CA-02-0029', '1357448 - sanitizer Label-70ml', 6, '2021-07-07 08:14:47', '2021-07-07 08:14:47', 2, NULL, 1),
('CA-02-003', '1357434 - Face Shield-Small', 6, '2021-07-07 08:07:48', '2021-07-07 08:07:48', 2, NULL, 1),
('CA-02-0030', '1357449 - sanitizer spray Bottle-100ml', 6, '2021-07-07 08:14:57', '2021-07-07 08:14:57', 2, NULL, 1),
('CA-02-0031', '1357450 - Plastic Disinfectant Bottle-500ml', 6, '2021-07-07 08:15:09', '2021-07-07 08:15:09', 2, NULL, 1),
('CA-02-0032', '1357451 - Plastic Bottle Cap-1000ml-Blue', 6, '2021-07-07 08:15:27', '2021-07-07 08:15:27', 2, NULL, 1),
('CA-02-0033', '1357451 - Plastic Bottle Cap-1000ml-Transparent', 6, '2021-07-07 08:15:42', '2021-07-07 08:15:42', 2, NULL, 1),
('CA-02-0034', '1357451 - Plastic Bottle Cap-100ml-Transparent', 6, '2021-07-07 08:15:54', '2021-07-07 08:15:54', 2, NULL, 1),
('CA-02-0035', '1357451 - Plastic Bottle Cap-250ml-Blue', 6, '2021-07-07 08:16:12', '2021-07-07 08:16:12', 2, NULL, 1),
('CA-02-0036', '1357451 - Plastic Bottle Cap-250ml-Transaparent', 6, '2021-07-07 08:20:44', '2021-07-07 08:20:44', 2, NULL, 1),
('CA-02-0037', '1357451 - Plastic Bottle Cap-500ml-Blue', 6, '2021-07-07 08:20:56', '2021-07-07 08:20:56', 2, NULL, 1),
('CA-02-0038', '1357451 - Plastic Bottle Cap-500ml-Transparent', 6, '2021-07-07 08:21:09', '2021-07-07 08:21:09', 2, NULL, 1),
('CA-02-0039', '1357451 - Plastic Bottle Cap-60ml-Transparent', 6, '2021-07-07 08:21:20', '2021-07-07 08:21:20', 2, NULL, 1),
('CA-02-004', '1357435 - Face Shield Kids', 6, '2021-07-07 08:08:22', '2021-07-07 08:08:22', 2, NULL, 0),
('CA-02-0040', '1357451 - Plastic Bottle Cap-70ml-Red', 6, '2021-07-07 08:21:29', '2021-07-07 08:21:29', 2, NULL, 1),
('CA-02-0041', '1357452 - Plastic Amber Luxury Bottle-100ml', 6, '2021-07-07 08:21:40', '2021-07-07 08:21:40', 2, NULL, 1),
('CA-02-0042', '1357453 - Plastic Jar-300gm', 6, '2021-07-07 08:21:53', '2021-07-07 08:21:53', 2, NULL, 1),
('CA-02-0043', '1357453 - Plastic Jar-100gm', 6, '2021-07-07 08:22:02', '2021-07-07 08:22:02', 2, NULL, 1),
('CA-02-0044', '1357453 - Plastic Jar-300ml-Transparent', 6, '2021-07-07 08:22:11', '2021-07-07 08:22:11', 2, NULL, 1),
('CA-02-0045', '1357454 - Infrared Gun', 6, '2021-07-07 08:22:31', '2021-07-07 08:22:31', 2, NULL, 0),
('CA-02-0046', '1357455 - Jade Roller ', 6, '2021-07-07 08:22:39', '2021-07-07 08:22:39', 2, NULL, 0),
('CA-02-0047', '1357456 - Darma Roller', 6, '2021-07-07 08:23:23', '2021-07-07 08:23:23', 2, NULL, 0),
('CA-02-0048', '1357457 - Castor oil ', 6, '2021-07-07 08:23:33', '2021-07-07 08:23:33', 2, NULL, 0),
('CA-02-0049', '1357458 - Glass Amber Dropper Bottle-30ml-Flat Shoulder', 6, '2021-07-07 08:23:44', '2021-07-07 08:23:44', 2, NULL, 1),
('CA-02-005', '1357436 - Oximeter ', 6, '2021-07-07 08:08:31', '2021-07-07 08:08:31', 2, NULL, 0),
('CA-02-0050', '1357458 - Glass Amber Dropper Bottle-30ml-Round Shoulder', 6, '2021-07-07 08:23:55', '2021-07-07 08:23:55', 2, NULL, 1),
('CA-02-0051', '1357459 - Glass Froasted Dropper bottle-30ml-Blue-Flat Shoulder', 6, '2021-07-07 08:29:31', '2021-07-07 08:29:31', 2, NULL, 1),
('CA-02-0052', '1357459 - Glass Froasted Dropper bottle-30ml-Flat Shoulder', 6, '2021-07-07 08:29:41', '2021-07-07 08:29:41', 2, NULL, 1),
('CA-02-0053', '1357459 - Glass Froasted Dropper bottle-30ml-Round Shoulder', 6, '2021-07-07 08:29:52', '2021-07-07 08:29:52', 2, NULL, 1),
('CA-02-0054', '1357460 - Dropper Cap-30ml-Black-Aluminium', 6, '2021-07-07 08:30:03', '2021-07-07 08:30:03', 2, NULL, 1),
('CA-02-0055', '1357460 - Dropper Cap-30ml-Black-Plastic', 6, '2021-07-07 08:30:13', '2021-07-07 08:30:13', 2, NULL, 1),
('CA-02-0056', '1357460 - Dropper Cap-30ml-Gold Black-Aluminium', 6, '2021-07-07 08:30:24', '2021-07-07 08:30:24', 2, NULL, 1),
('CA-02-0057', '1357460 - Dropper Cap-30ml-Gold White-Aluminium', 6, '2021-07-07 08:30:38', '2021-07-07 08:30:38', 2, NULL, 1),
('CA-02-0058', '1357460 - Dropper Cap-30ml-Silver Black-Aluminium', 6, '2021-07-07 08:30:52', '2021-07-07 08:30:52', 2, NULL, 1),
('CA-02-0059', '1357460 - Dropper Cap-30ml-Silver White-Aluminium', 6, '2021-07-07 08:31:05', '2021-07-07 08:31:05', 2, NULL, 1),
('CA-02-006', '1357437 - N95 With Filter ', 6, '2021-07-07 08:08:40', '2021-07-07 08:08:40', 2, NULL, 0),
('CA-02-0060', '1357460 - Dropper Cap-30ml-White-Plastic', 6, '2021-07-07 08:31:35', '2021-07-07 08:31:35', 2, NULL, 1),
('CA-02-0061', '1357461 - Plastic Lotion Pump Bottle Noor-100ml-Normal Base', 6, '2021-07-07 08:31:48', '2021-07-07 08:31:48', 2, NULL, 1),
('CA-02-0062', '1357462 - Plastic Lotion Pump Bottle Chinese-100ml-Normal Base', 6, '2021-07-07 08:31:58', '2021-07-07 08:31:58', 2, NULL, 1),
('CA-02-0063', '1357463 - Plastic Jar Cap-300ml-Transparent', 6, '2021-07-07 08:32:16', '2021-07-07 08:32:16', 2, NULL, 1),
('CA-02-0064', '1357464 - Vitamin C Serum 30ml', 6, '2021-07-07 08:32:26', '2021-07-07 08:32:26', 2, NULL, 0),
('CA-02-0065', '1357465 - Vitamin C Serum A 30ml', 6, '2021-07-07 08:32:47', '2021-07-07 08:32:47', 2, NULL, 0),
('CA-02-0066', '1357466 - Glycerein steak ', 6, '2021-07-07 08:32:58', '2021-07-07 08:32:58', 2, NULL, 0),
('CA-02-0067', '1357467 - Grap sced oil', 6, '2021-07-07 08:33:12', '2021-07-07 08:33:12', 2, NULL, 0),
('CA-02-0068', '1357468 - IPM oil csso', 6, '2021-07-07 08:33:29', '2021-07-07 08:33:29', 2, NULL, 0),
('CA-02-0069', '1357469 - Vitamin C Serum (Dr Rasheel)', 6, '2021-07-07 08:33:39', '2021-07-07 08:33:39', 2, NULL, 0),
('CA-02-007', '1357438 - N95 Without  Filter ', 6, '2021-07-07 08:08:57', '2021-07-07 08:08:57', 2, NULL, 0),
('CA-02-0070', '1357470 - Hyaluronic Acid Serum-30ml', 6, '2021-07-07 08:33:50', '2021-07-07 08:33:50', 2, NULL, 1),
('CA-02-0071', '1357471 - Aloevera Gel-100ml', 6, '2021-07-07 08:34:02', '2021-07-07 08:34:02', 2, NULL, 1),
('CA-02-0072', '1357471 - Aloevera Gel-500ml', 6, '2021-07-07 08:34:13', '2021-07-07 08:34:13', 2, NULL, 1),
('CA-02-0073', '1357472 - Pumpkin oil', 6, '2021-07-07 08:34:30', '2021-07-07 08:34:30', 2, NULL, 0),
('CA-02-0074', '1357473 - Steraic acid c540', 6, '2021-07-07 08:34:41', '2021-07-07 08:34:41', 2, NULL, 0),
('CA-02-0075', '1357474 - Petroluem Jelly-100gm', 6, '2021-07-07 08:35:17', '2021-07-07 08:35:17', 2, NULL, 1),
('CA-02-0076', '1357474 - Petroluem Jelly-50gm', 6, '2021-07-07 08:35:32', '2021-07-07 08:35:32', 2, NULL, 1),
('CA-02-0077', '1357475 - Ultimate Body Butter-100gm', 6, '2021-07-07 08:35:42', '2021-07-07 08:35:42', 2, NULL, 1),
('CA-02-0078', '1357476 - White oil', 6, '2021-07-07 08:36:00', '2021-07-07 08:36:00', 2, NULL, 0),
('CA-02-0079', '1357477 - Jogoba oil ', 6, '2021-07-07 08:37:04', '2021-07-07 08:37:04', 2, NULL, 0),
('CA-02-008', '1357439 - KN95 With Filter ', 6, '2021-07-07 08:09:07', '2021-07-07 08:09:07', 2, NULL, 0),
('CA-02-0080', '1357478 - Cocoa Butter-100gm', 6, '2021-07-07 08:37:14', '2021-07-07 08:37:14', 2, NULL, 1),
('CA-02-0081', '1357479 - Shea Butter-100gm', 6, '2021-07-07 08:37:24', '2021-07-07 08:37:24', 2, NULL, 1),
('CA-02-0082', '1357479 - Shea Butter-300gm', 6, '2021-07-07 08:37:34', '2021-07-07 08:37:34', 2, NULL, 1),
('CA-02-0083', '1357480 - Coconut oil Tin', 6, '2021-07-07 08:37:49', '2021-07-07 08:37:49', 2, NULL, 0),
('CA-02-0084', '1357481 - Hair Repair oil-100ml', 6, '2021-07-07 08:38:11', '2021-07-07 08:38:11', 2, NULL, 1),
('CA-02-0085', '1357482 - Aloevera Gel Lables-100ml', 6, '2021-07-07 08:38:22', '2021-07-07 08:38:22', 2, NULL, 1),
('CA-02-0086', '1357482 - Aloevera Gel Lables-500ml', 6, '2021-07-07 08:38:31', '2021-07-07 08:38:31', 2, NULL, 1),
('CA-02-0087', '1357483 - Vitamin C Lables-30ml', 6, '2021-07-07 08:38:42', '2021-07-07 08:38:42', 2, NULL, 1),
('CA-02-0088', '1357484 - Hyaluronic Acid Box-30ml', 6, '2021-07-07 08:38:51', '2021-07-07 08:38:51', 2, NULL, 1),
('CA-02-0089', '1357485 - Vitamin C Box-30ml', 6, '2021-07-07 08:39:02', '2021-07-07 08:39:02', 2, NULL, 1),
('CA-02-009', '1357440 - Disposible Gloves ', 6, '2021-07-07 08:09:33', '2021-07-07 08:09:33', 2, NULL, 0),
('CA-02-0090', '1357486 - Aloevera Gel Box-100ml', 6, '2021-07-07 08:39:16', '2021-07-07 08:39:16', 2, NULL, 1),
('CA-02-0091', '1357487 - Hand Santizer Box-1000ml', 6, '2021-07-07 08:39:55', '2021-07-07 08:39:55', 2, NULL, 1),
('CA-02-0092', '1357487 - Hand Santizer Box-100ml', 6, '2021-07-07 08:40:10', '2021-07-07 08:40:10', 2, NULL, 1),
('CA-02-0093', '1357487 - Hand Santizer Box-250ml', 6, '2021-07-07 08:40:24', '2021-07-07 08:40:24', 2, NULL, 1),
('CA-02-0094', '1357487 - Hand Santizer Box-500ml', 6, '2021-07-07 08:40:46', '2021-07-07 08:40:46', 2, NULL, 1),
('CA-02-0095', '1357487 - Hand Santizer Box-60ml', 6, '2021-07-07 08:40:58', '2021-07-07 08:40:58', 2, NULL, 1),
('CA-02-0096', '1357488 - Hand wash Box-500ml', 6, '2021-07-07 08:41:10', '2021-07-07 08:41:10', 2, NULL, 1),
('CA-02-0097', '1357489 - Hand Sanitizer spray Box-100ml', 6, '2021-07-07 08:41:21', '2021-07-07 08:41:21', 2, NULL, 1),
('CA-02-0098', '1357490 - Hyaluronic Acid Serum Label', 6, '2021-07-07 08:41:36', '2021-07-07 08:41:36', 2, NULL, 0),
('CA-02-0099', '1357491 - Hand Wash Label-500ml-Blueberry', 6, '2021-07-07 08:41:56', '2021-07-07 08:41:56', 2, NULL, 1),
('CA-03-001', 'Cash In Hand', 7, '2021-03-29 02:42:49', '2021-03-29 02:42:49', 2, NULL, NULL),
('CA-04-001', 'meezan bank - bahadrabad', 8, '2021-06-14 07:35:40', '2021-06-14 07:35:40', 2, NULL, NULL),
('CA-04-002', 'alfalah - shahrah e faisal', 8, '2021-06-29 07:59:39', '2021-06-29 07:59:39', 2, NULL, NULL),
('CL-01-001', 'Abbad - Abbas', 12, '2021-06-18 01:33:03', '2021-06-18 01:33:03', 2, NULL, NULL),
('CL-01-002', 'Ahmed Kapadia - Ahmed Kapadia', 12, '2021-06-18 01:33:11', '2021-06-18 01:33:11', 2, NULL, NULL),
('CL-01-003', 'hibah - posch care', 12, '2021-06-29 05:50:27', '2021-06-29 05:50:27', 2, NULL, NULL),
('CL-03-001', 'Tax', 22, '2021-04-09 00:50:43', '2021-04-09 00:50:43', 2, NULL, NULL),
('EQ-01-001', 'Capital', 15, '2021-03-29 02:46:43', '2021-03-29 02:46:43', 2, NULL, NULL),
('EXP-01-001', 'Outdoor Activities', 9, '2021-03-29 02:44:19', '2021-03-29 02:44:19', 2, NULL, NULL),
('EXP-01-002', 'PR', 9, '2021-03-29 02:44:25', '2021-03-29 02:44:25', 2, NULL, NULL),
('EXP-02-001', 'Cleaning', 10, '2021-03-29 02:44:47', '2021-03-29 02:44:47', 2, NULL, NULL),
('EXP-02-002', 'Rent', 10, '2021-03-29 02:44:54', '2021-03-29 02:44:54', 2, NULL, NULL),
('EXP-03-001', 'Electricity', 11, '2021-03-29 02:45:17', '2021-03-29 02:45:17', 2, NULL, NULL),
('EXP-03-002', 'Telephone', 11, '2021-03-29 02:45:32', '2021-03-29 02:45:32', 2, NULL, NULL),
('EXP-04-001', 'Discount', 20, '2021-03-30 03:31:24', '2021-03-30 03:31:24', 2, NULL, NULL),
('NCA-01-001', 'Table', 1, '2021-03-29 02:40:27', '2021-03-29 02:40:27', 2, NULL, NULL),
('NCA-01-002', 'Shelf', 1, '2021-03-29 02:40:36', '2021-03-29 02:40:36', 2, NULL, NULL),
('NCA-01-003', 'Chair', 1, '2021-03-29 02:40:42', '2021-03-29 02:40:42', 2, NULL, NULL),
('NCA-02-001', 'Computer', 2, '2021-03-29 02:40:53', '2021-03-29 02:40:53', 2, NULL, NULL),
('NCA-02-002', 'Server', 2, '2021-03-29 02:40:59', '2021-03-29 02:40:59', 2, NULL, NULL),
('NCA-02-003', 'IT Switch', 2, '2021-03-29 02:41:04', '2021-03-29 02:41:04', 2, NULL, NULL),
('NCA-03-001', 'Loop Machine', 3, '2021-03-29 02:41:19', '2021-03-29 02:41:19', 2, NULL, NULL),
('NCA-03-002', 'Cutting Machine', 3, '2021-03-29 02:41:29', '2021-03-29 02:41:29', 2, NULL, NULL),
('NCA-04-001', 'Mixture', 4, '2021-03-29 02:41:39', '2021-03-29 02:41:39', 2, NULL, NULL),
('NCA-04-002', 'Oven', 4, '2021-03-29 02:41:44', '2021-03-29 02:41:44', 2, NULL, NULL),
('NCL-01-001', 'Loan Provider', 14, '2021-03-29 02:46:27', '2021-03-29 02:46:27', 2, NULL, NULL),
('PUR-01-001', 'Purchase', 17, '2021-03-29 02:46:55', '2021-03-29 02:46:55', 2, NULL, NULL),
('RV-01-001', 'Sales', 16, '2021-03-29 02:46:49', '2021-03-29 02:46:49', 2, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bank`
--

CREATE TABLE `bank` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `branch` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_id` bigint(20) UNSIGNED DEFAULT NULL,
  `c_no` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bank`
--

INSERT INTO `bank` (`id`, `name`, `address`, `branch`, `c_id`, `c_no`, `created_at`, `updated_at`, `status`) VALUES
(1, 'meezan bank', 'bahadrabad', 'bahadrabad', 1, 3212020298, '2021-02-24 02:26:54', '2021-02-24 02:26:54', 1),
(3, 'alfalah', 'shahrah e faisal', 'shahrah e faisal', NULL, NULL, '2021-02-28 23:56:14', '2021-02-28 23:56:14', 1),
(4, 'ubl', 'shahrah e faisal', 'shahrah e faisal', 1, NULL, '2021-03-02 03:12:12', '2021-03-02 03:12:12', 1),
(5, 'hbl', 'shahrah e faisal', 'shahrah e faisal', 1, 2222, '2021-03-29 08:40:58', '2021-03-29 08:40:58', 1),
(6, 'nbp', 'ii chandigarh road', 'ii chandigarh road', 1, 837438473, '2021-04-10 05:34:20', '2021-04-10 05:34:20', 1);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `b_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_p_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_p_contactNo` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `b_name`, `c_p_name`, `c_p_contactNo`, `created_at`, `updated_at`, `status`, `created_by`, `updated_by`) VALUES
(1, 'A.S Mask', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(2, 'posch Care', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(3, 'Unknown', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(4, 'Chines N95', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(5, 'Chines KN95', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(6, 'Molly Dale', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(7, 'Nugard', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(8, 'No brand', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(9, 'Bo Hui', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(10, 'Noor Enterprises', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(11, 'Chinese', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(12, 'Dr Rashel', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(13, 'Z.', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(14, 'one cup', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(15, 'S.K Mask Blue', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(16, 'G.R Mask', NULL, NULL, NULL, NULL, 1, NULL, NULL),
(17, 'Adil', NULL, NULL, NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `cat_name`, `created_at`, `updated_at`, `status`, `created_by`, `updated_by`) VALUES
(1, 'Personal Protection', NULL, NULL, 1, NULL, NULL),
(2, 'Devices & Appliances', NULL, NULL, 1, NULL, NULL),
(3, 'Packaging Material', NULL, NULL, 1, NULL, NULL),
(4, 'Finished', NULL, NULL, 1, NULL, NULL),
(5, 'Skin Care', NULL, NULL, 1, NULL, NULL),
(6, 'Raw Material', NULL, NULL, 1, NULL, NULL),
(7, 'Hair Care', NULL, NULL, 1, NULL, NULL),
(8, 'Personal Care', NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `c_name`, `created_at`, `updated_at`, `created_by`, `updated_by`, `status`) VALUES
(1, 'Karachi', NULL, NULL, NULL, NULL, 1),
(2, 'Lahore', NULL, NULL, NULL, NULL, 1),
(3, 'Islamabad', NULL, NULL, NULL, NULL, 1),
(4, 'New York', NULL, NULL, NULL, NULL, 1),
(5, 'Jhang', NULL, NULL, NULL, NULL, 1),
(6, 'Rahim Yar Khan', NULL, NULL, NULL, NULL, 1),
(7, 'Dadu', NULL, NULL, NULL, NULL, 1),
(8, 'Chakwal', NULL, NULL, NULL, NULL, 1),
(9, 'Shikarpur', NULL, NULL, NULL, NULL, 1),
(10, 'Rawalpindi', NULL, NULL, NULL, NULL, 1),
(11, 'Gujar Khan', NULL, NULL, NULL, NULL, 1),
(12, ' YAR KHAN', NULL, NULL, NULL, NULL, 1),
(13, 'lahore cantt', NULL, NULL, NULL, NULL, 1),
(14, 'Gujranwala', NULL, NULL, NULL, NULL, 1),
(15, 'Sialkot', NULL, NULL, NULL, NULL, 1),
(16, 'Nawabshah', NULL, NULL, NULL, NULL, 1),
(17, 'Faisalabad', NULL, NULL, NULL, NULL, 1),
(18, 'Multan', NULL, NULL, NULL, NULL, 1),
(19, 'Kharian', NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_p_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_p_contactNo` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `current_stock`
--

CREATE TABLE `current_stock` (
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `w_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `unit_quantity` double DEFAULT NULL,
  `unit_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `finish_products`
--

CREATE TABLE `finish_products` (
  `id` bigint(20) UNSIGNED DEFAULT NULL,
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` double DEFAULT NULL,
  `w_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `general_ledger`
--

CREATE TABLE `general_ledger` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `source` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `account_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `link_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `accounting_date` date DEFAULT NULL,
  `posted_date` date NOT NULL,
  `period` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `account_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `currency_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PKR',
  `debit` double DEFAULT NULL,
  `credit` double DEFAULT NULL,
  `net_value` double DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `balance` double DEFAULT NULL,
  `stock_in` double DEFAULT NULL,
  `stock_out` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `g_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `g_type`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'test1', 'Customer', '2021-02-24 02:25:30', '2021-02-24 02:25:30', 1, 1),
(2, 'test2', 'Price', '2021-02-24 02:25:40', '2021-02-24 02:25:40', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `head_category`
--

CREATE TABLE `head_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `a_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `head_category`
--

INSERT INTO `head_category` (`id`, `name`, `a_id`, `created_at`, `updated_at`, `created_by`, `updated_by`, `code`) VALUES
(1, 'Furniture', 1, '2021-03-24 00:58:35', '2021-03-26 01:57:01', 2, 2, 'NCA-01'),
(2, 'IT Equipment', 1, '2021-03-24 01:01:49', '2021-03-24 01:01:49', 2, NULL, 'NCA-02'),
(3, 'Machinery', 1, '2021-03-24 01:13:34', '2021-03-26 01:50:14', 2, 2, 'NCA-03'),
(4, 'Lab Equipment', 1, '2021-03-24 01:14:01', '2021-03-24 01:14:01', 2, NULL, 'NCA-04'),
(5, 'Receivables', 6, '2021-03-24 01:14:25', '2021-03-24 01:14:25', 2, NULL, 'CA-01'),
(6, 'Inventory', 6, '2021-03-24 01:14:39', '2021-03-24 01:14:39', 2, NULL, 'CA-02'),
(7, 'Cash', 6, '2021-03-24 01:15:09', '2021-03-24 01:15:09', 2, NULL, 'CA-03'),
(8, 'Bank', 6, '2021-03-24 01:15:18', '2021-03-24 01:15:18', 2, NULL, 'CA-04'),
(9, 'Marketing', 2, '2021-03-24 01:16:25', '2021-03-24 01:16:25', 2, NULL, 'EXP-01'),
(10, 'Admin', 2, '2021-03-24 01:16:36', '2021-03-24 01:16:36', 2, NULL, 'EXP-02'),
(11, 'Utility', 2, '2021-03-24 01:16:48', '2021-03-24 01:16:48', 2, NULL, 'EXP-03'),
(12, 'Payables', 3, '2021-03-24 01:17:03', '2021-03-24 01:17:03', 2, NULL, 'CL-01'),
(13, 'Short Loan', 3, '2021-03-24 01:17:15', '2021-03-24 01:17:15', 2, NULL, 'CL-02'),
(14, 'Loan', 7, '2021-03-24 01:17:42', '2021-03-24 01:17:42', 2, NULL, 'NCL-01'),
(15, 'Equity', 4, '2021-03-25 00:48:17', '2021-03-25 00:48:17', 2, NULL, 'EQ-01'),
(16, 'Revenue', 5, '2021-03-25 00:48:31', '2021-03-25 00:48:31', 2, NULL, 'RV-01'),
(17, 'Purchases', 8, '2021-03-26 06:10:22', '2021-03-26 06:10:22', 2, NULL, 'PUR-01'),
(20, 'Discount', 2, '2021-03-30 03:30:52', '2021-03-30 03:30:52', 2, NULL, 'EXP-04'),
(22, 'Tax', 3, '2021-04-09 00:50:28', '2021-04-09 00:50:28', 2, NULL, 'CL-03');

-- --------------------------------------------------------

--
-- Table structure for table `head_of_accounts`
--

CREATE TABLE `head_of_accounts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `head_of_accounts`
--

INSERT INTO `head_of_accounts` (`id`, `name`, `created_at`, `updated_at`, `created_by`, `updated_by`) VALUES
(1, 'Non-current Asset', '2021-03-23 04:31:04', '2021-03-26 01:35:43', 2, 2),
(2, 'Expense', '2021-03-23 04:31:10', '2021-03-23 04:31:10', 2, NULL),
(3, 'Current Liability', '2021-03-23 04:31:52', '2021-03-23 04:31:52', 2, NULL),
(4, 'Equity', '2021-03-23 04:32:06', '2021-03-23 04:32:06', 2, NULL),
(5, 'Revenue', '2021-03-23 04:32:12', '2021-03-23 04:32:12', 2, NULL),
(6, 'Current Asset', '2021-03-23 04:57:16', '2021-03-23 04:57:16', 2, NULL),
(7, 'Non-current Liability', '2021-03-23 04:57:37', '2021-03-23 04:57:37', 2, NULL),
(8, 'Purchases', '2021-03-26 06:08:57', '2021-03-26 06:08:57', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_01_27_072907_create_brands_table', 1),
(5, '2021_01_27_072928_create_category_table', 1),
(6, '2021_01_27_072950_create_sub_category_table', 1),
(7, '2021_01_27_074651_create_units_table', 1),
(8, '2021_01_27_074850_create_product_table', 1),
(9, '2021_01_28_111032_add_status_to_brands_table', 1),
(10, '2021_01_28_111228_add_status_to_category_table', 1),
(11, '2021_01_28_111243_add_status_to_sub_category_table', 1),
(12, '2021_01_28_111309_add_status_to_units_table', 1),
(13, '2021_01_28_111327_add_status_to_products_table', 1),
(14, '2021_02_01_052037_add_image_to_products_table', 1),
(15, '2021_02_01_080013_create_city_table', 1),
(16, '2021_02_01_080045_create_warehouse_table', 1),
(17, '2021_02_02_130213_create_company_table', 1),
(18, '2021_02_02_130256_add_w_id_to_products_table', 1),
(19, '2021_02_03_052254_add_visibility_to_products_table', 1),
(20, '2021_02_03_065945_create_groups_table', 1),
(21, '2021_02_03_070000_create_vendors_table', 1),
(22, '2021_02_04_110227_create_purchase_order_table', 1),
(23, '2021_02_04_111406_create_purchase_order_details_table', 1),
(24, '2021_02_04_131917_add_total_to_purchase_order_table', 1),
(25, '2021_02_04_131950_add_sub_total_to_purchase_order_details_table', 1),
(26, '2021_02_08_111522_create_transaction_history_table', 1),
(27, '2021_02_08_111551_create_sales_table', 1),
(28, '2021_02_08_111606_create_sale_details_table', 1),
(29, '2021_02_09_110733_add_p_status_to_purchase_order_table', 1),
(30, '2021_02_11_074909_add_received_quantity_to_purchase_order_details_table', 1),
(31, '2021_02_11_075001_create_stocks_table', 1),
(32, '2021_02_12_045824_add_created_by_to_brands_table', 1),
(33, '2021_02_12_050055_add_created_by_to_category_table', 1),
(34, '2021_02_12_050259_add_created_by_to_city_table', 1),
(35, '2021_02_12_050327_add_created_by_to_company_table', 1),
(36, '2021_02_12_050404_add_created_by_to_groups_table', 1),
(37, '2021_02_12_050430_add_created_by_to_products_table', 1),
(38, '2021_02_12_050511_add_created_by_to_purchase_order_table', 1),
(39, '2021_02_12_050833_add_created_by_to_sales_table', 1),
(40, '2021_02_12_050910_add_created_by_to_stocks_table', 1),
(41, '2021_02_12_050940_add_created_by_to_sub_category_table', 1),
(42, '2021_02_12_051020_add_created_by_to_transaction_history_table', 1),
(43, '2021_02_12_051044_add_created_by_to_units_table', 1),
(44, '2021_02_12_051557_add_created_by_to_vendors_table', 1),
(45, '2021_02_12_051623_add_created_by_to_warehouse_table', 1),
(46, '2021_02_12_084454_create_current_stock_table', 1),
(47, '2021_02_17_111318_add_b_id_to_purchase_order_table', 1),
(48, '2021_02_17_122428_add_cost_to_purchase_order_details_table', 1),
(49, '2021_02_18_083806_add_p_d_id_to_stocks_table', 1),
(50, '2021_02_18_085043_add_s_type_to_stocks_table', 1),
(51, '2021_02_19_062005_add_cost_to_stocks_table', 1),
(52, '2021_02_19_074955_add_u_id_to_vendors_table', 1),
(53, '2021_02_19_112909_create_bank_table', 1),
(54, '2021_02_19_112954_add_sp_id_to_sales_table', 1),
(55, '2021_02_19_124955_add_b_id_to_transaction_history_table', 1),
(56, '2021_02_24_051543_add_price_to_sale_details_table', 1),
(57, '2021_02_24_064705_add_p_type_to_products_table', 1),
(58, '2021_02_24_090151_create_finish_products_table', 2),
(59, '2021_02_24_130255_add_w_id_to_finish_products_table', 3),
(60, '2021_03_01_113931_add_w_id_to_stocks_table', 4),
(61, '2021_03_03_075707_add_unit_quantity_to_current_stock_table', 5),
(62, '2021_03_03_105837_add_timestamp_to_sale_details_table', 6),
(63, '2021_03_19_120839_create_product_transfer_history_table', 7),
(64, '2021_03_19_123512_add_created_by_to_product_transfer_history_table', 8),
(65, '2021_03_23_080320_create_head_of_accounts_table', 9),
(66, '2021_03_23_080634_create_head_category_table', 9),
(68, '2021_03_23_081431_create_account_details_table', 10),
(69, '2021_03_23_085242_add_created_by_to_head_of_accounts_table', 11),
(70, '2021_03_23_085309_add_created_by_to_head_category_table', 11),
(71, '2021_03_23_085341_add_created_by_to_account_details_table', 11),
(72, '2021_03_23_125110_add_code_to_head_category_table', 12),
(76, '2021_03_25_072736_create_general_ledger_table', 14),
(77, '2021_03_30_060155_add_amount_to_general_ledger_table', 15),
(78, '2021_03_30_072142_add_grn_no_to_stocks_table', 16),
(79, '2021_04_02_122239_add_discount_type_to_sales_table', 17),
(80, '2021_04_09_065339_add_gdn_no_to_sales_table', 18),
(82, '2021_04_13_080937_create_stock_out_table', 19),
(83, '2021_04_14_063734_add_deliver_quantity_to_sale_details_table', 20),
(84, '2021_04_21_063017_add_balance_to_general_ledger_table', 21),
(85, '2021_04_23_063307_create_purchase_request_table', 22),
(92, '2021_04_28_072534_add_expected_date_to_sales_table', 25),
(97, '2021_05_17_120604_add_costsin__to_general_ledger_table', 27),
(104, '2021_05_20_063915_create_roles_table', 29),
(105, '2021_05_20_071128_add_roleid_to_users_table', 30),
(106, '2021_05_20_074138_create_user_menu_table', 31),
(107, '2021_05_20_132308_create_permissions_table', 32),
(110, '2021_05_24_080121_create_role_menu_table', 33),
(111, '2021_05_24_080725_create_role_permission_table', 33),
(112, '2021_06_07_060315_add_status_to_bank_table', 34),
(113, '2021_06_07_060424_add_status_to_city_table', 34),
(114, '2021_06_07_060455_add_status_to_product_transfer_history_table', 34),
(115, '2021_06_08_073842_add_status_to_vendors_table', 34),
(116, '2021_06_10_062518_create_notifications_table', 34),
(117, '2021_06_14_122253_add_name2_to_vendors_table', 35),
(118, '2021_06_14_140023_add_attributes_in_sales', 36),
(119, '2021_06_14_140443_add_attributes_in_sale_details', 37),
(120, '2021_06_16_130942_add_pay_type_to_sales_table', 38),
(126, '2021_06_18_081113_create_vendors_update_request_table', 40),
(127, '2021_06_18_081401_create_products_update_request_table', 41),
(128, '2021_06_21_072450_add_variant_to_current_stock_table', 42),
(130, '2021_06_21_125931_create_variants_table', 43),
(133, '2021_06_21_104733_create_stock_receiving_table', 45),
(135, '2021_06_22_212806_add_variation_to_purchase_request_details', 47),
(136, '2021_06_21_125956_create_product_variants_table', 48),
(137, '2021_06_22_082634_create_product_variant_update_table', 49),
(138, '2021_06_25_060119_add_status_to_product_variants', 50),
(139, '2021_04_23_063410_create_purchase_request_details_table', 51),
(140, '2021_06_25_103727_add_type_to_purchase_request_details', 52),
(141, '2021_06_25_115908_add_vstatus_to_products', 53),
(142, '2021_04_23_102859_create_quotation_table', 54),
(144, '2021_04_28_062233_add_attachment_to_quotation_table', 54),
(147, '2021_04_23_104223_create_quotation_details_table', 55),
(148, '2021_04_28_083336_add_quantity_to_quotation_details_table', 55),
(149, '2021_06_25_120604_add_type_to_quotation_details', 55),
(150, '2021_06_25_203339_add_type_to_purchase_order_details', 56),
(151, '2021_06_28_050802_add_type_to_sale_details', 57),
(154, '2021_06_28_052015_add_cost_to_product_variants', 58),
(155, '2021_06_28_052100_add_cost_to_product_variant_update', 58),
(156, '2021_06_28_074051_add_type_to_stocks', 59),
(157, '2021_06_28_074257_add_type_to_stock_out', 59),
(158, '2021_06_28_083611_add_type_to_current_stock', 60),
(159, '2021_06_28_102832_add_price_to_product_variant_update', 61),
(160, '2021_06_28_102926_add_price_to_product_variants', 61),
(161, '2021_06_29_072810_add_type_to_product_transfer_history', 62),
(162, '2021_07_02_120149_add_type_to_finish_products', 62),
(163, '2021_07_05_104728_add_type_to_account_details', 63);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) UNSIGNED NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `type`, `notifiable_type`, `notifiable_id`, `data`, `read_at`, `created_at`, `updated_at`) VALUES
('01011433-2493-4742-8f41-9cb10f6bde30', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 08:46:49', '2021-06-29 08:46:49'),
('07a0fd32-62fc-4b58-864f-50df12d697bf', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Packaging product has been added by hibah\",\"link\":\"\\/product\",\"name\":\"View Products\"}', NULL, '2021-07-05 02:43:47', '2021-07-05 02:43:47'),
('0ea1a04a-4b0d-4138-8053-d813c3ffe658', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 08:17:52', '2021-06-29 08:17:52'),
('273cbaf2-5510-436c-8723-09427311ec55', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\",\"name\":\"View Products\"}', NULL, '2021-07-05 08:21:36', '2021-07-05 08:21:36'),
('2c47d68f-5dbb-421b-9947-18acfdb59cff', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 08:17:52', '2021-06-29 08:17:52'),
('2e381b0c-247a-43ea-9d35-bb9b5780893d', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\",\"name\":\"View Products\"}', NULL, '2021-07-05 02:57:35', '2021-07-05 02:57:35'),
('440cf7a2-27df-4f28-af9a-a55ff117092e', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 08:28:45', '2021-06-29 08:28:45'),
('4fecb371-c1da-44b8-a3e1-388051d2048b', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 08:28:45', '2021-06-29 08:28:45'),
('5b333ff0-668e-4542-abf9-008e23c3994e', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\",\"name\":\"View Products\"}', NULL, '2021-07-05 02:57:35', '2021-07-05 02:57:35'),
('69ced6b4-cd3d-4ed3-87a9-2606f81452e2', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\\/finishProducts\",\"name\":\"View Finish Products\"}', NULL, '2021-07-05 01:05:24', '2021-07-05 01:05:24'),
('6b3648e3-7639-4a58-9701-89346867307d', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 08:46:49', '2021-06-29 08:46:49'),
('981fe1eb-56a6-4420-b261-78f0c76b4cb0', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Packaging product has been added by hibah\",\"link\":\"\\/product\",\"name\":\"View Products\"}', NULL, '2021-07-05 02:43:47', '2021-07-05 02:43:47'),
('aaf16348-2be8-47b5-8400-435093aad5f2', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\",\"name\":\"View Products\"}', NULL, '2021-07-05 08:21:36', '2021-07-05 08:21:36'),
('ab16064e-c836-4f22-a659-07797eba12ee', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 07:57:33', '2021-06-29 07:57:33'),
('bf5d2328-c4d2-4822-bb56-11d411d3b179', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\\/finishProducts\",\"name\":\"View Finish Products\"}', NULL, '2021-07-05 01:05:23', '2021-07-05 01:05:23'),
('c77f0559-98c2-4023-8482-be1d01cd131b', 'App\\Notifications\\AddNotification', 'App\\User', 6, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\\/finishProducts\",\"name\":\"View Finish Products\"}', NULL, '2021-07-05 01:01:39', '2021-07-05 01:01:39'),
('ed20aa0e-0ce4-4b73-bcfb-b12fc22a9890', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Purchase order request has been added by hibah\",\"link\":\"\\/purchase\",\"name\":\"View Purchase Orders\"}', NULL, '2021-06-29 07:57:33', '2021-06-29 07:57:33'),
('fed56d1e-8dd5-410d-9ce9-a8ce0088934d', 'App\\Notifications\\AddNotification', 'App\\User', 2, '{\"notification\":\"New Finished product has been added by hibah\",\"link\":\"\\/product\\/finishProducts\",\"name\":\"View Finish Products\"}', NULL, '2021-07-05 01:01:39', '2021-07-05 01:01:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `m_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `m_id`, `name`, `route`, `created_at`, `updated_at`) VALUES
(1, 11, 'edit', 'bank.edit', '2021-05-20 08:40:33', '2021-05-21 00:41:08'),
(2, 4, 'edit', 'roles.edit', '2021-05-20 08:44:08', '2021-05-20 08:44:08'),
(3, 4, 'status', 'roles.status', '2021-05-20 08:44:27', '2021-05-20 08:44:27'),
(4, 6, 'edit', 'menu.edit', '2021-05-20 08:45:00', '2021-05-21 06:26:47'),
(5, 14, 'edit', 'brands.edit', '2021-05-21 00:42:26', '2021-05-21 00:42:26'),
(6, 14, 'status', 'brands.status', '2021-05-21 00:42:44', '2021-05-21 00:42:44'),
(7, 17, 'edit', 'unit.edit', '2021-05-21 00:43:02', '2021-05-21 00:43:02'),
(8, 17, 'status', 'unit.status', '2021-05-21 00:43:13', '2021-05-21 00:43:13'),
(9, 20, 'edit', 'category.edit', '2021-05-21 00:43:35', '2021-05-21 00:43:35'),
(10, 20, 'status', 'category.status', '2021-05-21 00:43:46', '2021-05-21 00:43:46'),
(11, 23, 'edit', 'subcategory.edit', '2021-05-21 00:44:04', '2021-05-21 00:44:04'),
(12, 23, 'status', 'subcategory.status', '2021-05-21 00:44:15', '2021-05-21 00:44:15'),
(13, 26, 'edit', 'product.edit', '2021-05-21 00:46:26', '2021-05-21 00:46:35'),
(14, 26, 'status', 'product.status', '2021-05-21 00:46:56', '2021-05-21 00:46:56'),
(15, 28, 'status', 'product.finishProducts.status', '2021-05-21 00:48:44', '2021-05-21 00:48:44'),
(16, 31, 'edit', 'city.edit', '2021-05-21 00:49:02', '2021-05-21 00:49:02'),
(17, 26, 'product sale details', 'product.sales', '2021-05-21 00:54:16', '2021-05-21 00:54:16'),
(18, 26, 'product purchase details', 'purchase.product', '2021-05-21 00:55:24', '2021-05-21 01:00:15'),
(19, 28, 'product details', 'product.detail', '2021-05-21 01:00:25', '2021-05-21 01:00:25'),
(20, 28, 'product sale details', 'product.sales', '2021-05-21 01:01:40', '2021-05-21 01:01:40'),
(21, 34, 'edit', 'warehouse.edit', '2021-05-21 01:02:31', '2021-05-21 01:02:31'),
(22, 39, 'edit', 'company.edit', '2021-05-21 01:04:26', '2021-05-21 01:04:26'),
(23, 42, 'edit', 'group.edit', '2021-05-21 01:04:42', '2021-05-21 01:04:42'),
(24, 45, 'edit', 'customer.edit', '2021-05-21 01:05:02', '2021-05-21 01:05:02'),
(25, 47, 'edit', 'supplier.edit', '2021-05-21 01:05:26', '2021-05-21 01:05:26'),
(26, 49, 'edit', 'biller.edit', '2021-05-21 01:05:41', '2021-05-21 01:05:53'),
(27, 51, 'edit', 'saleperson.edit', '2021-05-21 01:06:12', '2021-05-21 01:06:12'),
(28, 14, 'show', 'brands.show', '2021-05-21 01:14:45', '2021-05-21 01:14:45'),
(29, 17, 'show', 'unit.show', '2021-05-21 01:15:01', '2021-05-21 01:15:01'),
(30, 20, 'show', 'category.show', '2021-05-21 01:15:16', '2021-05-21 01:15:16'),
(31, 23, 'show', 'subcategory.show', '2021-05-21 01:15:33', '2021-05-21 01:15:33'),
(32, 26, 'show', 'product.show', '2021-05-21 01:15:52', '2021-05-21 01:15:52'),
(33, 28, 'show', 'product.show', '2021-05-21 01:16:29', '2021-05-21 01:16:29'),
(34, 31, 'show', 'city.show', '2021-05-21 01:16:46', '2021-05-21 01:16:46'),
(35, 34, 'show', 'warehouse.show', '2021-05-21 01:17:07', '2021-05-21 01:17:07'),
(36, 39, 'show', 'company.show', '2021-05-21 01:17:24', '2021-05-21 01:17:24'),
(37, 42, 'show', 'group.show', '2021-05-21 01:17:52', '2021-05-21 01:17:52'),
(38, 45, 'show', 'customer.show', '2021-05-21 01:18:09', '2021-05-21 01:18:09'),
(39, 47, 'show', 'supplier.show', '2021-05-21 01:19:16', '2021-05-21 01:19:16'),
(40, 49, 'show', 'biller.show', '2021-05-21 01:19:30', '2021-05-21 01:19:30'),
(41, 51, 'show', 'saleperson.show', '2021-05-21 01:19:46', '2021-05-21 01:19:46'),
(42, 59, 'status', 'request.status', '2021-05-21 01:20:20', '2021-05-21 01:20:20'),
(43, 59, 'edit', 'request.edit', '2021-05-21 01:20:35', '2021-05-21 01:20:35'),
(44, 59, 'show', 'request.show', '2021-05-21 01:20:53', '2021-05-21 01:20:53'),
(45, 59, 'add quotation', 'quotation.newQuotation', '2021-05-21 01:22:15', '2021-05-21 01:22:15'),
(46, 61, 'status', 'quotation.status', '2021-05-21 01:27:50', '2021-05-21 01:27:50'),
(47, 61, 'edit', 'quotation.edit', '2021-05-21 01:28:02', '2021-05-21 01:28:02'),
(48, 61, 'show', 'quotation.show', '2021-05-21 01:28:23', '2021-05-21 01:28:23'),
(49, 64, 'show', 'purchase.show', '2021-05-21 01:36:44', '2021-05-21 01:36:44'),
(50, 64, 'report', 'purchase.report', '2021-05-21 01:42:10', '2021-05-21 01:42:10'),
(51, 64, 'edit', 'purchase.edit', '2021-05-21 01:42:50', '2021-05-21 01:42:50'),
(52, 64, 'View Payment', 'transaction.view', '2021-05-21 01:45:26', '2021-05-21 01:46:12'),
(53, 64, 'View Grn\'s', 'purchase.grn', '2021-05-21 01:46:52', '2021-05-21 01:46:52'),
(54, 64, 'Download Invoice', 'purchase.invoice', '2021-05-21 01:48:53', '2021-05-21 01:48:53'),
(55, 64, 'Download Grn', 'purchase.GRR', '2021-05-21 01:49:42', '2021-05-21 01:49:42'),
(56, 68, 'status', 'sales.status', '2021-05-21 01:51:54', '2021-05-21 01:51:54'),
(57, 68, 'edit', 'sales.edit', '2021-05-21 01:58:05', '2021-05-21 01:58:05'),
(58, 68, 'report', 'sales.report', '2021-05-21 01:58:40', '2021-05-21 01:58:40'),
(59, 68, 'show', 'sales.show', '2021-05-21 01:59:00', '2021-05-21 01:59:00'),
(60, 68, 'Ready To Deliver', 'sales.delivery', '2021-05-21 01:59:53', '2021-05-21 01:59:53'),
(61, 68, 'View Payment', 'transaction.view', '2021-05-21 02:00:49', '2021-05-21 02:00:49'),
(62, 68, 'View Gdn\'s', 'sales.gdn', '2021-05-21 02:02:08', '2021-05-21 02:02:08'),
(63, 64, 'Receive Order', 'purchase.updateQuantity', '2021-05-21 02:03:59', '2021-05-21 02:03:59'),
(64, 68, 'Download Invoice', 'sales.invoice', '2021-05-21 02:05:36', '2021-05-21 02:05:36'),
(65, 68, 'Download Gdr', 'sales.GDR', '2021-05-21 02:06:15', '2021-05-21 02:06:15'),
(66, 72, 'edit', 'headofaccounts.edit', '2021-05-21 02:07:47', '2021-05-21 02:07:47'),
(67, 74, 'edit', 'headcategory.edit', '2021-05-21 02:08:21', '2021-05-21 02:08:21'),
(68, 74, 'history', 'headcategory.history', '2021-05-21 02:08:36', '2021-05-21 02:08:36'),
(69, 76, 'history', 'accountdetails.history', '2021-05-21 02:10:06', '2021-05-21 02:10:06'),
(70, 4, 'View Menu', 'roles.menu', '2021-05-24 05:04:55', '2021-05-24 05:04:55'),
(71, 4, 'View Permissions', 'roles.permission', '2021-05-24 05:05:12', '2021-05-24 05:05:12'),
(72, 1, 'Edit Profile', 'users.editProfile', '2021-05-24 07:35:33', '2021-05-24 07:35:33'),
(73, 86, 'edit', 'users.edit', '2021-05-25 01:16:00', '2021-05-25 01:16:00'),
(74, 4, 'Add', 'roles.create', '2021-05-25 05:43:41', '2021-05-25 05:43:41'),
(75, 6, 'Add', 'menu.create', '2021-05-25 05:44:03', '2021-05-25 05:44:03'),
(76, 86, 'Add', 'users.create', '2021-05-25 05:44:27', '2021-05-25 05:44:27'),
(77, 11, 'Add', 'bank.create', '2021-05-25 05:44:48', '2021-05-25 05:44:48'),
(78, 14, 'Add', 'brands.create', '2021-05-25 05:46:04', '2021-06-18 02:33:48'),
(79, 17, 'Add', 'unit.create', '2021-05-25 05:46:18', '2021-05-25 05:46:18'),
(80, 20, 'Add', 'category.create', '2021-05-25 05:46:40', '2021-05-25 05:46:40'),
(81, 7, 'excel', 'transaction.excel', '2021-05-25 05:48:51', '2021-05-25 05:48:51'),
(82, 14, 'excel', 'brands.excel', '2021-05-25 05:49:41', '2021-05-25 05:49:41'),
(83, 20, 'excel', 'category.excel', '2021-05-25 05:50:10', '2021-05-25 05:50:10'),
(84, 23, 'Add', 'subcategory.create', '2021-05-25 05:50:26', '2021-05-25 05:50:26'),
(85, 26, 'excel', 'product.excel', '2021-05-25 05:51:24', '2021-05-25 05:51:24'),
(86, 26, 'Add', 'product.create', '2021-05-25 05:51:41', '2021-05-25 05:51:41'),
(87, 28, 'Add', 'product.add', '2021-05-25 05:52:47', '2021-05-25 05:52:47'),
(88, 28, 'excel', 'product.finish.excel', '2021-05-25 05:53:19', '2021-05-25 05:53:19'),
(89, 31, 'Add', 'city.create', '2021-05-25 05:53:50', '2021-05-25 05:53:50'),
(90, 34, 'Add', 'warehouse.create', '2021-05-25 05:54:34', '2021-05-25 05:54:34'),
(91, 36, 'Add', 'productTransfer.create', '2021-05-25 05:56:20', '2021-05-25 05:56:20'),
(92, 36, 'excel', 'productTransfer.excel', '2021-05-25 05:56:37', '2021-05-25 05:56:37'),
(93, 39, 'Add', 'company.create', '2021-05-25 05:57:34', '2021-05-25 05:57:34'),
(94, 39, 'excel', 'company.excel', '2021-05-25 05:57:53', '2021-05-25 05:57:53'),
(95, 39, 'pdf', 'company.pdf', '2021-05-25 05:58:05', '2021-05-25 05:58:05'),
(96, 42, 'Add', 'group.create', '2021-05-25 05:58:24', '2021-05-25 05:58:24'),
(97, 45, 'Add', 'customer.create', '2021-05-25 05:59:12', '2021-05-25 05:59:12'),
(98, 45, 'excel', 'customer.excel', '2021-05-25 05:59:24', '2021-05-25 05:59:24'),
(99, 45, 'pdf', 'customer.pdf', '2021-05-25 05:59:35', '2021-05-25 05:59:35'),
(100, 47, 'Add', 'supplier.create', '2021-05-25 05:59:57', '2021-05-25 05:59:57'),
(101, 47, 'excel', 'supplier.excel', '2021-05-25 06:00:20', '2021-05-25 06:00:20'),
(102, 47, 'pdf', 'supplier.pdf', '2021-05-25 06:00:32', '2021-05-25 06:00:32'),
(103, 49, 'Add', 'biller.create', '2021-05-25 06:00:50', '2021-05-25 06:00:50'),
(104, 49, 'excel', 'biller.excel', '2021-05-25 06:01:05', '2021-05-25 06:01:05'),
(105, 49, 'pdf', 'biller.pdf', '2021-05-25 06:01:17', '2021-05-25 06:01:17'),
(106, 51, 'Add', 'saleperson.create', '2021-05-25 06:01:42', '2021-05-25 06:01:42'),
(107, 51, 'excel', 'saleperson.excel', '2021-05-25 06:01:55', '2021-05-25 06:01:55'),
(108, 51, 'pdf', 'saleperson.pdf', '2021-05-25 06:02:05', '2021-05-25 06:02:05'),
(109, 54, 'Add', 'stocks.create', '2021-05-25 06:03:31', '2021-05-25 06:03:31'),
(110, 54, 'excel', 'stocks.excel', '2021-05-25 06:03:56', '2021-05-25 06:03:56'),
(111, 54, 'pdf', 'stocks.pdf', '2021-05-25 06:04:06', '2021-05-25 06:04:06'),
(112, 55, 'Add', 'stocks.create', '2021-05-25 06:04:34', '2021-05-25 06:04:34'),
(113, 59, 'Add', 'request.create', '2021-05-25 06:05:30', '2021-05-25 06:05:30'),
(114, 64, 'Add', 'purchase.create', '2021-05-25 06:06:21', '2021-05-25 06:06:21'),
(115, 64, 'excel', 'purchase.excel', '2021-05-25 06:07:46', '2021-05-25 06:07:55'),
(116, 68, 'Add', 'sales.create', '2021-05-25 06:08:33', '2021-05-25 06:08:33'),
(117, 68, 'excel', 'sales.excel', '2021-05-25 06:09:34', '2021-05-25 06:09:34'),
(118, 74, 'Add', 'headcategory.create', '2021-05-25 06:11:37', '2021-05-25 06:11:37'),
(119, 76, 'Add', 'accountdetails.create', '2021-05-25 06:12:02', '2021-05-25 06:12:02'),
(120, 84, 'Add', 'permission.create', '2021-05-25 06:12:34', '2021-05-25 06:12:34'),
(121, 80, 'excel', 'reports.stock.excel', '2021-05-25 06:13:29', '2021-05-25 06:13:29'),
(122, 80, 'pdf', 'reports.stock.pdf', '2021-05-25 06:13:46', '2021-05-25 06:13:46'),
(123, 84, 'edit', 'permission.edit', '2021-05-25 09:00:08', '2021-05-25 09:00:08'),
(124, 1, 'View Billers', 'biller.index', '2021-06-14 00:54:10', '2021-06-14 00:54:10'),
(125, 1, 'View Suppliers', 'supplier.index', '2021-06-14 00:54:30', '2021-06-14 00:54:30'),
(126, 1, 'View Customers', 'customer.index', '2021-06-14 00:56:33', '2021-06-14 00:56:33'),
(127, 1, 'View Sale Persons', 'saleperson.index', '2021-06-14 00:58:16', '2021-06-14 00:58:16'),
(128, 1, 'View Brands', 'brands.index', '2021-06-14 00:58:31', '2021-06-14 00:58:31'),
(129, 1, 'View Categories', 'category.index', '2021-06-14 00:58:49', '2021-06-14 00:58:49'),
(130, 1, 'View Purchase Orders', 'purchase.index', '2021-06-14 00:59:12', '2021-06-14 00:59:12'),
(131, 1, 'View Sales', 'sales.index', '2021-06-14 00:59:28', '2021-06-14 00:59:28'),
(132, 1, 'Add Bank', 'bank.create', '2021-06-14 00:59:59', '2021-06-14 01:00:09'),
(133, 1, 'Add Brand', 'brands.create', '2021-06-14 01:00:22', '2021-06-14 01:00:22'),
(134, 1, 'Add Product', 'product.create', '2021-06-14 01:00:34', '2021-06-14 01:00:34'),
(135, 1, 'Add Finish Product', 'product.add', '2021-06-14 01:01:31', '2021-06-14 01:01:31'),
(136, 1, 'Add Stock', 'stocks.create', '2021-06-14 01:01:51', '2021-06-14 01:01:51'),
(137, 1, 'Add Purchase Order', 'purchase.create', '2021-06-14 01:02:04', '2021-06-14 01:02:04'),
(138, 1, 'Add Sale', 'sales.create', '2021-06-14 01:02:20', '2021-06-14 01:02:20'),
(139, 8, 'excel', 'trialBalance.excel', '2021-06-14 01:10:02', '2021-06-14 01:10:02'),
(140, 11, 'status', 'bank.status', '2021-06-14 01:11:22', '2021-06-14 01:11:22'),
(141, 10, 'Add City', 'city.create', '2021-06-14 01:18:37', '2021-06-14 01:18:37'),
(142, 48, 'Add City', 'city.create', '2021-06-14 01:20:56', '2021-06-14 01:20:56'),
(143, 49, 'status', 'biller.status', '2021-06-14 01:21:53', '2021-06-14 01:21:53'),
(144, 31, 'status', 'city.status', '2021-06-14 01:24:39', '2021-06-29 02:50:26'),
(145, 44, 'Add City', 'city.create', '2021-06-14 01:26:00', '2021-06-14 01:26:00'),
(146, 45, 'status', 'customer.status', '2021-06-14 01:26:54', '2021-06-14 01:26:54'),
(147, 63, 'Add Supplier', 'supplier.create', '2021-06-14 01:36:47', '2021-06-14 01:36:47'),
(148, 63, 'Add Biller', 'biller.create', '2021-06-14 01:37:08', '2021-06-14 01:37:08'),
(149, 63, 'Add Product', 'product.create', '2021-06-14 01:37:31', '2021-06-14 01:37:31'),
(150, 64, 'Add Payment', 'payment', '2021-06-14 01:38:58', '2021-06-14 01:39:08'),
(151, 58, 'Add Product', 'product.create', '2021-06-14 01:40:36', '2021-06-14 01:40:36'),
(152, 25, 'Add Unit', 'unit.create', '2021-06-14 01:48:42', '2021-06-14 01:48:42'),
(153, 25, 'Add Brand', 'brands.create', '2021-06-14 01:49:22', '2021-06-14 01:49:22'),
(154, 25, 'Add Category', 'category.create', '2021-06-14 01:49:38', '2021-06-14 01:49:38'),
(155, 25, 'Add Sub Category', 'subcategory.create', '2021-06-14 01:49:54', '2021-06-14 01:49:54'),
(156, 27, 'Add Sub Category', 'subcategory.create', '2021-06-14 01:50:13', '2021-06-14 01:50:13'),
(157, 27, 'Add Unit', 'unit.create', '2021-06-14 01:50:27', '2021-06-14 01:50:27'),
(158, 27, 'Add Brand', 'brands.create', '2021-06-14 01:50:36', '2021-06-14 01:50:36'),
(159, 27, 'Add Category', 'category.create', '2021-06-14 01:50:48', '2021-06-14 01:50:48'),
(160, 50, 'Add City', 'city.create', '2021-06-14 01:57:15', '2021-06-14 01:57:15'),
(161, 51, 'status', 'saleperson.status', '2021-06-14 01:58:01', '2021-06-14 01:58:01'),
(162, 67, 'Add Customer', 'customer.create', '2021-06-14 02:06:05', '2021-06-14 02:06:05'),
(163, 67, 'Add Biller', 'biller.create', '2021-06-14 02:06:27', '2021-06-14 02:06:27'),
(164, 67, 'Add Sale Person', 'saleperson.create', '2021-06-14 02:06:46', '2021-06-14 02:06:46'),
(165, 67, 'Add Product', 'product.create', '2021-06-14 02:07:02', '2021-06-14 02:07:02'),
(166, 67, 'Tax', 'tax', '2021-06-14 02:07:55', '2021-06-14 02:07:55'),
(169, 68, 'Add Payment', 'payment', '2021-06-14 02:10:12', '2021-06-14 02:10:12'),
(170, 68, 'Download Invoice', 'invoice', '2021-06-14 02:11:38', '2021-06-14 02:11:38'),
(171, 68, 'Download Gdr', 'gdr', '2021-06-14 02:12:01', '2021-06-14 02:12:01'),
(172, 55, 'excel', 'stocks.excel', '2021-06-14 02:15:15', '2021-06-14 02:15:15'),
(173, 22, 'Add Category', 'category.create', '2021-06-14 02:16:35', '2021-06-14 02:16:35'),
(174, 46, 'Add City', 'city.create', '2021-06-14 02:18:35', '2021-06-14 02:18:35'),
(175, 47, 'status', 'supplier.status', '2021-06-14 02:19:05', '2021-06-14 02:19:05'),
(176, 36, 'status', 'productTransfer.status', '2021-06-14 02:20:24', '2021-06-14 02:20:24'),
(177, 33, 'Add City', 'city.create', '2021-06-14 02:21:23', '2021-06-14 02:21:23'),
(178, 1, 'count biller', 'count biller', '2021-06-14 06:25:18', '2021-06-14 06:25:18'),
(179, 1, 'count supplier', 'count supplier', '2021-06-14 06:25:32', '2021-06-14 06:25:32'),
(180, 1, 'count customer', 'count customer', '2021-06-14 06:25:45', '2021-06-14 06:25:45'),
(181, 1, 'count sale person', 'count sale person', '2021-06-14 06:25:59', '2021-06-14 06:25:59'),
(182, 1, 'count brands', 'count brands', '2021-06-14 06:26:19', '2021-06-14 06:26:19'),
(183, 1, 'count category', 'count category', '2021-06-14 06:26:42', '2021-06-14 06:26:42'),
(184, 1, 'count purchase orders', 'count purchase orders', '2021-06-14 06:27:02', '2021-06-14 06:27:02'),
(185, 1, 'count sales', 'count sales', '2021-06-14 06:27:12', '2021-06-14 06:27:12'),
(186, 1, 'count raw materials', 'count raw materials', '2021-06-14 06:27:29', '2021-06-14 06:27:29'),
(187, 1, 'count packaging', 'count packaging', '2021-06-14 06:28:30', '2021-06-14 06:28:30'),
(188, 1, 'count finish', 'count finish', '2021-06-14 06:28:44', '2021-06-14 06:28:44'),
(190, 87, 'excel', 'reports.incomeStatement.excel', '2021-06-14 06:38:17', '2021-06-14 06:43:08'),
(191, 87, 'pdf', 'reports.incomeStatement.pdf', '2021-06-14 06:38:27', '2021-06-14 06:43:16'),
(192, 67, 'show cost', 'cost', '2021-06-14 07:42:01', '2021-06-14 07:42:09'),
(193, 68, 'status Delivered', 'status Delivered', '2021-06-14 07:53:35', '2021-06-14 07:53:35'),
(194, 68, 'excel', 'sales.excel', '2021-06-16 09:07:33', '2021-06-16 09:07:33'),
(195, 64, 'status', 'purchase.status', '2021-06-18 01:41:41', '2021-06-18 01:41:41'),
(196, 89, 'status', 'billerUpdateRequest.status', '2021-06-18 04:52:41', '2021-06-18 05:05:30'),
(197, 90, 'status', 'customerUpdateRequest.status', '2021-06-18 04:53:03', '2021-06-18 05:05:23'),
(198, 92, 'status', 'supplierUpdateRequest.status', '2021-06-18 04:53:22', '2021-06-18 05:05:17'),
(199, 91, 'status', 'salepersonUpdateRequest.status', '2021-06-18 04:53:39', '2021-06-18 05:05:09'),
(200, 89, 'show', 'billerUpdateRequest.show', '2021-06-18 05:03:45', '2021-06-18 05:03:45'),
(201, 90, 'show', 'customerUpdateRequest.show', '2021-06-18 05:04:07', '2021-06-18 05:04:07'),
(202, 92, 'show', 'supplierUpdateRequest.show', '2021-06-18 05:04:24', '2021-06-18 05:04:24'),
(203, 91, 'show', 'salepersonUpdateRequest.show', '2021-06-18 05:04:45', '2021-06-18 05:04:45'),
(204, 28, 'edit', 'product.finishProducts.edit', '2021-06-18 06:58:06', '2021-06-18 06:58:06'),
(205, 93, 'show', 'productUpdateRequest.show', '2021-06-21 00:57:34', '2021-06-21 00:57:34'),
(206, 93, 'status', 'productUpdateRequest.status', '2021-06-21 00:57:46', '2021-06-21 00:57:54'),
(208, 26, 'variants', 'variants.show', '2021-06-25 01:04:49', '2021-06-25 01:04:49'),
(209, 28, 'variants', 'variants.show', '2021-06-25 02:14:32', '2021-06-25 02:14:32');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pro_code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `p_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `alert_quantity` bigint(20) DEFAULT 5,
  `unit_id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `s_cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visibility` int(11) NOT NULL DEFAULT 1,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `vstatus` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `pro_code`, `pro_name`, `p_type`, `weight`, `description`, `cost`, `price`, `alert_quantity`, `unit_id`, `brand_id`, `cat_id`, `s_cat_id`, `status`, `image`, `visibility`, `updated_by`, `created_by`, `updated_at`, `created_at`, `vstatus`) VALUES
(1, '1357433', '3 Ply Surgical Mask With Nose Pin', 'Finished', NULL, NULL, 120, 0, 10, 1, 1, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(2, '1357434', 'Face Shield', 'Finished', NULL, NULL, 142, 0, 10, 1, 2, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(3, '1357435', 'Face Shield Kids', 'Finished', NULL, NULL, 142, 0, 10, 1, 2, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(4, '1357436', 'Oximeter ', 'Finished', NULL, NULL, 2700, 0, 10, 1, 3, 2, 2, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(5, '1357437', 'N95 With Filter ', 'Finished', NULL, NULL, 1585, 0, 10, 1, 4, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(6, '1357438', 'N95 Without  Filter ', 'Finished', NULL, NULL, 1722, 0, 10, 1, 4, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(7, '1357439', 'KN95 With Filter ', 'Finished', NULL, NULL, 375, 0, 10, 1, 5, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(8, '1357440', 'Disposible Gloves ', 'Finished', NULL, NULL, 80, 0, 10, 1, 6, 1, 3, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(9, '1357441', 'Latex Gloves  ', 'Finished', NULL, NULL, 928, 0, 10, 1, 7, 1, 3, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(10, '1357442', 'P.P.E suite ', 'Finished', NULL, NULL, 515, 0, 10, 1, 2, 1, 4, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(11, '1357443', 'Gown Suite Blue ', 'Finished', NULL, NULL, 300, 0, 10, 1, 2, 1, 4, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(12, '1357444', 'Plastic Pump', 'Packaging', NULL, NULL, 0, 0, 10, 1, 8, 3, 5, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(13, '1357445', 'Infrared Thermometer Machine', 'Finished', NULL, NULL, 6100, 0, 10, 1, 9, 2, 2, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(14, '1357446', 'Plastic Sanitizer Bottle', 'Packaging', NULL, NULL, 20, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(15, '1357447', 'Hand wash Bottle', 'Packaging', NULL, NULL, 65, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(16, '1357448', 'sanitizer Label', 'Packaging', NULL, NULL, 4, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(17, '1357449', 'sanitizer spray Bottle', 'Packaging', NULL, NULL, 35, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(18, '1357450', 'Plastic Disinfectant Bottle', 'Packaging', NULL, NULL, 120, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(19, '1357451', 'Plastic Bottle Cap', 'Packaging', NULL, NULL, 5, 0, 10, 1, 8, 3, 8, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(20, '1357452', 'Plastic Amber Luxury Bottle', 'Packaging', NULL, NULL, 75, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(21, '1357453', 'Plastic Jar', 'Packaging', NULL, NULL, 150, 0, 10, 1, 8, 3, 9, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(22, '1357454', 'Infrared Gun', 'Finished', NULL, NULL, 2200, 0, 10, 1, 9, 4, 10, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(23, '1357455', 'Jade Roller ', 'Finished', NULL, NULL, 600, 0, 10, 1, 2, 5, 11, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(24, '1357456', 'Darma Roller', 'Finished', NULL, NULL, 400, 0, 10, 1, 2, 5, 11, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(25, '1357457', 'Castor oil ', 'Raw Material', NULL, NULL, 650, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(26, '1357458', 'Glass Amber Dropper Bottle', 'Packaging', NULL, NULL, 50, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(27, '1357459', 'Glass Froasted Dropper bottle', 'Packaging', NULL, NULL, 50, 0, 10, 1, 8, 3, 13, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(28, '1357460', 'Dropper Cap', 'Packaging', NULL, NULL, 0, 0, 10, 1, 8, 3, 13, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(29, '1357461', 'Plastic Lotion Pump Bottle Noor', 'Packaging', NULL, NULL, 240, 0, 10, 1, 10, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(30, '1357462', 'Plastic Lotion Pump Bottle Chinese', 'Packaging', NULL, NULL, 0, 0, 10, 1, 11, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(31, '1357463', 'Plastic Jar Cap', 'Packaging', NULL, NULL, 145, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(32, '1357464', 'Vitamin C Serum 30ml', 'Finished', NULL, NULL, 650, 0, 10, 1, 2, 5, 14, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(33, '1357465', 'Vitamin C Serum A 30ml', 'Finished', NULL, NULL, 120, 0, 10, 1, 2, 5, 14, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(34, '1357466', 'Glycerein steak ', 'Raw Material', NULL, NULL, 350, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(35, '1357467', 'Grap sced oil', 'Raw Material', NULL, NULL, 2200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(36, '1357468', 'IPM oil csso', 'Raw Material', NULL, NULL, 550, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(37, '1357469', 'Vitamin C Serum (Dr Rasheel)', 'Finished', NULL, NULL, 330, 0, 10, 1, 12, 5, 14, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(38, '1357470', 'Hyaluronic Acid Serum', 'Finished', NULL, NULL, 650, 0, 10, 1, 2, 5, 14, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(39, '1357471', 'Aloevera Gel', 'Finished', NULL, NULL, 350, 0, 10, 1, 2, 5, 15, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(40, '1357472', 'Pumpkin oil', 'Raw Material', NULL, NULL, 5000, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(41, '1357473', 'Steraic acid c540', 'Raw Material', NULL, NULL, 0.65, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(42, '1357474', 'Petroluem Jelly', 'Finished', NULL, NULL, 42, 0, 10, 1, 2, 5, 15, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(43, '1357475', 'Ultimate Body Butter', 'Finished', NULL, NULL, 390, 0, 10, 1, 2, 5, 16, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(44, '1357476', 'White oil', 'Raw Material', NULL, NULL, 240, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(45, '1357477', 'Jogoba oil ', 'Raw Material', NULL, NULL, 4000, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(46, '1357478', 'Cocoa Butter', 'Finished', NULL, NULL, 470, 0, 10, 1, 2, 5, 16, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(47, '1357479', 'Shea Butter', 'Finished', NULL, NULL, 380, 0, 10, 1, 2, 5, 16, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(48, '1357480', 'Coconut oil Tin', 'Raw Material', NULL, NULL, 300, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(49, '1357481', 'Hair Repair oil', 'Finished', NULL, NULL, 410, 0, 10, 1, 2, 7, 17, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(50, '1357482', 'Aloevera Gel Lables', 'Packaging', NULL, NULL, 15, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(51, '1357483', 'Vitamin C Lables ', 'Packaging', NULL, NULL, 19, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(52, '1357484', 'Hyaluronic Acid Box', 'Packaging', NULL, NULL, 18, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(53, '1357485', 'Vitamin C Box', 'Packaging', NULL, NULL, 19, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(54, '1357486', 'Aloevera Gel Box', 'Packaging', NULL, NULL, 22, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(55, '1357487', 'Hand Santizer Box ', 'Packaging', NULL, NULL, 54, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(56, '1357488', 'Hand wash Box', 'Packaging', NULL, NULL, 53, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(57, '1357489', 'Hand Sanitizer spray Box ', 'Packaging', NULL, NULL, 67, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(58, '1357490', 'Hyaluronic Acid Serum Label', 'Packaging', NULL, NULL, 6, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(59, '1357491', 'Hand Wash Label', 'Packaging', NULL, NULL, 9.1, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(60, '1357492', 'Pet Bottles', 'Packaging', NULL, NULL, 15, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(61, '1357493', 'Mask Box A qlty', 'Packaging', NULL, NULL, 16.6, 0, 10, 1, 8, 3, 19, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(62, '1357494', 'Mask Box B qlty', 'Packaging', NULL, NULL, 11.7, 0, 10, 1, 8, 3, 19, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(63, '1357495', 'Ultimate Butter box', 'Packaging', NULL, NULL, 14.5, 0, 10, 1, 8, 3, 19, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(64, '1357496', 'Shea Butter Box', 'Packaging', NULL, NULL, 14.5, 0, 10, 1, 8, 3, 19, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(65, '1357497', 'Bottles Pumps ', 'Packaging', NULL, NULL, 19.2, 0, 10, 1, 8, 3, 5, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(66, '1357498', 'Gift Wooden Box', 'Packaging', NULL, NULL, 1100, 0, 10, 1, 8, 3, 20, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(67, '1357499', 'Petroleum Jelly Label', 'Packaging', NULL, NULL, 4.5, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(68, '1357500', 'Empty  jar', 'Packaging', NULL, NULL, 9, 0, 10, 1, 8, 3, 6, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(69, '1357501', 'Gift Box posch care', 'Packaging', NULL, NULL, 180, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(70, '1357502', 'Butter Paper', 'Packaging', NULL, NULL, 3, 0, 10, 1, 2, 3, 21, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(71, '1357503', 'Gift Bags', 'Packaging', NULL, NULL, 20, 0, 10, 1, 8, 3, 22, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(72, '1357504', 'Gifts Box Stickers', 'Packaging', NULL, NULL, 20, 0, 10, 1, 8, 3, 23, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(73, '1357505', 'Ferrero Choclates', 'Packaging', NULL, NULL, 219, 0, 10, 1, 8, 3, 24, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(74, '1357506', 'Hair repair oil box', 'Packaging', NULL, NULL, 20, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(75, '1357507', 'Hair Repair oil lables ', 'Packaging', NULL, NULL, 6, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(76, '1357508', 'Insane Growth  oil lables ', 'Packaging', NULL, NULL, 7, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(77, '1357509', 'Insane Growth  Oil Box', 'Packaging', NULL, NULL, 20, 0, 10, 1, 8, 3, 18, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(78, '1357510', 'Alovera powder', 'Raw Material', NULL, NULL, 54, 0, 10, 5, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(79, '1357511', 'Alovera gel Material', 'Raw Material', NULL, NULL, 400, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(80, '1357512', 'IPA  ', 'Raw Material', NULL, NULL, 350, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(81, '1357513', 'Aloevera Gel Material Chinese', 'Raw Material', NULL, NULL, 0, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(82, '1357514', 'alovera essence', 'Raw Material', NULL, NULL, 180, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(83, '1357515', 'avocodo oil cold pressed ', 'Raw Material', NULL, NULL, 9400, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(84, '1357516', 'Bio Sodium hyaluronate kg', 'Raw Material', NULL, NULL, 27000, 0, 10, 5, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(85, '1357517', 'body butter Material ', 'Raw Material', NULL, NULL, 350, 0, 10, 5, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(86, '1357518', 'bootle for coca propul and phenoxy', 'Raw Material', NULL, NULL, 50, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(87, '1357519', 'Carbomer ', 'Raw Material', NULL, NULL, 9600, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(88, '1357520', 'carbopol', 'Raw Material', NULL, NULL, 1500, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(89, '1357521', 'cetyl alchohre', 'Raw Material', NULL, NULL, 0.45, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(90, '1357522', 'citric acid', 'Raw Material', NULL, NULL, 0.54, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(91, '1357523', 'coca propyl betame', 'Raw Material', NULL, NULL, 0.2, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(92, '1357524', 'Coco butter Material', 'Raw Material', NULL, NULL, 0.64, 0, 10, 5, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(93, '1357525', 'Coconat oil', 'Raw Material', NULL, NULL, 0.3, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(94, '1357526', 'Dettol', 'Raw Material', NULL, NULL, 0.9, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(95, '1357527', 'Disinfectant  liquid Material', 'Raw Material', NULL, NULL, 250, 0, 10, 3, 2, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(96, '1357528', 'distilled water', 'Raw Material', NULL, NULL, 15, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(97, '1357529', 'Edta', 'Raw Material', NULL, NULL, 200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(98, '1357530', 'fragrance  loverdose tatton ', 'Raw Material', NULL, NULL, 1680, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(99, '1357531', 'fragrance luzii poeny blush suede', 'Raw Material', NULL, NULL, 1890, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(100, '1357532', 'Gallon', 'Raw Material', NULL, NULL, 150, 0, 10, 1, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(101, '1357533', 'glycerin', 'Raw Material', NULL, NULL, 200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(102, '1357534', 'Gp-200 ', 'Raw Material', NULL, NULL, 1.2, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(103, '1357535', 'grape seed oil', 'Raw Material', NULL, NULL, 2200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(104, '1357536', 'Hair repair oil Material', 'Raw Material', NULL, NULL, 27.94, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(105, '1357537', 'Hand Sanitizer Material', 'Raw Material', NULL, NULL, 1950, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(106, '1357538', 'Hand Sanitizer Material ', 'Raw Material', NULL, NULL, 470, 0, 10, 3, 13, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(107, '1357539', 'Hand Sanitizer Spray Liqued Material', 'Raw Material', NULL, NULL, 1000, 0, 10, 3, 2, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(108, '1357540', 'Hand wash  Material', 'Raw Material', NULL, NULL, 70, 0, 10, 3, 2, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(109, '1357541', 'Hand wash  Material', 'Raw Material', NULL, NULL, 90, 0, 10, 3, 14, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(110, '1357542', 'Hemp Seed oil cold pressed ', 'Raw Material', NULL, NULL, 11400, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(111, '1357543', 'Hyaluronic Acid Material', 'Raw Material', NULL, NULL, 0, 0, 10, 3, 13, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(112, '1357544', 'Hydrogen ', 'Raw Material', NULL, NULL, 180, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(113, '1357545', 'ipa 99% taiwan', 'Raw Material', NULL, NULL, 0.35, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(114, '1357546', 'Keratin', 'Raw Material', NULL, NULL, 49000, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(115, '1357547', 'lemon grass oil ', 'Raw Material', NULL, NULL, 500, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(116, '1357548', 'methyl perabon', 'Raw Material', NULL, NULL, 1200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(117, '1357549', 'multipurple rose  bottle ', 'Raw Material', NULL, NULL, 140, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(118, '1357550', 'Natural Cocoa Butter Material', 'Raw Material', NULL, NULL, 7500, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(119, '1357551', 'NIACIMID', 'Raw Material', NULL, NULL, 400, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(120, '1357552', 'olive oil  ', 'Raw Material', NULL, NULL, 8.8, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(121, '1357553', 'Organic  extra virgin olive oil', 'Raw Material', NULL, NULL, 8.8, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(122, '1357554', 'organic almond  oil ', 'Raw Material', NULL, NULL, 9600, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(123, '1357555', 'Organic Apricot  kernet oli cold pressed  ', 'Raw Material', NULL, NULL, 8200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(124, '1357556', 'organic argan oil ', 'Raw Material', NULL, NULL, 15500, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(125, '1357557', 'Organic Grapeseed oli cold pressed  ', 'Raw Material', NULL, NULL, 16000, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(126, '1357558', 'Organic Rosehip  kernel oli cold pressed  ', 'Raw Material', NULL, NULL, 22000, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(127, '1357559', 'Organic virgin linseed oil ', 'Raw Material', NULL, NULL, 8400, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(128, '1357560', 'PETROLIUM JELLY Material ', 'Raw Material', NULL, NULL, 1000, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(129, '1357561', 'polawyas', 'Raw Material', NULL, NULL, 1.2, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(130, '1357562', 'rose water ', 'Raw Material', NULL, NULL, 120, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(131, '1357563', 'sheabutter Material ', 'Raw Material', NULL, NULL, 2200, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(132, '1357564', 'Vitamin C fach wash  tubes', 'Packaging', NULL, NULL, 27.5, 0, 10, 1, 2, 3, 25, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(133, '1357565', 'Tea Tree facewash tubes', 'Packaging', NULL, NULL, 27.5, 0, 10, 1, 2, 3, 25, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(134, '1357566', 'Plastic mist  Spray ', 'Packaging', NULL, NULL, 0, 0, 10, 1, 8, 3, 8, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(135, '1357567', 'Antibacterial Hand Wash Label', 'Packaging', NULL, NULL, 9.1, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(136, '1357568', 'Hyaluronic acid Label ', 'Packaging', NULL, NULL, 9.1, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(137, '1357569', 'Ultimat Body Butter label', 'Packaging', NULL, NULL, 9.1, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(138, '1357570', 'Cocoa Butter label', 'Packaging', NULL, NULL, 9.1, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(139, '1357571', 'Shea Butter Label ', 'Packaging', NULL, NULL, 9.1, 0, 10, 1, 8, 3, 7, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(140, '1357572', '3 Ply Surgical Mask With Nose Pin', 'Finished', NULL, NULL, 250, 0, 10, 1, 15, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(141, '1357573', '3 Ply Surgical Mask With Nose Pin', 'Finished', NULL, NULL, 250, 0, 10, 1, 16, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(142, '1357574', '3 Ply Surgical Mask With Nose Pin', 'Finished', NULL, NULL, 200, 0, 10, 1, 2, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(143, '1357575', 'Hand sanitizer 150ml', 'Finished', NULL, NULL, 130, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(144, '1357576', 'Hand Sanitizer 100ml', 'Finished', NULL, NULL, 100, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(145, '1357577', 'Handwash (Antibacterial) 250ml Blue', 'Finished', NULL, NULL, 160, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(146, '1357578', 'Handwash (Antibacterial) 250ml White', 'Finished', NULL, NULL, 95, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(147, '1357579', 'Hand Sanitizer 70ml', 'Finished', NULL, NULL, 70, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(148, '1357580', 'Disinfectant spray', 'Finished', NULL, NULL, 195, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 1),
(149, '1357581', 'Sles 70%', 'Raw Material', NULL, NULL, 400, 0, 0, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(150, '1357582', 'Hand wash 500ml', 'Finished', NULL, NULL, 168, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(151, '1357583', 'Tea Tree Facewash 100ml', 'Finished', NULL, NULL, 76, 0, 10, 1, 2, 8, 27, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(152, '1357584', 'strach powder', 'Raw Material', NULL, NULL, 150, 0, 0, 5, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(153, '1357585', 'Sunflower oil Refined ', 'Raw Material', NULL, NULL, 900, 0, 0, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(154, '1357586', 'Sunflower oil textron', 'Raw Material', NULL, NULL, 8200, 0, 0, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(155, '1357587', 'Vitamin C Facewash ', 'Finished', NULL, NULL, 95, 0, 10, 1, 2, 8, 27, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(156, '1357588', 'Hand Sanitizer 500ml', 'Finished', NULL, NULL, 405, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(157, '1357589', 'Hand wash 500ml Red Rose', 'Finished', NULL, NULL, 83, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(158, '1357590', 'Hand wash 500ml Lemon', 'Finished', NULL, NULL, 83, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(159, '1357591', 'Hand wash 500ml Lavander', 'Finished', NULL, NULL, 83, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(160, '1357592', 'Hand wash 500ml Blueberry', 'Finished', NULL, NULL, 83, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(161, '1357593', 'Vanillyl butyl ether', 'Raw Material', NULL, NULL, 491, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(162, '1357594', 'Vitamin C Serum 5% Material', 'Raw Material', NULL, NULL, 2200, 0, 10, 3, 17, 6, 28, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(163, '1357595', 'Vitamin E', 'Raw Material', NULL, NULL, 4000, 0, 0, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(164, '1357596', 'Hand wash 500ml Strawberry', 'Finished', NULL, NULL, 0, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(165, '1357597', 'Hand Sanitizer 100ml Z.', 'Finished', NULL, NULL, 88, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(166, '1357598', 'Hand Sanitizer Kids 60ml Blue Z.', 'Finished', NULL, NULL, 58, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(167, '1357599', 'Hand Sanitizer Kids 60ml Purple Z.', 'Finished', NULL, NULL, 58, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(168, '1357600', 'Hand Sanitizer Spray 100ml Z.', 'Finished', NULL, NULL, 100, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(169, '1357601', 'Hand Sanitizer 60ml Z.', 'Finished', NULL, NULL, 63, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(170, '1357602', 'Whitening face End body mask', 'Raw Material', NULL, NULL, 11465, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(171, '1357603', 'Xasthan Gun', 'Raw Material', NULL, NULL, 350, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(172, '1357604', 'Vitamin C Serum 3% Material', 'Raw Material', NULL, NULL, 1500, 0, 10, 3, 17, 6, 28, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(173, '1357605', 'Vitamin C Serum ETVC Material', 'Raw Material', NULL, NULL, 0, 0, 10, 3, 17, 6, 28, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(174, '1357606', 'Aloevera Gel Material Local', 'Raw Material', NULL, NULL, 400, 0, 10, 3, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(175, '1357607', '3 Ply Surgical Mask With Nose Pin (KIDS)', 'Finished', NULL, NULL, 180, 0, 10, 1, 2, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(176, '1357608', 'Insane Growth Oil 100ml', 'Finished', NULL, NULL, 0, 0, 10, 1, 2, 7, 17, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(177, '1357609', '3 Ply Surgical Mask With Nose Pin', 'Finished', NULL, NULL, 250, 0, 10, 1, 8, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(178, '1357610', 'Hand Sanitizer  5000ml', 'Finished', NULL, NULL, 3, 150, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(179, '1357611', 'Hand Sanitizer 500ml', 'Finished', NULL, NULL, 405, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(180, '1357612', 'Hand Sanitizer  250ml', 'Finished', NULL, NULL, 208, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(181, '1357613', 'Hand Sanitizer 100ml', 'Finished', NULL, NULL, 100, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(182, '1357614', 'Hand sanitizer  70ml', 'Finished', NULL, NULL, 70, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(183, '1357615', 'Hand sanitizer  60ml', 'Finished', NULL, NULL, 63, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(184, '1357616', 'Hand wash 500ml', 'Finished', NULL, NULL, 122, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(185, '1357617', 'Hand wash 250ml', 'Finished', NULL, NULL, 94, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(186, '1357618', 'Hand Sanitizer Spray 100ml Z.', 'Finished', NULL, NULL, 100, 0, 10, 1, 2, 8, 26, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(187, '1357619', 'Latex Gloves', 'Finished', NULL, NULL, 928, 0, 10, 1, 2, 8, 30, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(188, '1357620', 'Polyster Mask', 'Finished', NULL, NULL, 90, 0, 10, 1, 2, 1, 1, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(189, '1357621', 'Alovera Gell Material', 'Raw Material', NULL, NULL, 180, 0, 10, 3, 8, 5, 15, 1, NULL, 1, NULL, NULL, NULL, NULL, 0),
(190, '1357622', 'Disinfectant spray Label', 'Packaging', NULL, NULL, 4, 0, 10, 1, 8, 6, 12, 1, NULL, 1, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products_update_request`
--

CREATE TABLE `products_update_request` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pro_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` double DEFAULT NULL,
  `cost` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `alert_quantity` bigint(20) NOT NULL,
  `unit_id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` bigint(20) UNSIGNED DEFAULT NULL,
  `cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `s_cat_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `visibility` int(11) NOT NULL DEFAULT 1,
  `p_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_transfer_history`
--

CREATE TABLE `product_transfer_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `transfer_date` date NOT NULL,
  `from_w` bigint(20) UNSIGNED DEFAULT NULL,
  `to_w` bigint(20) UNSIGNED DEFAULT NULL,
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `quantity` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_variants`
--

CREATE TABLE `product_variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `cost` bigint(20) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_variants`
--

INSERT INTO `product_variants` (`id`, `p_id`, `name`, `created_at`, `updated_at`, `status`, `cost`, `price`) VALUES
(1, 1, '1357433 - 3 Ply Surgical Mask With Nose Pin-Ultrasonic punch', NULL, NULL, 0, NULL, NULL),
(2, 2, '1357434 - Face Shield-Large', NULL, NULL, 0, NULL, NULL),
(3, 2, '1357434 - Face Shield-Small', NULL, NULL, 0, NULL, NULL),
(4, 9, '1357441 - Latex Gloves-Medium', NULL, NULL, 0, NULL, NULL),
(5, 12, '1357444 - Plastic Pump-20mm-Transparent', NULL, NULL, 0, NULL, NULL),
(6, 12, '1357444 - Plastic Pump-28mm-Transparent', NULL, NULL, 0, NULL, NULL),
(7, 12, '1357444 - Plastic Pump-28mm-White', NULL, NULL, 0, NULL, NULL),
(8, 14, '1357446 - Plastic Sanitizer Bottle-1000ml', NULL, NULL, 0, NULL, NULL),
(9, 14, '1357446 - Plastic Sanitizer Bottle-100ml', NULL, NULL, 0, NULL, NULL),
(10, 14, '1357446 - Plastic Sanitizer Bottle-150ml', NULL, NULL, 0, NULL, NULL),
(11, 14, '1357446 - Plastic Sanitizer Bottle-250ml', NULL, NULL, 0, NULL, NULL),
(12, 14, '1357446 - Plastic Sanitizer Bottle-500ml', NULL, NULL, 0, NULL, NULL),
(13, 14, '1357446 - Plastic Sanitizer Bottle-60ml', NULL, NULL, 0, NULL, NULL),
(14, 14, '1357446 - Plastic Sanitizer Bottle-70ml', NULL, NULL, 0, NULL, NULL),
(15, 15, '1357447 - Hand wash Bottle-250ml', NULL, NULL, 0, NULL, NULL),
(16, 16, '1357448 - sanitizer Label-100ml', NULL, NULL, 0, NULL, NULL),
(17, 16, '1357448 - sanitizer Label-150ml', NULL, NULL, 0, NULL, NULL),
(18, 16, '1357448 - sanitizer Label-500ml', NULL, NULL, 0, NULL, NULL),
(19, 16, '1357448 - sanitizer Label-60ml', NULL, NULL, 0, NULL, NULL),
(20, 16, '1357448 - sanitizer Label-70ml', NULL, NULL, 0, NULL, NULL),
(21, 17, '1357449 - sanitizer spray Bottle-100ml', NULL, NULL, 0, NULL, NULL),
(22, 18, '1357450 - Plastic Disinfectant Bottle-500ml', NULL, NULL, 0, NULL, NULL),
(23, 19, '1357451 - Plastic Bottle Cap-1000ml-Blue', NULL, NULL, 0, NULL, NULL),
(24, 19, '1357451 - Plastic Bottle Cap-1000ml-Transparent', NULL, NULL, 0, NULL, NULL),
(25, 19, '1357451 - Plastic Bottle Cap-100ml-Transparent', NULL, NULL, 0, NULL, NULL),
(26, 19, '1357451 - Plastic Bottle Cap-250ml-Blue', NULL, NULL, 0, NULL, NULL),
(27, 19, '1357451 - Plastic Bottle Cap-250ml-Transaparent', NULL, NULL, 0, NULL, NULL),
(28, 19, '1357451 - Plastic Bottle Cap-500ml-Blue', NULL, NULL, 0, NULL, NULL),
(29, 19, '1357451 - Plastic Bottle Cap-500ml-Transparent', NULL, NULL, 0, NULL, NULL),
(30, 19, '1357451 - Plastic Bottle Cap-60ml-Transparent', NULL, NULL, 0, NULL, NULL),
(31, 19, '1357451 - Plastic Bottle Cap-70ml-Red', NULL, NULL, 0, NULL, NULL),
(32, 20, '1357452 - Plastic Amber Luxury Bottle-100ml', NULL, NULL, 0, NULL, NULL),
(33, 21, '1357453 - Plastic Jar-300gm', NULL, NULL, 0, NULL, NULL),
(34, 21, '1357453 - Plastic Jar-100gm', NULL, NULL, 0, NULL, NULL),
(35, 21, '1357453 - Plastic Jar-300ml-Transparent', NULL, NULL, 0, NULL, NULL),
(36, 26, '1357458 - Glass Amber Dropper Bottle-30ml-Flat Shoulder', NULL, NULL, 0, NULL, NULL),
(37, 26, '1357458 - Glass Amber Dropper Bottle-30ml-Round Shoulder', NULL, NULL, 0, NULL, NULL),
(38, 27, '1357459 - Glass Froasted Dropper bottle-30ml-Blue-Flat Shoulder', NULL, NULL, 0, NULL, NULL),
(39, 27, '1357459 - Glass Froasted Dropper bottle-30ml-Flat Shoulder', NULL, NULL, 0, NULL, NULL),
(40, 27, '1357459 - Glass Froasted Dropper bottle-30ml-Round Shoulder', NULL, NULL, 0, NULL, NULL),
(41, 28, '1357460 - Dropper Cap-30ml-Black-Aluminium', NULL, NULL, 0, NULL, NULL),
(42, 28, '1357460 - Dropper Cap-30ml-Black-Plastic', NULL, NULL, 0, NULL, NULL),
(43, 28, '1357460 - Dropper Cap-30ml-Gold Black-Aluminium', NULL, NULL, 0, NULL, NULL),
(44, 28, '1357460 - Dropper Cap-30ml-Gold White-Aluminium', NULL, NULL, 0, NULL, NULL),
(45, 28, '1357460 - Dropper Cap-30ml-Silver Black-Aluminium', NULL, NULL, 0, NULL, NULL),
(46, 28, '1357460 - Dropper Cap-30ml-Silver White-Aluminium', NULL, NULL, 0, NULL, NULL),
(47, 28, '1357460 - Dropper Cap-30ml-White-Plastic', NULL, NULL, 0, NULL, NULL),
(48, 29, '1357461 - Plastic Lotion Pump Bottle Noor-100ml-Normal Base', NULL, NULL, 0, NULL, NULL),
(49, 30, '1357462 - Plastic Lotion Pump Bottle Chinese-100ml-Normal Base', NULL, NULL, 0, NULL, NULL),
(50, 31, '1357463 - Plastic Jar Cap-300ml-Transparent', NULL, NULL, 0, NULL, NULL),
(51, 38, '1357470 - Hyaluronic Acid Serum-30ml', NULL, NULL, 0, NULL, NULL),
(52, 39, '1357471 - Aloevera Gel-100ml', NULL, NULL, 0, NULL, NULL),
(53, 39, '1357471 - Aloevera Gel-500ml', NULL, NULL, 0, NULL, NULL),
(54, 42, '1357474 - Petroluem Jelly-100gm', NULL, NULL, 0, NULL, NULL),
(55, 42, '1357474 - Petroluem Jelly-50gm', NULL, NULL, 0, NULL, NULL),
(56, 43, '1357475 - Ultimate Body Butter-100gm', NULL, NULL, 0, NULL, NULL),
(57, 46, '1357478 - Cocoa Butter-100gm', NULL, NULL, 0, NULL, NULL),
(58, 47, '1357479 - Shea Butter-100gm', NULL, NULL, 0, NULL, NULL),
(59, 47, '1357479 - Shea Butter-300gm', NULL, NULL, 0, NULL, NULL),
(60, 49, '1357481 - Hair Repair oil-100ml', NULL, NULL, 0, NULL, NULL),
(61, 50, '1357482 - Aloevera Gel Lables-100ml', NULL, NULL, 0, NULL, NULL),
(62, 50, '1357482 - Aloevera Gel Lables-500ml', NULL, NULL, 0, NULL, NULL),
(63, 51, '1357483 - Vitamin C Lables-30ml', NULL, NULL, 0, NULL, NULL),
(64, 52, '1357484 - Hyaluronic Acid Box-30ml', NULL, NULL, 0, NULL, NULL),
(65, 53, '1357485 - Vitamin C Box-30ml', NULL, NULL, 0, NULL, NULL),
(66, 54, '1357486 - Aloevera Gel Box-100ml', NULL, NULL, 0, NULL, NULL),
(67, 55, '1357487 - Hand Santizer Box-1000ml', NULL, NULL, 0, NULL, NULL),
(68, 55, '1357487 - Hand Santizer Box-100ml', NULL, NULL, 0, NULL, NULL),
(69, 55, '1357487 - Hand Santizer Box-250ml', NULL, NULL, 0, NULL, NULL),
(70, 55, '1357487 - Hand Santizer Box-500ml', NULL, NULL, 0, NULL, NULL),
(71, 55, '1357487 - Hand Santizer Box-60ml', NULL, NULL, 0, NULL, NULL),
(72, 56, '1357488 - Hand wash Box-500ml', NULL, NULL, 0, NULL, NULL),
(73, 57, '1357489 - Hand Sanitizer spray Box-100ml', NULL, NULL, 0, NULL, NULL),
(74, 59, '1357491 - Hand Wash Label-500ml-Blueberry', NULL, NULL, 0, NULL, NULL),
(75, 59, '1357491 - Hand Wash Label-500ml-Lavander', NULL, NULL, 0, NULL, NULL),
(76, 59, '1357491 - Hand Wash Label-500ml-Lemon', NULL, NULL, 0, NULL, NULL),
(77, 59, '1357491 - Hand Wash Label-500ml-Red Rose', NULL, NULL, 0, NULL, NULL),
(78, 60, '1357492 - Pet Bottles-500ml', NULL, NULL, 0, NULL, NULL),
(79, 66, '1357498 - Gift Wooden Box-8x11x4.5', NULL, NULL, 0, NULL, NULL),
(80, 66, '1357498 - Gift Wooden Box-8x8x4.5', NULL, NULL, 0, NULL, NULL),
(81, 67, '1357499 - Petroleum Jelly Label-100gm', NULL, NULL, 0, NULL, NULL),
(82, 67, '1357499 - Petroleum Jelly Label-50gm', NULL, NULL, 0, NULL, NULL),
(83, 68, '1357500 - Empty jar-Large', NULL, NULL, 0, NULL, NULL),
(84, 68, '1357500 - Empty jar-Medium', NULL, NULL, 0, NULL, NULL),
(85, 68, '1357500 - Empty jar-Small', NULL, NULL, 0, NULL, NULL),
(86, 71, '1357503 - Gift Bags-Large-Black', NULL, NULL, 0, NULL, NULL),
(87, 71, '1357503 - Gift Bags-Small-Black', NULL, NULL, 0, NULL, NULL),
(88, 71, '1357503 - Gift Bags-Small-White', NULL, NULL, 0, NULL, NULL),
(89, 72, '1357504 - Gifts Box Stickers-Blue', NULL, NULL, 0, NULL, NULL),
(90, 72, '1357504 - Gifts Box Stickers-Red', NULL, NULL, 0, NULL, NULL),
(91, 132, '1357564 - Vitamin C fach wash tubes-100ml', NULL, NULL, 0, NULL, NULL),
(92, 133, '1357565 - Tea Tree facewash tubes-100ml', NULL, NULL, 0, NULL, NULL),
(93, 134, '1357566 - Plastic mist  Spray-100ml', NULL, NULL, 0, NULL, NULL),
(94, 135, '1357567 - Antibacterial Hand Wash Label-500ml', NULL, NULL, 0, NULL, NULL),
(95, 136, '1357568 - Hyaluronic acid Label-30ml', NULL, NULL, 0, NULL, NULL),
(96, 138, '1357569 - Cocoa Butter label-100gm', NULL, NULL, 0, NULL, NULL),
(97, 139, '1357569 - Shea Butter label-100gm', NULL, NULL, 0, NULL, NULL),
(98, 139, '1357569 - Shea Butter label-300gm', NULL, NULL, 0, NULL, NULL),
(99, 137, '1357569 - Ultimat Body Butter label-100gm', NULL, NULL, 0, NULL, NULL),
(100, 142, '1357572 - 3 Ply Surgical Mask With Nose Pin-Glue', NULL, NULL, 0, NULL, NULL),
(101, 140, '1357572 - 3 Ply Surgical Mask With Nose Pin-Ultrasonic punch', NULL, NULL, 0, NULL, NULL),
(102, 141, '1357572 - 3 Ply Surgical Mask With Nose Pin-Ultrasonic punch', NULL, NULL, 0, NULL, NULL),
(103, 37, '1357580 - Disinfectant spray-500ml', NULL, NULL, 0, NULL, NULL),
(104, 191, '1357580 - Disinfectant spray Label-500ml', NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_variant_update`
--

CREATE TABLE `product_variant_update` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cost` bigint(20) DEFAULT NULL,
  `price` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_date` date NOT NULL,
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `w_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_id` bigint(20) UNSIGNED NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `total` double NOT NULL DEFAULT 0,
  `p_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `b_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_order_details`
--

CREATE TABLE `purchase_order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `o_id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `sub_total` double NOT NULL DEFAULT 0,
  `received_quantity` bigint(20) DEFAULT 0,
  `cost` double NOT NULL DEFAULT 0,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_request`
--

CREATE TABLE `purchase_request` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `req_date` date NOT NULL,
  `w_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `purchase_request_details`
--

CREATE TABLE `purchase_request_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `o_id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotation`
--

CREATE TABLE `quotation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `r_id` bigint(20) UNSIGNED NOT NULL,
  `s_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `attachment` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `quotation_details`
--

CREATE TABLE `quotation_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `q_id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `cost` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `description`, `status`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'show all modules', 1, 2, 2, '2021-05-24 07:05:05', '2021-06-25 02:17:52'),
(2, 'Sale Person', 'show sale module', 1, 2, 2, '2021-05-24 07:05:31', '2021-06-21 01:05:11'),
(3, 'warehouse person', 'add stock, check current stock, add purchase request', 1, 2, 2, '2021-05-25 01:19:21', '2021-06-18 02:59:41'),
(4, 'test role', 'jhgkjhg', 1, 2, 2, '2021-05-25 02:11:51', '2021-06-18 02:56:12'),
(5, 'Accounts manager', 'handle accounts', 1, 2, NULL, '2021-06-29 03:42:46', '2021-06-29 03:42:46');

-- --------------------------------------------------------

--
-- Table structure for table `role_menu`
--

CREATE TABLE `role_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `r_id` bigint(20) UNSIGNED DEFAULT NULL,
  `m_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_menu`
--

INSERT INTO `role_menu` (`id`, `r_id`, `m_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(97, 4, 2, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(98, 4, 3, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(99, 4, 4, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(100, 4, 24, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(101, 4, 26, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(1141, 3, 1, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1142, 3, 12, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1143, 3, 13, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1144, 3, 14, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1145, 3, 15, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1146, 3, 16, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1147, 3, 17, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1148, 3, 18, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1149, 3, 19, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1150, 3, 20, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1151, 3, 21, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1152, 3, 22, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1153, 3, 23, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1154, 3, 24, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1155, 3, 25, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1156, 3, 26, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1157, 3, 27, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1158, 3, 28, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1159, 3, 32, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1160, 3, 33, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1161, 3, 34, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1162, 3, 35, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1163, 3, 36, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1164, 3, 43, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1165, 3, 46, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1166, 3, 47, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1167, 3, 52, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1168, 3, 53, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1169, 3, 54, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1170, 3, 55, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1171, 3, 56, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1172, 3, 57, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1173, 3, 58, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1174, 3, 59, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1175, 3, 60, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1176, 3, 61, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1177, 3, 62, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1178, 3, 64, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1179, 3, 65, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(1734, 2, 24, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1735, 2, 26, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1736, 2, 43, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1737, 2, 44, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1738, 2, 45, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1739, 2, 57, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1740, 2, 58, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1741, 2, 59, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1742, 2, 66, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1743, 2, 67, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1744, 2, 68, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1745, 2, 69, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(1930, 1, 1, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1931, 1, 2, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1932, 1, 3, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1933, 1, 4, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1934, 1, 5, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1935, 1, 6, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1936, 1, 85, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1937, 1, 86, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1938, 1, 7, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1939, 1, 8, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1940, 1, 9, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1941, 1, 10, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1942, 1, 11, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1943, 1, 12, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1944, 1, 13, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1945, 1, 14, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1946, 1, 15, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1947, 1, 16, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1948, 1, 17, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1949, 1, 18, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1950, 1, 19, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1951, 1, 20, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1952, 1, 21, 2, NULL, '2021-06-25 02:17:52', '2021-06-25 02:17:52'),
(1953, 1, 22, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1954, 1, 23, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1955, 1, 24, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1956, 1, 25, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1957, 1, 26, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1958, 1, 27, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1959, 1, 28, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1960, 1, 93, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1961, 1, 29, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1962, 1, 30, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1963, 1, 31, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1964, 1, 32, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1965, 1, 33, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1966, 1, 34, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1967, 1, 35, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1968, 1, 36, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1969, 1, 37, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1970, 1, 38, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1971, 1, 39, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1972, 1, 40, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1973, 1, 41, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1974, 1, 42, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1975, 1, 43, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1976, 1, 44, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1977, 1, 45, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1978, 1, 46, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1979, 1, 47, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1980, 1, 48, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1981, 1, 49, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1982, 1, 50, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1983, 1, 51, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1984, 1, 89, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1985, 1, 90, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1986, 1, 91, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1987, 1, 92, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1988, 1, 52, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1989, 1, 53, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1990, 1, 54, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1991, 1, 55, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1992, 1, 56, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1993, 1, 57, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1994, 1, 58, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1995, 1, 59, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1996, 1, 60, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1997, 1, 61, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1998, 1, 62, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(1999, 1, 63, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2000, 1, 64, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2001, 1, 65, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2002, 1, 66, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2003, 1, 67, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2004, 1, 68, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2005, 1, 69, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2006, 1, 70, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2007, 1, 71, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2008, 1, 72, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2009, 1, 73, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2010, 1, 74, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2011, 1, 75, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2012, 1, 76, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2013, 1, 77, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2014, 1, 78, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2015, 1, 79, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2016, 1, 80, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2017, 1, 87, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2018, 1, 81, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2019, 1, 82, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2020, 1, 84, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2021, 1, 88, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(2022, 5, 70, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2023, 5, 71, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2024, 5, 72, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2025, 5, 73, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2026, 5, 74, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2027, 5, 75, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2028, 5, 76, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(2029, 5, 77, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE `role_permission` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `r_id` bigint(20) UNSIGNED DEFAULT NULL,
  `p_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_permission`
--

INSERT INTO `role_permission` (`id`, `r_id`, `p_id`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(90, 4, 17, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(91, 4, 18, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(92, 4, 32, 2, NULL, '2021-05-25 02:11:51', '2021-05-25 02:11:51'),
(93, 4, 3, 2, 2, NULL, NULL),
(2029, 3, 72, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2030, 3, 125, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2031, 3, 128, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2032, 3, 129, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2033, 3, 130, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2034, 3, 131, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2035, 3, 133, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2036, 3, 134, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2037, 3, 135, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2038, 3, 136, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2039, 3, 179, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2040, 3, 182, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2041, 3, 183, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2042, 3, 184, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2043, 3, 185, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2044, 3, 186, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2045, 3, 187, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2046, 3, 188, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2047, 3, 5, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2048, 3, 28, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2049, 3, 82, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2050, 3, 7, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2051, 3, 29, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2052, 3, 79, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2053, 3, 9, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2054, 3, 30, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2055, 3, 80, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2056, 3, 83, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2057, 3, 173, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2058, 3, 11, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2059, 3, 31, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2060, 3, 84, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2061, 3, 152, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2062, 3, 153, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2063, 3, 154, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2064, 3, 155, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2065, 3, 13, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2066, 3, 32, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2067, 3, 85, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2068, 3, 86, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2069, 3, 156, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2070, 3, 157, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2071, 3, 158, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2072, 3, 159, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2073, 3, 19, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2074, 3, 33, 2, NULL, '2021-06-18 02:59:41', '2021-06-18 02:59:41'),
(2075, 3, 87, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2076, 3, 88, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2077, 3, 177, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2078, 3, 21, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2079, 3, 35, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2080, 3, 90, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2081, 3, 91, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2082, 3, 92, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2083, 3, 174, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2084, 3, 39, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2085, 3, 100, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2086, 3, 109, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2087, 3, 110, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2088, 3, 111, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2089, 3, 112, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2090, 3, 172, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2091, 3, 151, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2092, 3, 43, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2093, 3, 44, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2094, 3, 48, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2095, 3, 49, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2096, 3, 50, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2097, 3, 53, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2098, 3, 54, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2099, 3, 55, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2100, 3, 63, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2101, 3, 115, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(2102, 3, 150, 2, NULL, '2021-06-18 02:59:42', '2021-06-18 02:59:42'),
(3318, 2, 72, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3319, 2, 124, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3320, 2, 126, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3321, 2, 128, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3322, 2, 129, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3323, 2, 131, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3324, 2, 138, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3325, 2, 180, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3326, 2, 182, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3327, 2, 183, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3328, 2, 185, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3329, 2, 186, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3330, 2, 187, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3331, 2, 188, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3332, 2, 13, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3333, 2, 32, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3334, 2, 24, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3335, 2, 38, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3336, 2, 97, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3337, 2, 98, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3338, 2, 99, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3339, 2, 43, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3340, 2, 44, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3341, 2, 162, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3342, 2, 57, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3343, 2, 58, 2, NULL, '2021-06-21 01:05:11', '2021-06-21 01:05:11'),
(3344, 2, 59, 2, NULL, '2021-06-21 01:05:12', '2021-06-21 01:05:12'),
(3345, 2, 169, 2, NULL, '2021-06-21 01:05:12', '2021-06-21 01:05:12'),
(3754, 1, 72, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(3755, 1, 124, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(3756, 1, 125, 2, NULL, '2021-06-25 02:17:53', '2021-06-25 02:17:53'),
(3757, 1, 126, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3758, 1, 127, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3759, 1, 128, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3760, 1, 129, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3761, 1, 130, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3762, 1, 131, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3763, 1, 132, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3764, 1, 133, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3765, 1, 134, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3766, 1, 135, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3767, 1, 136, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3768, 1, 137, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3769, 1, 138, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3770, 1, 178, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3771, 1, 179, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3772, 1, 180, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3773, 1, 181, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3774, 1, 182, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3775, 1, 183, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3776, 1, 184, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3777, 1, 185, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3778, 1, 186, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3779, 1, 187, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3780, 1, 188, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3781, 1, 2, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3782, 1, 3, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3783, 1, 70, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3784, 1, 71, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3785, 1, 74, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3786, 1, 4, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3787, 1, 75, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3788, 1, 73, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3789, 1, 76, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3790, 1, 81, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3791, 1, 139, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3792, 1, 141, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3793, 1, 1, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3794, 1, 77, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3795, 1, 140, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3796, 1, 5, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3797, 1, 6, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3798, 1, 28, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3799, 1, 78, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3800, 1, 82, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3801, 1, 7, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3802, 1, 8, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3803, 1, 29, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3804, 1, 79, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3805, 1, 9, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3806, 1, 10, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3807, 1, 30, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3808, 1, 80, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3809, 1, 83, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3810, 1, 173, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3811, 1, 11, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3812, 1, 12, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3813, 1, 31, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3814, 1, 84, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3815, 1, 152, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3816, 1, 153, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3817, 1, 154, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3818, 1, 155, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3819, 1, 13, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3820, 1, 14, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3821, 1, 17, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3822, 1, 18, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3823, 1, 32, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3824, 1, 85, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3825, 1, 86, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3826, 1, 208, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3827, 1, 156, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3828, 1, 157, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3829, 1, 158, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3830, 1, 159, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3831, 1, 15, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3832, 1, 19, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3833, 1, 20, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3834, 1, 33, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3835, 1, 87, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3836, 1, 88, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3837, 1, 204, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3838, 1, 209, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3839, 1, 205, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3840, 1, 206, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3841, 1, 144, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3842, 1, 16, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3843, 1, 34, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3844, 1, 89, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3845, 1, 177, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3846, 1, 21, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3847, 1, 35, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3848, 1, 90, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3849, 1, 91, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3850, 1, 92, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3851, 1, 176, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3852, 1, 22, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3853, 1, 36, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3854, 1, 93, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3855, 1, 94, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3856, 1, 95, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3857, 1, 23, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3858, 1, 37, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3859, 1, 96, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3860, 1, 145, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3861, 1, 24, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3862, 1, 38, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3863, 1, 97, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3864, 1, 98, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3865, 1, 99, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3866, 1, 146, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3867, 1, 174, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3868, 1, 25, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3869, 1, 39, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3870, 1, 100, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3871, 1, 101, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3872, 1, 102, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3873, 1, 175, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3874, 1, 142, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3875, 1, 26, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3876, 1, 40, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3877, 1, 103, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3878, 1, 104, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3879, 1, 105, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3880, 1, 143, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3881, 1, 160, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3882, 1, 27, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3883, 1, 41, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3884, 1, 106, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3885, 1, 107, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3886, 1, 108, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3887, 1, 161, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3888, 1, 196, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3889, 1, 200, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3890, 1, 197, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3891, 1, 201, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3892, 1, 199, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3893, 1, 203, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3894, 1, 198, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3895, 1, 202, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3896, 1, 109, 2, NULL, '2021-06-25 02:17:54', '2021-06-25 02:17:54'),
(3897, 1, 110, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3898, 1, 111, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3899, 1, 112, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3900, 1, 172, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3901, 1, 151, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3902, 1, 42, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3903, 1, 43, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3904, 1, 44, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3905, 1, 45, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3906, 1, 113, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3907, 1, 46, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3908, 1, 47, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3909, 1, 48, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3910, 1, 147, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3911, 1, 148, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3912, 1, 149, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3913, 1, 49, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3914, 1, 50, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3915, 1, 51, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3916, 1, 52, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3917, 1, 53, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3918, 1, 54, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3919, 1, 55, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3920, 1, 63, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3921, 1, 114, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3922, 1, 115, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3923, 1, 150, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3924, 1, 195, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3925, 1, 162, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3926, 1, 163, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3927, 1, 164, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3928, 1, 165, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3929, 1, 166, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3930, 1, 192, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3931, 1, 56, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3932, 1, 57, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3933, 1, 58, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3934, 1, 59, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3935, 1, 60, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3936, 1, 61, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3937, 1, 62, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3938, 1, 64, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3939, 1, 65, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3940, 1, 116, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3941, 1, 117, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3942, 1, 169, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3943, 1, 170, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3944, 1, 171, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3945, 1, 193, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3946, 1, 194, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3947, 1, 66, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3948, 1, 67, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3949, 1, 68, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3950, 1, 118, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3951, 1, 69, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3952, 1, 119, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3953, 1, 121, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3954, 1, 122, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3955, 1, 190, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3956, 1, 191, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3957, 1, 120, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3958, 1, 123, 2, NULL, '2021-06-25 02:17:55', '2021-06-25 02:17:55'),
(3959, 5, 66, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(3960, 5, 67, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(3961, 5, 68, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(3962, 5, 118, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(3963, 5, 69, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47'),
(3964, 5, 119, 2, NULL, '2021-06-29 03:42:47', '2021-06-29 03:42:47');

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sale_date` date NOT NULL,
  `ref_no` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `b_id` bigint(20) UNSIGNED NOT NULL,
  `c_id` bigint(20) UNSIGNED NOT NULL,
  `w_id` bigint(20) UNSIGNED NOT NULL,
  `s_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Pending',
  `p_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `total` double NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `sp_id` bigint(20) UNSIGNED DEFAULT NULL,
  `discount_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `expected_date` date DEFAULT NULL,
  `advance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pay_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sale_details`
--

CREATE TABLE `sale_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `s_id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `sub_total` double NOT NULL DEFAULT 0,
  `price` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `delivered_quantity` bigint(20) DEFAULT 0,
  `discount_percent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `discounted_amount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) UNSIGNED NOT NULL,
  `s_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` bigint(20) NOT NULL,
  `stock_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `purchase_d_id` bigint(20) UNSIGNED DEFAULT NULL,
  `s_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Purchase',
  `cost` bigint(20) DEFAULT NULL,
  `w_id` bigint(20) UNSIGNED DEFAULT NULL,
  `grn_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stock_out`
--

CREATE TABLE `stock_out` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) UNSIGNED NOT NULL,
  `quantity` double NOT NULL,
  `stock_date` date NOT NULL,
  `s_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `w_id` bigint(20) UNSIGNED NOT NULL,
  `sale_d_id` bigint(20) UNSIGNED DEFAULT NULL,
  `gdn_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stock_receiving`
--

CREATE TABLE `stock_receiving` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `s_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cat_id` bigint(20) UNSIGNED NOT NULL,
  `s_cat_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `cat_id`, `s_cat_name`, `created_at`, `updated_at`, `status`, `created_by`, `updated_by`) VALUES
(1, 1, 'Face Protection', NULL, NULL, 1, NULL, NULL),
(2, 2, 'Devices', NULL, NULL, 1, NULL, NULL),
(3, 1, 'Gloves', NULL, NULL, 1, NULL, NULL),
(4, 1, 'Body Protection', NULL, NULL, 1, NULL, NULL),
(5, 3, 'Pump', NULL, NULL, 1, NULL, NULL),
(6, 3, 'Bottles', NULL, NULL, 1, NULL, NULL),
(7, 3, 'Label', NULL, NULL, 1, NULL, NULL),
(8, 3, 'Bottle Cap', NULL, NULL, 1, NULL, NULL),
(9, 3, 'Jar', NULL, NULL, 1, NULL, NULL),
(10, 4, 'Equipments', NULL, NULL, 1, NULL, NULL),
(11, 5, 'Equipments', NULL, NULL, 1, NULL, NULL),
(12, 6, 'No sub category', NULL, NULL, 1, NULL, NULL),
(13, 3, 'Dropper', NULL, NULL, 1, NULL, NULL),
(14, 5, 'Serum', NULL, NULL, 1, NULL, NULL),
(15, 5, 'Gel', NULL, NULL, 1, NULL, NULL),
(16, 5, 'Butter', NULL, NULL, 1, NULL, NULL),
(17, 7, 'Hair Oil', NULL, NULL, 1, NULL, NULL),
(18, 3, 'Box', NULL, NULL, 1, NULL, NULL),
(19, 3, 'Card Box', NULL, NULL, 1, NULL, NULL),
(20, 3, 'Wooden Box', NULL, NULL, 1, NULL, NULL),
(21, 3, 'Gift Paper', NULL, NULL, 1, NULL, NULL),
(22, 3, 'Bags', NULL, NULL, 1, NULL, NULL),
(23, 3, 'Sticker', NULL, NULL, 1, NULL, NULL),
(24, 3, 'No sub category', NULL, NULL, 1, NULL, NULL),
(25, 3, 'Tubes', NULL, NULL, 1, NULL, NULL),
(26, 8, 'Hand Care', NULL, NULL, 1, NULL, NULL),
(27, 8, 'Facewash', NULL, NULL, 1, NULL, NULL),
(28, 6, 'Serum', NULL, NULL, 1, NULL, NULL),
(29, 8, 'Hand Care', NULL, NULL, 1, NULL, NULL),
(30, 8, 'Gloves', NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaction_history`
--

CREATE TABLE `transaction_history` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_s_id` bigint(20) NOT NULL,
  `p_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `t_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `paid_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ref_no` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total` double NOT NULL,
  `cheque_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gift_no` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_holder` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `b_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `u_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `u_name`, `created_at`, `updated_at`, `status`, `created_by`, `updated_by`) VALUES
(1, 'pieces', NULL, NULL, 1, NULL, NULL),
(2, 'Mililiter', NULL, NULL, 1, NULL, NULL),
(3, 'Liter', NULL, NULL, 1, NULL, NULL),
(4, 'Grams', NULL, NULL, 1, NULL, NULL),
(5, 'Kilograms', NULL, NULL, 1, NULL, NULL),
(6, 'No Unit', NULL, NULL, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `r_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `r_id`) VALUES
(1, 'samad', 'samad@gmail.com', NULL, '$2y$10$/j./m0XPJNtf.NhgcYtfmuz2Li8z.KMey6heBkGFFdh7JINJ9iX8m', NULL, '2021-02-24 02:35:59', '2021-02-24 02:35:59', 2),
(2, 'hibah', 'hibah@gmail.com', NULL, '$2y$10$qXamJ/gieSeDrBj0g8p7OuFcbyUIWw9wyEtZwQCOMfnHMApfpCG8.', NULL, '2021-02-24 02:35:59', '2021-02-24 02:35:59', 1),
(3, 'Ahsan', 'ahsan@gmail.com', NULL, '$2y$10$Y7zY/wWC4RgTTPgzV/x3nOiehYyB3KdOPi.0pRp7um8BOx/fXyFsS', NULL, '2021-03-01 00:38:34', '2021-06-18 03:07:53', 2),
(5, 'rafay', 'rafay@gmail.com', NULL, '$2y$10$JDfgXFItdGb.tuBGzPBuKuMNs6/HrPT12vWVKrjVEh2hGWnXAxmwG', NULL, '2021-05-25 02:04:03', '2021-06-18 03:07:36', 3),
(6, 'Arsalan Bostan', 'arsalan@gmail.com', NULL, '$2y$10$zO.5cL.uIJ27XVGv7.vD4uyDgq7FwffNlKu5a7onWxPb/EriMixBW', NULL, '2021-06-11 07:52:12', '2021-06-18 03:07:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) NOT NULL DEFAULT 0,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sort` bigint(20) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `p_id`, `name`, `icon`, `route`, `sort`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 0, 'Dashboard', 'icon-home', 'dashboard', 1, 2, '2021-05-20 04:07:25', '2021-05-20 04:22:35'),
(2, 0, 'User Management', 'fa-user', NULL, 2, 2, '2021-05-20 04:52:38', '2021-05-20 04:52:38'),
(3, 2, 'Add Role', NULL, 'roles.create', 3, 2, '2021-05-20 04:54:50', '2021-05-20 04:54:50'),
(4, 2, 'View Roles', NULL, 'roles.index', 4, 2, '2021-05-20 04:56:03', '2021-05-20 04:56:03'),
(5, 2, 'Add Menu', NULL, 'menu.create', 5, 2, '2021-05-20 04:56:22', '2021-05-20 04:56:22'),
(6, 2, 'View Menus', NULL, 'menu.index', 6, 2, '2021-05-20 04:56:41', '2021-05-20 04:56:41'),
(7, 0, 'Transaction History', 'fa-history', 'transaction.index', 7, 2, '2021-05-20 04:59:54', '2021-05-20 04:59:54'),
(8, 0, 'Trial Balance', 'fa-balance-scale', 'trialBalance', 8, 2, '2021-05-20 05:00:36', '2021-05-20 05:00:36'),
(9, 0, 'Banks', 'fa-bank', NULL, 9, 2, '2021-05-20 05:01:04', '2021-05-20 05:01:04'),
(10, 9, 'Add Bank', NULL, 'bank.create', 10, 2, '2021-05-20 05:03:20', '2021-05-20 05:03:20'),
(11, 9, 'View Banks', NULL, 'bank.index', 11, 2, '2021-05-20 05:03:43', '2021-05-20 05:03:43'),
(12, 0, 'Brands', 'fa-bold', NULL, 12, 2, '2021-05-20 05:04:32', '2021-05-20 05:04:32'),
(13, 12, 'Add Brand', NULL, 'brands.create', 13, 2, '2021-05-20 05:04:59', '2021-05-20 05:04:59'),
(14, 12, 'View Brands', NULL, 'brands.index', 14, 2, '2021-05-20 05:05:23', '2021-05-20 05:05:23'),
(15, 0, 'Unit', 'fa-balance-scale', NULL, 15, 2, '2021-05-20 05:11:20', '2021-05-20 05:11:20'),
(16, 15, 'Add Unit', NULL, 'unit.create', 16, 2, '2021-05-20 05:11:40', '2021-05-20 05:11:40'),
(17, 15, 'View Unit', NULL, 'unit.index', 17, 2, '2021-05-20 05:11:59', '2021-05-20 05:11:59'),
(18, 0, 'Category', 'fa-contao', NULL, 18, 2, '2021-05-20 05:12:59', '2021-05-20 05:12:59'),
(19, 18, 'Add Category', NULL, 'category.create', 19, 2, '2021-05-20 05:13:22', '2021-05-20 05:13:22'),
(20, 18, 'View Categories', NULL, 'category.index', 20, 2, '2021-05-20 05:14:13', '2021-05-20 05:14:13'),
(21, 0, 'Sub Category', 'fa-sitemap', NULL, 21, 2, '2021-05-20 05:15:37', '2021-05-20 05:15:37'),
(22, 21, 'Add Sub Category', NULL, 'subcategory.create', 22, 2, '2021-05-20 05:16:06', '2021-05-20 05:16:06'),
(23, 21, 'View Sub Categories', NULL, 'subcategory.index', 23, 2, '2021-05-20 05:16:39', '2021-05-20 05:16:39'),
(24, 0, 'Products', 'fa-list-alt', NULL, 24, 2, '2021-05-20 05:17:12', '2021-05-20 05:17:12'),
(25, 24, 'Add Product', NULL, 'product.create', 25, 2, '2021-05-20 05:17:48', '2021-05-20 05:17:48'),
(26, 24, 'View Products', NULL, 'product.index', 26, 2, '2021-05-20 05:27:43', '2021-05-20 05:27:43'),
(27, 24, 'Add Finish Product', NULL, 'product.add', 27, 2, '2021-05-20 05:28:27', '2021-05-20 05:28:27'),
(28, 24, 'View Finish Products', NULL, 'product.finishProducts', 28, 2, '2021-05-20 05:29:00', '2021-05-20 05:29:00'),
(29, 0, 'City', 'fa-building', NULL, 29, 2, '2021-05-20 05:31:31', '2021-05-20 05:31:31'),
(30, 29, 'Add City', NULL, 'city.create', 30, 2, '2021-05-20 05:32:57', '2021-05-20 05:32:57'),
(31, 29, 'View City', NULL, 'city.index', 31, 2, '2021-05-20 05:33:27', '2021-05-20 05:33:27'),
(32, 0, 'Warehouse', 'fa-map-marker', NULL, 32, 2, '2021-05-20 05:34:46', '2021-05-20 05:34:46'),
(33, 32, 'Add Warehouse', NULL, 'warehouse.create', 33, 2, '2021-05-20 05:35:16', '2021-05-20 05:35:16'),
(34, 32, 'View Warehouse', NULL, 'warehouse.index', 34, 2, '2021-05-20 05:35:45', '2021-05-20 05:35:45'),
(35, 32, 'Transfer Product', NULL, 'productTransfer.create', 35, 2, '2021-05-20 05:36:12', '2021-05-20 05:36:12'),
(36, 32, 'Transfer Product History', NULL, 'productTransfer.index', 36, 2, '2021-05-20 05:37:48', '2021-05-20 05:37:48'),
(37, 0, 'Company', 'fa-industry', NULL, 37, 2, '2021-05-20 05:38:16', '2021-05-20 05:38:16'),
(38, 37, 'Add Company', NULL, 'company.create', 38, 2, '2021-05-20 05:41:42', '2021-05-20 05:41:42'),
(39, 37, 'View Company', NULL, 'company.index', 39, 2, '2021-05-20 05:42:15', '2021-05-20 05:42:15'),
(40, 0, 'Groups', 'fa-users', NULL, 40, 2, '2021-05-20 05:43:33', '2021-05-20 05:43:33'),
(41, 40, 'Add Group', NULL, 'group.create', 41, 2, '2021-05-20 05:44:07', '2021-05-20 05:44:07'),
(42, 40, 'View Groups', NULL, 'group.index', 42, 2, '2021-05-20 05:44:24', '2021-05-20 05:44:24'),
(43, 0, 'Stake Holders', 'fa-user', NULL, 43, 2, '2021-05-20 05:45:19', '2021-06-14 06:54:45'),
(44, 43, 'Add Customer', NULL, 'customer.create', 44, 2, '2021-05-20 05:46:14', '2021-05-20 05:46:14'),
(45, 43, 'View Customers', NULL, 'customer.index', 45, 2, '2021-05-20 05:46:45', '2021-05-20 05:46:45'),
(46, 43, 'Add Supplier', NULL, 'supplier.create', 46, 2, '2021-05-20 05:47:40', '2021-05-20 05:47:40'),
(47, 43, 'View Suppliers', NULL, 'supplier.index', 47, 2, '2021-05-20 05:48:13', '2021-05-20 05:48:13'),
(48, 43, 'Add Biller', NULL, 'biller.create', 48, 2, '2021-05-20 05:48:41', '2021-05-20 05:48:41'),
(49, 43, 'View Billers', NULL, 'biller.index', 49, 2, '2021-05-20 05:49:09', '2021-05-20 05:49:09'),
(50, 43, 'Add Sale Person', NULL, 'saleperson.create', 50, 2, '2021-05-20 05:49:34', '2021-05-20 05:49:34'),
(51, 43, 'View Sale Persons', NULL, 'saleperson.index', 51, 2, '2021-05-20 05:49:59', '2021-05-20 05:49:59'),
(52, 0, 'Stocks', 'fa-tasks', NULL, 52, 2, '2021-05-20 06:01:18', '2021-05-20 06:01:18'),
(53, 52, 'Add Stock', NULL, 'stocks.create', 53, 2, '2021-05-20 06:01:58', '2021-05-20 06:01:58'),
(54, 52, 'Current Stock', NULL, 'stocks.current', 54, 2, '2021-05-20 06:02:22', '2021-05-20 06:02:22'),
(55, 52, 'Stock In History', NULL, 'stocks.index', 55, 2, '2021-05-20 06:02:55', '2021-05-20 06:02:55'),
(56, 52, 'Stock Out History', NULL, 'stockout.index', 56, 2, '2021-05-20 06:03:21', '2021-05-20 06:03:21'),
(57, 0, 'Purchase Request', 'fa-shopping-cart', NULL, 57, 2, '2021-05-20 06:03:53', '2021-05-20 06:03:53'),
(58, 57, 'New Purchase Request', NULL, 'request.create', 58, 2, '2021-05-20 06:04:23', '2021-05-20 06:04:23'),
(59, 57, 'View Purchase Requests', NULL, 'request.index', 59, 2, '2021-05-20 06:04:44', '2021-05-20 06:04:44'),
(60, 0, 'Quotations', 'fa-quote-left', NULL, 60, 2, '2021-05-20 06:05:20', '2021-05-20 06:05:20'),
(61, 60, 'View Quotations', NULL, 'quotation.index', 61, 2, '2021-05-20 06:06:20', '2021-05-20 06:06:20'),
(62, 0, 'Purchase Orders', 'fa-shopping-cart', NULL, 62, 2, '2021-05-20 06:08:18', '2021-05-20 06:08:18'),
(63, 62, 'Add Purchase Order', NULL, 'purchase.create', 63, 2, '2021-05-20 06:09:35', '2021-05-20 06:09:35'),
(64, 62, 'View Purchase Orders', NULL, 'purchase.index', 64, 2, '2021-05-20 06:09:57', '2021-05-20 06:09:57'),
(65, 62, 'Purchase Order Report', NULL, 'purchase.report', 65, 2, '2021-05-20 06:10:20', '2021-05-20 06:10:20'),
(66, 0, 'Sales', 'fa-credit-card', NULL, 66, 2, '2021-05-20 06:12:09', '2021-05-20 06:12:09'),
(67, 66, 'New Sale Order', NULL, 'sales.create', 67, 2, '2021-05-20 06:12:32', '2021-05-20 06:12:32'),
(68, 66, 'View Sales', NULL, 'sales.index', 68, 2, '2021-05-20 06:12:53', '2021-05-20 06:12:53'),
(69, 66, 'Sales Report', NULL, 'sales.report', 69, 2, '2021-05-20 06:14:08', '2021-05-20 06:14:08'),
(70, 0, 'Accounts', 'fa-money', NULL, 70, 2, '2021-05-20 06:14:38', '2021-05-20 06:14:38'),
(71, 70, 'Add Head of Accounts', NULL, 'headofaccounts.create', 71, 2, '2021-05-20 06:15:20', '2021-05-20 06:15:20'),
(72, 70, 'View Head of Accounts', NULL, 'headofaccounts.index', 72, 2, '2021-05-20 06:15:42', '2021-05-20 06:15:42'),
(73, 70, 'Add Head Category', NULL, 'headcategory.create', 73, 2, '2021-05-20 06:16:11', '2021-05-20 06:16:11'),
(74, 70, 'View Head Category', NULL, 'headcategory.index', 74, 2, '2021-05-20 06:16:37', '2021-05-20 06:16:37'),
(75, 70, 'Add Account Details', NULL, 'accountdetails.create', 75, 2, '2021-05-20 06:17:03', '2021-05-20 06:17:03'),
(76, 70, 'View Account Details', NULL, 'accountdetails.index', 76, 2, '2021-05-20 06:17:24', '2021-05-20 06:17:24'),
(77, 70, 'General Ledger Entry', NULL, 'generalLedger.create', 77, 2, '2021-05-20 06:17:53', '2021-05-20 06:17:53'),
(78, 0, 'Chart of Accounts', 'fa-500px', NULL, 78, 2, '2021-05-20 06:19:21', '2021-05-20 06:19:21'),
(79, 0, 'Reports', 'fa-bar-chart', NULL, 79, 2, '2021-05-20 06:27:07', '2021-05-20 06:27:07'),
(80, 79, 'Profit/Loss Statement', NULL, 'reports.salesCosting', 80, 2, '2021-05-20 06:27:32', '2021-06-14 06:41:43'),
(81, 0, 'Permission', 'fa-lock', NULL, 81, 2, '2021-05-20 08:28:10', '2021-05-20 08:28:10'),
(82, 81, 'Add Permission', NULL, 'permission.create', 82, 2, '2021-05-20 08:28:32', '2021-05-20 08:28:32'),
(84, 81, 'View Permissions', NULL, 'permission.index', 83, 2, '2021-05-20 08:50:35', '2021-05-20 08:50:35'),
(85, 2, 'Add User', NULL, 'users.create', 84, 2, '2021-05-24 06:48:00', '2021-05-24 06:48:00'),
(86, 2, 'View Users', NULL, 'users.index', 85, 2, '2021-05-24 06:48:28', '2021-05-24 06:48:28'),
(87, 79, 'Income Statement', NULL, 'reports.incomeStatement', 86, 2, '2021-06-14 06:42:16', '2021-06-18 03:02:17'),
(88, 0, 'Notifications', 'fa-bell', 'viewAllNotifications', 87, 2, '2021-06-18 03:02:07', '2021-06-18 03:04:51'),
(89, 43, 'View Billers Update Request', NULL, 'billerUpdateRequest.index', 88, 2, '2021-06-18 04:44:52', '2021-06-18 04:44:52'),
(90, 43, 'View Customers Update Request', NULL, 'customerUpdateRequest.index', 89, 2, '2021-06-18 04:45:35', '2021-06-18 04:45:35'),
(91, 43, 'View Sale Person Update Request', NULL, 'salepersonUpdateRequest.index', 90, 2, '2021-06-18 04:46:17', '2021-06-18 04:46:17'),
(92, 43, 'View Suppliers Update Request', NULL, 'supplierUpdateRequest.index', 91, 2, '2021-06-18 04:46:46', '2021-06-18 04:46:46'),
(93, 24, 'View Product Update Requests', NULL, 'productUpdateRequest.index', 92, 2, '2021-06-21 00:55:46', '2021-06-21 00:55:46');

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `p_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `variants`
--

INSERT INTO `variants` (`id`, `p_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 0, 'Size', NULL, NULL),
(2, 0, 'Color', NULL, NULL),
(3, 0, 'Shoulder', NULL, NULL),
(4, 0, 'Material', NULL, NULL),
(5, 0, 'Type', NULL, NULL),
(6, 1, 'Small', NULL, NULL),
(7, 1, 'Large', NULL, NULL),
(8, 1, 'Medium', NULL, NULL),
(9, 1, '28mm', NULL, NULL),
(10, 1, '20mm', NULL, NULL),
(11, 1, '70ml', NULL, NULL),
(12, 1, '500ml', NULL, NULL),
(13, 1, '250ml', NULL, NULL),
(14, 1, '60ml', NULL, NULL),
(15, 1, '100ml', NULL, NULL),
(16, 1, '150ml', NULL, NULL),
(17, 1, '1000ml', NULL, NULL),
(18, 1, '100gm', NULL, NULL),
(19, 1, '30ml', NULL, NULL),
(20, 1, '300ml', NULL, NULL),
(21, 1, '50gm', NULL, NULL),
(22, 1, '300gm', NULL, NULL),
(23, 2, 'Red', NULL, NULL),
(24, 2, 'White', NULL, NULL),
(25, 2, 'Blue', NULL, NULL),
(26, 2, 'Transparent', NULL, NULL),
(27, 2, 'Gold', NULL, NULL),
(28, 2, 'Black', NULL, NULL),
(29, 2, 'Gold White', NULL, NULL),
(30, 2, 'Silver White', NULL, NULL),
(31, 2, 'Gold Black', NULL, NULL),
(32, 2, 'Silver Black', NULL, NULL),
(33, 2, 'Normal Base', NULL, NULL),
(34, 2, 'White Base', NULL, NULL),
(35, 2, 'Purple', NULL, NULL),
(36, 2, 'Lemon', NULL, NULL),
(37, 2, 'Lavander', NULL, NULL),
(38, 2, 'Red Rose', NULL, NULL),
(39, 2, 'Blueberry', NULL, NULL),
(40, 2, 'Strawberry', NULL, NULL),
(41, 3, 'Round Shoulder', NULL, NULL),
(42, 3, 'Flat Shoulder', NULL, NULL),
(43, 1, '8x11x4.5', NULL, NULL),
(44, 1, '8x8x4.5', NULL, NULL),
(45, 4, 'Plastic', NULL, NULL),
(46, 4, 'Aluminium', NULL, NULL),
(47, 5, 'Ultrasonic punch', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors`
--

CREATE TABLE `vendors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `v_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_no` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_id` bigint(20) UNSIGNED DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postalCode` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_group` bigint(20) UNSIGNED DEFAULT NULL,
  `p_group` bigint(20) UNSIGNED DEFAULT NULL,
  `VAT` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GST` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `u_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `c_no2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `vendors`
--

INSERT INTO `vendors` (`id`, `v_type`, `name`, `address`, `c_no`, `company`, `country`, `c_id`, `email`, `state`, `postalCode`, `c_group`, `p_group`, `VAT`, `GST`, `u_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`, `name2`, `c_no2`) VALUES
(1, 'Saleperson', 'Zia Khan', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(2, 'Saleperson', 'Ahsan Iqbal', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(3, 'Saleperson', 'Atif Khuwaja', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(4, 'Saleperson', 'Aqib Khuwaja', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(5, 'Saleperson', 'Arsalan Bostan', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(6, 'Saleperson', 'Danish Messani', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(7, 'Biller', 'Posch Care', '', '', 'Posch Care', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(8, 'Biller', 'Hashlob', '', '', 'Hashlob', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(9, 'Biller', 'Big Bang', '', '', 'Big Bang', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(10, 'Customer', 'Naiyar', 'Dolmen Mall Tariq Road', '', 'Dolmen Mall Tariq Road', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(11, 'Customer', 'nayyar', 'dolmen mall, main tariq road', '03212437097', 'domen real estate management pvt ltd', 'Pakistan', 1, 'nayyar@dolmen.com.pk', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(12, 'Customer', 'Qazi Muhammad Ziauddin', 'Sector 11-A , North Karachi Flat: B-603 Block B', '03352224080', '', 'Pakistan', 1, 'arsalanvirgo3@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(13, 'Customer', 'Priya Assrani', 'Flat no. 211, Erose complex near Nasheman cinema n', '3351254043', 'Priya Assrani', 'Pakistan', NULL, NULL, NULL, NULL, 1, 2, NULL, NULL, NULL, NULL, 2, '0000-00-00 00:00:00', '2021-06-28 07:52:08', 1, 'Priya Assrani', '3351254043'),
(14, 'Customer', 'Rumsha Sadaf', '27/ 1- D block no 2 PECHS karachi', '03333193195', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(15, 'Customer', 'Haseeb Ahmed', 'ali akbar street shop 9 khoya mandi juna market Ka', '03323825291', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(16, 'Customer', 'touqeer', '', '03332723062', 'Medical store', 'Pakistan', 1, 'tauqeermanzoor123@gmail.com', 'Islamabad', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(17, 'Customer', 'Dr Irma Omair.', 'Flat number 413, Datari Castle, Gulistan e Jauhar,', '03453140410', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(18, 'Customer', 'Rukhsana Qasim', 'office No 213 Ambar Pride Karachi', '03332469402', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(19, 'Customer', 'Fahad Ali', 'Plot # 19/6, Building Name Aisha Manzil Apt # B2, ', '03003505961', '', 'Pakistan', 1, 'fahadali@abc.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(20, 'Customer', 'Dr. M Talha Siddiqui', 'Flat # J8, Yaqoob Terrace, Gurumandir', '03323554254', '', 'Pakistan', 1, 'abcd@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(21, 'Customer', 'sara syed anis', 'A.866 2nd Floor Block 12 Federal b Area  ', '3313640019', '', 'Pakistan', 1, 'sara@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(22, 'Customer', 'Arsalan Ali Bostan', '107/108, Amber Pride, Block 6, P.E.C.H.S', '03212118833', '', 'Pakistan', 1, 'arsalanbostan08@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(23, 'Customer', 'Zulfiqar DCH', 'Gulistan e Johar', '03425010409', 'DCH Services', 'Pakistan', 1, 'abcd@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(24, 'Customer', 'Saif Malik', 'block A near Kda chorangi qamer ibrahim street Gro', '3082166313', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(25, 'Customer', 'Kashif Siddiqui', 'MK marketing & Developers cm 03 Bismillah Arcade S', '03331007171', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(26, 'Customer', 'sheena lucas', 'flat 24B 3rd floor ahmed mahal lucky star saddar', '03333005547', '', 'Pakistan', 1, 'info@pposchcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(27, 'Customer', 'Rayyan Ahmed', 'C-59, 1st Floor, Ishaqabad, F.B Area Block 1 Karac', '03002949831', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(28, 'Customer', 'Wasi Ahmed', 'B68, Block N, Street #7, North Nazimabad Karachi', '03000888880', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(29, 'Customer', 'Muhammad Azam Shakeel', 'Plot No.C8, Street No.21, Darus Salam Cooperative ', '03088882655', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(30, 'Customer', 'Ayaz Aziz', 'SU-539, Street 9, Askari 5 Malir Cantt, Karachi', '03008551152', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(31, 'Customer', 'Agha Noor', 'A-204 Samar Heights block 2 PECHS, Shaeed-e-millat', '03223266597', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(32, 'Customer', 'Ahmed Siddiqui', '5/21 zeenat square FC Area Zeenat square FC Area k', '03162334656', 'Posch Care', 'Pakistan', 1, 'info@poschcare.om', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(33, 'Customer', 'Mushahid Raza', 'Malik society scheem33 gulzar-hijri R163 15A karac', '03180206254', 'Posch Care', 'Pakistan', 1, 'info@poschcare.c om', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(34, 'Customer', 'Muzammil', 'Alfalah Bank', '03332142478', 'M/S Alfalah Bank', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(35, 'Customer', 'Syed Asif Saeed ', '20th floor  Shaheed-e-Millat Expressway near KPT I', '03009254236', 'Silk Bank LTD', 'Pakistan', 1, 'asif.saeed@silkbank.com.pk', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(36, 'Customer', 'Shahab Shah', '49 D, Askari 3, School Road Cantt karachi', '03328299666', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(37, 'Customer', 'Syed Hussaini', '4A 4/9 Nazimabad #4 ground floor', '02136707233', 'Posch  Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(38, 'Customer', 'james thows', 'House no 123 Apartment 101,suit 03 karachi', '03123296476', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(39, 'Customer', 'Osama Siddiqui', 'B-302, block-15 gulistan e johar karachi near daru', '03332007863', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(40, 'Customer', 'Abdul Shakeel', 'A 703 Harmain Royal Residence, Gulshan block 1 kar', '03350121221', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(41, 'Customer', 'Umer Abdul Rauf', 'C-602 Mehran Square Ch. Khaliquzzaman Rd Near Frer', '03212153581', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(42, 'Customer', 'Maha Ashraf', 'House No. 83, Street-6, A.O.H.S (Next to National ', '03458433406', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(43, 'Customer', 'Tanveer Ahmed', 'C 521 lucknow society korangi karachi', '03312862016', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(44, 'Customer', 'Yousuf Jamal', 'House # SDH5 321 st Falcon Complex Jinnah Avenue ,', '3212702040', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(45, 'Customer', 'Malik kamran', 'c-243 Block 14 Gulistan-Johar karachi', '3323034313', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(46, 'Customer', 'Aamir', 'House No 57/2 31 Street Kayban Mujahid Phase 5 DHA', '03003116745', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(47, 'Customer', 'Dr Sher Ahmed ', 'Karachi Colony ,Memon Goth Road , Malir 15', '3363648180', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(48, 'Customer', 'Mister Hussain', 'House 4 street 4 phase 2 Gulshan mehar ali Abdul h', '03008617271', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(49, 'Customer', 'Mian Hassan', '38 B street 37 Phase 1 DHA Lahore', '3099988855', 'PPosch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(50, 'Customer', 'Abdul Wajid Kandhar', 'Iqbal Memorial Hospital Ghousia Mohalla Opposite M', '03043894347', 'Posch Care', 'Pakistan', 6, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(51, 'Customer', 'Sana Suffain', 'H.no. 2 Iqbal Garden near Canal Country Club Rahim', '03327308661', 'Posch Care', 'Pakistan', 7, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(52, 'Customer', 'Barira Zulfiqar', 'D-87, Block 5, F.B Area', '03218235641', '', 'Pakistan', 1, 'aaqibk@hotmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(53, 'Customer', 'Imran Ali Bostan', '107/108, Amber Pride, Block 6, P.E.C.H.S ', '03212118833', '', 'Pakistan', 1, 'arsalanbostan08@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(54, 'Customer', 'POSCH Care', '205, Amber Pride, Block 6, PECHS', '2134322103', 'M/S POSCH Care', 'Pakistan', 1, 'support@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(55, 'Customer', 'Pakistan Currency Exchange', 'Al rasheed chamber, Shop No. 7,8,9 Shahrah-e-Faisa', '03212118833', '', 'Pakistan', 1, 'arsalanbostan08@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(56, 'Customer', 'Mr. Imran', 'UBL Bank Rashid Minhas Rd', '03453009106', 'United Bank Limited', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(57, 'Customer', 'Mr Naeem', 'UBL Malir City Branch', '3363862547', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(58, 'Customer', 'mr Zia', 'Port Qasim Branch', '3462809535', 'UBL', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(59, 'Customer', 'Zia', 'UBL Port Qasim Branch', '03462809535', 'United Bank Limited', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(60, 'Customer', 'M/S Avicon', 'Amber Pride, Block 6, PECHS', '0213333333', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(61, 'Customer', 'Khawaja Atif Shaukat', '205, Amber Pride, Block 6, PECHS', '03219499944', '', 'Pakistan', 1, 'atif.cads@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(62, 'Customer', 'Sohail', 'FB Area', '3456112232', 'Aimm', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(63, 'Customer', 'NAYYAR QAZI', 'D14 BLOCK C, NORTH NAZIMABAD ', '03212437097', 'DOLMEN REAL ESTATE MANAGEMENT PRIVATE LIMITED', 'Pakistan', 1, 'NAYYAR@DOLMEN.COM.PK', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(64, 'Customer', 'Abdul Hameed', 'south city hospital clifton', '03002171134', '', 'Pakistan', 1, 'abdul.hameed@southcityhospital.org', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(65, 'Customer', 'Mudassar', 'DHA Khayaban e seher', '3004348817', '', 'Pakistan', 1, 'zia.mkhan91@gmail.com', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(66, 'Customer', 'M Sarmad Khan', 'S-37, iqbal Lane 10, Khayaban-e-Ghaznavi, phase-8,', '03218204477', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(67, 'Customer', 'Synergy Computers (pvt) Ltd', 'Muhammad Ali Society', '3002039868', 'synergy Computers (pvt) Ltd', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(68, 'Customer', 'Faryal Rafiq', 'Shaheed-e-Millat Road Branch, SINDH BANK LIMITED k', '03453564206', '', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(69, 'Customer', 'Tariq Memon', 'G 18 7th Gizri Street Phase 4 DHA karachi', '03332392782', 'Posch care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(70, 'Customer', 'Kashif Jamil', '213, Amber Pride, Block 6, PECHS', ' 03111839392', 'M/S POSCH Care', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(71, 'Customer', 'UBL - Tanki Branch', 'Kalaboard', '2134508300', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(72, 'Customer', 'M/S Hashlob', '107/108, Amber Pride, PECHS', '03212118833', '', 'Pakistan', 1, 'arsalanbostan08@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(73, 'Customer', 'Kamran Ahmed', 'A52, Block 13/D, Gulshan e Iqbal', '03333239354', '', 'Pakistan', 1, 'aaqibk@hotmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(74, 'Customer', 'UBL - Bhattai Colony Branch', 'Korangi crossing , Bhattai Colony', '2135110791', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(75, 'Customer', 'UBL - Nazar Muhammad Goth', 'Nazar goth', '3168250020', '', 'Pakistan', 7, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(76, 'Customer', 'Aziz Ahmed', 'Channa government degree college Mehar District da', '03456999444', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(77, 'Customer', 'OURANGZEB', '603 6TH FLOOR BUSINESS AVENUE MAIN SHAHRAH E FAISA', '03002211553', 'DIGITONIC LABS', 'Pakistan', 1, 'A.H.ELECTRONICS@HOTMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(78, 'Customer', 'Shahzaib soomro', 'Meezan Bank Gulshan-e-Iqbal Branch', '33623922807', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(79, 'Customer', 'UbL - Gulshan-e-Hadeed Branch', 'Gulshan-e-Hadeed', '3333589223', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(80, 'Customer', 'Amjad', 'saima mall', '03402298398', 'CNM Mart', 'Pakistan', 1, 'tauqeermanzoor123@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(81, 'Customer', 'Mr. Tariq', 'Meezan Bank - Disco Bakery Branch', '3332197425', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(82, 'Customer', 'ASLAM', 'AYESHA ARCADE GULSHAN IQBAL BLOCK 2', '03002444410', 'NIL', 'Pakistan', 1, 'amjad.ishaq88@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(83, 'Customer', 'UBL - Chakwal District ', 'Chakwal', '3343903065', '', 'Pakistan', 8, 'ahsaniqbal@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(84, 'Customer', 'Zubair Khan', 'Bank Islami Shahbaz Commercial', '3422400431', '', 'Pakistan', 1, 'zia.mkhan91@gmail.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(85, 'Customer', 'JAMAL ALAM', '03456124975', '03456124975', 'K-ELECTRIC PVT LTD', 'Pakistan', 1, 'ALAMJUNAID@HOTMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(86, 'Customer', 'Umar Afzal', 'Pharmacy Store, Shahrah-e-Faisal Near Habbitt', '03036660997', '', 'Pakistan', 1, 'umarafzal11@yahoo.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(87, 'Customer', 'SOHAIL BUTT', 'FB AREA BLOCK 7 AL GHAFOOR HEAVEN', '03332125146', '', 'Pakistan', 1, 'SOHAIL.ULHASSAN@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(88, 'Customer', 'Rukhsana Khatri', 'Amber Pride', '03332469402', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(89, 'Customer', 'Fawad ul Haq', 'Stationary, Amber Pride, Block 6, PECHS', '03012512220', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(90, 'Customer', 'Mr. Sami Saghir', '1-A/1, Sector 20. Korangi Industrial Area', '03333284057', 'M/S CLARIANT CHEMICAL PAKISTAN (PVT) LTD', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(91, 'Customer', 'Fareed', 'Sharah e Faisal', '3011173773', 'System Ltd', 'Pakistan', 1, 'zia.mkhan91@gmail.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(92, 'Customer', 'IMRAN NAGORI ', 'UBL KHAYABAN E JAMI', '0300232121', 'UNITED BANK LIMITED', 'Pakistan', 1, 'AMJAD.ISHAQ@POSCHCARE.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(93, 'Customer', 'Rehan ', 'Burger Lab, Khayaban-e-Rahat, DHA', '03018422210', 'Burger Lab', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(94, 'Customer', 'Anwar Shamim', 'UBL - Still Mill Branch', '3452139076', '', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(95, 'Customer', 'Kutyana Memon Hospital', 'Kharadar', '2132313837', '', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(96, 'Customer', 'Shahzaib Khanan', 'Gulistan e johar block 17', '3343581671', 'Hashlob', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(97, 'Customer', 'Khawaja Aqib Shaukat', '213, Amber Pride, Block 6, PECHS', '03333737896', 'POSCH Care', 'Pakistan', 1, 'aaqibk@hotmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(98, 'Customer', 'Mr. Emaad', 'Khayaban e Nishat Branch', '3331024782', 'Bank Islami', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(99, 'Customer', 'Farhan Ahmed Raza', '20-C, Mezzanine Floor, Indus Center, 14th Commerci', '2135390481', 'IDEA', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(100, 'Customer', 'Abtach Ltd', 'Clifton, Block 8', '2132462414', '', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(101, 'Customer', 'Sidra', 'UBL - K Area Branch', '2135040530', 'United Bank Limited', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(102, 'Customer', 'Taha Ali', 'House no p-8 , State Bank of Pak bungalows, Tipu S', '03121041870', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(103, 'Customer', 'USMAN KHAN', 'SHAHRAH E FAISAL branch', '03333076235', 'MCB', 'Pakistan', 1, 'OPS0042@MCB.COM.PK', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(104, 'Customer', 'Naz Rasool ', 'UBL - Airport', '3332242983', 'United Bank Limited', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(105, 'Customer', 'Umair', '205, Amber Prider, Block 6, PECHS', '03110838403', 'POSCH Care', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(106, 'Customer', 'Aqib Sohail Mazher', '213, Amber Pride, Block 6, PECHS', ' 03218739020', 'POSCH Care', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(107, 'Customer', 'Mr. Naveed Naqvi', 'NBP Branch, 7th Floor, Clifton Diamond Building, B', '03012259509', 'NBP Funds', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(108, 'Customer', 'Taimur Mirza', 'Office No. 3, 2nd Floor, 9-C, Street No. 10, Badar', '03002016262', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(109, 'Customer', 'Nasser', 'House # 22 (TOP BELL), 1st Street, Off Khayaban-e-', '03452005872', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(110, 'Customer', 'Sadia', 'UBL - Babar Market Branch', '3422640918', 'United Bank Limited', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(111, 'Customer', 'Muhammadi Exchange', '207, Amber Pride, Block 6, PECHS', '03218922511', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(112, 'Customer', 'Serat Jehan', 'A-116,al falah housing project, malir halt Karachi', '3332459067', 'Posch Care', 'Pakistan', 1, 'info@poschcare.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(113, 'Customer', 'Atif', 'A183 gulshan iqbal block 1', '03102315109', 'nil', 'Pakistan', 1, 'amjad.ishaq1988@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(114, 'Customer', 'Zain PCE', 'Al rasheed chamber, Shop No. 7,8,9 Shahrah-e-Faisa', '', 'Pakistan Currency Exchange ', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(115, 'Customer', 'Mr. Muhammad Haroon', '34/A-3, 2nd Floor, Lalazar Drive, Queens Road, Opp', '03008298941', 'M/S Crescent Syndicate', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(116, 'Customer', 'Mr.Salman', 'Plot No. 38 | Sector 15, Korangi Industrial Area |', '3330134135', 'Oxford University Press', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(117, 'Customer', 'Mr.Rehan', 'Rahat commercial', '03018422210', 'Burger Lab', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(118, 'Customer', 'Nazeer Ahmed', 'H/No. 200, Aachar Street, Hathi Gate, Near HMB Tow', '03058049141', '', 'Pakistan', 9, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(119, 'Customer', 'Arshad Meher', 'Office # 207/208, Amber Pride, Block 6, P.E.C.H.S', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(120, 'Customer', 'Asad', 'Mehmoodabad Branch', '3111839392', 'United Bank Limited', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(121, 'Customer', 'Hanif ', 'Rashid Minhas Branch', '3468200710', 'Meezan Bank', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(122, 'Customer', 'tauqeer', 'dha', '03332723062', '', 'Pakistan', 1, 'tauqeermanzoor123@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(123, 'Customer', 'Faheem c/o Irfan Bostan', 'Amber Pride, Shara e Faisal, Karachi', '3218739020', '', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(124, 'Customer', 'Sindh Police ', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(125, 'Customer', 'Muhammad Zahid', 'Green Town, Shah Faisal', '03120828304', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(126, 'Customer', 'Syed Arsalan', '107/108, Amber Pride, Block 6, P.E.C.H.S', '03132956825', 'POSCH Care', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(127, 'Customer', 'Muhammad ZIa Khan', 'dha', '3302821688', 'Posch Care', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(128, 'Customer', 'Hammad', 'Saima Mall', '03407383316', 'H & M', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(129, 'Customer', 'Care Medical', 'dolmen mall, main tariq road', '03320756232', 'Care Medical', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(130, 'Customer', 'Khalid', 'Khayaban e Shahabz', '3323089081', 'Bank Islami', 'Pakistan', 1, '', 'Islam', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(131, 'Customer', 'Ahsan Iqbal', 'Shahra-e-faisal', '03132117840', 'posch care', 'Pakistan', 1, 'ahsaniqbal@poschcare.com', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(132, 'Customer', 'Rustam Guard', 'Amber Pride, Block 6, PECHS', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(133, 'Customer', 'Omer Naseer', 'Office 402, 4th floor, ITC building, Behind new UB', '03458272112', 'Ameen Trading Co.', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(134, 'Customer', 'Raheel', 'Park Tower, 1st Floor', '2135876912', 'Rashid Saloon', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(135, 'Customer', 'Sikandar', 'Meezan Bank Sohrab Goth Branch ', '03052270734', 'Meezan Bank', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(136, 'Customer', 'asdsad', '234 - FF Commercial Block DHA Phase 4 Lahore, paki', '03342974083', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(137, 'Customer', 'Arsalan', 'Shahra-e-faisal', '3452289641', 'Suzuki Motors', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(138, 'Customer', 'Syed Arshad Masud', 'Plot# 4, Near Cantonment Board Faisal Main Shahrah-e-Faisal', '3002246923', 'Suzuki Motorways', 'Pakistan', 1, 'motorways1@hotmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(139, 'Customer', 'Mujtaba Manzoor', 'Zaib House 58-B/2, Khayaban-e-Shahbaz, Phase 7 DHA', '03233254425', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(140, 'Customer', 'Adnan', 'UBL Kashmir Road Saddar Rawalpindi', '3365378151', 'UBL Rawalpindi', 'Pakistan', 10, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(141, 'Customer', 'Mudassir', 'Davis road ', '', 'Capital tv ', 'Pakistan', 2, 'Nil', 'Nil', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(142, 'Customer', 'Naeem', 'UBL - Landhi Industrial Branch', '3363862547', 'United Bank Limited', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(143, 'Customer', 'Rizwan', '24-A, Masson road near PDMA office', '', 'Hum tv ', 'Pakistan', 2, 'Nil', 'Nil', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(144, 'Customer', 'Dawood', 'Model town link road branch', '', 'Fine Pizza ', 'Pakistan', 2, 'Nil', 'Nil', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(145, 'Customer', 'rao basit', 'lahore', '3002599183', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(146, 'Customer', 'Samira', 'UBL main Branch 0022 GT rd Gujar Khan Punajb', '3219515221', 'UBL Gujar Khan', 'Pakistan', 11, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(147, 'Customer', 'Muneeb Ahmed', 'Khalid Bin Waleed Road', '03453297929', 'Fit And Fight Gym', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(148, 'Customer', 'Asad Paracha', '142/1 P Street, Khayaban-e-Rahat, Phase 7, DHA', '03061531223', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(149, 'Customer', 'Haris Rafiq', 'Plot # 355, Flat# 205, Shangrilla Garden, Shahani Street, Garden east', '03452751494', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(150, 'Customer', 'Ahtisham', 'Shahra-e-faisal', '03132117840', 'Hashlob', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(151, 'Customer', 'Arsalan Bostan', 'Shahra-e-faisal', '03212118833', 'Hashlob', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(152, 'Customer', 'Natasha', 'Bcs Office Behria Complex 2 3rd florr mt khan rd ', '3333222149', '', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(153, 'Customer', 'SAMPLE ACCOUNT', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(154, 'Customer', 'zeeshan zamir', 'Karimabad', '3132117840', '', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(155, 'Customer', 'RAZA', 'SHOP NO 1 COURT VIEW BUILDING CAMPBELL STREET NEXT TO UBL ', '03363378813', 'KHURRAM SCIENTIFIC CENTER', 'Pakistan', 1, 'RAZA_180@HOTMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(156, 'Customer', 'TARIQ', 'SHOP NO 1 GULSHAN IQBAL 13D/2 OPP RUFI CENTRE', '03363656887', 'AHSAN GENERAL STORE', 'Pakistan', 1, 'FREHMANKT@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(157, 'Customer', 'ALAM', '13D/3 NEAR NOMAN COMPLEX GULSHAN', '03002321367', 'ALAM MEDICAL STORE', 'Pakistan', 1, 'FREHMANKT@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(158, 'Customer', 'FARAZ', 'SHOP NO 7 ABID TOWN IQRA UNIVERSTITY GULSHAN', '03002238935', 'FARAZ GENERAL STORE', 'Pakistan', 1, 'FREHMANKT@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(159, 'Customer', 'ANAS', 'SHOP NO 4 , SHOAIB PLAZA IQRA UNIVERSITY ABID TOWN GULSHAN', '03330250734', 'GHOUSIA GENERAL STORE', 'Pakistan', 1, 'FREHMANKT@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(160, 'Customer', 'NADEEM', 'SHOP NO 1 RIZWAN TOWN PLAZA KDA MARKET GULSHAN', '03312812941', 'SUPER RICE GENERAL STORE', 'Pakistan', 1, 'FREHMANKT@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(161, 'Customer', 'PPHI DISTRICT OFFICE MIRPURKHAS', 'PPHI DISTRICT OFFICE MIRPURKHAS-A,  Bungalow no 22, Saadi Town, Near Main Bus Terminal Mirpurkhas', '03453203215', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(162, 'Customer', 'SUBHAN', 'GULSHAN IQBAL BLOCK 2 ', '03002540251', 'TRADE N TRADE', 'Pakistan', 1, 'NIL', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(163, 'Customer', 'FAIZAN', 'PLOT R784 BLOCK T NORTH NAZIMABAD ', '03472564701', 'FAIZAN ASSOCIATE', 'Pakistan', 1, 'FREHMANKT@GMAIL.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(164, 'Customer', 'Faraz', 'Shop#19 nazia apt Block-6 Opp Disco Bakery', '3333343136', 'Bilawal super Store', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(165, 'Customer', 'Farooq', 'Shop # 11 Civic View Block-13D Gulshan', '311323351', 'Al-Noorani Store', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(166, 'Customer', 'Saad Mateen', '17-A 1/1 Golf Course Road #1 Phase 4 DHA', '3337702201', 'Psoch care', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(167, 'Customer', 'Umair', 'Malir 15', '3002241186', 'UBL Malir City Branch', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(168, 'Customer', 'Akhter Chandiyo', 'FF3 Block 29 Seaview Appt, DHA Phase 5 Karachi', '3322350536', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(169, 'Customer', 'JAISSE', 'F50 E STREET BLOCK 4 CLIFTON', '03033331883', 'NGENTS', 'Pakistan', 1, 'FRONTDESK@NGENTS.COM', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(170, 'Customer', 'ARSALAN', 'SHOP NO G4G AL SYED SQUARE GULSHAN IQBAL BLOCK 13D1', '03129023444', 'A.A MINI MART', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(171, 'Customer', 'Bank Alfalah (Mr Zulfiqar Khan)', 'Tariq Road Branch, Karachi', '03222528691', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(172, 'Customer', 'Anwar qureshi', 'Bandhan Marquee Choa Saiden Road Sutwal Tehseel & District Chakwal', '3365215288', 'Bandhan Marquee ', 'Pakistan', 8, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(173, 'Customer', 'Suneel', '204.f PECHS Block-2 Near Jheel Park Tariq Road', '3411291946', 'The Smart School', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(174, 'Customer', 'ahtisham ul haq', 'Office 59/3 Abdali Road Multan Near JS Bank', '3008632962', 'Fazal cloth Mill', 'Pakistan', 18, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(175, 'Customer', 'Shahzad', 'PRC Tower Building FBr LTU Department Lalzar', '3333295070', 'PRC Tower Building FBr LTU Department Lalzar', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(176, 'Customer', 'yaqoob', 'shop no 2 ayesha arcade moti mahal block 2 gulshan', '03326858094', 'smart diapers', 'Pakistan', 1, 'frehmankt@gmail.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(177, 'Customer', 'UMAIR', 'K-Area', '3002241186', 'UBL K-Area Branch', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(178, 'Customer', 'Umair', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(179, 'Customer', 'Ahmer Bashir', 'Babar Market', '3002241186', 'UBL - Babar Market', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(180, 'Customer', 'Manzzor', 'Office No 103 Amber Pride 2nd Floor ', '', '', 'Pakistan', 1, '', 'ISLAM', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(181, 'Customer', 'Ghufran', 'Steel Mill', '03332420952', 'UBL - Steel Mill Branch', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(182, 'Customer', 'Meezan Bank Head Office', 'Gulshan-e-Hadeed', '3332894797', 'UBL - Gulshan-e-Hadeed', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(183, 'Customer', 'Meezan Bank Sample', 'Gulbai', '3468200037', 'Meezan Bank', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(184, 'Customer', 'Star Dental', 'Gulbai', '3468200037', 'Meezan Bank', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(185, 'Customer', 'UBL Malir City Sample', 'Office 111, 2nd floor, Amber Pride, Sharah e Faisal, Karachi.', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(186, 'Customer', 'UBL Malir City Sample', 'Malir 15', '3002241186', 'UBL', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(187, 'Customer', 'MAAZ ', 'Malir 15', '3002241186', 'UBL', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(188, 'Customer', 'Arsalan Akbar', '', '03042224572', 'PCE', 'Pakistan', 1, '', 'Islam ', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(189, 'Customer', 'Salma Altaf', 'Progressive Centre Shahra-e-Faisal', '3422111006', 'Software House', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(190, 'Customer', 'muhammad raza', 'Karimabad', '3132117840', 'House Wife', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(191, 'Customer', 'Khurram', 'City Court Near UBL Bank', '3363378813', 'Supplier ', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(192, 'Customer', 'Waseem Ahmed', 'Soldier Bazar No. 2 Near Shell Petrol Pump', '03219284892', 'AAN Decoration', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(193, 'Customer', 'Ashfaq Ahmed ', 'Street # 8, House # 6, Servey 87, Golden Town Airport karachi.', '3139222107', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(194, 'Customer', 'Sheraz', 'Office No 209 Amber Pride PECHS KARACHI', '03343633644', '', 'Pakistan', 1, '', 'ISLAM', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(195, 'Customer', 'ASAD', '20th Floor, Techno City, I.I Chundrigar Road', '3335978978', 'Bank Al HAbib', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(196, 'Customer', 'suleman saleem', 'DHA PHASE 6', '3472571446', 'DUN & BRADSTREET', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(197, 'Customer', 'Arslan khan', '', '03328975656', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(198, 'Customer', 'MUHAMMAD ALI BAIG', '', '3142120811', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(199, 'Customer', 'Narjis', 'B 14 SANA AVENUE BLK 12 GULSHAN E JOHAR', '03343410996', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(200, 'Customer', 'seema F', 'UBL Cantt Branch Near GPO Chowk Rawalpindi', '3315130838', 'UBL - Cantt Branch Rawalpindi', 'Pakistan', 3, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(201, 'Customer', ' Sidra Imran', 'Dha phase 1', '3219499944', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(202, 'Customer', 'Zahida Zahid', 'Dha phase 1', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(203, 'Customer', 'Rehana Tahir', 'SHAHFAISAL COLONY', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(204, 'Customer', 'Seema F', 'Malir can\'t', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(205, 'Customer', 'Tasawar abbas ', 'Dha Phase 6', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(206, 'Customer', 'asif noman', '', '03042224466', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(207, 'Customer', 'ali medical store', '', '03229457914', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(208, 'Customer', 'lala store', 'korangi road near NMC', '3312227721', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(209, 'Customer', 'pasha store', 'DHA PHASE 1 ', '03357546551', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(210, 'Customer', 'Faryal Azam', 'house no 113/1 lane 6 khayaban e  Rahat Phase 7 DHA', '03238226747', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(211, 'Customer', 'Basharat', 'House No 113/1 Lane 6 Khayaban E  Rahat Phase 7 DHA', '03238226747', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(212, 'Customer', 'Tariq Mahmood', 'Office No 112 Amber Pride Shahra E Faisal ', '03234456282', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(213, 'Customer', 'Tahir Jamali', '', '03042227513', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(214, 'Customer', 'Mujtaba', 'PECHS Shahra E Faisal ', '', '', 'Pakistan', 1, '', 'Islam', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(215, 'Customer', 'Faizan Karimi', 'Plot No. 91, Grove Residency, Adjacent KPT Interchange, Near DHA Phase 7 Ext', '2135892005', 'Dolmen Pvt Ltd', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(216, 'Customer', 'Sheraz', 'Soneri Bank Ltd., 10th Floor, PNSC Building, M.T. Khan Road,', '111567890', 'Soneri bank Ltd', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(217, 'Customer', 'Sarmad Saleem', 'Shaheed e Millat Road ', '3213789586', 'MCR (Pvt) Ltd', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL);
INSERT INTO `vendors` (`id`, `v_type`, `name`, `address`, `c_no`, `company`, `country`, `c_id`, `email`, `state`, `postalCode`, `c_group`, `p_group`, `VAT`, `GST`, `u_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`, `name2`, `c_no2`) VALUES
(218, 'Customer', 'POSCHCARE', 'UBL Kallar Syedan Tehsil Kallar Syedan District Rawalpindi', '3215146588', 'UBL Kallar Syedan', 'Pakistan', 10, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(219, 'Customer', 'Arsalan Bostan', 'OFFIC ENO 205 Amber Pride PECHS', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(220, 'Customer', 'AHMED NAVEED', 'Muhammadi House House # 08-A, West Avenue Phase 1 Dha Karachi', '3212118833', '', 'Pakistan', 1, 'arsalanbostan08@gmail.com', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(221, 'Customer', 'MUHAMMAD SOHAIL', 'AHMED NAVEED ST NO 04 H NO 94-l-8/2 ', '03135187153', '', 'Pakistan', 3, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(222, 'Customer', 'Imran', '29B/1 PECHS BLOCK 6 ', '03333437876', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(223, 'Customer', 'Imran', 'Plot 30 C Phase 5 Flat 4 3rd Floor Stadium Lane 1', '03072637240', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(224, 'Customer', 'Talha', 'Plot 30 C Phase 5 Flat 4 3rd Floor Stadium Lane 1', '03072637240', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(225, 'Customer', 'Faizan', 'Airport', '21111345111', 'DHL Express', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(226, 'Customer', 'adeel', 'C-109 Amir Khusro Road? KDA Scheme 1', '2134375701', 'Paragon Construction', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(227, 'Customer', 'Muhammad Khan', 'Adeel 35 - 37 Main Khayaban-e- Momin, Sindh Karachi - DHA Phase 5 Karachi - DHA Pakistan', '3162226662', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(228, 'Customer', 'Muhammad Khan', 'Rehman Medical Store Chenab Nagar Dist. Chiniot Punjab Chiniot Chiniot Chiniot Pakistan', '3337704249', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(229, 'Customer', 'Laiba ', 'Rehman Medical Store Chenab Nagar Dist. Chiniot Punjab Chiniot Chiniot Chiniot Pakistan', '03337704249', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(230, 'Customer', 'Areeb', 'Block F2 Wapda town phase 1 ', '04235210283', 'Depilex Saloon', 'Pakistan', 2, 'raobasitali1993@gmail.com', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(231, 'Customer', 'Areeb', 'shah faisal colony aswan town ', '03322209551', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(232, 'Customer', 'HASSAN MUSTAFA', 'shah faisal colony aswan town ', '03322209551', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(233, 'Customer', 'Dr Mukhtiar Chandio', 'CHURCH ROAD ALLAMA IQBAL TOWN RAHIM', '03029666733', '', 'Pakistan', 6, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(234, 'Customer', 'umair khan', 'life Pharmacy shop no 10 Rani Arcade wadhu wah road qasimabad Hyderabad Sindh Hyderabad Qasimabad Hyderabad Pakistan', '3331449444', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(235, 'Customer', 'abdul rehman', 'lepord office havelian abbottabad Khyber Pakhtunkhwa Abottabad Ayub Medical Complex Abottabad', '3161567655', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(236, 'Customer', 'Rashid', 'A181 block 5 gulshan iqbal karachi Sindh Karachi - Gulshan-e-Iqbal Block 5 Karachi - Gulshan-e-Iqbal', '3132818746', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(237, 'Customer', 'Siyal ', 'Charcoal fair price Shop ferozpor road opposite nishter colony', '03088889054', 'Charcoal', 'Pakistan', 2, 'raobasitali1993@gmail.com', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(238, 'Customer', 'THE VAULT', 'Siyal Chowk Shadab Colony', '3002599183', 'Siyal Super Store', 'Pakistan', 2, 'raobasitali1993@gmail.com', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(239, 'Customer', 'Abdullah ', 'EMERALD MALL CLIFTON', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(240, 'Customer', 'Ali', 'Gulshan-e-ravi house#573 E-block', '03204835140', 'Private ', 'Pakistan', 2, 'Nil', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(241, 'Customer', 'CIA SADDAR', 'Model town link road GECHS Phase 3 Muhammad pura', '03074157172', 'Medicos', 'Pakistan', 2, 'Nil', 'Punjab ', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(242, 'Customer', 'Hina Khan', 'NEAR RAINBOW CENTER SADDAR', '03343470426', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(243, 'Customer', 'Nosheen Waheed', 'Capital Residentia near Marquees E11 main Margalla road.Tower Blue apat 306 Islamabad Islamabad - E E - 11 ', '03168828483', '', 'Pakistan', 3, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(244, 'Customer', 'Sana Ullah', 'Chakwal Punjab', '3132117840', '', 'Pakistan', 8, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(245, 'Customer', 'Muneeb Akhtar', 'Flat No B-3 2nd Floor Lashari building opposite alzohra trust street 11', '03173820550', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(246, 'Customer', 'Adeel Baig', 'Fit And Fight yGm', '345 3297929', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(247, 'Customer', 'Blue Cherry Nutrition', '291 Tipu Sultan Block sector F Bahria Town Punjab Lahore - Bahria Town Tulip Block', '03008471652', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(248, 'Customer', 'Masood', 'Shop No G-20 Jumera Mall Tariq Road', '0333 3737896', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(249, 'Customer', 'zubair soomro', 'Masood Spencer Pharmacy 155- A H.B.F.C, Faisal Town Opposite Jinah Hospital Faisal Town Lahore Pakistan Punjab Lahore', '3008488111', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(250, 'Customer', 'Asfand ', 'D-22 Sector 1 Sukkur Township Sindh Sukkur', '3452222051', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(251, 'Customer', 'ZEESHAN BOSTAN', 'Model town link road mc donald opposite ', '03164702079', 'Doctor pharma', 'Pakistan', 2, 'Asfandyar5@gmail.com', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(252, 'Customer', 'Saleem', 'G-10', '', '', 'Pakistan', 3, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(253, 'Customer', 'saba', 'Peco Road', '', 'Private', 'Pakistan', 2, 'Nil', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(254, 'Customer', 'Sana', 'Mcb Bank Shah Faisal Branch', '03070204311', 'MCB Bank', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(255, 'Customer', 'HIBA KHAN', 'SD 369 STREET 3 FALCON COMPLEX SHAHEED MILLAT ROAD ', '03018207947', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(256, 'Customer', 'MUSTAJAB FATIMA', 'FLAT NO 404 BLK K-21 IQRA COMPLEX GULISTAN E JOUHAR', '3172117207', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(257, 'Customer', 'Umair  ', 'DFB LODHYIA GYPSUM INDUSTRIES OFFICE 105 IST FLOOR STATE BANK CHUNDIGARH ROAD', '0308290270', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(258, 'Customer', 'HUNAIN', 'Memon goth', '3002241186', 'UBL - Memon Goth Branch', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(259, 'Customer', 'ASIF MAHMOOD', 'BOLTON MARKET', '03077222000', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(260, 'Customer', 'irfan', 'KAREEM BLOCK PUNJAB ', '03364608418', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(261, 'Customer', 'YASMIN MUGHAL', 'Rashid Minhas Rd', '03083652820', 'UBL', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(262, 'Customer', 'Arsalan', 'r 42 Block 13d/3 baloch goth  gulshan iqbal ', '03022202562', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(263, 'Customer', 'Haalim', 'PIA Near Airport UBL Branch', '21399249171', 'UBL - PIA', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(264, 'Customer', 'Sarah Nasa', 'no e-177 st no 1/3  moh arif abad bhatta kohard ', '03484320899', '', 'Pakistan', 13, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(265, 'Customer', 'YASMIN MUGHAL ', '80/1 18th lane off khybane e hilal phase 7 DHA', '03343296513', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(266, 'Customer', 'SUMBUL ABSAR', 'R 42 BLOCK 13D/3 BALOUCH GOTH GULSHAN IQBAL ', '03022202562', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(267, 'Customer', 'ATIR AHMED', 'H NO 301 BLOCK 9 DASTGIR SOCIETY FEDERAL AREA', '03333271918', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(268, 'Customer', 'SARAH NISA ', 'FLAT NO 204 BURJ VIEW BLOCK H NAZIMABAD', '03168622037', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(269, 'Customer', 'MARYAM YASEEN', '80/1 18TH LANE OFF KHYBAN E HILAL PHASE 7 DHA', '03343296513', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(270, 'Customer', 'SANA ULLAH', 'H NO E17/8 STREET NO 1 LINK 1 SUPER TOWN ', '03000 811188', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(271, 'Customer', 'farida', '16/1 C MOHAMMAD ALI SOCIETY ', '03322184640', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(272, 'Customer', 'Attia Malik', 'Chakwal Punjab', '3333879726', 'House Wife', 'Pakistan', 8, '', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(273, 'Customer', 'Maryam Yaseen', '572 f gulshan ravi lahore', '3044802730', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(274, 'Customer', 'Madyan Jahanzeb ', 'House number E17/8 , street number 1, link 1, super town', '3000811188', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(275, 'Customer', 'Salman raja', 'Askaribank bank abpara branch Islamabad', '3335301062', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(276, 'Customer', 'Syed chan shah', '5, house no 126, askari 1, opposite jhnda chichi', '3355612766', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(277, 'Customer', 'HASSAN KHAN', 'musa traders super market kahcheri road rakhsha stand muzaffarabad', '3005143208', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(278, 'Customer', 'SULEMAN ', '28 B3 PUNJAB SOCIETY NEAR WANPA TOWN PIA ROAD PGECHS PHASE 1 BLK B-3 ', '03008111189', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(279, 'Customer', 'Abbas', 'REGAL MANSION MOBILE MARKET NEAR BVS PARSI SCHOOL SINDH SADDAR', '03212310229', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(280, 'Customer', 'Rias mumtaz', 'Pechs Block 3, 164/F, Flat # C-4, Al rehmat Appt, Opp Central Model High School, Karachi. ', '3312105689', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(281, 'Customer', 'Mughees butt', 'Street no 2, Muhlla meer abid Hussain, old Sadiqabad.', '3156719898', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(282, 'Customer', 'Uzma Ali Goher', 'Address:740 k block sharuknealam ', '3166643702', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(283, 'Customer', 'Saira Farukh', 'Block J North nazimabad Karachi house on B 178', '3312119364', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(284, 'Customer', 'Rafi Raza', '67 Khayaban Tariq, DHA phase 6', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(285, 'Customer', 'KAMRAN PCE', '10-D ASKRI 2CHI', '03004802791', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(286, 'Customer', 'Tania Shahid', 'PCE HEAD OFFICE KARACHI.', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(287, 'Customer', 'UMAIR BAIG', 'Bahria Town Ph.3 House 1381 BC', '3331582004', '', 'Pakistan', 10, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(288, 'Customer', 'Tahmena H Khan', 'H NO  C125 BLOCK I, NOERTH NAZIMA ABAD NEAR BATHA MASJID ', '03342490909', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(289, 'Customer', 'Hina', '5 B 5TH Central land DHA Phase 2', '03312213300', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(290, 'Customer', 'Maham Abbasi', 'Gujranwala waste management company, second floor chamber of commerce and industry, aiwan-e-tajarat road', '3041925990', '', 'Pakistan', 14, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(291, 'Customer', 'Urva', 'House #794 street num 29 G-15/1 ', '03165499227', '', 'Pakistan', 3, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(292, 'Customer', 'Urva', 'street 3 phase 4 officers colony Wah cantt', '03335138533', '', 'Pakistan', 15, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(293, 'Customer', 'Asghar Ali', 'street 3 phase 4 officers colony Wah cantt', '03335138533', '', 'Pakistan', 15, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(294, 'Customer', 'Javeria Ahmed', 'Street no:8, house no:267, IJ Colony Kharian Cantt Kharian', '03335534283', '', 'Pakistan', 19, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(295, 'Customer', 'HASNAIN ABBAS', '13 floor centre point off shared Millat expressway near kpt Interchange flyover', '03008254572', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(296, 'Customer', 'Effat Khanzada', 'H NO 10-N Block 6 PECHS shahra e faisal ', '03182971822', '', 'Pakistan', 1, '', ' islam', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(297, 'Customer', 'Sohail Khakwani', 'house no ic-74khanzada mohallah sakrandnear harmain super store distric nawabshah sakrand Sindh', '03323220900', '', 'Pakistan', 16, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(298, 'Customer', 'Abdul Basit', '11A.block g.gulberg 2.mustaq Ahmed gurmani road', '03009499474', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(299, 'Customer', 'Aman Ahmed', '', '3212118833', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(300, 'Customer', 'Sehaam', '167 shahjamal', '03008835981', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(301, 'Customer', 'Isna Maqsood', '176 block N street 7 phase 6 Dha', '3222220585', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(302, 'Customer', 'Kehkashan Mustafa', '4D New garden block. Saeed colony 2', '03218659667', '', 'Pakistan', 17, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(303, 'Customer', 'Wardah Khan', '189b block7/8 K.M.C.H.S justice inmullah road  Karachi', '3242166470', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(304, 'Customer', 'Laeeq', 'H NO B-251 Ground Floor Block 2 Gulstan E Johar ', '03363655110', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(305, 'Customer', 'Shamaim', 'Mohallah akakhiel village badrashi tehsil and distt Nowshera KPK', '03183443024', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(306, 'Customer', 'Mohammad Dochki', ' House number : 301, capital street,H-13 Islamabad', '03335913998', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(307, 'Customer', 'Natasha Ahmed', '113/1, 12th street, khayaban e rahat dha phase 6 karachi Sindh Karachi - DHA Phase 6 Karachi - DHA', '3218232789', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(308, 'Customer', 'Fatima shamshad', 'House 2, Street 14, F-7/2 Islamabad', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(309, 'Customer', 'Seema Sadiq', 'Agha Street , house # B-VI,507 , Muhallah Muhammad pura Daska Punjab', '3017144445', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(310, 'Customer', 'Faiz ', 'Flat No. 511 sea world residency block 6 near ziauddin hospital cilifton', '3432280427', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(311, 'Customer', 'Javed Iqbal', 'Rashid Minhas Road', '3373172353', 'Bank Al Habib', 'Pakistan', 1, '', 'Islam', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(312, 'Customer', 'Test', ' behind qasim marriage hall Punjab Gujrat Lala Musa Gujrat Pakistan', '3334311347', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(313, 'Customer', 'Test', 'karachi pakistan', '03112255221', 'Test Company', 'Pakistan', 1, 'abc@gmail.com', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(314, 'Customer', 'HADIA', 'karachi', '033323323232', 'karachi', 'Pakistan', 1, 'ab@gmail.com', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(315, 'Customer', 'Shahzad ', '412-D3 street:7 Wapda Town', '03224033458', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(316, 'Customer', 'Kanwal Shaikh', 'Township Bazar near madni masjid', '03004967167', 'Chini store', 'Pakistan', 2, 'Nil', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(317, 'Customer', 'Asifa khalili', 'House B-50 block 8 AEECH society Gulistane johar', '3322779167', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(318, 'Customer', 'Hasnain Abbas', 'A-1/9 teacher cooperative housing society scheme 33 gulzare hijri', '3222586897', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(319, 'Customer', 'umair ', 'P.C.E.H block number 6 house number 10N shah re Faisal Sindh Karachi - P.E.C.H.S Block 6 Karachi', '3182971822', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(320, 'Customer', 'Arsalan Shaikh', ' No B-33, street 1, liaquat ashraf colony Sindh Karachi - Baloch Colony Baloch Colony Karachi - Baloch Colony', '3110838403', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(321, 'Customer', 'Mina Nadeem', 'B/20 - 710 Malkani Lane Shahi Bazar Hyderabad', '3213081031', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(322, 'Customer', 'Samra Asad', 'House # 272 Kabul Block, DC Colony Gujranwala cantt', '3237404892', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(323, 'Customer', 'Dua Anees', 'R - 801 Block, 17, FB Area Karachi.', '3072253890', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(324, 'Customer', 'Moiz Akhter', 'Flat # 7 Block F, Bhayani Heights Gulshan e iqbal Karachi.', '3343982043', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(325, 'Customer', 'Raees Aziz (Daraz)', '182-1-C1 Town Ship Lahore', '3209467481', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(326, 'Customer', 'Nasir (Daraz)', 'House # 93/5 Near Golf Club Sialkot Punjab', '3004500800', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(327, 'Customer', 'Rafay (Daraz)', 'House # 70/2 30th Street Hilal DHA Phase 6 Karachi.', '3005553370', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(328, 'Customer', 'Urwa Iftikhar (online)', 'Karachi pakistan', '312506961', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(329, 'Customer', 'Qaisar', 'House # 475 Block G1 Johar Town lahore', '3009405037', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(330, 'Customer', 'Maxus Power Group (ONLINE) Banita Adnan', 'House # 149/150, Street # 27, Kashmir Road, Mag Town Sialkot, Punjab. ', '03328361919', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(331, 'Customer', 'Abdul Malik', 'Suite# 15, 4th Floor, 80 west, Malik complex, Blue Area, Jinnah Avenue  Islamabad', '03340627777', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(332, 'Customer', 'Muhammad Ibhrahim (ONLINE)', 'Gulshan Iqbal', '2134986523', 'Shah Commerce', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(333, 'Customer', 'M Afzal', 'Ayan karyana General Store Manika Kenal Road DG Khan.', '3313795533', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(334, 'Customer', 'UBL (MR KAMRAN)', 'Township B2 Sector 4', '03014762162', 'Data Nagri Shop', 'Pakistan', 2, 'Nil', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(335, 'Customer', 'Waqar Mehbob (online)', 'Main Branch Karachi', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(336, 'Customer', 'TAHSEEN', 'H no 1595 st no 13 A sec e Rafi Block safari velly Phase 8 Behria town', '03455329301', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(337, 'Customer', 'DURRIYAH REHMAN', '650 umer block Allama iqbal town Lahore', '03364109015', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(338, 'Customer', 'Zainab Javed', 'Appt.201, D-5/ii, Park Lane, Clifton block 5, near KGS boat basin nehr e khayyam street branch karachi', '03213840949', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(339, 'Customer', 'Talal Adil', 'D-5/37, Ground Floor, Qayyumabad, Korangi Road, Karachi. Sindh Karachi - Korangi Korangi Industrial Area Karachi', '03342647463', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(340, 'Customer', 'Zuhaib Abro', '108, Sadaf Palace, Garden East Sindh Karachi - Garden Garden East Karachi', '03343486236', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(341, 'Customer', 'Rohail Ijaz', 'office # 107 amber pride 1st floor near karachi foods Sindh Karachi - P.E.C.H.S Block 6 Karachi', '03343806304', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(342, 'Customer', 'Sharmeen Arshad', 'Ambar Pride pechs shahr e faisal Sindh Karachi - P.E.C.H.S Block 6 ', '03072984864', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(343, 'Customer', 'Kamil', 'Amber pride, Office # 107, near Karachi Foods, Shahrah e Faisal Sindh Karachi', '03331268853', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(344, 'Customer', 'AMMAR MALIK', '4th floor, room no 7, soldier bazar no 3, Rodrigues street,Karachi Sindh Karachi - M.A Jinnah Road M.A Jinnah Road Karachi', '03471247537', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(345, 'Customer', 'AMMAR MALIK', 'SHOP NO G-20 JUMERA MALL TARIQ ROAD ', '3332023729', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(346, 'Customer', 'AMIS', 'SHOP NO G-20 JUMERA MALL TARIQ ROAD ', '3332023729', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(347, 'Customer', 'Zeeshan Qasim', 'AMIS MARKET KHADA MARKET PHASE 6 ', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(348, 'Customer', 'ZEESHAN QASIM', 'poschcare ', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(349, 'Customer', 'Qaiser Abbas', 'POSCHCARE', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(350, 'Customer', 'sharmeen', 'House No 50 Street No 8-A Bani Gala Green Society Islamabad', '03335176350', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(351, 'Customer', 'Muhammad Usama', 'B-51 Muslim housing society, near mehran tv office Hyderabad Sindh Hyderabad Qasimabad Hyderabad', '03413517700 ', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(352, 'Customer', 'Mahnoor Amir. (blogsbymahnoor)', 'Bilal Masjid Street, house no: DD-168, street no: 19, Ojhri Camp Ground, Rawalpindi Punjab Rawalpindi - Satellite Town Satellite Town Block A Rawalpindi', '03331912296', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(353, 'Customer', 'Umme Habiba (Blog handle)', 'House no L4, block 13 near zuberi family park, Gulshan Iqbal, Karachi', '03350369959', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(354, 'Customer', 'UMAR AYAZ', 'A 529 Block L Street 8 North Nazimabad Karachi', '03341335904', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(355, 'Customer', 'Zarlish Kanwal', 'Fouji foundation hospital Rawalpindi', '03105303492', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(356, 'Customer', 'Mohammad Nasir', 'Gulshan E Iqbal Block 11 Flat name Gulshan E Gala Flat A401', '03343358434', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(357, 'Customer', 'Adil', 'CIA center saddar ', '03213022821', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(358, 'Customer', 'Javairia Rehman', 'AJWA SUPER MARKET REHMAN MASJID DHA PHASE 4', '03337544322', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(359, 'Customer', 'UMER BUTT', 'A-424 3rd Floor, 14th Street, Block H, North Nazimabad Karachi Sindh Karachi - North Nazimabad Block H Karachi', '3335819286', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(360, 'Customer', 'UMER BUTT', 'Mohla bakhshu pora street Ahmad Khan kidr Gulzar e Madina road Gujrat', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(361, 'Customer', 'UNZILA JAWAID', 'Mohla bakhshu pora street Ahmad Khan kidr Gulzar e Madina road Gujrat', '03225570701', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(362, 'Customer', 'NAEEM AHMAD KHARAL', 'Flat no 407, Fourth floor, Lakhani golden tower, Flynn street, East Garden, Karachi', '03312522379', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(363, 'Customer', 'AIJAZ', 'House no184/2 street 33 khayabane Amir khusro phase 6 DHA Karachi', '03022311602', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(364, 'Customer', 'Ramzan', 'City khipro near ubl offices end tcs office khipro. District . Mirpurkhas', '03334266222', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(365, 'Customer', 'HURAIN', 'Township Madina Market Opposite ', '03016670910', '140 Each Dollar shop', 'Pakistan', 2, 'Nil', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(366, 'Customer', 'USMAN MALIK', 'Sonic computer Firdous market shahi road Rahim Yar Khan', '03441064392', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(367, 'Customer', 'ADEEL ANWAR', 'House nmbr 13/b block z scheme no 2 gulshan iqbal rahim yar khan', ' 03071799017', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(368, 'Customer', 'AMAN ALI', 'House no 8 street no 4 block y housing clony layyah Punjab', '03336766111', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(369, 'Customer', 'SALAR SAMI ', 'malir kala borad near tcs Karachi', '03112656984', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(370, 'Customer', 'Adeel anwar', 'Tcs office thari mirwah khairpur mirs', '03152900259', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(371, 'Customer', 'nasreen', 'House no 8 street no 4 block y housing clony layyah', '03336766111', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(372, 'Customer', 'urwa amin', 'C/0 Muhammad afzal umra House no: L-3 T.P.S, WAPDA colony Guddu.P.C: 79220', '03337228791', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(373, 'Customer', 'IQRA (online)', 'urwa amin G 111 street 3 phase 4 Punjab Wah Cantt Officers Colony Wah Cantt', '03335138533', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(374, 'Customer', 'mujtabah pk', 'Arfa Tower Technology Park, floor 5, ferozpur road Lahore', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(375, 'Customer', 'saira Irfan (online)', '58-B/2, Khy e Shahbaz, Phase 7 DHA', '3233254425', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(376, 'Customer', 'Fozia naseem (Online)', 'house 306 area 2/c landhi 3 �/� back side of HBL Sindh Karachi - Landhi Zamanabad Karachi - Landhi', '03162062142', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(377, 'Customer', 'ARSALAN JAN (online)', 'Banglow no 7 judicle colony jail road mardan Khyber Pakhtunkhwa Mardan Sheikh Maltoon Town Mardan', '03335300723', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(378, 'Customer', 'M. Khizar Batla', 'toseefa bad , Dera Ismail khan', '03405214635', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(379, 'Customer', 'Haroon', 'Shop# 02 Akber Market Plot2/53, Chaddi Line, Marriott Road Boltan Karachi', '0336-0204196', 'Saeed\'s Grand Sons (SGS)', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(380, 'Customer', 'M.Usman Dealer', 'CHAHTI STREET, BOLTAN MARKET', '3242062792', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(381, 'Customer', 'DAMSA IMTIAZ (online)', 'Haroon Centre shop no-8 Boltan Market, Karachi', '3002309678', 'Usmna Store wholesale Dealer', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(382, 'Customer', 'MEHREEN AHMAD', 'Street 138 House No 132 Islamic Street Itehad Colony icchra Lahore', '03019497718', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(383, 'Customer', 'JAMSHAID ASHFAQ', 'Arfa Tower Technology Park, floor 5, ferozpur road', '03224600391', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(384, 'Customer', 'SYED ALI RAZA', '167 b block government colony depalpur district okara', '03214830831', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(385, 'Customer', 'AMNA ABID', 'Dress jala pur bhattian, kott jaffer, vehari', ' 03066863780', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(386, 'Customer', 'RANA MEESAM JAFRY (online)', '19k model town 4th floor ( Leo communication office) Lahore', '3040760818', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(387, 'Customer', 'MURTAZA KHAN', 'jafry..house No 112,Block No 5, Sector C2,Near Muslim Chock Green Town Lahore', '3114351064', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(388, 'Customer', 'SHAZIA AMEEN', 'A- 5 B -502 Sector 11 -A North Karachi', '03408365891', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(389, 'Customer', 'FAHMIDA SHAHZOR', 'House no 69/7 Arangabad Quaters Nazimabad no 3 Nearst Malik medical center Karachi', '03161117342', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(390, 'Customer', 'MRS ERROL MATHEW (online)', 'Flat no 103.block 2 gray river appartment korangi crossing Karachi', '03102154826', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(391, 'Customer', 'NASREEN (online)', 'Sayenna Crescent 1st floor flat 105 Raja ghazanfar Ali road saddar Karachi', '03052011760', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(392, 'Customer', 'HAMID HUSSAIN', 'New girls hostel bolan medical college Bruri road Quetta', '03033844820', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(393, 'Customer', 'HAMID HUSSAIN (ONLINE)', 'S 590 korangi no 1 Karachi', 'O3152520939', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(394, 'Customer', 'M Abunasar Shirani (ONLINE)', 'S 590 korangi no 1 Karachi', 'O3152520939', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(395, 'Customer', 'Zaheer', 'House 127, Block A, Street 4, Pak P.W.D Housing Society. Punjab Rawalpindi - Highway Lohi Bhair Rawalpindi', '03219999991', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(396, 'Customer', 'Muhammad Shahid ', 'Shahrah-e-Faisal, Block 6 PECHS', '3410638207', 'Traffic Digital', 'Pakistan', 1, 'Shahrah-e-Faisal, Block 6 PECHS', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(397, 'Customer', 'FARHAN HAMEED', 'House no 8/1 , 6th Zamzama street Dha phase 5 Sindh Karachi - DHA Phase 5 Karachi ', '03321122201', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(398, 'Customer', 'FARHAN HAMEED', 'FESCO/WAPDA CIRCLE, MIANWALI NEAR JHAMBRA GHARBI, M.M ROAD Mianwali', '3067990725', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(399, 'Customer', 'SAHAR ABOR', 'FESCO/WAPDA CIRCLE, MIANWALI NEAR JHAMBRA GHARBI, M.M ROAD Mianwali', '03067990725', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(400, 'Customer', 'Aamir Mehmood', 'A 1657 gulshan e hadeed phase 1 district malir Karachi', ' 03442950363', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(401, 'Customer', 'Khan Baba', 'Aamir Mehmood Ibrahim Lodge Education Street Church Road JHANG Punjab Jhang Jhang', '03346500829', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(402, 'Customer', 'Usman Ayub Chughtai', 'House no.15, Street no.18, Sector- E, DHA-2, Islamabad Punjab Rawalpindi - DHA Phase 2 Rawalpindi ', '03005675205', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(403, 'Customer', 'Rafi', '406 - B , Street # 25 Punjab Lahore - DHA Phase 5 Lahore - DHA Pakistan', '03214070611', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(404, 'Customer', 'RAFI RAZA', 'Faisal Bank Shahr e Faisal Karachi', '03004802791', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(405, 'Customer', 'SOHAIB TANVEER (online)', 'Faisal Bank Shahr E Faisal Road Nursery Karachi', '03004802791', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(406, 'Customer', 'FAREED', 'Muhallah sheikhan no 2 akora khattak KPK', '03348914108', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(407, 'Customer', 'QURATULAIN MAHA', 'Ubl defence Garden branch shop no 29 30 dha phase 1 karachi', '03332278796', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(408, 'Customer', 'QURATULAIN MAHA (online)', 'B-102, Street 7, block 4, Saadi town Karachi', '03062568851', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(409, 'Customer', 'Fayyaz ', ' B-102, Street 7, block 4, Saadi town Karachi', '03062568851', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(410, 'Customer', 'MAIYAMALIK (Online)', '67 M Indusrial Area Quaid e azam', '', 'City Group', 'Pakistan', 2, 'Nil', 'Punjab', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(411, 'Customer', 'ASAD KHAN', 'TCS jauharabad', '03349077124', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(412, 'Customer', 'ASAD KHAN (online)', '57/A .. Ahmed housing scheme..ittefaq town mansoora bazar lahore', '03316696010', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(413, 'Customer', 'SOHA ZAIN (online)', '57/A .. Ahmed housing scheme..ittefaq town mansoora bazar lahore', ' 03316696010', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(414, 'Customer', 'AZRA KHAN (online)', 'C-47/D block 12 Gulistan-e-johar', '03357196346', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(415, 'Customer', 'RANA MEESAM JAFRY (Online)', 'Near govt boys high school kanju chowk kanju swat kpk', '03440970458', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(416, 'Customer', 'HAFSA (onlone)', 'jafry..house No 112,Block No 5, Sector C2,Near Muslim Chock Green Town Lahore', '3114351064', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(417, 'Customer', 'Hamz (Online)', 'KDA Overseas appartments Gulshan e Iqbal block 13 KARACHI', '03162666966', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(418, 'Customer', 'Hamz (Online)', 'KARACHI', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(419, 'Customer', 'ROOSHNA (online)', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(420, 'Customer', 'IFRAH ASIF (ONLINE)', 'Sahiwal Medical College, Sahiwal,opposite DPS school, DPS Road Sahiwal', '03090400718', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(421, 'Customer', 'NIDA (online)', '7a1 tnt abpara housing society near peerus cafe raiwind Lahore', '03021843305', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(422, 'Customer', 'SEHRISH HASRAT (online)', 'House #C-1, Near Polite School Government central Colony, Wahdath Road Lahore.', ' 03231480291', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(423, 'Customer', 'AYESHA NISAR (online)', 'House No. 67, Street 20 Jinnah Garden. Phase 1 Islamabad', '03365272092', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(424, 'Customer', 'Waseem ', 'Pandorian Chak#122/RB. Sangla Hill, district Nankana sahib', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(425, 'Customer', 'HIFZA SHAHID (online)', '8/6 srv 87 golden town karachi', ' 3139222107', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(426, 'Customer', 'FAISAL SIDDIQUI (online)', 'Makkah colony St No 11 House NO 1089 Gulberg III', '3034924443', '', 'Pakistan', 2, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(427, 'Customer', 'MAHNOOR JAMSHED (online)', 'Bank Alfalah gulshan Market branch', '03214614011', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL);
INSERT INTO `vendors` (`id`, `v_type`, `name`, `address`, `c_no`, `company`, `country`, `c_id`, `email`, `state`, `postalCode`, `c_group`, `p_group`, `VAT`, `GST`, `u_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `status`, `name2`, `c_no2`) VALUES
(428, 'Customer', 'MAHNOOR (online)', 'police foundation house no. 571 and street no. 23 Rawat', '03340501632', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(429, 'Customer', 'UMME KULSOOM (online)', 'House 107 street 10 jinnahbad abbottabad', '03000119313', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(430, 'Customer', 'ALIYAH (online)', 'old whadat colony near dr muhamd Ali clinic alshebaz house nmbr b.28', '3302397125', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(431, 'Customer', 'NADIA MALIK (Online)', 'House no 3, Street no 1, G6/3', '3115144095', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(432, 'Customer', 'Usama (online)', 'House no 569, street 18, Chaklala Scheme 3 Rawalpindi', '03435039028', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(433, 'Customer', 'Izma (Online)', 'Gulistane johar blok 12 sind Baloch houseing sucity house num A279.', '03332992016', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(434, 'Customer', 'Fariha khan (ONLINE)', 'AFOHS Complex opposite to malir cantt check post 05 street 04 SDH 319 new malir', '03342708173', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(435, 'Customer', 'URVA (Online)', 'C-122 block C, north nazimabad', '03333123218', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(436, 'Customer', 'Suman', 'Street 3, phase 4 officers colony wah cantt', '3335138533', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(437, 'Customer', 'SYEDA HAMNA HASSAN (online)', 'ubl Malir city branch', '3132988215', 'Ubl malir branch', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(438, 'Customer', 'Bagallery', '0324-4530663', '3244530663', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(439, 'Customer', 'Arsalan UBL', 'PECHS', '3362622254', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(440, 'Customer', 'MOHAMMAD MUSA (online)', 'PIA Transport and Overhaul Branch 1636 United Bank limited Airport Karachi', '3333608903', 'Ubl pia branch', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(441, 'Customer', 'SIDRA (Online)', 'P�438, St #20, Nishtar Abad, Near Arshad Cloth Market, Rajbah Road', '3216606881', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(442, 'Customer', 'Waqar (Online)', 'New girls hostel bolan medical college Bruri road', '3131086294', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(443, 'Customer', 'Mustajab Fatima (online)', '1/1102 shah faisal colony', '03130285773', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(444, 'Customer', 'Arshad', 'DFB lodhia gypsum industries office number 105 1st floor Business finance c nter I. I chundrigar road Karachi front of state bank', '03083290270', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(445, 'Customer', 'Muhammad Arshad', 'Office No 207 Block 6 Amber Pride Karachi', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(446, 'Customer', 'SARAH FARZAND (online)', 'Office No 207 Block 6 PECHS Karachi', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(447, 'Customer', 'Riffaq Fatima (Online)', 'House 10b, Forman christan college', '3370428043', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(448, 'Customer', 'URVA (Online)', 'H# 329,Block A, North Nazimabad near Allimia Masjid', '03322274979', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(449, 'Customer', 'Muhammad Bukhs', 'Street 3, phase 4 officers colony wah cantt', '3335138533', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(450, 'Customer', 'Rehan', 'Banking Complex Near Tariq Hotel UBL Port Qasim Branch 1513 Karachi. ', '03323868161', 'UBL Port Qasim Branch', 'Pakistan', 1, '', 'Sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(451, 'Customer', 'MOHAMMD BUKHS (Online)', 'CHAHTI STREET, BOLTAN MARKET', '3212130909', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(452, 'Customer', 'kamran ', 'UBL Port Qasim Branch, Banking Complex Near Tariq Hotel UBL Port QasimBranch 1513 Karachi', '03323868161', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(453, 'Customer', 'NOUMAN RIAZ (Online)', 'shop No 225 Tariq Road ', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(454, 'Customer', 'MOHAMMAD MUSA (Online)', 'B103, Aitchison Housing Society, Raiwand Road', '3007628113', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(455, 'Customer', 'AYESHA (Online)', 'P�438, St #20, Nishtar Abad, Near Arshad Cloth Market, Rajbah Road', '3216606881', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(456, 'Customer', 'ABIYA ARZOO (Online)', '31/3 B3rd street off khyabane mujahid phase 5 Dha', '3322333153', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(457, 'Customer', 'MEHRUNNISA (Online)', 'House no C747 street no 14 line no 4 Khabarstan road lala ruk wah cantt', '3105663658', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(458, 'Customer', 'NOUMAN RIAZ (Online)', 'mohallah suhbet kheil.village adina tehsil razar district swabi shewa adda', ' 3169260812', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(459, 'Customer', 'MAHNOOR (online)', 'B103, Aitchison Housing Society, Raiwand Road', '3007628113', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(460, 'Customer', 'Maira Naveed (Online)', 'Numania road street no 32 house no 1', '03225542484', '', 'Pakistan', 14, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(461, 'Customer', 'vicky,s Cosmetices', 'house 63 A , Lane no 7A , Gulistan Colony Rawalopindi Rawalpindi', '03074440973', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(462, 'Customer', 'Women Day Gifts', 'Khada Market Phase 5 ', '3.21213E+11', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(463, 'Customer', 'Imran Ali', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(464, 'Customer', 'Usama (Online)', 'The Smart School Tariq Road', '3331291946', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(465, 'Customer', 'Cyra', 'Orangi town 4 number opposite muhammadi masjid', '03162849263', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(466, 'Customer', 'Madiha Rizvi (Online)', 'Oak apartments Razi road E3 PECH', '03008286013', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(467, 'Customer', 'Conure IT Service (Online)', 'b209 block 13 d 1 gulshan e iqbal', '03312468132', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(468, 'Customer', 'UBL Rashid Minhas Rd Btanch', 'Office#801 & 802, Flr# 8th, Mehdi Tower, SMCHS Blk#A, Main Shahrah e Faisal', '03002329629', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(469, 'Customer', 'Zainab (Online)', 'Rashid Minhas Rd', '2199333443', 'United Bank Limited', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(470, 'Customer', 'Areej Khan', 'House no 89A, Street 44, F-10/4 Islamabad capital territory', '03214258220', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(471, 'Customer', 'Nasreen baloch (Onlinbe)', 'Sec 1 plot B 136 metroville site', '03072566422', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(472, 'Customer', 'Nasreen baloch (Onlinbe)', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(473, 'Customer', 'Nasreen Baloch (Online)', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(474, 'Customer', 'Tuba (Online)', 'New girls hostel bolan medical college Bruri road', '03131086294', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(475, 'Customer', 'Esha (Online)', '\"564 Street 6 G-11/1Islamabad Capital Territory\"', '03345599658', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(476, 'Customer', 'Misbah (Online)', 'House num 24 street num8 ghahri shahu mumtaz street', '03014176457', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(477, 'Customer', 'Kashif Ali', '108 A1 valancia town', '03204058136', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(478, 'Customer', 'Sana (Online)', 'UBL Pasmic 1133 Branch Commercial Banking Complex, Steel Mills co,', '333202114', 'UBL - Steel Mill Branch', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(479, 'Customer', 'Kainat (Online)', 'House no. A/60 survey 333 jaffer-etayyar society malir', '03333000610', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(480, 'Customer', 'Aliha Imran', 'H#180-B, Millat colony, committee chowk, dhoke khabba road, near Allied Bank', '03235008919', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(481, 'Customer', 'SEHAR FATIMA', ': Dawood villa opposite paragon city barki road', '03201010271', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(482, 'Customer', 'Humna Meer', 'House no 394 street 12 sector A Askari 14 ,caltex road Rawalpindi cantt', '03248673344', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(483, 'Customer', 'Javaria Khan', '103/5 A-2 Valencia Town', '03214488839', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(484, 'Customer', 'Fatima', '23c p block johar town', '03091017459', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(485, 'Customer', 'Muhammad Abid', 'House no 1081, st 21, Phase 5, bahria Town', '03335046238', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(486, 'Customer', 'Muhammad Abid (Forrun) Online', ': House#A300,street#7 docks colony keamari karachi Karachi, Sindh, 75620', '03212343143', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(487, 'Customer', 'Seemab Khan', 'House#A300,street#7 docks colony keamari karachi Karachi, Sindh, 75620', '03212343143', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(488, 'Customer', 'Faisal Abbas', 'House no 70, street no 3, Asif Town Rafi Qamar Road', '03045000789', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(489, 'Customer', 'Subhan Cobra', 'Karachi, Pakistan', '3463535638', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(490, 'Customer', 'Noor Ul Ain (Online by Leopard)', 'Dolmen mall, Karachi, Pakistan', '3322402635', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(491, 'Customer', 'Kainat', 'House no 112 Block M Wapda Town Phase 2 Lahore', '3213835331', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(492, 'Customer', 'M Arsalan (Online by Leopard)', 'H#180-B, Millat colony, committee chowk, dhoke khabba road, near Allied Bank', '03235008919', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(493, 'Customer', 'Mayra Faisal', 'PIA Transport and Overhaul Branch 1636United Bank limited Airport', '3333608903', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(494, 'Customer', 'Mayra Faisal (Online by Leopard)', 'H no 63t block 6 PECHS society near Ambala Bakery', '3142015177', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(495, 'Customer', 'Muhammad Umer (Online By Leopard)', 'H no 63t block 6 PECHS society near Ambala Bakery', '3142015177', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(496, 'Customer', 'Faisal Abbas', 'Office 402, 4th Floor, ITC Building, Behind new UBL Plaza, Hasrat Mohani Road', '3458272112', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(497, 'Customer', '', 'Chili�s Fast Food Shop 1, Street 1, Sector C, all� Karachi', '3463535638', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(498, 'Customer', 'Faisal Abbas', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(499, 'Customer', 'Umer ', 'Chili�s Fast Food Shop 1, Street 1, Sector C, all�, Adjacent to DHA Phase 1', '3463535638', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(500, 'Customer', 'Subhan Cobra', 'Near Ambala PECHS', '3110838403', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(501, 'Customer', 'Blogger ACTIVITY (Online by Forrun)', 'Dolmen mall, Karachi, Pakistan', '03322402635', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(502, 'Customer', 'Areeb Ali', 'Azeem Avenue 2nd floor Lane 13 Khayaban e Bhukhari Near Chaye Khana ,Karachi Karachi, Sindh', '03408056126', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(503, 'Customer', 'Iqra Naseeb ', 'flat B-1, Block 13-b, gulshan e iqbal', '3321741999', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(504, 'Customer', 'Gulam Murtaza (Online by Leopards)', '208A iqbal Avenue phase 2, johar town Lahore', '3350662794', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(505, 'Customer', 'Nafees Sufyan (Online by Leopards)', 'umer farooq block town kauser house no 383 street no 7', '3144360742', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(506, 'Customer', 'TAHIRA RAO', 'House no 117 D block near jalian wali masjid adam chowk ghulam mohammad abad faisal abad', '3080010248', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(507, 'Customer', 'Tahir Iqbal Khan (Online By Leopards)', 'street # 15, House # 69, Gujjar Pura ghorey Shah road, baghbanpura', '3228045563', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(508, 'Customer', 'Hira Raja (Online By Leopards)', 'House no 454 block no 3 sector d 2 green town', '3077160668', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(509, 'Customer', 'Arooj Ishaq (Online By Leopards)', ' 181 Q DHA PHASE 2, Street 9', '3364511839', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(510, 'Customer', 'Nasreen Balouch (Online By Leopards)', 'Baghbanpura madina colony street no 182 house no 10', '3124202790', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(511, 'Customer', 'Rabita Tariq (Online By Leopards)', 'shaheed nawab ghous baksh memorial teaching hospital mastung', '3322840225', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(512, 'Customer', 'Khadijah (Online by Leopards)', 'HOUSE.no 466, liaquat Ashraf colony, no.1, mehmodabad gate, karachi', '3362393101', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(513, 'Customer', 'Kainat (Online by Leopards)(Gift)', 'House no 677, street no 4, Block Z, Phase 3, Dha', '3482955684', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(514, 'Customer', 'Kainat (Online By Leopards)(Gift)', 'H#180-B, Millat colony, committee chowk, dhoke khabba road, near Allied Bank', '3235008919', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(515, 'Customer', 'Laiba ', 'H#180-B, Millat colony, committee chowk, dhoke khabba road, near Allied Bank', '03235008919', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(516, 'Customer', 'Iqra Hussain (Online by Loepards)', 'Gulshan-e-Hadeed', '3310343165', 'beauty parlor', 'Pakistan', 1, '', 'sindh', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(517, 'Customer', 'Ambar Shaikh (Online By Leopards)', 'Kot khadim Ali shah street # 7 Sahiwal 57000', '3025854561', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(518, 'Customer', 'Zainab Bint e Ali (Online By Leopards)', 'A465 Block \'L\' Street 9 North nazimabad ', '3347481056', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(519, 'Customer', 'Abdul Rafay (Online By Leopards)', 'Ali farms, farm 39, street 4, pv scheme 1 ,Tarlai Farms', '3181989897', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(520, 'Customer', 'Hadiqa (Online by Leopards)', 'korangi allah wala town ,sector 31/g, h no7 ,st no 7', '03162465485', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(521, 'Customer', 'Abdul Rafay (Online By Leopards)', 'Ward no.3 near pilot school ground Bhimber', '3470046676', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(522, 'Customer', 'Hamna Uppal (Online by Leopards)', 'korangi allah wala town ,sector 31/g, h no7 ,st no 7', '03162465485', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(523, 'Customer', 'Waqas Khan (Online By Leopards)', '99D phase 2 garden town gujranwala', '3039537944', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(524, 'Customer', 'Areeb Ali (Online By Leopards)', 'R1072 15A/5 bufferzone', '3152980434', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(525, 'Customer', 'Mehak Yaqoob (Online By Leopards)', 'flat B-1, Khursheed homes, Block 13-b, gulshan e iqbal', '3321741999', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(526, 'Customer', 'Mian Zubair (Online By Leopards)', 'Shaikh zayed hospital doctors hostel lahore', '3077933777', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(527, 'Customer', 'Laila Hasan (Online by Leopards)', 'pura hiran near mehar ishaq karkhana, near umer bilal general store, rangpura', '3314269140', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(528, 'Customer', 'Drtahsin Panhwar (Onlien By Leopards)', 'Creek vista apartments block A apartment 1001', '3232815406', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(529, 'Customer', 'Drtahsin Panhwar (Onlien By Leopards)', 'Fno403khurum square plaza near faran hotel cantonment sader', '3333492818', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(530, 'Customer', 'Mr. Drtahsin Panhwar (Onlien By Leopards)', 'Fno403khurum square plaza near faran hotel cantonment sader', '3333492818', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(531, 'Customer', 'Bism Bilal (Online By Leopards)', 'Fno403khurum square plaza near faran hotel cantonment sader', '3333492818', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(532, 'Customer', 'Bismah Bilal (Online By Leopards)', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(533, 'Customer', 'Asifa Amir (Online By Leopards)', 'House no A-28/1 4th floor shanti nagar sindhi society gulshan e iqbal block 19', '3338271155', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(534, 'Customer', 'Benish (Online By Leopards)', '74 canal breez House Ali town raiwind road lahore', '3124789911', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(535, 'Customer', 'Waqar Gohar (Online By Leopards)', 'P block 941 sabzaar', '3217411771', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(536, 'Customer', 'Sonia Rajar (Onlie)', '1/1102 shah faisal colony', '3130285773', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(537, 'Customer', 'Soonia ', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(538, 'Customer', 'Sohnia Rajar (Online By Leopards)', '', '', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(539, 'Customer', 'Hamna (Online By Leopards)', '135/1 street 7 khayabanebukhari phase 6 dha', '3009378111', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(540, 'Customer', 'Rafay Sheraz (Online By Leopards)', '', '3042079544', '', 'Pakistan', 1, '2k 7/3 nazimabad no 2 near jamia imam bargah', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(541, 'Customer', 'Sadafjan Jan (Online By Leopards)', 'Karim Park Ravi Road House # 10, Block # 2', '0310 4899241', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(542, 'Customer', 'Javaria Khan (Online By Leopards)', 'B 39 block 4 gulshan-e-iqbal near byco CNG pump', '3232733312', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(543, 'Customer', 'Seher Fatima (Online By Leopards)', '23c p block johar town', '3091017459', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(544, 'Customer', 'Malik Iftekhar Ahmed (Online By Leopards)', 'House no 394 street 12 sector A Askari 14 ,caltex road Rawalpindi cantt', '03248673344', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(545, 'Customer', 'Bakhtawar Baloch', ': House#788/1,OPP FATIMA SHOPPING CENTER, SURJ KUND ROAD, CHOWK SHAH ABBAS,', '3098733335', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(546, 'Customer', 'Seher Fatima (Online By Leopards)', ': Kainat nagar housing scheme banglow 18 near subhanallah cng hyderabad by pass', '3160265405', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(547, 'Customer', 'Kamran Bhai Gym ', 'House no 394 street 12 sector A Askari 14 ,caltex road Rawalpindi cantt', '03248673344', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(548, 'Customer', 'Ifrah Qureshi (Online By Leopards)', 'Apsara cosmetics  Shop # 25  Tariq centre  Tariq road Karachi', '3333239354', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(549, 'Customer', 'Farzana Kamal (Online By Leopards)', '254-E Al-Ahmad Garden Manavan', '3354338013', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(550, 'Customer', 'MEHRUNNISSA HAROON', 'B24, Al Muslim Society, sector 34A, scheme 33, opposite Memon medical hospital', '3312275673', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(551, 'Customer', 'Laiba Amir (Online By Leopards)', ' House:11, street:90, G-6/3,Embassy road', '3219090090', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(552, 'Customer', 'MOMINA (Online By Leopards)', '74 canal breeze ali town raiwind road lahore', '3084784704', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(553, 'Customer', 'Ghosia Khan (Online By Leopards)', 'near eidghah, muhallah mughal pura, Sambrial Road Daska', '3187854570', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(554, 'Customer', 'NOORULAIN BASIT (Online By Leopards)', '285D,C block, street#3 margzar colony', '3095366664', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(555, 'Customer', 'TALAT FAROOQ (Online By Leopards)', 'House no 299, Street 8, lane 5, cavalry ground extension.', '3004272233', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(556, 'Customer', 'Sidra', 'School Flat numbers G8, jason condominiums apartments, block 7, 1st floor , mai kolachi road , Clifton boatbasin Karachi ', '3212216923', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(557, 'Customer', 'Anushay (PR Package)', 'Korangi no 5 near sultan hospital flat no 401 al.majeed tower 4th floor Karachi', '3323013777', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(558, 'Customer', 'Ejaz (PR Package)', 'Flat#24 3rd floor rangoonwala building, ghani moosa Seth house near selani chowk. Dhoraji', '3331318063', 'ejaz', 'Pakistan', NULL, NULL, NULL, NULL, 1, 2, NULL, NULL, NULL, NULL, 2, '0000-00-00 00:00:00', '2021-06-18 06:28:55', 1, 'asad', '3311321354'),
(559, 'Customer', 'Bushra Fatima (Online By Leopards)', 'C-305 Almaryam Apartment Garden East, karachi', '3040234002', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(560, 'Customer', 'Muhammad Asif (Online by Leopards)', ': Block 6 house B-49 near dhaka sweets Gulshan e Iqbal', '3226200201', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(561, 'Customer', 'Bushra Nadeem (Online By Leopards)', 'Sambrial Road daska Near Telenor office Daska', '3007132834', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(562, 'Customer', 'Abbas', '19A Sukh Chayan Gardans 19A Sukh Chayan Gardans 19', '3325940363', '', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(598, 'Supplier', 'Abbad', '', '', 'Abbas', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(599, 'Supplier', 'Ahmed Kapadia', '', '', 'Ahmed Kapadia', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(600, 'Supplier', 'Ajwa', '', '', 'Ajwa', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(601, 'Supplier', 'Ali Medical', '', '', 'Ali Medical', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(602, 'Supplier', 'Alpha Tubes', '', '', 'Alpha Tubes', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(603, 'Supplier', 'Amis', '', '', 'Amis', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(604, 'Supplier', 'Anas Adil', '', '', 'Anas Adil', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(605, 'Supplier', 'Aqib Sohail', '', '', 'Aqib Sohail', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(606, 'Supplier', 'AS ', '', '', 'AS', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(607, 'Supplier', 'Asad', '', '', 'Asad ', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(608, 'Supplier', 'Athar Chandio', '', '', 'Athar Chandio', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(609, 'Supplier', 'Atif Faisalabad', '', '', 'Atif Faisalabad', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(610, 'Supplier', 'Atif Lahore', '', '', 'Atif Lahore', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(611, 'Supplier', 'Basit Enterprise', '', '', 'Basit Enterprise', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(612, 'Supplier', 'Boltan Market', '', '', 'Boltan Market', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(613, 'Supplier', 'Chandio', '', '', 'Chandio', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(614, 'Supplier', 'Himani', '', '', 'Himani', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(615, 'Supplier', 'Jawwad Khan', '', '', 'Jawwad Khan', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(616, 'Supplier', 'KAS Chemicals', '', '', 'KAS Chemicals', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(617, 'Supplier', 'Khalid Faisalabad', '', '', 'Khalid Faisalabad', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(618, 'Supplier', 'Khawaja Aqib', '', '', 'Khawaja Aqib', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(619, 'Supplier', 'Khawaja Bilal ', '', '', 'Khawaja Bilal', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(620, 'Supplier', 'Mahnoor Printer', '', '', 'Mahnoor Printer ', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(621, 'Supplier', 'Mubashir C/O Dawar', '', '', 'Mubashir C/O Dawar', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(622, 'Supplier', 'Mursaleen', '', '', 'Mursaleen', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(623, 'Supplier', 'Noor Enterprises / Lucky', '', '', 'Noor Enterprises / Lucky', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(624, 'Supplier', 'Pure Care', '', '', 'Pure Care', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(625, 'Supplier', 'Saad Ent', '', '', 'Saad Ent', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(626, 'Supplier', 'Saad pindi', '', '', 'Saad pindi', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(627, 'Supplier', 'Saad Trading', '', '', 'Saad Trading', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(628, 'Supplier', 'Shahrukh', '', '', 'Shahrukh', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(629, 'Supplier', 'Shariq', '', '', 'Shariq', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(630, 'Supplier', 'Tariq Sahb', '', '', 'Tariq Sahb', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(631, 'Supplier', 'Tasneem Packages', '', '', 'Tasneem Packages', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(632, 'Supplier', 'Zia Pharma', '', '', 'Zia Pharma', 'Pakistan', 1, '', '', '', NULL, NULL, '', '', NULL, NULL, NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, NULL, NULL),
(633, 'Supplier', 'hibah', 'bahadrabad', '03343715757', 'posch care', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, '2021-06-29 05:50:27', '2021-06-29 05:50:27', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `vendors_update_request`
--

CREATE TABLE `vendors_update_request` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `c_group` bigint(20) UNSIGNED DEFAULT NULL,
  `p_group` bigint(20) UNSIGNED DEFAULT NULL,
  `v_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_no` bigint(20) NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `NTN` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GST` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postalCode` bigint(20) DEFAULT NULL,
  `c_id` bigint(20) UNSIGNED DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `name2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_no2` bigint(20) NOT NULL,
  `u_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `warehouse`
--

CREATE TABLE `warehouse` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `w_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `w_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `w_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_id` bigint(20) UNSIGNED NOT NULL,
  `w_contactNo` bigint(20) DEFAULT NULL,
  `c_p_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `c_p_contactNo` bigint(20) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `warehouse`
--

INSERT INTO `warehouse` (`id`, `w_name`, `w_type`, `w_address`, `c_id`, `w_contactNo`, `c_p_name`, `c_p_contactNo`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'Karachi Warehouse', 'standard', 'Shahrah Faisal', 1, NULL, '', 0, NULL, NULL, NULL, NULL),
(2, 'Lahore', 'standard', 'Lahore', 2, NULL, '', 0, NULL, NULL, NULL, NULL),
(3, 'Islamabad', 'standard', 'Islamabad', 3, NULL, '', 0, NULL, NULL, NULL, NULL),
(4, 'USA', 'standard', 'New York', 4, NULL, '', 0, NULL, NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_details`
--
ALTER TABLE `account_details`
  ADD PRIMARY KEY (`Code`),
  ADD UNIQUE KEY `account_details_code_unique` (`Code`),
  ADD KEY `account_details_created_by_foreign` (`created_by`),
  ADD KEY `account_details_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `current_stock`
--
ALTER TABLE `current_stock`
  ADD UNIQUE KEY `p_id` (`p_id`,`w_id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_ledger`
--
ALTER TABLE `general_ledger`
  ADD PRIMARY KEY (`id`),
  ADD KEY `general_ledger_created_by_foreign` (`created_by`),
  ADD KEY `general_ledger_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `head_category`
--
ALTER TABLE `head_category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `head_category_code_unique` (`code`);

--
-- Indexes for table `head_of_accounts`
--
ALTER TABLE `head_of_accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `head_of_accounts_created_by_foreign` (`created_by`),
  ADD KEY `head_of_accounts_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `updated_by` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Indexes for table `product_transfer_history`
--
ALTER TABLE `product_transfer_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_transfer_history_created_by_foreign` (`created_by`);

--
-- Indexes for table `product_variants`
--
ALTER TABLE `product_variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_variant_update`
--
ALTER TABLE `product_variant_update`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_request`
--
ALTER TABLE `purchase_request`
  ADD PRIMARY KEY (`id`),
  ADD KEY `purchase_request_created_by_foreign` (`created_by`),
  ADD KEY `purchase_request_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `purchase_request_details`
--
ALTER TABLE `purchase_request_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quotation`
--
ALTER TABLE `quotation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `quotation_created_by_foreign` (`created_by`),
  ADD KEY `quotation_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `quotation_details`
--
ALTER TABLE `quotation_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `roles_created_by_foreign` (`created_by`),
  ADD KEY `roles_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_menu_created_by_foreign` (`created_by`),
  ADD KEY `role_menu_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `role_permission_created_by_foreign` (`created_by`),
  ADD KEY `role_permission_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `sale_details`
--
ALTER TABLE `sale_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `stock_out`
--
ALTER TABLE `stock_out`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stock_out_created_by_foreign` (`created_by`),
  ADD KEY `stock_out_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `stock_receiving`
--
ALTER TABLE `stock_receiving`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_menu_created_by_foreign` (`created_by`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vendors`
--
ALTER TABLE `vendors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Indexes for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bank`
--
ALTER TABLE `bank`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_ledger`
--
ALTER TABLE `general_ledger`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `head_category`
--
ALTER TABLE `head_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `head_of_accounts`
--
ALTER TABLE `head_of_accounts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=164;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=210;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT for table `product_transfer_history`
--
ALTER TABLE `product_transfer_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_variants`
--
ALTER TABLE `product_variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `product_variant_update`
--
ALTER TABLE `product_variant_update`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order`
--
ALTER TABLE `purchase_order`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_order_details`
--
ALTER TABLE `purchase_order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_request`
--
ALTER TABLE `purchase_request`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `purchase_request_details`
--
ALTER TABLE `purchase_request_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation`
--
ALTER TABLE `quotation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `quotation_details`
--
ALTER TABLE `quotation_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `role_menu`
--
ALTER TABLE `role_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2030;

--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3965;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sale_details`
--
ALTER TABLE `sale_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_out`
--
ALTER TABLE `stock_out`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_receiving`
--
ALTER TABLE `stock_receiving`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `transaction_history`
--
ALTER TABLE `transaction_history`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `vendors`
--
ALTER TABLE `vendors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=634;

--
-- AUTO_INCREMENT for table `warehouse`
--
ALTER TABLE `warehouse`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `account_details`
--
ALTER TABLE `account_details`
  ADD CONSTRAINT `account_details_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `account_details_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `brands`
--
ALTER TABLE `brands`
  ADD CONSTRAINT `brands_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `brands_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `category_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `city`
--
ALTER TABLE `city`
  ADD CONSTRAINT `city_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `city_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `company`
--
ALTER TABLE `company`
  ADD CONSTRAINT `company_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `company_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `general_ledger`
--
ALTER TABLE `general_ledger`
  ADD CONSTRAINT `general_ledger_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `general_ledger_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `groups_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `groups_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `head_of_accounts`
--
ALTER TABLE `head_of_accounts`
  ADD CONSTRAINT `head_of_accounts_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `head_of_accounts_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `product_transfer_history`
--
ALTER TABLE `product_transfer_history`
  ADD CONSTRAINT `product_transfer_history_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD CONSTRAINT `purchase_order_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `purchase_order_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `purchase_request`
--
ALTER TABLE `purchase_request`
  ADD CONSTRAINT `purchase_request_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `purchase_request_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `quotation`
--
ALTER TABLE `quotation`
  ADD CONSTRAINT `quotation_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `quotation_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `roles_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_menu`
--
ALTER TABLE `role_menu`
  ADD CONSTRAINT `role_menu_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `role_menu_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
  ADD CONSTRAINT `role_permission_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `role_permission_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `sales`
--
ALTER TABLE `sales`
  ADD CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `sales_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `stock_out`
--
ALTER TABLE `stock_out`
  ADD CONSTRAINT `stock_out_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `stock_out_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD CONSTRAINT `sub_category_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `sub_category_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `transaction_history`
--
ALTER TABLE `transaction_history`
  ADD CONSTRAINT `transaction_history_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `transaction_history_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `units`
--
ALTER TABLE `units`
  ADD CONSTRAINT `units_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `units_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD CONSTRAINT `user_menu_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `vendors`
--
ALTER TABLE `vendors`
  ADD CONSTRAINT `vendors_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `vendors_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `warehouse`
--
ALTER TABLE `warehouse`
  ADD CONSTRAINT `warehouse_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `warehouse_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
